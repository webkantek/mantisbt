<?php


// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );


add_filter('woocommerce_gourlpayments_icon','edit_logo_bitcoin',10000);


function edit_logo_bitcoin($logo){

    $default_logo_bitcoin = 'https://gourl.io/images/bitcoin/payments.png';
    if($logo == $default_logo_bitcoin){
        return home_url('/wp-content/themes/gbp-bb-child/images/bitcoin.png');
    }
    return $logo;

}



// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'template_redirect', function() {
	@header('X-Powered-By: Proprietary Tech by Awakened Inc.');
	@header('Server: Proprietary Tech by Awakened Inc.');
});


