/**
 * URL shortener Admin Script Doc Comment
 *
 * @category Script
 * @package  Yith Custom Thank You Page for Woocommerce
 * @author    Armando Liccardo
 * @license  http://www.gnu.org/licenses/gpl-3.0.txt GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * @link http://www.yithemes.com
 */

jQuery(function ($) {
		$( document ).ready(function () {
				"use strict";

                /* URL shortening select change event */
				$( '#ctpw_url_shortening' ).change(function () {

					var option = $( 'option:selected', this ).val(),
						google = $( '#ctpw_google_api_key' ),
						bitly = $( '#ctpw_bitly_access_token' );

					switch (option) {
						/* select bitly service */
						case 'bitly':
							bitly.parent().parent().show();
							bitly.prop( 'required', true );
							google.parent().parent().hide();
							google.prop( 'required', false );
							break;
						 /* select google service */
						case 'google':
							bitly.parent().parent().hide();
							bitly.prop( 'required', false );
							google.parent().parent().show();
							google.prop( 'required', true );
							break;

						default:
							bitly.parent().parent().hide();
							bitly.prop( 'required', false );
							google.parent().parent().hide();
							google.prop( 'required', false );

					}

				}).change();

            /* change event on Select Custom page or Custom Url */
            $( '#yith_ctpw_general_page_or_url' ).change(function () {
                var option = $( 'option:selected', this ).val(),
                    ctwp_page = $( '#yith_ctpw_general_page' ),
                    ctpw_url = $( '#yith_ctpw_general_page_url' );

                switch (option) {
                    case 'ctpw_page':
                        ctwp_page.parent().parent().show();
                        ctpw_url.parent().parent().hide();
                        break;
                    case 'ctpw_url':
                        ctpw_url.parent().parent().show();
                        ctwp_page.parent().parent().hide();
                        break;
                    default:
                        ctwp_page.parent().parent().hide();
                        ctpw_url.parent().parent().hide();
                }

            }).change();

            /* edit\new category select change event */
            $('#yith_ctpw_or_url_product_cat_thankyou_page').change( function() {
                var option = $( 'option:selected', this ).val(),
                    ctwp_page = $( '#yith_ctpw_product_cat_thankyou_page' ),
                    ctpw_url = $( '#yith_ctpw_url_product_cat_thankyou_page' );

                switch(option) {
                    case 'ctpw_page':
                        ctwp_page.parent().parent().show();
                        ctpw_url.parent().parent().hide();
                        break;
                    case 'ctpw_url':
                        ctpw_url.parent().parent().show();
                        ctwp_page.parent().parent().hide();
                        break;
                    default:
                        ctwp_page.parent().parent().hide();
                        ctpw_url.parent().parent().hide();
                }
            }).change();

            //product page tab
            $('#ctpw_tab_data #yith_ctpw_product_thankyou_page_url').change( function() {
                var option = $( 'option:selected', this ).val(),
                    ctwp_page = $( '#yith_product_thankyou_page' ),
                    ctpw_url = $( '#yith_ctpw_product_thankyou_url' );

                switch(option) {
                    case 'ctpw_page':
                        ctwp_page.parent().show();
                        ctpw_url.parent().hide();
                        break;
                    case 'ctpw_url':
                        ctpw_url.parent().show();
                        ctwp_page.parent().hide();
                        break;
                    default:
                        ctwp_page.parent().hide();
                        ctpw_url.parent().hide();
                }
            }).change();

            $('#woocommerce-product-data').on('woocommerce_variations_loaded', function(event) {
                //product variation options


                $('.woocommerce_variation .yith_ctpw .yith_ctpw_product_thankyou_page_url').change( function() {
                    var item_class = '.' + $(this).parent().attr('ctpw_item');
                    var option = $( 'option:selected', this ).val(),
                        ctwp_page = $( item_class + ' .yith_ctpw_product_thankyou_page' ),
                        ctpw_url = $( item_class + ' .yith_ctpw_product_thankyou_url' );

                    switch(option) {
                        case 'ctpw_page':
                            ctwp_page.show();
                            ctpw_url.hide();
                            break;
                        case 'ctpw_url':
                            ctpw_url.show();
                            ctwp_page.hide();
                            break;
                        default:
                            ctwp_page.hide();
                            ctpw_url.hide();
                    }

                }).change();
            });


		}); //end document ready
});
