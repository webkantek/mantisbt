=== YITH Payment Method Restrictions for WooCommerce Premium ===

== Changelog ==

= Version 1.0.4 - Released: Nov 06, 2017 =

* New: Added restriction by roles
* Update: Plugin core

= Version 1.0.3 - Released: Oct 10, 2017 =

* New: Support to WooCommerce 3.2.0 RC2
* Update: Plugin core
* Dev: Added filter yith_wcgpf_print_notices

= Version 1.0.2 - Released: Aug 24, 2017 =

* Fix show restriction message on select payment gateway section

= Version 1.0.1 - Released: Aug 23, 2017 =
* New Italian Translation
* Fix Error activate license

= Version 1.0.0 - Released: Aug 07, 2017 =

* First release

== Suggestions ==

If you have suggestions about how to improve YITH Payment Method Restrictions for WooCommerce Premium, you can [write us](mailto:plugins@yithemes.com "Your Inspiration Themes") so we can bundle them into the next release of the plugin.

== Translators ==

If you have created your own language pack, or have an update for an existing one, you can send [gettext PO and MO file](http://codex.wordpress.org/Translating_WordPress "Translating WordPress")
[use](http://yithemes.com/contact/ "Your Inspiration Themes") so we can bundle it into YITH Payment Method Restrictions for WooCommerce Premium languages.

 = Available Languages =
 * English
