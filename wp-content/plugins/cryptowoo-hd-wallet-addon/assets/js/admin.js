jQuery(document).ready(function () {
// HD Wallet Discovery
    jQuery("[id^=cw-discover-]").click(function (click) {

        var currency = jQuery(this).attr("id").replace('cw-discover-', '');

        hide_fields(currency);
        jQuery('#cw-discover-' + currency + '-response').replaceWith('<div id="cw-discover-' + currency + '-response"><i class="fa fa-2x fa-refresh fa-spin"></i> Discovering ' + currency.toUpperCase() + ' addresses. Please be patient. This may take a while...</div>');
        var data = {
            'action': 'hd_address_discovery',
            'currency': currency
        };
        //alert(currency);

        // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        jQuery.post(ajaxurl, data, function (response) {
            jQuery('#cw-discover-' + currency + '-response').replaceWith('<div style="padding:1em; background-color: #fff;" id="cw-discover-' + currency + '-response">' + response + '</div>');
        });
        // Cancel the default action
        click.preventDefault();
    });
    // Fresh key
    jQuery("[id^=cw-skip-]").click(function (click) {
        var currency = jQuery(this).attr("id").replace('cw-skip-', '');
        jQuery('#cw-discover-' + currency + '-response').replaceWith('<div id="cw-discover-' + currency + '-response"><i class="fa fa-2x fa-refresh fa-spin"></i> Saving ' + currency.toUpperCase() + ' address index...</div>');
        var data = {
            'action': 'hd_address_fresh_key',
            'currency': currency
        };
        // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        jQuery.post(ajaxurl, data, function (response) {
            hide_fields(currency);
            jQuery('#cw-discover-' + currency + '-response').replaceWith('<div style="padding:1em; background-color: #fff;" id="cw-discover-' + currency + '-response">' + response + '</div>');
        });
        // Cancel the default action
        click.preventDefault();
    });
    // Set index
    jQuery("[id^=cw-set-]").click(function (click) {
        var currency = jQuery(this).attr("id").replace('cw-set-', '');
        hide_fields(currency);
        jQuery('#cw-discover-' + currency + '-response').replaceWith('<div id="cw-discover-' + currency + '-response"><i class="fa fa-2x fa-refresh fa-spin"></i> Saving ' + currency.toUpperCase() + ' address index...</div>');
        var data = {
            'action': 'hd_address_set_index',
            'currency': currency,
            'index':  jQuery('#cw-index-'+currency).val()
        };
        console.log(data);
        // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        jQuery.post(ajaxurl, data, function (response) {
            jQuery('#cw-discover-' + currency + '-response').replaceWith('<div style="padding:1em; background-color: #fff;" id="cw-discover-' + currency + '-response">' + response + '</div>');
        });
        // Cancel the default action
        click.preventDefault();
    });
    // Set index on debugging page
    jQuery("[id^=cw-set-index]").click(function (click) {
        var currency = jQuery("#set-index-currency").find("option:selected").text();
        jQuery('#set-index-response').replaceWith('<div id="set-index-response"><i class="fa fa-2x fa-refresh fa-spin"></i> Saving ' + currency.toUpperCase() + ' address index...</div>');
        var data = {
            'action': 'hd_address_set_index',
            'currency': currency,
            'index':  jQuery('#index').val()
        };
        console.log(data);
        // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        jQuery.post(ajaxurl, data, function (response) {
            jQuery('#set-index-response').replaceWith('<div style="padding:1em; background-color: #fff;" id="set-index-response">' + response + '</div>');
        });
        // Cancel the default action
        click.preventDefault();
    });
// HD Wallet Discovery Debugging
    jQuery("#cw-discover").click(function (click) {
        var currency = jQuery("#discover-currency").find("option:selected").text();
        jQuery('#cw-discover-response').replaceWith('<div id="cw-discover-response"><i class="fa fa-2x fa-refresh fa-spin"></i> Discovering ' + currency.toUpperCase() + ' addresses. Please be patient. This may take a while...</div>');
        var data = {
            'action': 'hd_address_discovery',
            'currency': currency
        };

        // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        jQuery.post(ajaxurl, data, function (response) {
            jQuery('#cw-discover-response').replaceWith('<div style="padding:1em; background-color: #fff;" id="cw-discover-response">' + response + '</div>');
        });
        // Cancel the default action
        click.preventDefault();

    });
// Get address from path
    jQuery("#create-address").click(function (click) {
        jQuery('#hdwallet-address').replaceWith('<div style="padding:1em; background-color: #fff;" id="cw-btc-loading"><i class="fa fa-2x fa-refresh fa-spin"></i> Creating address...</div>');
        var data = {
            'action': 'get_hd_address',
            'currency': jQuery("#address-currency").find("option:selected").text(),
            'path': jQuery('input[name="path"]').val()
        };
        // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        jQuery.post(ajaxurl, data, function (response) {
            jQuery('#hdwallet-result').replaceWith('<div style="padding:1em; background-color: #fff;" id="hdwallet-result">' + response + '</div>');
        });
        // Cancel the default action
        click.preventDefault();
    });
// Get last order details
    jQuery("#cw-bridge-gap").click(function (click) {
        jQuery('#cw-bridge-gap-response').replaceWith('<div style="padding:1em; background-color: #fff;" id="cw-bridge-gap-response"><i class="fa fa-2x fa-refresh fa-spin"></i> Running...</div>');
        var data = {
            'action': 'cw_bridge_the_gap',
            'currency': jQuery("#cw-bridge-gap-currency").find("option:selected").text()
        };
        // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        jQuery.post(ajaxurl, data, function (response) {
            jQuery('#cw-bridge-gap-response').replaceWith('<div style="padding:1em; background-color: #fff;" id="cw-bridge-gap-response">' + response + '</div>');
        });
        // Cancel the default action
        click.preventDefault();
    });
// List addresses in key
    jQuery("#cw-list-addresses").click(function (click) {
        jQuery('#cw-list-addresses-response').replaceWith('<div style="padding:1em; background-color: #fff;" id="cw-list-addresses-response"><i class="fa fa-2x fa-refresh fa-spin"></i> Running...</div>');
        var data = {
            'action': 'cw_list_addresses',
            'currency': jQuery("#cw-list-addresses-currency").find("option:selected").text()
        };
        // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        jQuery.post(ajaxurl, data, function (response) {
            jQuery('#cw-list-addresses-response').replaceWith('<div style="padding:1em; background-color: #fff;" id="cw-list-addresses-response">' + response + '</div>');
        });
        // Cancel the default action
        click.preventDefault();
    });
});

function hide_fields(currency) {
    console.log("hide "+'#inline-discovery-notice-' + currency);
    jQuery('#inline-discovery-notice-' + currency).hide();
    jQuery('#cw-set-' + currency).replaceWith('');
    jQuery('#cw-index-' + currency).replaceWith('');
    jQuery('#cw-skip-' + currency).replaceWith('');
    jQuery('#cw-discover-' + currency).replaceWith('');
}