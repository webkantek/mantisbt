<?php
/**
 * Plugin Name: CryptoWoo HD Wallet Add-on
 * Description: Create payment addresses from a hierarchical deterministic master public key. Requires CryptoWoo Plugin
 * Version: 0.7.5
 * Author: flxstn
 * Plugin URI: http://www.cryptowoo.com/cryptowoo-hd-wallet-addon/
 * Author URI: http://www.cryptowoo.com/
 * License: GPL2
 * Text Domain: cryptowoo-hd-wallet-addon
 * Domain Path: /lang
 */
/*
Copyright 2017 flxstn

CryptoWoo HD Wallet Add-on is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

CryptoWoo HD Wallet Add-on is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with CryptoWoo HD Wallet Add-on; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

define('HDWALLET_VER', '0.7.5');
define('HDWALLET_PATH', plugins_url(basename(plugin_dir_path(__FILE__)), basename(__FILE__)) . '/');
define('HDWALLET_DIR', plugin_dir_path(__FILE__)); // /wp-content/plugins/cryptowoo-hd-wallet-addon/
define('HDWALLET_FILE', __FILE__);

// Load the API Key library if it is not already loaded. Must be placed in the root plugin file.
if ( ! class_exists( 'CWHD_License_Menu' ) && file_exists(plugin_dir_path( __FILE__ ) . 'am-license-menu.php')) {
    // Uncomment next line if this is a plugin
    require_once( plugin_dir_path( __FILE__ ) . 'am-license-menu.php' );

    // Uncomment next line if this is a theme
    // require_once( get_stylesheet_directory() . 'am-license-menu.php' );

    /**
     * @param string $file             Must be __FILE__ from the root plugin file, or theme functions file.
     * @param string $software_title   Must be exactly the same as the Software Title in the product.
     * @param string $software_version This product's current software version.
     * @param string $plugin_or_theme  'plugin' or 'theme'
     * @param string $api_url          The URL to the site that is running the API Manager. Example: https://www.toddlahman.com/
     *
     * @return \CWHD_License_Submenu|null
     */
    CWHD_License_Menu::instance( __FILE__, 'CryptoWoo-HD-Wallet-Addon', HDWALLET_VER, 'plugin', 'https://www.cryptowoo.com/' );
}

// CryptoID.info Helper
$cryptoid_helper = HDWALLET_DIR . 'class.cryptoid-helper.php';
if(file_exists($cryptoid_helper)) {
    include_once($cryptoid_helper);
}

// HD Wallet Debugging
include_once(HDWALLET_DIR . 'admin/hd-debug.php');

/**
 * Load textdomain
 *
 * @since 0.14.0
 */
function cryptowoo_hd_textdomain() {
    load_plugin_textdomain( 'cryptowoo-hd-wallet-addon', false, plugin_basename( dirname( __FILE__ ) ) . '/lang' );
}
cryptowoo_hd_textdomain(); // Run this without hook to load before Redux Framework https://github.com/reduxframework/redux-framework/issues/1546#issuecomment-51727596
//add_action('plugins_loaded', 'cryptowoo_hd_textdomain');


/**
 * Enqueue admin JS
 */
function cryptowoo_hd_admin_js() {
    wp_enqueue_script('hd-wallet', HDWALLET_PATH.'assets/js/admin.js');
}
add_action('admin_enqueue_scripts', 'cryptowoo_hd_admin_js');

/**
 * Plugin activation
 */
function cryptowoo_hdwallet_addon_activate() {

    include_once (ABSPATH . 'wp-admin/includes/plugin.php');

    if(validate_file('cryptowoo/cryptowoo.php') || !file_exists(WP_PLUGIN_DIR . '/cryptowoo/cryptowoo.php') ) {

        // If WooCommerce is not installed then show installation notice
        add_action('admin_notices', 'cryptowoo_hd_notinstalled_notice');
        return;
    } elseif (!is_plugin_active('cryptowoo/cryptowoo.php')) {
        add_action('admin_notices', 'cryptowoo_hd_inactive_notice');
        return;
    }
}
register_activation_hook(__FILE__, 'cryptowoo_hdwallet_addon_activate');
add_action('admin_init', 'cryptowoo_hdwallet_addon_activate');

/**
 * Initiate HD Wallet Add-on
 */
function woocommerce_hdwallet_init() {
	if (!class_exists('WC_Payment_Gateway')) {
		return;
	};

	// Maybe display HD Wallet discovery notice
	if(strpos($_SERVER['REQUEST_URI'], 'cryptowoo') && class_exists('CW_HDwallet') ) {
		$currencies =  apply_filters('cw_discovery_notice_action', array('BTC', 'LTC', 'DOGE', 'BTCTEST', 'BLK'));
		foreach($currencies as $currency) {
			$transient_name = strtolower($currency).'_hd_wallet_discovery_notice';
			$transient = get_transient($transient_name);
			if($transient) {
				add_action('admin_notices', 'cryptowoo_hd_wallet_discovery_notice');
			}
		}
	}
}
add_action('plugins_loaded', 'woocommerce_hdwallet_init', 0);

/**
 * Adress discovery notice
 */
function cryptowoo_hd_wallet_discovery_notice() {
    $currencies = apply_filters('cw_discovery_notice', array('BTC', 'LTC', 'DOGE', 'BTCTEST', 'BLK'));
    $message    = '';
    foreach ($currencies as $currency) {
        if (false !== ($transient = get_transient(sprintf('%s_hd_wallet_discovery_notice', strtolower($currency)))) && isset($transient->message)) {
            if ($transient->message !== 'success') {
                $message .= $transient->message;
            }
        }
    }
    if ($message !== '') {
        printf('<div class="update-nag redux-message notice is-dismissible redux-notice">%s</div>', $message);
    }
}

/**
 * CryptoWoo inactive notice
 */
function cryptowoo_hd_inactive_notice() {
    $update_actions =  array(
        '<a class="button" href="' . wp_nonce_url('plugins.php?action=activate&amp;plugin='.urlencode('cryptowoo/cryptowoo.php'), 'activate-plugin_' . 'cryptowoo/cryptowoo.php') . '" title="' . esc_attr__('Activate CryptoWoo Plugin') . '" target="_parent">' . __('Activate CryptoWoo Plugin') . '</a>',
        /*'<a class="button" href="'.self_admin_url('admin.php?page=cryptowoo-options').'" title="' . esc_attr__('Go to CryptoWoo Checkout Settings') . '" target="_parent">' . __('Enable CryptoWoo Payment Gateway') . '</a>'*/
        /*'<a href="' . self_admin_url('plugins.php') . '" title="' . esc_attr__('Go to plugins page') . '" target="_parent">' . __('Return to Plugins page') . '</a>'*/
    );
    ?>
    <div class="error">
        <p><?php _e('<b>CryptoWoo HD Wallet Add-on has been disabled!</b> It seems like CryptoWoo has been deactivated.<br>
       				Click the button below or go to the Plugins menu and make sure that the CryptoWoo plugin is activated.', 'cryptowoo-hd-wallet-addon');?></p>
        <?php foreach ($update_actions as $update_action) {
            printf('%s ', $update_action);
        }
        ?>
    </div>
    <?php
}

/**
 * CryptoWoo not installed notice
 */
function cryptowoo_hd_notinstalled_notice() {
    /*$update_actions =  array(
        'activate_plugin_wc' => '<a class="button" href="' . wp_nonce_url( urlencode('update.php?action=install-plugin&plugin=woocommerce') ) . '" title="' . esc_attr__('Activate WooCommerce Plugin') . '" target="_parent">' . __('Install WooCommerce') . '</a>',
        'plugins_page' => '<a href="' . self_admin_url('plugins.php') . '" title="' . esc_attr__('Go to plugins page') . '" target="_parent">' . __('Return to Plugins page') . '</a>'
        );
        */
    ?>
    <div class="error">
        <p><?php _e('<b>CryptoWoo payment gateway has been disabled!</b> It seems like the CryptoWoo plugin is not installed.<br>
					The CryptoWoo HD Wallet Add-on will only work in combination with the CryptoWoo Main Plugin.', 'cryptowoo-hd-wallet-addon');?></p>
        <?php /*foreach ($update_actions as $key => $value) {
				echo $value . ' ';
			} */
        ?>
    </div>
    <?php
}

/**
 * Address discovery callback
 */
function hd_address_discovery_callback() {
    $currency = strtoupper($_POST['currency']);
	$response = CW_HDwallet::get_address_details_from_mpk($currency);
	$rendered_details = CW_HDwallet::render_address_details_from_mpk($response);

	$transient_name = strtolower($currency).'_hd_wallet_discovery_notice';

	if(false !== ($transient = get_transient($transient_name))) {
		$message = isset($transient->message) ? $transient->message : '';
		if ($message === 'success') {
			printf(__('%sAddress discovery successful!<br>%s HD wallet details below:%s<br>', 'cryptowoo-hd-wallet-addon'), '<span style="color:darkgreen; font-weight:bold;">', $currency, '</span>');
		} else {
			printf('<br><div id="hd-status-message"><span style="color:#B94A48; font-weight:bold;">%s</span></div>', $message);
		}
	}
	echo $rendered_details;
	wp_die(); // this is required to terminate immediately and return a proper response
}
add_action( 'wp_ajax_hd_address_discovery', 'hd_address_discovery_callback' );


/**
 * Skip address discovery callback
 *
 * @throws Exception
 */
function hd_address_fresh_key_callback() {
    $currency = strtoupper($_POST['currency']);
    $mpk_data = CW_HDwallet::get_mpk_data($currency);
    if(update_option($mpk_data->index_key, '0')) {
	    CW_HDwallet::dump_addresses('0', 5, $mpk_data);
        set_transient(sprintf('%s_hd_wallet_discovery_notice', $currency), 'success', 30);
    } else {
        printf(__('Error saving index data for %s. Please try again.', 'cryptowoo-hd-wallet-addon'), $mpk_data->index_key);
    }
    wp_die();
}
add_action( 'wp_ajax_hd_address_fresh_key', 'hd_address_fresh_key_callback' );

/**
 * Skip address discovery callback
 *
 * @throws Exception
 */
function hd_address_set_index_callback() {
    $currency = strtoupper($_POST['currency']);
    $index = is_numeric($_POST['index']) ? (int)$_POST['index'] : 0;
    $mpk_data = CW_HDwallet::get_mpk_data($currency);
    $prev_index = get_option($mpk_data->index_key);
    printf(__('Setting %s index from %d to %d... ','cryptowoo-hd-wallet-addon'), htmlentities($currency), false === $prev_index ? 'not set' : (int)$prev_index, (int)$index);
    if(update_option($mpk_data->index_key, $index)) {
        echo '<span style="color:darkgreen; font-weight:bold;">Success!</span>';
        $mpk = CW_HDwallet::create_address_from_mpk($currency, false, sprintf('%s%d', $mpk_data->derivation_path, (int)$index));
        printf(__('%sNext payment address is %s %s %1$s%sPlease make sure you can see this address in your wallet client before accepting any transactions!%s', 'cryptowoo-hd-wallet-addon'), '<br>', $mpk['mpk_key_position'], CW_Formatting::link_to_address($currency, $mpk['address'], false, true), '<strong>', '</strong>');
        set_transient(sprintf('%s_hd_wallet_discovery_notice', $currency), 'success', 30);
    } else {
        printf(__('Error saving index data for %s. Please try again.', 'cryptowoo-hd-wallet-addon'), $mpk_data->index_key);
    }
    wp_die();
}
add_action( 'wp_ajax_hd_address_set_index', 'hd_address_set_index_callback' );

/**
 * Create address callback
 */
function get_hd_address_callback() {

    if(false === preg_match('/m(\/\d*)+/', $_POST['path'])) {
        printf(__('Derivation path invalid.', 'cryptowoo-hd-wallet-addon'));
    } else {
        // Prepare path
        $path = str_replace('m/', '', $_POST['path']);
        // Create address
        $response = CW_HDwallet::create_address_from_mpk($_POST['currency'], false, $path);

        if($response['status'] === true) {
            printf(
                __('%sDerived %s address: %s%s%s%s%s%s%s', 'cryptowoo-hd-wallet-addon'), '<div id="hd-status-message"><strong>', htmlentities($_POST['currency']),'</strong><p>MPK: <code>', $response['mpk'], '</code><br>Address: <code>', $response['address'], '</code><br>Path: <code>', $response['mpk_key_position'], '</code></p></div>');
        } else {
            printf(__('%s%s Address derivation error: %s%s'), '<div id="hd-status-message"><span style="color:#B94A48; font-weight:bold;">', htmlentities($_POST['currency']) , $response['message'], '</span></div>');
        }
    }
    wp_die();
}
add_action( 'wp_ajax_get_hd_address', 'get_hd_address_callback' );

/**
 * Find last order details callback
 */
function cw_bridge_the_gap_callback() {

    $currency = strtoupper($_POST['currency']);

    // Get the current index
    $mpk_data      = CW_HDwallet::get_mpk_data($currency);
    $current_index = isset($mpk_data->index) ? (int)$mpk_data->index : false;
    if ($current_index === false) {
        printf('<p>%s</p>', __('Address index error. Please run the address discovery function at the top of this page.', 'cryptowoo-hd-wallet-addon'));
    } else {
        $in_gap = array();
        $current_address = CW_HDwallet::create_address_from_mpk($currency, false, $mpk_data->derivation_path . $current_index);
        printf('<p>%s %s %s</p>', __('Next payment address in CryptoWoo:', 'cryptowoo-hd-wallet-addon'), $current_address['mpk_key_position'], CW_Formatting::link_to_address($currency, $current_address['address'], false, true));

        // Create the addresses that will reset the gap limit if used
        $i = 0;
        if (($rounds = ($current_index / 20)) >= 2) {
            printf('<p>%s</p>', __('Make sure the following addresses have received a payment:', 'cryptowoo-hd-wallet-addon'));
            do {
                $current_index -= 20;
                $gap_trigger            = CW_HDwallet::create_address_from_mpk($currency, false, $mpk_data->derivation_path . $current_index);
                $api_context            = CW_Blockcypher::prepare_blockcypher_api($currency, get_option('cryptowoo_payments'));
                $blockcypher_currencies = array('BTC',
                                                'DOGE',
                                                'LTC',
                                                'DASH',
                                                'BTCTEST'); // hackity hack TODO proper selection of API to find out if the address has been used before
                if (in_array($currency, $blockcypher_currencies)) {  // TODO remove hackity hack
                    $addressClient = new BlockCypher\Client\AddressClient($api_context);
                    $received      = json_decode($addressClient->getBalance($gap_trigger['address']));
                    sleep(1); // One address per second
                } else {
                    // Currency not supported by BlockCypher
                    $received = sprintf(__('Error: %s address lookup is not supported yet. Please click on the address to check it manually.', 'cryptowoo-hd-wallet-addon'), $currency);
                }
                if (!isset($received) || !is_object($received)) {
                    // Blockcypher API error
                    $received = sprintf('API Error: %s', var_export($received, true));
                }
                if ($current_index > 19) {
                    $in_gap[] = false;
                    $color = isset($received->n_tx) && is_numeric($received->n_tx) && $received->n_tx > 0 ? 'darkgreen' : 'red';
                    $total = isset($received->total_received) ? __('Total received: ', 'cryptowoo-hd-wallet-addon') . CW_Formatting::fbits($received->total_received) : 'Unused address';
                    echo $gap_trigger['mpk_key_position'] . ': ' . CW_Formatting::link_to_address($currency, $gap_trigger['address'], false, true) . ' <span style="color:' . $color . ';">' . $total . '</span><br>';
                } else {
                    $in_gap[] = true;
                }
                $i++;
            } while ($i <= $rounds);
        } else {
            $in_gap[] = true;
        }
        // First checked address within in gap limit?
        if($in_gap[0]) {
            printf(__('%sThe next payment address in derivation path %s is within the gap limit of 20. All payments should be visible to your wallet client. Please contact us if you experience any problems.', 'cryptowoo-hd-wallet-addon'), '<p>', $current_address['mpk_key_position'], '</p>');
        }
    }
    wp_die();
}
add_action( 'wp_ajax_cw_bridge_the_gap', 'cw_bridge_the_gap_callback' );

function cw_list_addresses_callback() {
    $currency = strtoupper($_POST['currency']);
    // Get the current index
    $mpk_data      = CW_HDwallet::get_mpk_data($currency);
    $current_index = isset($mpk_data->index) ? (int)$mpk_data->index : false;
    if ($current_index === false) {
        printf('<p>%s</p>', __('Address index error. Please run the address discovery function at the top of this page or set the current index manually.', 'cryptowoo-hd-wallet-addon'));
    } else {
        $i = 0;
        printf('<p>%s</p><table>', $mpk_data->mpk);
        do {
            $address_data = CW_HDwallet::create_address_from_mpk($currency, false, $mpk_data->derivation_path . $i);
            $fw = $i > 0 && $i %20 != 0 ? 'normal' : 'bold';
            printf('<tr style="font-weight: %s;"><td>%d</td><td>%s</td><td>%s</td></tr>', $fw, $i, $address_data['mpk_key_position'], CW_Formatting::link_to_address($currency, $address_data['address'], false, true));
            $i++;
        } while ($i <= $current_index);
        echo '</table>';
    }
    wp_die();
}
add_action( 'wp_ajax_cw_list_addresses', 'cw_list_addresses_callback' );
