<?php

/**
 * cryptoID API Helper
 *
 * https://chainz.cryptoid.info/api.dws
 *
 */
class CW_CryptoID
{
    /**
     * Get data for each address and update the amount received
     *
     * @param $currency
     * @param $batch
     * @param $orders
     * @param $options
     * @return array
     */
    static function _batch_tx_update($currency, $batch, $orders, $options) {
        $tx_data = array();
        $key = isset($options['cryptoid_api_key']) ? $options['cryptoid_api_key'] : false;
        foreach($orders as $order) {
            $api_data = self::get_received_by_address($currency, $order->address, $key); // No txids
            $tx_data[] = self::cryptoid_data_update($order, $api_data, $options);
        }
        return $tx_data;
    }

    /**
     * Get data for each address and update the amount received
     *
     * @param $currency
     * @param $batch
     * @param $orders
     * @param $options
     * @return array
     */
    static function batch_tx_update($currency, $batch, $orders, $options) {
        $tx_data = array();
        $key = isset($options['cryptoid_api_key']) ? $options['cryptoid_api_key'] : false;
        foreach($orders as $order) {
            $response = self::get_multiaddr($currency, $batch, $key);
            $analyze_timestamps = self::analyze_multiaddr($response, $order);
            //CW_AdminMain::cryptowoo_log_data(0, 'batch_tx_update', $analyze_timestamps, 'cryptowoo-tx-update.log');
            $api_data['balance'] = isset($analyze_timestamps['balance']) ? $analyze_timestamps['balance'] : 0;
            $api_data['txids'] = isset($analyze_timestamps['txids']) ? $analyze_timestamps['txids'] : array();
            //$api_data['txs'] = isset($analyze_timestamps['txs']) ? $analyze_timestamps['txs'] : array();
            $tx_data[] = self::cryptoid_data_update($order, $api_data, $options);
        }
        return $tx_data;
    }

    /**
     * Update order data
     *
     * @param $order_data
     * @param $api_data
     * @param $options
     * @return mixed
     */
    static function cryptoid_data_update($order_data, $api_data, $options) {
        $dbupdate = 0;
        $is_numeric = isset($api_data['balance']) && is_numeric($api_data['balance']);
        $amount = $is_numeric ? $api_data['balance'] : 0;
        $txids_serialized = json_encode($api_data['txids']);

        $payment_array[$order_data->order_id] = array('status' => $is_numeric ? 'cryptoID.info success' : sprintf('cryptoID.info error: %s', implode('|',$api_data)),
                                                      'address' => $order_data->address,
                                                      'order_id' => $order_data->order_id,
                                                      'total_received_confirmed' => $amount,
                                                      'txids' => $txids_serialized,
                                                      'confidence' => 'not_supported',
        );
        if (WP_DEBUG && (bool)$options['logging']['transactions']) {
            file_put_contents(CW_LOG_DIR . 'cryptowoo-tx-update.log', date('Y-m-d H:i:s') . " cryptoID.info|#{$order_data->order_id}|$order_data->address|{$amount}\r\n", FILE_APPEND);
        }
        // Calculate order age
        $payment_array[$order_data->order_id]['order_age'] = time() - strtotime($order_data->created_at);

        // Maybe update order data // TODO move to update_tx_details()
        if (strpos($payment_array[$order_data->order_id]['status'], 'success') || $payment_array['force_update'] === 'yes') {

            // Force order processing
            $payment_array['force_update'] = 'yes'; // TODO Revisit force processing

            // Update payments table
            $dbupdate += CW_OrderProcessing::update_address_info($order_data->address, $payment_array[$order_data->order_id]['total_received_confirmed'], 0, 'not_supported', $order_data->order_id);

            // Update order meta
            $order_meta = array(
                'order_id' => $order_data->order_id,
                'received_confirmed' => $payment_array[$order_data->order_id]['total_received_confirmed'],
                'received_unconfirmed' => 0,
                'txids' => $txids_serialized,
                'has_txids' => $is_numeric,
            );

            CW_OrderProcessing::cw_update_order_meta($order_meta);

        } else {
            $payment_array[$order_data->order_id] = array_merge($payment_array[$order_data->order_id], array('timeout_in' => $order_data->timeout_value - time(),
                                                                                                             'timeout' => $order_data->timeout));
        }
        //}
        $payment_array['dbupdate'] = $dbupdate;
        return $payment_array;
    }

    /**
     * WP Remote GET
     *
     * @param $url
     * @param bool $returns_string
     * @return array|mixed|object|string
     */
    static function remote_get($url, $returns_string = false) {
        $request = wp_remote_get($url);
        return is_wp_error($request) ? $request->get_error_message() : !$returns_string ? json_decode($request['body'], true) : $request['body'];
    }

    /**
     * Get received by address, authentication optional
     * For incorrect addresses or addresses never seen on the network, the returned amount is zero.
     * Unauthenticated requests can be delayed by up to 1 hour.
     *
     * @param $currency
     * @param $address
     * @param bool $key
     * @return int
     */
    static function get_received_by_address($currency, $address, $key = false) {
        $auth = $key ? "&key={$key}" : '';
        $url  = sprintf('https://chainz.cryptoid.info/%s/api.dws?q=getreceivedbyaddress&a=%s%s', strtolower($currency), $address, $auth);
        $data = self::remote_get($url);
        if (!is_numeric($data)) {
            CW_AdminMain::cryptowoo_log_data(0, 'cryptoid_receivedbyaddress', $data, 'cryptowoo-tx-update.log');
            $data = 0;
        }
        return array(
            'balance' => CW_OrderProcessing::cw_float_to_int($data),
            'txs' => 'not_supported'
        );
    }

    /**
     * Get address batch data, authentication required
     * Returns summary information about those addresses (balance, total sent/receive and latest transactions).
     * By default returns the last 50 transactions, but this number can be adjusted by the optional 'n' parameter.
     *
     * @param $currency
     * @param $addresses
     * @param $key
     * @return array|bool|mixed|object|string
     */
    static function get_multiaddr($currency, $addresses, $key) {
        $impl_addresses = implode('|', $addresses);
        $url            = sprintf('https://chainz.cryptoid.info/%s/api.dws?q=multiaddr&active=%s&key=%s', strtolower($currency), $impl_addresses, $key);
        $data           = self::remote_get($url);
        if (!is_array($data)) {

            // Rate limit transient
            $limit_transient = get_transient('cryptowoo_limit_rates');

            // Action hook for CryptoID API error
            do_action('cryptowoo_api_error', array('error' => 'CryptoID Multiaddress API Error'));

            // Update rate limit transient
            $transient       = isset($limit_transient[$currency]['count']) ? array( $currency => array('count' => (int)$limit_transient[$currency]['count'] + 1, 'api' => 'cryptoid')) : array( $currency => array('count' => 1, 'api' => 'cryptoid'));
            // Keep error data until the next full hour (rate limits refresh every full hour). We'll try again after that time.
            set_transient('cryptowoo_limit_rates', $transient, CW_AdminMain::seconds_to_next_hour());

            CW_AdminMain::cryptowoo_log_data(0, 'cryptoid_multiaddr API Error ', $data, 'cryptowoo-tx-update.log');
            $data = 0;
        }
        return $data;
    }

    /**
     * Calculate address balance from transactions in the latest set
     *
     * @param $data
     * @param $order
     * @return mixed
     */
    static function analyze_multiaddr($data, $order) {
        $txs = self::analyze_timestamps($data, $order); // Prevent counting old transaction amounts when accidentially reusing addresses
        $return['balance'] = 0;
        foreach($txs as $tx) {
            $return['balance'] += $tx['data']['change'];
            $return['txids'][] = $tx['data']['hash'];
        }
        //CW_AdminMain::cryptowoo_log_data(0, 'cryptoid_multiaddr', $return, 'cryptowoo-tx-update.log');
        return $return;
    }

    /**
     * Group transaction data by hour and return only the latest batch
     *
     * @param $data
     * @param $order
     * @return mixed
     */
    static function analyze_timestamps($data, $order) {
        if (is_array($data['txs'])) {
            foreach ($data['txs'] as $tx_data) {
                $tx_time = strtotime($tx_data['time_utc']);
                // Only consider transaction if it has been seen after the order was created
                if (strtotime($order->created_at) <= $tx_time) {
                    $tx_time_hr                 = (int)round($tx_time / 60 / 60) * 60 * 60; // Group transaction data by hour
                    $txs[$tx_time_hr][]['data'] = array('hash' => $tx_data['hash'],
                                                        'change' => $tx_data['change']);
                }
            }
        }
        krsort($txs);
        return array_values($txs)[0]; // Return only the latest set of transactions
    }

    /**
     * Get single address balance, authentication required
     * Returns the current value without delays.
     *
     * @param $currency
     * @param $address
     * @param $key
     * @return int
     */
    static function get_balance($currency, $address, $key) {
        $url  = sprintf('https://chainz.cryptoid.info/%s/api.dws?q=getbalance&a=%s&key=%s', strtolower($currency), $address, $key);
        $data = self::remote_get($url);
        if (!is_numeric($data)) {
            CW_AdminMain::cryptowoo_log_data(0, 'cryptoid_getbalance', $data, 'cryptowoo-tx-update.log');
            $data = false;
        }
        return CW_OrderProcessing::cw_float_to_int($data);
    }

    /**
     * Get address usage status
     * Returns the date and time of the block in which the address was first seen, or a string beginning with "ERROR:" otherwise.
     *
     * @param $currency
     * @param $address
     * @param $key
     * @return int
     */
    static function get_address_first_seen($currency, $address, $key) {
        $url  = sprintf('https://chainz.cryptoid.info/%s/api.dws?q=addressfirstseen&a=%s&key=%s', strtolower($currency), $address, $key);
        $data = self::remote_get($url, true);
        if (false === strpos($data, 'ERROR')) {
            CW_AdminMain::cryptowoo_log_data(0, 'cryptoid_getaddressfirstseen', "{$currency} Address reuse detected|{$address}|{$data}", 'cryptowoo-tx-update.log');
            $data = true;
        } else {
            $data = false;
        }
        return (bool)$data;
    }

}
