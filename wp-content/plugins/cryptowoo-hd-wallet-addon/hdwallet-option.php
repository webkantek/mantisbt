<?php
if(class_exists('Redux') && isset($opt_name)) {

    /**
     * Redux Master Public Key validation helper
     */
    if (!function_exists('redux_validate_mpk')):

        function redux_validate_mpk($field, $value, $existing_value) {

            $error = false;

            // Only continue if value has changed
            if(empty($value) || strcmp($value, $existing_value) === 0) {
                $return['value'] = $value;
                return $return;
            }

            // Get currency parameters
            $currency_params = CW_HDwallet::get_currency_params($field);

            // Delete the old HD Wallet index details for this currency
            delete_option($currency_params->index_key);

            $strlen_mpk = strlen($value);

            if(false !== $currency_params && strncmp($currency_params->mand_mpk_prefix, $value, 4) === 0 && (int)$strlen_mpk == (int)$currency_params->strlen) {

                // Remove old index data for this mpk
                $mpk_data = CW_HDwallet::get_mpk_data($currency_params->currency);
                if(isset($mpk_data->index_key)) {
                    delete_option($mpk_data->index_key);
                }

                // use base58 to validate mpk
                $validate = new CW_Validate();
                $result = $validate->validate_mpk($value, $currency_params->mand_mpk_prefix, $currency_params->mand_base58_prefix);

                if(!$result) {
                    $error = true;
                    $field['msg'] = sprintf(__('Master Public Key is invalid.%sReload Page%s', 'cryptowoo-hd-wallet-addon'), '<a class="button-primary" href="javascript: window.location.reload(true)">','</a>');
                    $value = '';//$existing_value;
                } elseif(!preg_match('/\_[0-9]/', $field['id'])) { // No discovery notice for cosigner xpubs

                    // We have a valid MPK - now let's find the first unused address
                    //$message = sprintf(__('%sHD Address discovery incomplete.%s', 'cryptowoo-hd-wallet-addon'), '<p>', '</p>');
                    $discovery_info = new stdClass();
                    $discovery_info->message = sprintf(__('%s%s HD Address discovery incomplete%s', 'cryptowoo-hd-wallet-addon'), '<br><strong>', $currency_params->currency, '</strong>');
                    $output = CW_HDwallet::set_hd_wallet_discovery_notice($currency_params->currency, $discovery_info, true);
                    $error = true;

                    $field['msg'] = isset( $output->message ) ?  $output->message : ' Y no message? :(';//$notice;
                }
            } else {
                $field['msg'] = sprintf(__('Master Public Key is invalid.%sReload Page%s', 'cryptowoo-hd-wallet-addon'), '<a class="button-primary" href="javascript: window.location.reload(true)">','</a>');
                $error = true;
                $value = '';//$existing_value;
            }
            $return['value'] = $value;
            if ($error == true) {
                $return['error'] = $field;
                /*if(WP_DEBUG) {
                    file_put_contents(WC_LOG_DIR . 'cryptowoo-error.log', date("Y-m-d H:i:s") . __FILE__ . "\n".'redux_validate_mpk - key: '.var_export($value, true) . ' | result: ' .var_export($return, true) ."\n", FILE_APPEND);
                }*/
            }
            return $return;
        }

    endif;
    /**
     * Redux Master Public Key validation helper
     */
    if (!function_exists('redux_validate_derivation_path')):

        function redux_validate_derivation_path($field, $value, $existing_value) {

	        // Only continue if value has changed
	        if ( strcmp( $value, $existing_value ) === 0 ) {
		        $return['value'] = $value;

		        return $return;
	        }

	        $currency = strtoupper( str_replace( 'derivation_path_', '', $field['id'] ) );

	        $mpk_data = CW_HDwallet::get_mpk_data( $currency );

	        $mpk_data->derivation_path = str_replace('m', '', $value);

	        $field['msg'] = CW_HDwallet::dump_addresses( $mpk_data->index, 5, $mpk_data, true );

	        $return['error'] = $field;
	        $return['value'] = $value;
	        return $return;
        }

    endif;

    /**
     * Redux multisig validation helper
     */
    if (!function_exists('redux_validate_multisig')):

        function redux_validate_multisig($field, $value, $existing_value) { // TODO add more validation or improve library error messages

	        $return['value'] = $value;
	        return $return;

	        // Only continue if value has changed
	        if ( strcmp( $value, $existing_value ) === 0 ) {
		        $return['value'] = $value;
		        return $return;
	        }

	        $currency = strtoupper( preg_replace( '/_multisig_scheme_n|_multisig_scheme_m/', '', $field['id'] ) );


	        $mpk_data = CW_HDwallet::get_mpk_data( $currency );

	        if(false !== strpos($field['id'], '_multisig_scheme_m')) {
	        	// Number of signatures changed
		        $mpk_data->signatures_required = $value;
	        } else {
	        	// Number of cosigners changed - slice unneeded xpubs
		        if(count($mpk_data->mpk) > $value) {
			        $mpk_data->mpk = array_slice( $mpk_data->mpk, 0, (int) $value, true );
		        }
	        }

	        // Reset index
	        $mpk_data->index = 0;
	        update_option($mpk_data->index_key, 0);

	        $field['msg'] = CW_HDwallet::dump_addresses( $mpk_data->index, 5, $mpk_data, true );
	        //$format = '<div class="button" id="cw-list-multisig-addresses-%1$s">Display payment addresses</div><div id="cw-list-multisig-addresses-%1$s-response" ></div>';
	        //$field['msg'] = sprintf($format, strtolower($currency));

	        $return['error'] = $field;
	        $return['value'] = $value;
	        return $return;
        }

    endif;

    /**
     * Get the HD wallet discovery notice transient
     *
     *
     * @param $field
     * @return mixed
     */
    function hd_wallet_discovery_button($field) {

        $currency_params = CW_HDwallet::get_currency_params($field);
        $transient_name = strtolower($currency_params->currency) . '_hd_wallet_discovery_notice';

        $mpk_data = CW_HDwallet::get_mpk_data($currency_params->currency, false);
        // Add info about current index to field
        $current_index = get_option($mpk_data->index_key, false);
        $lc_currency = strtolower($currency_params->currency);
        $notice = '';

        if(false === $current_index && isset($mpk_data->mpk) && (bool)$mpk_data->mpk) {
            // Discovery button
            $notice .= sprintf('<p><div class="button-primary" id="cw-discover-%s" href="#">%s</div></p>', $lc_currency, sprintf(__('Discover %s Addresses', 'cryptowoo-hd-wallet-addon'),$currency_params->currency), $lc_currency, $lc_currency);
            // New key button
            $notice .= sprintf('<div style="padding: 1em 0 1em 0;">%s<div class="button" id="cw-skip-%s" href="#">%s</div></div>', $current_index,$lc_currency, sprintf(__('This is a new key - skip %s address discovery', 'cryptowoo-hd-wallet-addon'), $currency_params->currency), $lc_currency, $lc_currency);
            // Set index button
            $notice .= sprintf('<div style="padding: 1em 0 1em 0;">%s<div class="button" id="cw-set-%s" href="#">%s</div><input type="number" min="0" id="cw-index-%s" name="cw-index-%s"</div>', $current_index,$lc_currency, sprintf(__('Set %s address index to:', 'cryptowoo-hd-wallet-addon'), $currency_params->currency), $lc_currency, $lc_currency);
            // Result area
            $notice .= sprintf('<div id="cw-discover-%s-response"></div>', $lc_currency);
        }

        $transient = get_transient($transient_name);
        if (false !== $transient && (bool)$mpk_data->mpk) {
            if (isset($transient->message) && $transient->message === 'success') {
                $notice .= sprintf(__('%sAddress discovery successful!%s', 'cryptowoo-hd-wallet-addon'), '<p><span style="color:darkgreen; font-weight:bold;">', '</span></p>');
            } /*else {
                $notice .= isset($transient->message) ? $transient->message : '';
                //$notice .= sprintf('<div style="padding: 1em 0em 1em 0em;">%s<div class="button" id="cw-skip-%s" href="#">This is a new key - skip %s address discovery</div></div><div id="cw-skip-%s-loading"> ', $current_index,$lc_currency, $currency_params->currency, $lc_currency, $lc_currency, $lc_currency);
                //$notice .= sprintf('<div class="button-primary" id="cw-discover-%s" href="#">Discover %s Addresses</div></div><div id="cw-%s-loading"><i class="fa fa-refresh fa-2x"></i></div>', $lc_currency, $currency_params->currency, $lc_currency, $lc_currency, $lc_currency);
            }*/
            //$notice .= sprintf('<pre><div id="cw-discover-%s-response"></div></pre>', $lc_currency);
        }

        // Maybe add addresses
        if(false !== $current_index && isset($mpk_data->mpk) && false !== $mpk_data->mpk) {
        	if($mpk_data->index > 2) {
		        $index = $mpk_data->index - 2;
	        } else {
		        $index = $mpk_data->index;
	        }
	        $notice .= sprintf(__('%sCurrent index: %s%s%s%s%s', 'cryptowoo-hd-wallet-addon'), '<p>', '<code>', $current_index, '</code></p>', PHP_EOL, CW_HDwallet::dump_addresses($index, 4, $mpk_data, true));
        }

        $field['desc'] .= $notice;
        return $field;
    }
    add_filter("redux/options/{$opt_name}/field/cryptowoo_btc_mpk", 'hd_wallet_discovery_button');
    //add_filter("redux/options/{$opt_name}/field/cryptowoo_ltc_mpk", 'hd_wallet_discovery_button');
    add_filter("redux/options/{$opt_name}/field/cryptowoo_ltc_mpk_xpub", 'hd_wallet_discovery_button');
    //add_filter("redux/options/{$opt_name}/field/cryptowoo_doge_mpk", 'hd_wallet_discovery_button');
    add_filter("redux/options/{$opt_name}/field/cryptowoo_doge_mpk_xpub", 'hd_wallet_discovery_button');
    add_filter("redux/options/{$opt_name}/field/cryptowoo_btc_test_mpk", 'hd_wallet_discovery_button');
    //add_filter("redux/options/{$opt_name}/field/cryptowoo_blk_mpk", 'hd_wallet_discovery_button');
    add_filter("redux/options/{$opt_name}/field/cryptowoo_blk_mpk_xpub", 'hd_wallet_discovery_button');


    /**
     * Reset MPK data.
     *
     * @param $redux_object
     */
    function hd_wallet_reset_mpk_data($redux_object) {

        $index_keys = array(
            'btc'=> 'cryptowoo_btc_index',
            'btctest'=> 'cryptowoo_btc_test_index',
            'doge'=> 'cryptowoo_doge_index',
            //'dogetest'=>  'cryptowoo_doge_test_index',  // @todo Uncomment when DOGETEST HD wallet is supported
            'ltc'=> 'cryptowoo_ltc_index',
            'blk'=> 'cryptowoo_blk_index',
        );

        foreach($index_keys as $currency => $index_key) {
            delete_option($index_key);
            usleep(200000);
            delete_transient("{$currency}_hd_wallet_discovery_notice");
        }

    }
    add_action("redux/options/{$opt_name}/reset", 'hd_wallet_reset_mpk_data'); // Fires when the redux settings are reset globally

    $pubkey_hint =  array(
        'title' => 'Please Note:',
        'content' => sprintf(__('If you enter a used key you will have to run the address discovery process after saving this setting.%sUse a dedicated HD wallet (or at least a dedicated xpub) for your store payments to prevent address reuse.', 'cryptowoo-hd-wallet-addon'), '<br>'),
    );

    /**
     * HD Wallet Admin Settings
     * TODO Add dropdown menu "Select wallet client" for each currency
     */
    Redux::setSection($opt_name, array(
        'title' => __('HD Wallet', 'cryptowoo-hd-wallet-addon'),
        'id' => 'wallets-hdwallet',
        'desc' => __('<p>Hierarchical deterministic ("<a href="https://bitcoin.org/en/glossary/hd-protocol" target="_blank" title="Bitcoin.org Glossary: HD Protocol">HD</a>") wallets enable you to receive payments directly to addresses under your control without relying on any third party service.
                            <br>You need to create an "Extended Public Key" with a <a href="https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki" target="_blank">BIP32</a> or <a href="https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki" target="_blank">BIP44</a> compatible client to use this feature. Check out the <a href="http://www.cryptowoo.com/hd-wallet-tutorials?utm_source=config" target="_blank" title="HD Wallet Tutorials">HD wallet tutorial section</a> on our website to learn how to make the most out of CryptoWoo.</p>
                            <p><span class="cryptowoo-warning"><strong>Please note:</strong></span> Use a dedicated HD wallet (or at least a dedicated xpub) for your store payments. CryptoWoo just uses the next address in the derivation path and does not check for address re-use while deriving a new payment address. This means that if you use your wallet to accept payments outside of the store, CryptoWoo will not know about it and re-use these addresses.</p>
                                <table>
                                    <tr><th>Compatible clients</th></tr>
                                    <tr>
                                        <td>Bitcoin</td>
                                        <td><a href="https://electrum.org/" title="Electrum" target="_blank">Electrum</a> | <a title="Mycelium Wallet" href="https://mycelium.com/mycelium-wallet.html" target="_blank">Mycelium Wallet</a> | <a href="https://www.cryptowoo.com/ledger-wallet" target="_blank">Ledger Wallet</a> | <a href="https://github.com/dcpos/bip39" target="_blank">BIP39 Tool</a></td>
                                    </tr>
                                    <tr>
                                        <td>Litecoin</td>
                                        <td><a href="https://electrum-ltc.org/" title="Electrum for Litecoin" target="_blank">Electrum-LTC</a> | <a href="https://coinomi.com/" title="Coinomi" target="_blank">Coinomi</a> | <a href="https://github.com/dcpos/bip39" target="_blank">BIP39 Tool</a></td>
                                    </tr>
                                    <tr>
                                        <td>Dogecoin</td>
                                        <td><a href="https://coinomi.com/" title="Coinomi" target="_blank">Coinomi</a> | <a href="https://github.com/dcpos/bip39" target="_blank">BIP39 Tool</a></td>
                                    </tr>
                                    <tr>
                                        <td>BlackCoin</td>
                                        <td><a href="https://coinomi.com/" title="Coinomi" target="_blank">Coinomi</a> | <a href="http://blackcoin.co/" title="BlackCoin Website" target="_blank">More</a></td>
                                    </tr>
                                </table>Derivation path: <code>m/0/i</code>', 'cryptowoo-hd-wallet-addon'),
        'subsection' => true,
        'icon' => 'fa fa-shield',
        'fields' => array(
	        array(
		        'id' => 'missing_cosigners',
		        'type' => 'info',
		        'style' => 'critical',
		        'icon' => 'el el-warning-sign',
		        'title' => _n('Missing Cosigner - using single signature addresses', 'Missing Cosigners - using single signature addresses', 2, 'cryptowoo-hd-wallet-addon'),
		        'required' => array(
			        array('use_multisig_btc', 'equals', true),
			        array('cryptowoo_btc_mpk_2', 'equals', '')
		        ),
		        'desc' => __('Please enter the extended public key of cosigner 2', 'cryptowoo-hd-wallet-addon'),
	        ),
	        array(
		        'id' => 'missing_mpk_3',
		        'type' => 'info',
		        'style' => 'critical',
		        'icon' => 'el el-warning-sign',
		        'title' => _n('Missing Cosigner - using single signature addresses', 'Missing Cosigners - using single signature addresses', 2, 'cryptowoo-hd-wallet-addon'),
		        'required' => array(
			        array('use_multisig_btc', 'equals', true),
			        array('btc_multisig_scheme_n', '>', '2'),
			        array('cryptowoo_btc_mpk_3', 'equals', '')
		        ),
		        'desc' => __('Please enter the extended public key of cosigner 3', 'cryptowoo-hd-wallet-addon'),
	        ),
	        array(
		        'id' => 'missing_mpk_4',
		        'type' => 'info',
		        'style' => 'critical',
		        'icon' => 'el el-warning-sign',
		        'title' => _n('Missing Cosigner - using single signature addresses', 'Missing Cosigners - using single signature addresses', 2, 'cryptowoo-hd-wallet-addon'),
		        'required' => array(
			        array('use_multisig_btc', 'equals', true),
			        array('btc_multisig_scheme_n', '>', '3'),
		            array('cryptowoo_btc_mpk_4', 'equals', '')
		        ),
		        'desc' => __('Please enter the extended public key of cosigner 4', 'cryptowoo-hd-wallet-addon'),
	        ),
	        array(
		        'id' => 'missing_mpk_5',
		        'type' => 'info',
		        'style' => 'critical',
		        'icon' => 'el el-warning-sign',
		        'title' => _n('Missing Cosigner - using single signature addresses', 'Missing Cosigners - using single signature addresses', 2, 'cryptowoo-hd-wallet-addon'),
		        'required' => array(
			        array('use_multisig_btc', 'equals', true),
			        array('btc_multisig_scheme_n', '>', '4'),
		            array('cryptowoo_btc_mpk_5', 'equals', '')
		        ),
		        'desc' => __('Please enter the extended public key of cosigner 5', 'cryptowoo-hd-wallet-addon'),
	        ),
            array(
                'id' => 'wallets-hdwallet-bitcoin',
                'type' => 'section',
                'title' => __('Bitcoin', 'cryptowoo-hd-wallet-addon'),
                //'required' => array('testmode_enabled','equals','0'),
                'icon' => 'cc BTC',
                //'subtitle' => __('Use the field with the correct prefix of your Litecoin MPK. The prefix depends on the wallet client you used to generate the key.', 'cryptowoo-hd-wallet-addon'),
                'indent' => true,
            ),
            array(
                'id' => 'cryptowoo_btc_mpk',
                'type' => 'text',
                'ajax_save' => false,
                'username' => false,
                'desc' => __('Bitcoin HD Wallet Extended Public Key (xpub...)', 'cryptowoo-hd-wallet-addon'),
                'title' => __('Bitcoin HD Wallet Extended Public Key', 'cryptowoo-hd-wallet-addon'),
                'validate_callback' => 'redux_validate_mpk',
                //'required' => array('cryptowoo_btc_api', 'equals', ''),
                'placeholder' => 'xpub...',
                'text_hint' => $pubkey_hint
            ),
            array(
                'id'       => 'use_multisig_btc',
                'type'     => 'switch',
                'title'    => __('m-of-n Multi-Signature Wallet', 'cryptowoo-hd-wallet-addon'),
                'subtitle' => __('Enable this to add multiple cosigners to your payment addresses.', 'cryptowoo-hd-wallet-addon'),
                'desc'     => sprintf(__('%sMore info%s', 'cryptowoo-hd-wallet-addon'), '<a href="https://www.cryptowoo.com/multisignature-hd-wallets?utm_source=hd-options" target="_blank">','</a>'),
                'default'  => false,
            ),
	        array(
		        'id' => 'btc_multisig_scheme_m',
		        'type'     => 'spinner',
		        'title'    => __('Signature requirements (m)', 'cryptowoo-hd-wallet-addon'),
		        'subtitle' => __('Number of signatures','cryptowoo-hd-wallet-addon'),
		        'desc'     => __('Enter the number of signatures required to unlock the coins', 'cryptowoo-hd-wallet-addon'),
		        'default'  => '2',
		        'min'      => '2',
		        'step'     => '1',
		        'max'      => '3',
		        'ajax_save' => false,
		        'validate_callback' => 'redux_validate_multisig',
		        'required' => array('use_multisig_btc', 'equals', true),
	        ),
	        array(
		        'id' => 'btc_multisig_scheme_n',
		        'type'     => 'spinner',
		        'title'    => __('Number of Cosigners (n)', 'cryptowoo-hd-wallet-addon'),
		        'subtitle' => __('Number of public keys','cryptowoo-hd-wallet-addon'),
		        'desc'     => __('Enter the number of public keys used to create the addresses and add them to the fields below.', 'cryptowoo-hd-wallet-addon'),
		        'default'  => '3',
		        'min'      => '3',
		        'step'     => '1',
		        'max'      => '5',
		        'ajax_save' => false,
		        'validate_callback' => 'redux_validate_multisig',
		        'required' => array('use_multisig_btc', 'equals', true),
	        ),
            array(
                'id' => 'cryptowoo_btc_mpk_2',
                'type' => 'text',
                'ajax_save' => false,
                'username' => false,
                'desc' => sprintf(__('Cosigner %d Extended Public Key (xpub...)', 'cryptowoo-hd-wallet-addon'), 2),
                'title' => sprintf(__('Cosigner %d', 'cryptowoo-hd-wallet-addon'), 2),
                'validate_callback' => 'redux_validate_mpk',
                'required' => array('use_multisig_btc', 'equals', true),
                'placeholder' => 'xpub...',
            ),
            array(
                'id' => 'cryptowoo_btc_mpk_3',
                'type' => 'text',
                'ajax_save' => false,
                'username' => false,
                'desc' => sprintf(__('Cosigner %d Extended Public Key (xpub...)', 'cryptowoo-hd-wallet-addon'), 3),
                'title' => sprintf(__('Cosigner %d', 'cryptowoo-hd-wallet-addon'), 3),
                'validate_callback' => 'redux_validate_mpk',
                'required' => array(
	                array('use_multisig_btc', 'equals', true),
	                array('btc_multisig_scheme_n', '>', 2),
                ),
                'placeholder' => 'xpub...',
            ),
            array(
                'id' => 'cryptowoo_btc_mpk_4',
                'type' => 'text',
                'ajax_save' => false,
                'username' => false,
                'desc' => sprintf(__('Cosigner %d Extended Public Key (xpub...)', 'cryptowoo-hd-wallet-addon'), 4),
                'title' => sprintf(__('Cosigner %d', 'cryptowoo-hd-wallet-addon'), 4),
                'validate_callback' => 'redux_validate_mpk',
                'placeholder' => 'xpub...',
                'required' => array(
	                array('use_multisig_btc', 'equals', true),
	                array('btc_multisig_scheme_n', '>', 3),
                ),
            ),
	        array(
		        'id' => 'cryptowoo_btc_mpk_5',
		        'type' => 'text',
		        'ajax_save' => false,
		        'username' => false,
		        'desc' => sprintf(__('Cosigner %d Extended Public Key (xpub...)', 'cryptowoo-hd-wallet-addon'), 5),
		        'title' => sprintf(__('Cosigner %d', 'cryptowoo-hd-wallet-addon'), 5),
		        'validate_callback' => 'redux_validate_mpk',
		        'required' => array(
		        	array('use_multisig_btc', 'equals', true),
		            array('btc_multisig_scheme_n', '>', 4),
		        ),
		        'placeholder' => 'xpub...',
	        ),
	        array(
		        'id'         => 'derivation_path_btc',
		        'type'       => 'select',
		        'subtitle'   => '',
		        'title'             => sprintf( __( '%s Derivation Path', 'cryptowoo-hd-wallet-addon' ), 'BTC' ),
		        'desc'              => __('Change the derivation path to match the derivation path of your wallet client.', 'cryptowoo-hd-wallet-addon'),
		        'validate_callback' => 'redux_validate_derivation_path',
		        'options'    => array(
			        '0/' => __('m/0/i (e.g. Electrum Standard Wallet)', 'cryptowoo-hd-wallet-addon'),
			        'm' => __('m/i (BIP44 Account)', 'cryptowoo-hd-wallet-addon'),
		        ),
		        'default'    => '0/',
		        'select2'    => array( 'allowClear' => false )
	        ),
            array(
                'id' => 'section-end',
                'type' => 'section',
                'indent' => false,
            ),
            array(
                'id' => 'wallets-hdwallet-dogecoin',
                'type' => 'section',
                'title' => __('Dogecoin', 'cryptowoo-hd-wallet-addon'),
                //'required' => array('testmode_enabled','equals','0'),
                'icon' => 'cc DOGE-alt',
                'subtitle' => sprintf(__('Use the field with the correct prefix of your %s extended public key. The prefix depends on the wallet client you used to generate the key.', 'cryptowoo-hd-wallet-addon'), 'Litecoin'),
                'indent' => true,
            ),
            array(
                'id' => 'cryptowoo_doge_mpk',
                'type' => 'text',
                'ajax_save' => false,
                'username' => false,
                'desc' => '', //__('<b>Dogecoin <a href="https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki" target="_blank">BIP32</a> Wallet Master Public Key</b>', 'cryptowoo-hd-wallet-addon'),
                'title' => sprintf(__('%sprefix%s', 'cryptowoo-hd-wallet-addon'), '<b>Dogecoin "dgub..." ', '</b>'),
                'validate_callback' => 'redux_validate_mpk',
                //'required' => array('cryptowoo_doge_api', 'equals', ''),
                /*'required' => array(
                                array('wallets-hd-enable','equals','1'),
                                array('cryptowoo_doge_api','equals','')
                              ), */
                'placeholder' => 'dgub...',
                'text_hint' => $pubkey_hint
            ),
            array(
                'id' => 'cryptowoo_doge_mpk_xpub',
                'type' => 'text',
                'ajax_save' => false,
                'username' => false,
                'title' => sprintf(__('%sprefix%s', 'cryptowoo-hd-wallet-addon'), '<b>Dogecoin "xpub..." ', '</b>'),
                'desc' => sprintf(__('Remove this key to use the %s prefix format.', 'cryptowoo-hd-wallet-addon'), 'dgub'),
                'validate_callback' => 'redux_validate_mpk',
                //'required' => array('cryptowoo_doge_mpk', 'equals', ''),
                'placeholder' =>  'xpub...', // xpub format
                'text_hint' => $pubkey_hint
            ),
	        array(
		        'id'         => 'derivation_path_doge',
		        'type'       => 'select',
		        'subtitle'   => '',
		        'title'             => sprintf( __( '%s Derivation Path', 'cryptowoo-hd-wallet-addon' ), 'DOGE' ),
		        'desc'              => __('Change the derivation path to match the derivation path of your wallet client.', 'cryptowoo-hd-wallet-addon'),
		        'validate_callback' => 'redux_validate_derivation_path',
		        'options'    => array(
			        '0/' => __('m/0/i (e.g. Electrum Standard Wallet)', 'cryptowoo-hd-wallet-addon'),
			        'm' => __('m/i (BIP44 Account)', 'cryptowoo-hd-wallet-addon'),
		        ),
		        'default'    => '0/',
		        'select2'    => array( 'allowClear' => false )
	        ),
            array(
                'id' => 'section-end',
                'type' => 'section',
                'indent' => false,
            ),
            array(
                'id' => 'wallets-hdwallet-litecoin',
                'type' => 'section',
                'title' => __('Litecoin', 'cryptowoo-hd-wallet-addon'),
                //'required' => array('testmode_enabled','equals','0'),
                'icon' => 'cc LTC-alt',
                'subtitle' => sprintf(__('Use the field with the correct prefix of your %s extended public key. The prefix depends on the wallet client you used to generate the key.', 'cryptowoo-hd-wallet-addon'), 'Litecoin'),
                'indent' => true,
            ),
            array(
                'id' => 'cryptowoo_ltc_mpk',
                'type' => 'text',
                'ajax_save' => false,
                'username' => false,
                //'subtitle' => __('', 'cryptowoo-hd-wallet-addon'),
                'desc' => '',
                'title' => sprintf(__('%sprefix%s', 'cryptowoo-hd-wallet-addon'), '<b>Litecoin "Ltub..." ', '</b>'),
                'validate_callback' => 'redux_validate_mpk',
                'placeholder' => 'Ltub...', // BIP32.org format
                'text_hint' => $pubkey_hint
            ),
            array(
                'id' => 'cryptowoo_ltc_mpk_xpub',
                'type' => 'text',
                'ajax_save' => false,
                'username' => false,
                'title' => sprintf(__('%sprefix%s', 'cryptowoo-hd-wallet-addon'), '<b>Litecoin "xpub..." ', '</b>'),
                'desc' => sprintf(__('Remove this key to use the %s prefix format.', 'cryptowoo-hd-wallet-addon'), 'Ltub'),
                'validate_callback' => 'redux_validate_mpk',
                'placeholder' =>  'xpub...', // electrum-ltc format
                'text_hint' => $pubkey_hint
            ),
	        array(
		        'id'         => 'derivation_path_ltc',
		        'type'       => 'select',
		        'subtitle'   => '',
		        'title'             => sprintf( __( '%s Derivation Path', 'cryptowoo-hd-wallet-addon' ), 'LTC' ),
		        'desc'              => __('Change the derivation path to match the derivation path of your wallet client.', 'cryptowoo-hd-wallet-addon'),
		        'validate_callback' => 'redux_validate_derivation_path',
		        'options'    => array(
			        '0/' => __('m/0/i (e.g. Electrum Standard Wallet)', 'cryptowoo-hd-wallet-addon'),
			        'm' => __('m/i (BIP44 Account)', 'cryptowoo-hd-wallet-addon'),
		        ),
		        'default'    => '0/',
		        'select2'    => array( 'allowClear' => false )
	        ),
            array(
                'id' => 'section-end',
                'type' => 'section',
                'indent' => false,
            ),
            array(
                'id' => 'wallets-hdwallet-blackcoin',
                'type' => 'section',
                'title' => __('BlackCoin', 'cryptowoo-hd-wallet-addon'),
                //'required' => array('testmode_enabled','equals','0'),
                'icon' => 'cc BLK-alt',
                'subtitle' => sprintf(__('Use the field with the correct prefix of your %s extended public key. The prefix depends on the wallet client you used to generate the key.', 'cryptowoo-hd-wallet-addon'), 'BlackCoin'),
                'indent' => true,
            ),
            array(
                'id' => 'cryptowoo_blk_mpk',
                'type' => 'text',
                'ajax_save' => false,
                'username' => false,
                //'subtitle' => __('', 'cryptowoo-hd-wallet-addon'),
                'title' => sprintf(__('%sprefix%s', 'cryptowoo-hd-wallet-addon'), '<b>BlackCoin "bcpb..." ', '</b>'),
                'desc' => '',
                //'required' => array('cryptowoo_blk_mpk_xpub', 'equals', ''),
                'validate_callback' => 'redux_validate_mpk',
                'placeholder' => 'bcpb...',
                'text_hint' => $pubkey_hint
            ),
            array(
                'id' => 'cryptowoo_blk_mpk_xpub',
                'type' => 'text',
                'ajax_save' => false,
                'username' => false,
                'title' => sprintf(__('%sprefix%s', 'cryptowoo-hd-wallet-addon'), '<b>BlackCoin "xpub..." ', '</b>'),
                'desc' => sprintf(__('Remove this key to use the %s prefix format.', 'cryptowoo-hd-wallet-addon'), 'bcpb'),
                'validate_callback' => 'redux_validate_mpk',
                //'required' => array('cryptowoo_blk_mpk', 'equals', ''),
                'placeholder' =>  'xpub...', // xpub format
                'text_hint' => $pubkey_hint
            ),
	        array(
		        'id'         => 'derivation_path_blk',
		        'type'       => 'select',
		        'subtitle'   => '',
		        'title'             => sprintf( __( '%s Derivation Path', 'cryptowoo-hd-wallet-addon' ), 'BLK' ),
		        'desc'              => __('Change the derivation path to match the derivation path of your wallet client.', 'cryptowoo-hd-wallet-addon'),
		        'validate_callback' => 'redux_validate_derivation_path',
		        'options'    => array(
			        '0/' => __('m/0/i (e.g. Electrum Standard Wallet)', 'cryptowoo-hd-wallet-addon'),
			        'm' => __('m/i (BIP44 Account)', 'cryptowoo-hd-wallet-addon'),
		        ),
		        'default'    => '0/',
		        'select2'    => array( 'allowClear' => false )
	        ),
            array(
                'id' => 'section-end',
                'type' => 'section',
                'indent' => false,
            ),
            array(
                'id' => 'wallets-hdwallet-testnet',
                'type' => 'section',
                'title' => __('TESTNET', 'cryptowoo-hd-wallet-addon'),
                //'required' => array('testmode_enabled','equals','0'),
                'icon' => 'fa fa-flask',
                'desc' => __('Accept BTC testnet coins to addresses created via a "tpub..." extended public key. (testing purposes only!)<br><b>Depending on the position of the first unused address, it could take a while until your changes are saved.</b>', 'cryptowoo-hd-wallet-addon'),
                'indent' => true,
            ),
            array(
                'id' => 'cryptowoo_btc_test_mpk',
                'type' => 'text',
                'ajax_save' => false,
                'username' => false,
                'desc' => __('Bitcoin TESTNET extended public key (tpub...)', 'cryptowoo-hd-wallet-addon'),
                'title' => __('Bitcoin TESTNET HD Wallet Extended Public Key', 'cryptowoo-hd-wallet-addon'),
                'validate_callback' => 'redux_validate_mpk',
                'placeholder' => 'tpub...',
                'text_hint' => $pubkey_hint
            ),
	        array(
		        'id'         => 'derivation_path_btctest',
		        'type'       => 'select',
		        'subtitle'   => '',
		        'title'             => sprintf( __( '%s Derivation Path', 'cryptowoo-hd-wallet-addon' ), 'BTCTEST' ),
		        'desc'              => __('Change the derivation path to match the derivation path of your wallet client.', 'cryptowoo-hd-wallet-addon'),
		        'validate_callback' => 'redux_validate_derivation_path',
		        'options'    => array(
			        '0/' => __('m/0/i (e.g. Electrum Standard Wallet)', 'cryptowoo-hd-wallet-addon'),
			        'm' => __('m/i (BIP44 Account)', 'cryptowoo-hd-wallet-addon'),
		        ),
		        'default'    => '0/',
		        'select2'    => array( 'allowClear' => false )
	        ),
            /*
        array(
            'id'       => 'cryptowoo_doge_test_mpk',  @todo Uncomment when DOGETEST HD wallet is supported
            'type'     => 'text',
            'username' => false,
            'desc'       => __('<b>Dogecoin TESTNET BIP44 wallet extended public key</b>', 'cryptowoo-hd-wallet-addon'),
            'title'    => 'Dogecoin TESTNET HD Wallet Extended Public Key',
            'validate_callback' => 'redux_validate_mpk',
            'required' => array('testmode_enabled','equals','0'),
            'placeholder' => 'tgub...',
        ),
        */
            array(
                'id' => 'section-end',
                'type' => 'section',
                'indent' => false,
            ),

        )
    ));
}
