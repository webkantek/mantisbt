# CryptoWoo HD Wallet Add-on #

This add-on for the [CryptoWoo](http://www.cryptowoo.com/) plugin enables support for extended public keys from hierarchical deterministic ("[HD](https://bitcoin.org/en/glossary/hd-protocol)") which allows you to receive payments directly to addresses under your control without relying on any third party service.  

### Summary ###

* Uses [BitWasp's bitcoin-php](https://github.com/Bit-Wasp/bitcoin-php)

Supported master public key prefixes:

  * xpub (Bitcoin, Litecoin, Dogecoin, Blackcoin)  
  * dgub (Dogecoin)  
  * Ltub (Litecoin)  
  * bcpb (BlackCoin)  
  * tpub (Testnet Bitcoin)  
  
### Dependencies ###

* WordPress  
* WooCommerce  
* CryptoWoo  


### Who do I talk to? ###

* felix@cryptowoo.com