<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

require_once(WP_PLUGIN_DIR . '/cryptowoo/vendor/autoload.php');
require_once (WP_PLUGIN_DIR . '/cryptowoo/includes/processing/class.blockcypher.php');
use BitWasp\Bitcoin\Network\NetworkFactory;
use BitWasp\Bitcoin\Key\Deterministic\HierarchicalKeyFactory;

class CW_HDwallet
{
    /**
     * Create a new payment address from master public key.
     * If $path is not specified return the address on the saved index.
     *
     * @param $currency
     * @param bool $options
     * @param bool $path
     * @param int $retry
     * @return mixed
     * @throws Exception
     */
    public static function create_address_from_mpk($currency, $options = false, $path = false, $retry = 1) {

        if (!$options) {
            $options = get_option('cryptowoo_payments');
        }

        $return = array('status' => true);

        $mpk_data      = self::get_mpk_data($currency, $options);
        $return['mpk'] = $mpk = isset($mpk_data->mpk) ? $mpk_data->mpk : false;

        // Don't use saved index data if a path is specified
        if ($path === false) {
            $index     = isset($mpk_data->index) ? (int)$mpk_data->index : false;
            $index_key = $mpk_data->index_key;
        } else {
            $index = 0;
        }

        if (!$mpk) {
            $status['message'] = sprintf(__('%s master public key not found', 'cryptowoo-hd-wallet-addon'), $currency);
            $return['status']  = false;
            return $status;
        }

        if ($index === false) {
            $status['message'] = sprintf(__('% HD Address creation error', 'cryptowoo-hd-wallet-addon'), $currency);
            $return['status']  = false;
            return $status;
        }

        if ($path !== false) {
            $def = str_replace("m/", '', $path);
        } else {
            $def = self::get_path_level($currency, $options) . $index;
        }

        // Derive address
        $address = self::derive_address($mpk, $mpk_data, $def);

        // Offline validate the newly created address
        if ($address) {
            $validate      = new CW_Validate();
            $address_valid = $validate->offline_validate_address($address, $currency);
        } else {
            $address_valid = false;
        }

        if (!$address_valid) {
            $return['message'] = __('Address invalid', 'cryptowoo-hd-wallet-addon');
            $return['status']  = false;
        } /* elseif($currency === 'BLK') {
            // The BlackCoin address is valid - check if it has been used before
            if(CW_CryptoID::get_address_first_seen($currency, $address, $options['cryptoid_api_key'])) {
                $return['message'] = __('Address re-use detected. Please re-run the address discovery or use a fresh key.', 'cryptowoo-hd-wallet-addon');
                $return['status']  = false;
                $do_retry = true;
                $retry = isset($retry) ? $retry + 1 : 1;
            }
        } */
        if (!$return['status']) {
            file_put_contents(CW_LOG_DIR . 'cryptowoo-error.log', date("Y-m-d H:i:s") . ' Create address from mpk error: ' . $address . ' Status: ' . var_export($return['message'], true) . "\r\n", FILE_APPEND);
        }

        $return['address'] = $address;

        // Only display current index if path has not been specified
        if ($path === false) {
            $return['mpk_key_index'] = (int)$index + 1; // (int)$hk->derivePath($def)->getSequence() + 1;
        }
        $return['mpk_key_position'] = "m/{$def}";

        // Only increase address index if no path has been specified when calling the function
        if (isset($index_key) && $path === false) {
            update_option($index_key, $return['mpk_key_index']);
        }

        /*if(isset($do_retry) && $retry <= 3) {
            // Max 3 retries
            $data = var_export($return, true);
            CW_AdminMain::cryptowoo_log_data(0, 'create_address_from_mpk', "{$currency} Address reuse detected|{$address}|{$data}", 'cryptowoo-tx-update.log');
            $return = self::create_address_from_mpk($currency, $options, $path, $retry+1);
        }*/
        return $return;

    }


    /**
     * Return a specified number of payment addresses. Optionally specify an mpk and an index.
     *
     * $mpk_data = array(
     *      'mpk' => array('xpub...', 'M'),
     *      'index' => 0
     * );
     *
     * @param $currency
     * @param int $count
     * @param bool|false $mpk_data
     * @return array
     */
    public static function return_addresses_from_mpk($currency, $count = 10, $mpk_data = false) {

        $options = get_option('cryptowoo_payments');

        if (false === $mpk_data) {
            // If no mpk data is specified
            $mpk_data = self::get_mpk_data($currency, $options);
            $mpk      = isset($mpk_data->mpk) ? $mpk_data->mpk : false;
            $index    = isset($mpk_data->index) ? (int)$mpk_data->index : 0;
        } else {
            $mpk   = isset($mpk_data->mpk) ? $mpk_data->mpk : false;
            $index = isset($mpk_data->index) ? (int)$mpk_data->index : false;
        }

        if (false === $mpk) {
            $status['mpk']    = sprintf(__('%s master public key not found', 'cryptowoo-hd-wallet-addon'), $currency);
            $return['status'] = false;
            return $status;
        }

        if (false === $index) {
            $status['address_index'] = sprintf(__('%s HD Address creation error. Index not specified.', 'cryptowoo-hd-wallet-addon'), $currency);
            $return['status']        = false;
            return $status;
        }

        $addresses         = array();
        $addresses['info'] = "Generating {$count} {$currency} address(es) starting from #{$index}. Key: {$mpk_data->mpk}\r\n";
        $i                 = 0;
        do {

            $def     = self::get_path_level($currency, $options) . $index;
            $hk      = HierarchicalKeyFactory::fromExtended($mpk, $mpk_data->network);
            $addresses[$def] = $hk->derivePath($def)->getPublicKey()->getAddress()->getAddress($mpk_data->network);
            $index++;
            $i++;
        } while ((int)$i < (int)$count);
        //}while((int)$start < ($index -1)); // return all addresses before the current index
        //}while((int)$start < $index + $count); // return all addresses before the current index and a specified number after
        return $addresses;
    }

    /**
     * HD account discovery
     *
     *
     * When the master seed is imported from an external source the software should start to discover the accounts in the following manner:
     *
     *  - derive the first account's node (index = 0)
     *  - derive the external chain node of this account
     *  - scan addresses of the external chain; respect the gap limit described below
     *  - if no transactions are found on the external chain, stop discovery
     *  - if there are some transactions, increase the account index and go to step 1
     *
     *  This algorithm is successful because software should disallow creation of new accounts if previous one has no transaction history, as described in chapter "Account" above.
     *  Please note that the algorithm works with the transaction history, not account balances, so you can have an account with 0 total coins and the algorithm will still continue with discovery.
     *
     * Address gap limit
     *
     * Address gap limit is currently set to 20. If the software hits 20 unused addresses in a row, it expects there are no used addresses beyond this point and stops searching the address chain.
     * We scan just the external chains, because internal chains receive only coins that come from the associated external chains.
     *
     * @todo Wallet software should warn when the user is trying to exceed the gap limit on an external chain by generating a new address.
     *
     * Source: https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki#account-discovery
     *
     * @param $currency
     * @param bool $mpk_specified
     * @return array
     */
    public static function get_address_details_from_mpk($currency, $mpk_specified = false) {

        $start_time = time();

        // Try to increase execution time
        //$set_max_execution_time = ini_set('max_execution_time', 60);
        $time_limit = ($max_exec_time = ini_get('max_execution_time')) > 5 ? (int)$max_exec_time - 2 : 30;//!(bool)$set_max_execution_time ?  ini_get('max_execution_time') : $set_max_execution_time;

        $options = get_option('cryptowoo_payments');

        // Prepare Blockcypher API
        $api_context   = CW_Blockcypher::prepare_blockcypher_api($currency, $options); //self::DELprepare_blockcypher_api($currency, $options);
        $blockcypher_currencies = apply_filters('cw_blockcypher_currencies', array('BTC', 'DOGE', 'LTC', 'BTCTEST')); // hackity hack TODO proper selection of API to find out if the address has been used before

        $error     = false;
        $addresses = array();
        //$addresses['mpk_info']['currency'] = $currency;

        $mpk_data = self::get_mpk_data($currency, $options);

        if (!isset($mpk_data->mpk) && $mpk_specified === false) {
            $status['mpk'] = sprintf(__('%s master public key not found', 'cryptowoo-hd-wallet-addon'), $currency);
            return $status;
        }

        $mpk = (bool)$mpk_specified ? $mpk_specified : $mpk_data->mpk;

        // Prepare discovery object
        $discovery_info = new stdClass();

        // Set index key to update the final discovery result
        $discovery_info->index_key = $mpk_data->index_key;

        // Electrum derivation path: m/0/i
        $def = self::get_path_level($currency, $options); //"0/";

        // Maybe we were interrupted last time. Check transient to find out where to continue.
        $transient_name = sprintf('%s_hd_wallet_discovery_notice', strtolower($currency));
        $transient = get_transient($transient_name);

        if (false !== $transient && isset($transient->message) && $transient->message !== 'success' && isset($transient->next_index)) {
            $addresses['mpk_info']['resuming_process'] = sprintf(__('Resumed from previous discovery at address m/0/%d', 'cryptowoo-hd-wallet-addon'), $transient->next_index);
            $discovery_info                            = $transient;
        } else {
            $discovery_info->gap = $discovery_info->wallet_received = $discovery_info->wallet_balance = $discovery_info->next_index = $discovery_info->last_used_index = 0;
        }
        $discovery_info->error = false;
        $gap_limit = 20;
        $success   = $stop = $null_has_balance = false;
        do {

            // Create address
            $derivation_path                     = $def . $discovery_info->next_index;
            //$hk                                  = HierarchicalKeyFactory::fromExtended($mpk, $mpk_data->network);
            //$address                             = $hk->derivePath($derivation_path)->getPublicKey()->getAddress()->getAddress($mpk_data->network);
            $address = self::derive_address($mpk, $mpk_data, $derivation_path);
            $key_nicename                        = "m/{$derivation_path}";
            $addresses[$key_nicename]['def']     = $key_nicename;
            $addresses[$key_nicename]['address'] = $address;

            try {
                if(in_array($currency, $blockcypher_currencies)) {  // TODO remove hackity hack
                    $addressClient = new BlockCypher\Client\AddressClient($api_context);
                    $received      = $addressClient->getBalance($address);
	                // TODO add block explorer alternative
                } else {
                    // Currency not supported by BlockCypher
                    $ex = new Exception(sprintf('Error: %s address discovery is not supported yet. Using address at index 0.%sPlease use a freshly generated key to avoid address reuse.', $currency, '<br>'));
                    $discovery_info->error = true;
                    update_option($discovery_info->index_key, 0);
                    throw $ex; // TODO enable BlackCoin address discovery via custom API
                }
            } catch (Exception $ex) {
                $error['message'] = $ex->getMessage();
                $received = $addresses[$key_nicename]['received'] = sprintf('API Error: %s', $error['message']);
                $discovery_info->error = true;
            }
            // Stop API requests on error
            $stop = $err = isset($error['message']);

            if (isset($received->final_n_tx) && $received->final_n_tx > 0) {

                $addresses[$key_nicename]['received'] = isset($received->total_received) ? CW_Formatting::fbits($received->total_received) : 0;
                $discovery_info->wallet_received += isset($received->total_received) ? $received->total_received : 0;
                $discovery_info->wallet_balance += isset($received->final_balance) ? $received->final_balance : 0;

                // Reset unused address counter
                $discovery_info->gap = 0;

                // Set last index position with a balance
                $last_index                      = $key_nicename . ' | ' . $address;
                $discovery_info->last_used_index = $discovery_info->next_index;
                $null_has_balance = true;

            } else {

                $addresses[$key_nicename]['received'] = 'unused';
                // Count unused address
                $discovery_info->gap++;

            }

            // Increase index
            $discovery_info->next_index++;

            // Check gap of unused addresses
            $stop = $stop === false && $discovery_info->gap <= $gap_limit ? false : true;

            // Discovery finished successfully?
            if ($stop) {
                // Set transient, keep for 1.5 minutes
                $msg             = isset($error['message']) ? $error['message'] : var_export($received, true);
                $status          = new stdClass();
                $status->message = $err === true ? $msg : 'success';
                set_transient($transient_name, $status, 90);
                $current_index = 0 == $discovery_info->last_used_index && !$null_has_balance ? 0 : (int)$discovery_info->last_used_index + 1;
                if($status->message === 'success') {
                    update_option($discovery_info->index_key, $current_index);
                }
                $success = true;
            }

            // Check if we run out of time
            $current_runtime = time() - $start_time;
            if ($current_runtime >= $time_limit) {
                $discovery_info->error = $err === true;
                self::set_hd_wallet_discovery_notice($currency, $discovery_info, false);
                $success = false;
                // Stop loop
                $stop = true;
            }

            usleep(500000); // 2 requests/second
        } while (!$stop); // return details for addresses within $gap_limit

        // Additional info
        $addresses['mpk_info']['wallet_received']      = CW_Formatting::fbits($discovery_info->wallet_received);
        $addresses['mpk_info']['total_balance_of_mpk'] = CW_Formatting::fbits($discovery_info->wallet_balance);
        $addresses['mpk_info']['last_checked_index']   = $key_nicename;

        // Get next unused address
        $derivation_path                       = $def . (0 == $discovery_info->last_used_index && !$null_has_balance ? 0 : (int)$discovery_info->last_used_index + 1);

        $addresses['mpk_info']['next_address'] = sprintf('m/%s | %s', $derivation_path, self::derive_address($mpk, $mpk_data, $derivation_path));

        // Gap limit and duration info
        $addresses['mpk_info']['current_gap_limit'] = $gap_limit;
        $addresses['mpk_info']['duration']          = time() - $start_time;

        // If the discovery has finished successfully
        $addresses['status'] = $success ? 'success' : false;

        return $addresses;
    }

    public static function set_hd_wallet_discovery_notice($currency, $discovery_info, $return = false) {


        if(!isset($discovery_info->message)) {
            $discovery_info->message = sprintf(__('%s%s HD Wallet Address discovery is incomplete!%s', 'cryptowoo-hd-wallet-addon'), '<br><span style="color:#B94A48; font-weight:bold;">', $currency, '</span>');
        }
        if ($return) {
            $discovery_info->message .= sprintf(__('%sIf addresses of this key have not received any transactions you can click the "Skip %s Address Discovery" button to set the index to the first address at m/0/0.', 'cryptowoo-hd-wallet-addon'), '<br>', $currency);
            $discovery_info->message .= sprintf(__('%sIf you are unsure, click the "Discover %s Addresses" button to detect the next unused address for this key.', 'cryptowoo-hd-wallet-addon'), '<br>', $currency);
        } else {
            // We have only 2 seconds left before the script is killed - save current address discovery state
            $discovery_info->message = sprintf(__('%sWe reached the maximum execution time of your PHP environment. Click the "Discover %s Addresses" button again to continue.%s', 'cryptowoo-hd-wallet-addon'), '<br>', $currency, '<br>');
        }

        // Set transient, keep for 30 minutes
        $transient_name = sprintf('%s_hd_wallet_discovery_notice', strtolower($currency));
        set_transient($transient_name, $discovery_info, 1800);

        $discovery_info->message = sprintf( '<div id="inline-discovery-notice-%s">%s</div>', strtolower( $currency ), $discovery_info->message );

	    return $return ? $discovery_info : true;
    }

    public static function get_mpk_data($currency, $options = false) {

        if (!$options) {
            $options = get_option('cryptowoo_payments');
        }

        // Option identifiers
        $key_id    = strpos($currency, 'TEST') ? strtolower(str_replace('TEST', '_test', $currency)) : strtolower($currency);
        $index_key = "cryptowoo_{$key_id}_index";

        $mpk_data = new stdClass();

        switch ($currency) {
            case 'BTC':
                $mpk_data->network = NetworkFactory::bitcoin();
                $mpk_key   = "cryptowoo_{$key_id}_mpk";
                break;
            case 'BTCTEST':
                $mpk_data->network = NetworkFactory::bitcoinTestnet();
                $mpk_key   = "cryptowoo_{$key_id}_mpk";
                break;
            case 'DOGE':
                if(isset($options['cryptowoo_doge_mpk_xpub']) && $options['cryptowoo_doge_mpk_xpub'] !== '') {
                    $mpk_data->network = NetworkFactory:: create('1e', '16', '9e')->setHDPubByte('0488b21e')->setHDPrivByte('02fac398')->setNetMagicBytes('c0c0c0c0');
                    $mpk_key = "cryptowoo_{$key_id}_mpk_xpub";
                } else {
	                $mpk_data->network = NetworkFactory::create('1e', '16', '9e')->setHDPubByte('02facafd')->setHDPrivByte('02fac398')->setNetMagicBytes('c0c0c0c0');
                    $mpk_key           = "cryptowoo_{$key_id}_mpk";
                }
                break;
            case 'DOGETEST':
                $mpk_data->network = NetworkFactory::dogecoinTestnet();
                $mpk_key   = "cryptowoo_{$key_id}_mpk";
                break;
            case 'LTC':
                if(isset($options['cryptowoo_ltc_mpk_xpub']) && $options['cryptowoo_ltc_mpk_xpub'] !== '') {
                    $mpk_data->network = NetworkFactory::create('30', '05', 'b0')->setHDPubByte('0488b21e')->setHDPrivByte('0488ade4')->setNetMagicBytes('dbb6c0fb');
                    $mpk_key   = "cryptowoo_{$key_id}_mpk_xpub";
                } else {
                    $mpk_data->network = NetworkFactory::litecoin();
                    $mpk_key   = "cryptowoo_{$key_id}_mpk";
                }
                break;
            case 'BLK':
                if(isset($options['cryptowoo_blk_mpk_xpub']) && $options['cryptowoo_blk_mpk_xpub'] !== '') {
                    $mpk_data->network = NetworkFactory::create('19', '55', '99')->setHDPubByte('0488b21e')->setHDPrivByte('02cfbf60')->setNetMagicBytes('d9b4bef9');
                    $mpk_key   = "cryptowoo_{$key_id}_mpk_xpub";
                } else {
                    $mpk_data->network = NetworkFactory::create('19', '55', '99')->setHDPubByte('02cfbede')->setHDPrivByte('02cfbf60')->setNetMagicBytes('d9b4bef9');
                    $mpk_key   = "cryptowoo_{$key_id}_mpk";
                }
                break;
            default:
	            $mpk_data->network = false;
	            $mpk_key   = '';

        }

        $mpk_data->derivation_path = CW_HDwallet::get_path_level($currency, $options);

	    $mpk_data = apply_filters('get_mpk_data_network', $mpk_data, $currency, $options);
	    $mpk_key = apply_filters('get_mpk_data_mpk_key', $mpk_key, $currency, $options);

	    if(!$mpk_data->network || !$mpk_key) {
		    throw new \Exception( "Unsupported coin symbol: {$currency}" );
	    }

        // MPK and associated derivation info
        $mpk_data->index_key = $index_key;

        $mpk_data->mpk = isset($options[$mpk_key]) && $options[$mpk_key] !== '' ? $options[$mpk_key] : false;

        // Maybe create array of extended public keys for multisig addresses // TODO refactor
        if(isset($options['use_multisig_btc']) && (bool)$options['use_multisig_btc'] && $currency === 'BTC') {
	        $mpk_2 = sprintf( '%s_2', $mpk_key );
	        $mpk_3 = sprintf( '%s_3', $mpk_key );
	        $mpk_4 = sprintf( '%s_4', $mpk_key );
	        $mpk_5 = sprintf( '%s_5', $mpk_key );

	        // Create single signature address in the first cosigner's wallet if not all cosigner keys are set
	        $cosigner_num_key = sprintf('%s_multisig_scheme_n', strtolower($currency));
	        $cosigner_num = isset($options[$cosigner_num_key]) ? $options[$cosigner_num_key] : 1;

	        $sig_num_key = sprintf('%s_multisig_scheme_m', strtolower($currency));

	        if ( $mpk_data->mpk && $options[$cosigner_num_key] > 1) {
		        switch ( (int)$cosigner_num ) {
			        default:
			        case 3:
				        if ( isset( $options[ $mpk_2 ] ) && ! empty( $options[ $mpk_2 ] ) && isset( $options[ $mpk_3 ] ) && ! empty( $options[ $mpk_3 ] ) ) {
					        $mpk_data->mpk = array(
						        0 => $mpk_data->mpk,
						        1 => $options[ $mpk_2 ],
						        2 => $options[ $mpk_3 ]
					        );
				        }
				        break;
			        case 4:
				        if ( isset( $options[ $mpk_2 ] ) && ! empty( $options[ $mpk_2 ] ) && isset( $options[ $mpk_3 ] ) && ! empty( $options[ $mpk_3 ] ) && isset( $options[ $mpk_4 ] ) && ! empty( $options[ $mpk_4 ] ) ) {
					        $mpk_data->mpk = array(
						        0 => $mpk_data->mpk,
						        1 => $options[ $mpk_2 ],
						        2 => $options[ $mpk_3 ],
						        3 => $options[ $mpk_4 ]
					        );
				        }
				        break;
			        case 5:
				        if ( isset( $options[ $mpk_2 ] ) && ! empty( $options[ $mpk_2 ] ) && isset( $options[ $mpk_3 ] ) && ! empty( $options[ $mpk_3 ] ) && isset( $options[ $mpk_4 ] ) && ! empty( $options[ $mpk_4 ] ) && isset( $options[ $mpk_5 ] ) && ! empty( $options[ $mpk_5 ] ) ) {
					        $mpk_data->mpk = array(
						        0 => $mpk_data->mpk,
						        1 => $options[ $mpk_2 ],
						        2 => $options[ $mpk_3 ],
						        3 => $options[ $mpk_4 ],
						        4 => $options[ $mpk_5 ]
					        );
				        }
				        break;
		        }
	        }
	        $mpk_count = count($mpk_data->mpk);
	        $mpk_data->signatures_required = isset( $options[ $sig_num_key ] ) && (int)$options[ $sig_num_key ] <= $mpk_count ? (int)$options[ $sig_num_key ] : 1;
        }

        $index               = get_option($index_key);
        $mpk_data->index     = false !== $index ? $index : 0;

        return $mpk_data;
    }

    /**
     * Get HD wallet path level according to BIP44 path derivation
     *
     * @todo Remove hardcoded Electrum derivation path and add support for custom derivation paths
     *
     * We define the following 5 levels in BIP32 path:
     *
     *   m / purpose' / coin_type' / account' / change / address_index
     *
     * Apostrophe in the path indicates that BIP32 hardened derivation is used.
     *
     * Source: https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki#path-levels
     * Path levels for registered coin types: http://doc.satoshilabs.com/slips/slip-0044.html
     *
     * @param $currency
     * @param $options
     * @return string
     */
    public static function get_path_level($currency, $options) {
    	if(!is_array($options)) {
    		$options = get_option('cryptowoo_payments');
	    }
	    $derivation_path_key = sprintf('derivation_path_%s', strtolower($currency));
        return isset($options[$derivation_path_key]) ? str_replace('m', '', $options[$derivation_path_key]) : '0/';
    }


    /**
     * Get chain parameters for a specified currency
     *
     * @param $field
     * @return bool|stdClass
     */
    public static function get_currency_params($field) {


        $currency_params         = new stdClass();
        $currency_params->strlen = 111;

        if (false !== strpos($field['id'], 'cryptowoo_btc_mpk')) {

            // Bitcoin multisig fields cryptowoo_btc_mpk_2 and cryptowoo_btc_mpk_3 apply here, too.

            $currency_params->mand_mpk_prefix    = 'xpub';   // bip32.org & Electrum prefix
            $currency_params->mand_base58_prefix = '0488b21e'; // Bitcoin
            $currency_params->currency           = 'BTC';
            $currency_params->index_key          = 'cryptowoo_btc_index';
        } elseif (strcmp($field['id'], 'cryptowoo_btc_test_mpk') === 0) {
            $currency_params->mand_mpk_prefix    = 'tpub';   // bip32.org
            $currency_params->mand_base58_prefix = '043587cf'; // Testnet Bitcoin
            $currency_params->currency           = 'BTCTEST';
            $currency_params->index_key          = 'cryptowoo_btctest_index';
        } elseif (strcmp($field['id'], 'cryptowoo_doge_mpk') === 0) {
            $currency_params->mand_mpk_prefix    = 'dgub';    // bip32.org prefix
            $currency_params->mand_base58_prefix = '02facafd'; // Dogecoin
            $currency_params->currency           = 'DOGE';
            $currency_params->index_key          = 'cryptowoo_doge_index';
        } elseif (strcmp($field['id'], 'cryptowoo_doge_mpk_xpub') === 0) {
            $currency_params->mand_mpk_prefix    = 'xpub';    // xpub prefix
            $currency_params->mand_base58_prefix = '0488b21e'; // Dogecoin
            $currency_params->currency           = 'DOGE';
            $currency_params->index_key          = 'cryptowoo_doge_index';
        } elseif (strcmp($field['id'], 'cryptowoo_doge_test_mpk') === 0) {
            $currency_params->mand_mpk_prefix    = 'tgub';    // bip32.org prefix
            $currency_params->mand_base58_prefix = '0432a9a8'; // Testnet Dogecoin
            $currency_params->currency           = 'DOGETEST';
            $currency_params->index_key          = 'cryptowoo_doge_test_index';
        } elseif (strcmp($field['id'], 'cryptowoo_ltc_mpk') === 0) {
            $currency_params->mand_mpk_prefix    = 'Ltub';  // bip32.org mpk prefix
            $currency_params->mand_base58_prefix = '019da462'; // bip32.org base58 prefix
            $currency_params->currency           = 'LTC';
            $currency_params->index_key          = 'cryptowoo_ltc_index';
        } elseif (strcmp($field['id'], 'cryptowoo_ltc_mpk_xpub') === 0) {
            $currency_params->mand_mpk_prefix    = 'xpub';    // Electrum-LTC mpk prefix
            $currency_params->mand_base58_prefix = '0488b21e'; // Litecoin Electrum base58 prefix
            $currency_params->currency           = 'LTC';
            $currency_params->index_key          = 'cryptowoo_ltc_index';
        } elseif (strcmp($field['id'], 'cryptowoo_blk_mpk') === 0) {
            $currency_params->mand_mpk_prefix    = 'bcpb';    // mpk prefix
            $currency_params->mand_base58_prefix = '02cfbede'; // base58 prefix
            $currency_params->currency           = 'BLK';
            $currency_params->index_key          = 'cryptowoo_blk_index';
        } elseif (strcmp($field['id'], 'cryptowoo_blk_mpk_xpub') === 0) {
            $currency_params->mand_mpk_prefix    = 'xpub';    // mpk xpub prefix
            $currency_params->mand_base58_prefix = '0488b21e'; // base58 xpub prefix
            $currency_params->currency           = 'BLK';
            $currency_params->index_key          = 'cryptowoo_blk_index';
        } else {
            $currency_params = false;
        }
        return apply_filters('cw_get_currency_params', $currency_params, $field['id']);
    }

    /**
     * Render HD wallet address details
     *
     * @param $response
     * @return string
     */
    public static function render_address_details_from_mpk($response) {

        $return               = '<pre>';
        $use_nice_description = array('duration',
                                      'resuming_process',
                                      'current_gap_limit',
                                      'wallet_received',
                                      'total_balance_of_mpk',
                                      'last_checked_index',
                                      'next_address');
        $gap_limit_info       = '<a title="BIP44: Address gap limit" target="_blank" href="https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki#address-gap-limit">More Info</a>';

        if (is_array($response)) {

            $successfully_finished = $response['status'] === 'success' ? true : false;

            if (isset($response['mpk_info']['resuming_process'])) {
                $return .= "\nStatus: {$response['mpk_info']['resuming_process']}\n";
            }
            if (isset($response['mpk_info']['last_checked_index'])) {
                $return .= "\nLast checked address: {$response['mpk_info']['last_checked_index']}\n";
            }
            if (isset($response['mpk_info']['next_address']) && $successfully_finished) {
                $return .= sprintf("Next unused address: %s\n" , $response['mpk_info']['next_address']);
            }
            if (isset($response['mpk_info']['wallet_received'])) {
                $return .= "Amount received: {$response['mpk_info']['wallet_received']}\n";
            }
            if (isset($response['mpk_info']['total_balance_of_mpk'])) {
                $return .= "Balance in path: {$response['mpk_info']['total_balance_of_mpk']}\n";
            }
            if (isset($response['mpk_info']['current_gap_limit'])) {
                $return .= sprintf("\nCurrent gap limit: %s %s\n", $response['mpk_info']['current_gap_limit'], $gap_limit_info);
            }
            if (isset($response['mpk_info']['duration'])) {
                $return .= "Runtime: {$response['mpk_info']['duration']} seconds\n\n";
            }
            if (!$successfully_finished) {
                // Return the current address batch details if we did not yet finish the discovery successfully
                foreach ($response as $addresses) {
                    if (is_array($addresses)) {
                        foreach ($addresses as $key => $value) {
                            if ($key === 'def') {
                                $return .= $value;
                            } elseif ($key === 'address') {
                                $return .= " | {$value}";
                            } elseif ($key === 'received') {
                                $return .= " | {$value}\n";
                            } elseif (is_array($value) && isset($value['message'])) {
                                $return .= "\n{$value['message']}\n";
                            } elseif (!in_array($key, $use_nice_description)) {
                                $return .= "\n{$key} | {$value}\n";
                            }
                        }
                    }
                }
            }
        } else {
            $return .= "Error rendering details!";
        }
        $return .= '</pre>';

        return $return;
    }

    /**
     * Check blockcypher.com API if the address has been used before
     *
     * DOGETEST not supported!
     *
     * @param $address
     * @param $currency
     * @param bool|false $options
     * @return mixed
     */
    public static function get_address_status($address, $currency, $options = false) {

        if (!$options) {
            $options = get_option('cryptowoo_payments');
        }
        // Call Blockcypher API
        $args = array('timeout' => 5,
                      'redirection' => 5,
                      'httpversion' => '1.0',
                      'user-agent' => 'localhost',
                      'blocking' => true,
                      'headers' => array(),
                      'cookies' => array(),
                      'body' => null,
                      'compress' => false,
                      'decompress' => true,
                      'sslverify' => true,
                      'stream' => false,
                      'filename' => null);

        $network_string = $currency === 'BTCTEST' ? 'btc/test3' : strtolower($currency) . '/main';
        $url            = 'http://api.blockcypher.com/v1/' . $network_string . '/addrs/' . $address;

        $response = wp_remote_get($url, $args);

        if (is_array($response)) {
            $header = $response['headers']; // array of http header lines

            $result   = array();
            $received = json_decode($response['body']);
            $status   = isset($received->address) ? 'success' : 'Blockcypher API error: ' . var_export($header, true);
            $count    = 0;

            if ($status == 'success') {
                $count                        = (int)$received->final_n_tx;
                $final_balance                = (int)$received->final_balance; // Satoshi
                $result['received_value']     = $final_balance / 100000000;
                $result['blockcypher_status'] = 'success';
            } else {
                $result['blockcypher_status'] .= '|' . $received->error;
                $final_balance = 0;
            }
            if ($count <= 0 && $final_balance <= 0) {
                $result['address_is_usable'] = $address_is_usable = true;
            }
        } else {
            $result['blockcypher_status'] = 'API Error';
        }
        return $result;
    }

    /**
     * Return the current block height of the specified coin networks. Return all if no network is specified.
     *
     * curl https://api.blockcypher.com/v1/btc/main
     *
     *    {
     * "name": "BTC.main",
     *    "height": 360060,
     *    "hash": "000000000000000000bf56ff4a81e399374a68344a64d6681039412de78366b8",
     *    "time": "2015-06-08T22:57:08.260165627Z",
     *    "latest_url": "https://api.blockcypher.com/v1/btc/main/blocks/000000000000000000bf56ff4a81e399374a68344a64d6681039412de78366b8",
     *    "previous_hash": "000000000000000011c9511ae1265d34d3c16fff6e8f94380425833b3d0ae5d8",
     *    "previous_url": "https://api.blockcypher.com/v1/btc/main/blocks/000000000000000011c9511ae1265d34d3c16fff6e8f94380425833b3d0ae5d8",
     *    "peer_count": 239,
     *    "unconfirmed_count": 617,
     *    "high_fee_per_kb": 46086,
     *    "medium_fee_per_kb": 29422,
     *    "low_fee_per_kb": 12045,
     *    "last_fork_height": 359865,
     *    "last_fork_hash": "00000000000000000aa6462fd9faf94712ce1b5a944dc666f491101c996beab9"
     *    }
     *
     * @param string $method
     * @param bool|false $networks
     * @param bool $options
     * @return array
     */
    public static function get_block_height($method = 'blockcypher', $networks = false, $options = false) {

        if (!$options) {
            $options = get_option('cryptowoo_payments');
        }

        if (!$networks) {
            $networks = array('BTC', 'DOGE', 'LTC', 'BTCTEST'); // DOGETEST not supported!
        }
        /*
        if($method === 'blockcypher') {

        // Prepare url
        $network_string = $currency === 'BTCTEST' ? 'btc/test3' : strtolower($currency).'/main';
        $url =  'http://api.blockcypher.com/v1/'.$network_string;
        } */
        $url    = true;
        $result = array();
        if (isset($url)) {
            foreach ($networks as $network) {

                // Prepare url
                if (strcmp($network, 'BTCTEST') === 0) {
                    $network_string = 'btc/test3';
                } else {
                    $network_string = strtolower($network) . '/main';
                }
                $url = 'http://api.blockcypher.com/v1/' . $network_string;

                $curl_response_object = CW_ExchangeRates::curl_it($url, false);
                usleep(100000); // Limit to 10 API calls/second
                if ($curl_response_object) {

                    $result[$network]['height']            = (int)$curl_response_object->height;
                    $result[$network]['time']              = (string)$curl_response_object->time;
                    $result[$network]['unconfirmed_count'] = (int)$curl_response_object->unconfirmed_count;
                    $result[$network]['high_fee_per_kb']   = (int)$curl_response_object->high_fee_per_kb;
                    $result[$network]['medium_fee_per_kb'] = (int)$curl_response_object->medium_fee_per_kb;
                    $result[$network]['low_fee_per_kb']    = (int)$curl_response_object->low_fee_per_kb;
                    $result[$network]['last_fork_height']  = (int)$curl_response_object->last_fork_height;

                    $options['blockchain_data'] = $result;
                    update_option('cryptowoo_payments', $options);
                }
            }
        }
        return $result;
    }

    static function save_hd_address($address, $mpk, $wallet_index) {
        global $wpdb;
        $origin_id = 'mpk.' . md5($mpk);

        return $wpdb->insert($wpdb->prefix . "cryptowoo_hd_addresses", array('address' => $address,    // string
                                                                             'created' => time(),    // int
                                                                             'origin_id' => $origin_id, // string
                                                                             'wallet_index' => $wallet_index // int
        ), array('%s',    // value1
                 '%d',    // value2
                 '%s',    // value3
                 '%d'    // value4
                             ));
    }

    /**
     * Derive a single-signature address from a single xpub or a multi-signature address from an array of xpub keys
     *
     * @param $mpk
     * @param $mpk_data
     * @param $def
     * @return string
     */
    static function derive_address($mpk, $mpk_data, $def) {
	    if ( ! is_array( $mpk_data->mpk ) ) {
		    // Single-signature address
		    $hk     = HierarchicalKeyFactory::fromExtended( $mpk, $mpk_data->network );
		    $pubkey = $hk->derivePath( $def )->getPublicKey();
	    } else {
		    // Multi-signature address
		    $signers = $mpk_data->mpk;

		    // Prepare extended public keys
		    $k     = array();
		    $count = 0;
		    foreach ( $signers as $i => $xpub ) {
			    if ( ! empty( $xpub ) ) {
				    $k[ $i ] = \BitWasp\Bitcoin\Key\Deterministic\HierarchicalKeyFactory::fromExtended( $xpub, $mpk_data->network );
				    $count ++;
			    }
		    }
		    // Create m-of-n multi-sig wallet
		    $m = $mpk_data->signatures_required;

		    $ec = \BitWasp\Bitcoin\Bitcoin::getEcAdapter();

		    $sequences = new \BitWasp\Bitcoin\Key\Deterministic\HierarchicalKeySequence($ec->getMath());
		    $hd        = new \BitWasp\Bitcoin\Key\Deterministic\MultisigHD( $m, 'm', $k, $sequences, true );

		    // Derive pubkey in path
		    $pubkey = $hd->derivePath( $def );
	    }
		// Return address
	    return $pubkey->getAddress()->getAddress( $mpk_data->network );
    }

    static function dump_addresses($i, $count, $mpk_data, $return = false) {
	    $limiter = 0;
	    $addresses = '';
	    do{
		    $path = sprintf('%s%d', str_replace('m', '', $mpk_data->derivation_path), $i);
		    $addresses .= sprintf("m/%s\t%s\n",$path,  self::derive_address($mpk_data->mpk, $mpk_data, $path));
		    $i++;
		    $limiter++;
	    } while($limiter <= $count);

	    if(!$return) {
		    printf( '<br>%s<pre>%s</pre>', __( 'Please compare the addresses below with those displayed in your wallet client to make sure the derivation path is correct:', 'cryptowoo' ), $addresses );
	    } else {
	    	return sprintf( '<br>%s<pre>%s</pre>', __( 'Please compare the addresses below with those displayed in your wallet client to make sure the derivation path is correct:', 'cryptowoo' ), $addresses );
	    }

    }
}
