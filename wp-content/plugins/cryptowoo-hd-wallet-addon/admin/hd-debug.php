<?php
/**
 * HD Wallet debugging
 */


// Create a seperate admin submenu if the license manager is disabled
add_action( 'admin_menu', 'display_hd_debugging_page', 699);

function display_hd_debugging_page() {
    add_submenu_page('cryptowoo', __('HD Wallet Debugging'), __('Debug HD Wallet'), 'manage_woocommerce', 'hd-debugging', 'cw_hd_debugging_page' );
}

/**
 * Display HD Wallet Debugging page
 * TODO refactor hardcoded option keys
 */
function cw_hd_debugging_page() {
    $options = get_option('cryptowoo_payments');
    $mpks    = apply_filters('mpk_key_ids',array("BTC" => "cryptowoo_btc_mpk",
                     "BTCTEST" => "cryptowoo_btc_test_mpk",
                     "DOGE" => "cryptowoo_doge_mpk",
                     "DOGE_E" => "cryptowoo_doge_mpk_xpub",
                     //"DOGETEST" => "cryptowoo_doge_test_mpk", // @todo Uncomment when DOGETEST HD wallet is supported
                     "LTC" => "cryptowoo_ltc_mpk",
                     "LTC_E" => "cryptowoo_ltc_mpk_xpub",
                     "BLK" => "cryptowoo_blk_mpk",
                     "BLK_E" => "cryptowoo_blk_mpk_xpub",
                     ));

    // Add HD wallet info
    $index_keys = apply_filters('index_key_ids', array(
        'BTC' => 'cryptowoo_btc_index',
        'BTCTEST' => 'cryptowoo_btc_test_index',
        'DOGE' => 'cryptowoo_doge_index',
        'DOGETEST' => 'cryptowoo_doge_test_index',
        'LTC' => 'cryptowoo_ltc_index',
        'BLK' => 'cryptowoo_blk_index'
    ));

    // Prepare dropdown and HD wallet info
    $dropdown = array();
    $current_address_info = '';
    foreach ($mpks as $currency => $mpk_key) {
        if (isset($options[$mpk_key]) && $options[$mpk_key] !== '') {
            $currency = str_replace('_E', '', $currency);

	        $derivation_path = CW_HDwallet::get_path_level($currency, $options);

            // Prepare currency dropdown
            $dropdown[] = sprintf('<option value="%s">%s</option>', $currency, $currency);

            // Prepare HD wallet info
            if(false !== ($current_index = get_option($index_keys[$currency]))) {
                $raw_address = CW_HDwallet::create_address_from_mpk($currency, $options, sprintf('%s%s', $derivation_path, $current_index));
                $address     = CW_Formatting::link_to_address($currency, $raw_address['address'], $options, true);
                $current_address_info .= sprintf('<tr><td>%s</td><td>%s</td><td>%s</td></tr>', $currency, $raw_address['mpk_key_position'], $address);
            } else {
                $current_address_info .= sprintf('<tr><td></td><td>%s</td><td></td></tr>', sprintf(__('Current %s index not set', 'cryptowoo-hd-wallet-addon'), $currency));
            }
        }
    }

    // Create currency option
    $select = implode($dropdown);
    ?>
    <div class="wrap postbox cw-postbox">
    <?php
    // HD address discovery and address creation
    if (count($dropdown) < 1) {
        printf(__('%sPlease enter your extended public key in the %s"Wallet Settings" -> "HD Wallet"%s section on the %s%s%sCryptoWoo settings%s page.%s', 'cryptowoo-hd-wallet-addon'), '<div class="update-nag">', '<code>', '</code>', '<a href="', admin_url('admin.php?page=cryptowoo'), '">', '</a>', '</div></div>');
    } else { ?>
            <form id="discover-form" class="form" name="form" action="" method="post"
                  enctype="application/x-www-form-urlencoded" accept-charset="UTF-8">
                <h3><?php echo __('HD Wallet Address Discovery', 'cryptowoo-hd-wallet-addon'); ?></h3>

                <div class="content">
                    <div class="intro">
                        <p><?php echo __('Discover address details for the current master public key and save them to the database.', 'cryptowoo-hd-wallet-addon'); ?>
                            <a href="https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki#account-discovery"
                               title="BIP44 account discovery"
                               target="_blank"><?php echo __('More Info', 'cryptowoo-hd-wallet-addon'); ?></a></p>
                        <p><?php echo __('Note that the address discovery will request the transactions to the addresses in your wallet from blockcypher.com. If you have many used addresses in your wallet, you may either want to enter the current index manually or use a new extended public key to save resources.', 'cryptowoo-hd-wallet-addon') ?></p>
                    </div>
                    <div id="section0">
                        <select title="Currency" id="discover-currency" name="discover-currency" required><?php echo $select; ?></select>
                        <input id="cw-discover" name="cw-discover"
                               value="<?php echo __('Discover Addresses', 'cryptowoo-hd-wallet-addon'); ?>" type="submit"></div>
                </div>
                <div id="cw-discover-response"></div>
            </form>
        </div>
        <div class="wrap postbox cw-postbox">
                <h3><?php echo __('Set Current Index', 'cryptowoo-hd-wallet-addon'); ?></h3>

                <div class="content">
                    <div class="intro">
                        <p><?php echo __('Set the derivation path index number for the extended public key of the selected currency.', 'cryptowoo-hd-wallet-addon'); ?>
                            <a href="https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki#path-levels" title="BIP44 path levels" target="_blank"><?php echo __('More Info', 'cryptowoo-hd-wallet-addon'); ?></a></p>
                        <form id="set-index-form" class="form" name="set-index-form" action="" method="post"
                              enctype="application/x-www-form-urlencoded" accept-charset="UTF-8">
                            <div id="section0">
                                <div class="field">
                                    <input id="index" name="index" value="" placeholder="0" required type="number" min="0">
                                    <select title="Currency" id="set-index-currency" name="set-index-currency" required><?php echo $select; ?></select>
                                    <input id="cw-set-index" name="cw-set-index" value="<?php echo __('Set Index', 'cryptowoo-hd-wallet-addon'); ?>" type="submit"></div>
                            </div>
                        </form>
                        <div id="set-index-response"></div>
                        <p><?php echo __('Next payment address:', 'cryptowoo-hd-wallet-addon') ?></p>
                        <table>
                            <?php
                            if(isset($current_address_info)) {
                                echo $current_address_info;
                            } else {
                                echo __('Configuration error. Please check your HD wallet settings.', 'cryptowoo-hd-wallet-addon');
                            }
                            ?>
                        </table>
                    </div>
                </div>
        </div>
        <div class="wrap postbox cw-postbox">
            <form id="address-form" class="form" name="form" action="" method="post"
                  enctype="application/x-www-form-urlencoded" accept-charset="UTF-8">
                <h3><?php echo __('Derive Address in Path', 'cryptowoo-hd-wallet-addon'); ?></h3>

                <div class="content">
                    <div class="intro">
                        <p><?php echo __('Derive the address in the specified path of the wallet for the selected currency.', 'cryptowoo-hd-wallet-addon'); ?>
                            <a
                                href="https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki#path-levels"
                                title="BIP44 path levels" target="_blank"><?php echo __('More Info', 'cryptowoo-hd-wallet-addon'); ?></a>
                        </p>
                    </div>
                    <div id="section0">
                        <div class="field">
                            <input id="path" name="path" value="m/0/0" placeholder="m/0/i" required type="text">
                            <select title="Currency" id="address-currency" name="address-currency" required><?php echo $select; ?></select>
                            <input id="create-address" name="create" value="<?php echo __('Create Address', 'cryptowoo-hd-wallet-addon'); ?>" type="submit"></div>
                    </div>
                    <div id="hdwallet-result"></div>
                </div>
            </form>
        </div>
        <div class="wrap postbox cw-postbox">
            <h3><?php echo __('List all Addresses in Wallet', 'cryptowoo-hd-wallet-addon'); ?></h3>
            <form action="" method="post" id="cw-list-addresses-form">
                <div id="section0">
                    <label for="cw-bridge-gap-currency"><?php echo __('Click the "List Addresses" button to display all addresses up to the current index.', 'cryptowoo-hd-wallet-addon'); ?></label>
                    <select title="Currency" id="cw-list-addresses-currency" name="cw-list-addresses-currency" required><?php echo $select; ?></select>
                    <input id="cw-list-addresses" name="cw-list-addresses" value="<?php echo __('List Addresses', 'cryptowoo-hd-wallet-addon'); ?>" type="submit">
                </div>
                <div id="cw-list-addresses-response"></div>
            </form>
        </div>
        <div class="wrap postbox cw-postbox">
            <div class="content">
                <div class="intro">
                    <h3><?php echo __('Payments are not displayed in your wallet client?', 'cryptowoo-hd-wallet-addon'); ?></h3>
                    <p><?php echo __('If your customers created multiple orders in a row and did not send a payment to the payment address, it may happen that payments for subsequent orders will not be detected in your wallet client.
                        The reason for this is that you have reached the "gap limit" of your wallet client.
                        The gap limit is the maximum number of consecutive unused addresses in the deterministic sequence of addresses in your HD wallet.', 'cryptowoo-hd-wallet-addon'); ?></p>

                    <h4><?php echo __('Why?','cryptowoo-hd-wallet-addon'); ?></h4>
                    <p><?php printf(__('To ensure your wallet can be synchronised correctly on different devices, the %sBIP44 specification%s implements a look-ahead window on how many addresses in your HD wallet are looked up by your wallet client.
                        If this limit did not exist, all wallets would have to look up a basically unlimited number of addresses on the blockchain to find out if they have received payment.','cryptowoo-hd-wallet-addon'), '<a href="https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki#Address_gap_limit" title="BIP44 specification" target="_blank">', '</a>'); ?></p>

                    <h4><?php echo __('What should I do now?','cryptowoo-hd-wallet-addon'); ?></h4>
                    <p><?php echo __('To see all transactions in your wallet client you can pay a small amount to the address that is located 20 addresses after the address that your wallet client has last detected a payment on.
                        This resets the gap limit during the address lookup and your wallet will now find the balance of subsequent addresses until it hits the gap limit again and so on.', 'cryptowoo-hd-wallet-addon'); ?></p>

                    <h4><?php echo __('How to bridge the gap', 'cryptowoo-hd-wallet-addon'); ?></h4>
                    <p><?php echo __('Use one of the methods below to automatically find the address(es) in your wallet that need to receive a payment to make sure that your wallet client discovers all used addresses.
                        The amount you send to these addresses can be as small as you like. You may also send the amount to a different address afterwards. The important point is that the address is recognized as "used" by your wallet client.', 'cryptowoo-hd-wallet-addon');?></p>

                    <p><strong><?php echo __('1. Automatic address lookup', 'cryptowoo-hd-wallet-addon');?></strong></p>
                    <form action="" method="post" id="cw-bridge-gap-form">
                        <div id="section0">
                            <label for="cw-bridge-gap-currency"><?php echo __('Click the "Resolve Gap" button to find the addresses that have to receive a payment to bridge the gap.', 'cryptowoo-hd-wallet-addon'); ?></label>
                            <select title="Currency" id="cw-bridge-gap-currency" name="cw-bridge-gap-currency" required><?php echo $select; ?></select>
                            <input id="cw-bridge-gap" name="cw-bridge-gap" value="<?php echo __('Resolve Gap', 'cryptowoo-hd-wallet-addon'); ?>" type="submit">
                        </div>
                        <p class="cryptowoo-warning"><?php echo __('Note that the automatic lookup will make a request to blockcypher.com for every address in your wallet up to the index number set in CryptoWoo.', 'cryptowoo-hd-wallet-addon') ?> </p>
                        <div id="cw-bridge-gap-response"></div>
                    </form>
                    <?php /* TODO enable lookup by address displayed in wallet client
                    <form action=""  method="post" id="gap-resolver">
                        <div id="section0">
                            <label for="current_address"><?php echo __('Enter the current receiving address that is displayed in your wallet client', 'cryptowoo-hd-wallet-addon'); ?></label>
                            <select title="Currency" id="gap-resolve-currency" name="gap-resolve-currency" required><?php echo $select; ?></select>
                            <input name="current_address" size="45" type="text" id="current_address">
                            <input id="cw-gap-resolve" name="cw-gap-resolve" value="<?php echo __('Resolve Gap', 'cryptowoo-hd-wallet-addon'); ?>" type="submit">
                        </div>
                        <div id="cw-gapresolve-response"></div>
                    </form>
                    */ ?>
                    <p><strong><?php echo __('2. Resolve manually', 'cryptowoo-hd-wallet-addon');?></strong></p>
                    <div align="left"><img style="border: 1px solid black;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAggAAABPCAYAAACOPj2eAAAQw0lEQVR4nO2d6VcT5wKH79/Eub3UGhSFioii3CoqdaPVCkJEAVEB971udUFbxeqtXnu17lqrUrVaKotUogKCiAZQQIGQZcgkk8z87gcCBgIhaYkk4fec83xoO+8Mbw6Z92Fmkv7DarWCUkoppSPXuro69OUfw/1DUUoppXR4ZSBQSiml1EUGAqWUUkpdZCBQSiml1EUGAqWUUkpdZCBQSiml1EUGAqWUUkpdZCBQSiml1EUGAqWUUkpdZCBQSiml1EWPA6Guro7SEaE3bx5KKQ10hyQQDAYDTCYTTCYTBEGgNCjs/p02GAxu3yx8T1BKg0FPz3leBYIgCBBFERaLhdKgUhRFCILgdSDwPUEpDUQ9Oed5FQiiKEKSJNjtdkqDSkmSIIqi14HA9wSlNBD15JznVSBYLBbY7XYoikJpUGm322GxWLwOBL4nKKWBqCfnvL8cCIQEC56+WfieIIQEAwwEQjyEgUAIGUkwEAjxEAYCIWQkEfiBIGmRHx+JxfcF344hIx7/CwQZutuZCI/PxwsJkE1VOJ07H+EfqxAyajLmbLqKF+a+x3MaY7Wg4W4+VsyNxb9Cu8bMXnsWFUb5/dYe7ZMQEowwEIIB6RWOzYvDsl/b0XVql9F6PQOLzjbDPuA2xFv8LhDkVlxNisLMU42wyXr8vmYyVAvzUfRaj47GYuQlTkD09nIISv9jJOMj7EnNwbe/VqChoxOGhkLsSRiHiI1/QgAAT/dJCAlKGAjBgGyA5sxxXNNaHP/cgYJlMUhwDoS+2xCv8VUg5CdMR+7508hJnImJUVEYHZWItRefovjkWiRMm4bIiEiMn7MZl+qtvcbZmy9ibsRX+KnZDqXjNpLGxePAc6nnv1ufHcTkiEzc0Sv9julnhtDdTMLHM46jToLH+ySEBCcfNBC8PhFKWuTPmYP9lfW4tVONz6ZMgmpiInLOPUdn9677LPZK5zMcTZyIuF2l0A/0p/IgYxSxHtd2JGHCaBVCQicgLvMEHnbIgPQKR+LHY845p4UXCnS/pkMVsxtPRDfHG2we6Druz7tSETNGhZDQsYicvwn/qxK6/uKXjdCcWoP4CWMQEqrCR5EzkXSwFB3y+/ksvGME5HbcXjcDH4WqEBI6Bh+NHo/JWx/D7LyNJ8eTtMhPmI41l85iw8JZiI6JQVjkDCQfLkH7CL0E4bNAiFchZPI23H1nAyDh9eVlGBWqwpj0q9BaFEDW448tUzE65Rbael57G7Qn52Ns8g20yoClci+iP81FcafTjoVipEfE4etKS79jXJHw8lgCVMm30O7xPgkhwcqHDQRvT4SSFvkJsUj+9jJKmiUACjqr/4vE8GnYWCZAAXov9rZmXM2KRWTmz3gtufy873E3RjGhdNt0fJp+FppWEVZ9LS5mxyIs9TpaZBu0J+dh1LxzeN1dCEoHCtImIHpXFQY8ZXoyD8WIok3T8Mm8Q7jXIMBqfodHpzMxLjIDV1vssFbnYUrkUvz4TA+rJOBtbSnuPGqFzWk+PYu/3IbLiZGY1esWQ59tBjle1/YqhETl4mq9BQoA6+urSB4/BWsfmd28uMGL7wJhDKblv0L3r6zceg1zR03BFqfi7CzNRXjsXlR0/5JJdTgcPwlL7+qhAOgszUH41H3otW6LFdgaE4X0os5+x/RFbvsNK6PjsL7E6Pk+CSFBywcOBC9PhJK2a8zRl3i/3oso3xKLsStLuu6Tdi/2d9+gdM/nGLvge1R0DnL5080YpeMOkiMX4L+v318jsL44itiwJFxtk2F/cw6fhy/Aqdc2x/a/QR3xb+x09xeVB/OQ2wuwaOxn2F3ltB+5BecTI/HZcS1MlfswKSIN5xqsA+zfu0AY7HiS42eecrgOPUdUOvDLwkjMONXo/vUNUnwXCBH4ouD9oq103ELimDk40WDr2abz0UZExOyAxvFWsTw7gClRa/GHsWuUu8V8uWMx7zum19yEShz5YjJm5T2FyfGfPdknISR4+cCB4OWJUNK6jAFktFxaiI8TzqDR7thm5gTMWqtGWOgsHHrh7tKBAzdjrNUHMSlUhRAX5+J4vQ2Q3+LCwk8Rf7IRNigw3MvC2KkHUNXPut3reIPMw/rsAKLHLIXTHQAAFlTsioNqeREEWYeig8kI/yQGn+ccxc+VOth67d+7QBj0eI6fOfGW889swp2kSMQd0w7+GgchvguEPrd+Om4hcexc/NA4UCCIKN8Wh6hN5ehepi1V+xEzYQ1KXG4HTMPWp2K/Y7qRTZXI/yoWM3YV9bp9NPg+CSHBzAcOBC9PhJIW+fHjseCX9l4La9O5xN6BEK9CSFw2NiyKQljaNTTZ4B43YyzP9iNalYIC/UCDZbReT0XY7JPQSkbcXzEJU/Y/h7s+8GQelmf73SzYD+B4WgLWd09x9dAqTFWNx+yDGhj7PoMAeBQIgx6vn2cWGAh+Egimh8iOnt7rOQBFfx/qyFk4WOscu3mICV+KX9rlfscAgGx8gsMLY5Gw/2HX8yzO8x1sn4SQoCYAAiEMUbud7u8rRhSunogxfW8x3Bcgt/2GVVGRSPyx3oMFu/8xcnsBFoXFYG3ZwJ9wkHW3seTTeThRWYisqJnYV+P2aB7NQ9H9iqTwPrcq5BacT4xA3DEt+l4XMVfmYXL4ctw1op9AaMfVLyIQf/r1gIEw6PEYCC74RyAoMNxfhXGffYfnzr8UioDSLXEYs/gEHjYLEJrL8N3CSIzPKUSH3P8Y2fAI++dHY9qWAjxveouWlm5bYZCUQfbpi1eYEOJPBEAgqBASlowjpc3olARob+/C1FGx/T+kCBn6ou2YGJaIo8/dXAJ1N0YxonjLNPwzdj1+etwMg0mHN89LcO2a5v3lV0WPu5mxmJ67FJHTv0PNYHc1PJmHYsLDbf/G6Ll5uNcoQDK/Q/npFRg3Lg0Xm+2Qmh/glwfP0WKUINv0qD6/AuMm78TjntfJ+bXtRElOND5JOo+XZjusog2Ky0OK7o/nuk+AgeAHgdDZjhvqaMz4Tz36XihTzLX4aUMixn2sQkjoRMRnn+n60iO5/zGmwnTHp136OhG5jgdRB9wnISTo8f9AmBmFL47kI2v2xK6TV+QC5Lr5mCMUAY/2zsTHM/LwZKCHFQcbY32Ngn3pmDrOccIMn4Gv9hc73Z9VYCxaizGhKsR+W+f+akX38QabBwBYGnHzm2WYEt71EcWIeRtw6okRMgCp4SKyZk3CPx0fXxyXkIsTGv37jyT2eW2t2itYOX08QkJViFz7Jzr7W/DdHI+B4IrffVESIYT4EP/+oiTHIrXornHwbT8w8tsrmKuahcOePhTpp/MgnsNAIISMJAIiEHr/FevhxDruIGlsf5dPJyGr5O9+REtGy5UlCJv9A15JHhyvsOovz4P4DwwEQshIImgDwTfIsEoSTC+vY1VMNNQ3Wj37/xr43TzIX4GBQAgZSTAQvMHyDHumhyFkVCy+3P/A6WtvB8Hf5kH+EgwEQshIwr8DgRA/goFACBlJMBAI8RAGAiFkJOHzQKA0mGQgEEJGCp6e87wOBEmSYLfbKQ0qJUliIBBCRgQ+CQRKR4IMBEJIMDPkgUAp7S0DgRASiDAQKPWxDARCSCDCQKDUxzIQCCGBCAOBUh/LQCCEBCIMBEp9LAOBEBKIMBAo9bEMBEJIIMJAoNTHMhAIIYEIA4FSH8tAIIQEIkMeCBaLBfX19Xj8+DHKysoCzsePH6O+vh4WiyWo5uXNXCkDgRBChjwQtFotampqYDAYIIpiwGkwGFBTUwOtVhtU8/JmrpSBQAghQx4IGo0GRqNx2Be/v6PRaIRGowm6eXk6V8pAIISQIQ+EsrKyYV/0hsKysrKgnJcnc6UMBEIIYSAwEBgIDARCCHGBgcBAYCAwEAghxAUGAgOBgcBAIIQQFxgIDAQGAgOBEEJcGLZA2LNgFr5MXQq1OgXJycuxMf82XnSY3Y4xtxRgzfLvUW10t28D6v4oxWtheAJhQ8o+aPRO/06vwb4l63C/zbvjCw2XsGrFGdSZPvxcKQOBEEKGLxCS16Ow3bHwGxpQeDQdi7fdxhs3i51HgaArx+FVeb0X6WANBB/NlTIQCCHELwKhayGtwJGUdPzvhQmiaET97UPIychARvoyZO68hGqduU8g9LNNWzOKj63GvBnzkZG7DjvO1sAwwL6GIxDMLQVYv+oYzuZ/g53bN2L1is04Xd4GsyhCNHeg5vLXyFAvR2ZWFrI352JxhiMQzO14dGIjVmdnY1VGGjJ234DW0OKzuVIGAiGE+E8giO0o3PAlNhXpIDT+jPXZP6Jab4Yo6lHx/XKsvPAKJqdAGGgbof0BNqv39yzSA203XIGwat4KXK4XIIoijHXnkJWWjyqDCOHVeWSpD6Gs3QxRNOHVpSzMSXt/BaHTJHSFROcbXM9Jx39qjBB9NFfKQCCEEEVR8PLlS4iiOPyB8Pv6hdharEP7Hxswb/5yrM7JQU5ODlZnpGLFsQp0OAXCQNvo+yyaA203XIGQ6wgCURQhGmvwQ/oqXGkU0Pb7BizZ+wgdjnGmujPIzHQEgqkJpecPY9fO3di7bzfWLEzC4ad6l0AYqrlSBgIhhCiKggsXLvScx4YvEHTlOJicifOvBLQXrsfiHaXQ9RnjfIthoG1cFs2BtvNRIGxL2YmHOud5PcTXyZvxoN0RCOrDqOgOCGM1vl/WHQjrseSbcugd44RX55CVeQZ1pk403VqHJTvuoVkQIZpb8dtaNQ71FwhDNFfKQCCEEEVRIMsyJEkaxocUdbUo2JeKpN2FeNspQqi/guzFm3GzwdS1vaBDq773MwgDbSPqSrEjeSsK27ruvQ+4nY8C4daGFGy7qYVRFCGKRmhvbEXK+pt40+m4xZCQih+e6WEWzdBVHEeauuuKgqnuLDLTjuKxzgxRFNB4PRfz0s6gziTg1bkMqPOrYBBFCM13sT1xUVcg+GiulIFACBm5VFVVQa/XQ1EUFBcXD+PHHFOXYPGSTGw/WYj6nk8nGNFw7why1SlQp6mRol6NY086XB5S7G8b0dyGP/OzsFidhc0nK6EfaDsfBYLQ9AAnNqYhOUWNpepUZGw9hZJmoecKSO6Sdfh642qsXLEcqUs34rSm+yFFHaoubMdydTqystdj76lTWJd9FnUmEcKbeziQkYylK1Yjd2s+fvwmF0ef6n02V8pAIISMXJwD4cGDBx/+CoK/64svSjK3FGBN+gnUuP0eh+GfK2UgEEJI9y2G7vMYA8HXgTDoFz0N/1wpA4EQMrKoqqpCVVUVAKCwsBAtLS09DymK4gf+FIO/y0CgDARCyEhBr9dDr9cDAN6+fQtRFKEoCrRaLW8xfIhA8FcZCAwEQgjpS/cXJXWfxxgIDATq40CglFJ/UKvV4uLFi1AUBSUlJSgqKoKiKLh06RLq6+uhKAry8vIgikN0i0Gj0cBkMg37ovd3NBqN0Gg0QTcvT+dKfRMIkiTBbrdTSqlfKEkSbDYbZFmGzWaD3W7v+d6D7m0sFgtsNtvQBMKLFy9QU1MTsIupyWRCTU0Namtrg2pe3syVDn0gUEppoPu3A6GpqQm1tbUoLy9HWVlZwFleXo7a2lo0NTUF1by8mSullFLqqR4HgtVqxbt371BdXY3KysqAs7q6Gu/evQu6eXk7V0oppdQTvQoESimllI4MGQiUUkopdZGBQCmllFIXGQiUUkopdZGBQCmllFIXGQiUUkopdZGBQCmllFIX+wuE/wPyv5HBtv5tdQAAAABJRU5ErkJggg==" alt="Order details custom field" /></div>
                    <p><?php echo __('Pick the latest WooCommerce order. You can find the derivation path of a specific payment address in a custom field named "mpk_key_position", at the bottom of the WooCommerce order details page for the respective order.', 'cryptowoo-hd-wallet-addon');?></p>
                    <p><?php echo __('The number after the last "/" determines the index number of the address that received the payment.
                        Subtract 20 from that number and use the "Create Address in Path" feature above to generate the address and send a payment to it.
                        The payment to that address will cause the gap counter of your wallet client to reset and it will proceed further down the derivation path.
                        If the gap between addresses was larger than 20, you may have to repeat that process, this time subtracting 20 from the index number you calculated in the previous step.', 'cryptowoo-hd-wallet-addon');?></p>
                </div>
            </div>
        </div>
        <?php
    }
}