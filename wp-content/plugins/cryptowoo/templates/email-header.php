<?PHP
/**
 * Email Header
 *
 * Modified from WooCommerce
 *
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>
<!DOCTYPE html>
<html dir="<?php echo is_rtl() ? 'rtl' : 'ltr'?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
    <title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
    <style>
        <?php wc_get_template('email-styles'); ?>
    </style>
</head>
<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<div id="wrapper" dir="<?php echo is_rtl() ? 'rtl' : 'ltr'?>" style="font-family: Arial, Helvetica, Verdana, sans-serif;">
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr>
            <td align="center" valign="top">
                <div id="template_header_image" style="background: #666; height: 6em;">
                    <?php $img = file_get_contents(CWOO_PLUGIN_DIR.'assets/images/cryptowoo-redux.png'); ?>
                <div style="padding: 2em 2em 0 0;"><img src="data:image/x-icon;base64,<?php echo base64_encode($img); ?>" alt="CryptoWoo" /></div>
                </div>
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container">
                    <tr>
                        <td align="center" valign="top">
                            <!-- Header -->
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header">
                                <tr>
                                    <td id="header_wrapper">
                                        <h2>{{EMAIL_HEADING}}</h2>
                                    </td>
                                </tr>
                            </table>
                            <!-- End Header -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <!-- Body -->
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
                                <tr>
                                    <td valign="top" id="body_content">
                                        <!-- Content -->
                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="top">
                                                    <div id="body_content_inner">