jQuery(document).ready(function () {

    // Check order status response
    function cwCheckResponse(response) {

        if(response.received == null) {
            response.received = 0.00;
        }

        if(response.unconfirmed == null) {
            response.unconfirmed = 0.00;
        } else if(response.unconfirmed > 0) {
            jQuery('#amount-incoming').removeClass('hidden');
            jQuery('#countdown-timer').hide();
            jQuery('#progress').hide();
        }

        if (response.redirect) {
            window.location.href = response.redirect;
        }
        jQuery( '#cw-confirmed' ).replaceWith( '<div id="cw-confirmed">'+response.received+'</div>' );
        jQuery( '#cw-unconfirmed' ).replaceWith( '<div id="cw-unconfirmed">'+response.unconfirmed+'</div>' );
    }

    // Customer clicked the "Check payment status button"
    jQuery("#check").click(function( c ) {
        c.preventDefault();
        setTimeout(function(){
                            jQuery( '#cw-loading' ).addClass( 'fa-spin' );
                          }, 250);
	    setTimeout(function(){
                            jQuery( '#cw-loading' ).removeClass( 'fa-spin' );
                          }, 2000);	
                    
	        var vars = {
	            'action': 'check_receipt',
	            'address': CryptoWoo.payment_address
	        };
	        var ajaxurl = CryptoWoo.admin_url;
				jQuery.ajax({
				    type: 'POST',
				    url: ajaxurl,
				    data: vars,
				    dataType: 'json',
				    success: function(response) {
				    	cwCheckResponse(response);
				    }
				});
    });

    // Poll payment info from database
    (function poll() {
        setTimeout(function () {
            jQuery('#cw-loading').addClass( 'fa-spin' );
        }, 14000);
        setTimeout(function () {
            var vars = {
                'action': 'poll',
                'address': CryptoWoo.payment_address
            };
            var ajaxurl = CryptoWoo.admin_url;
            jQuery.ajax({
                type: 'POST',
                url: ajaxurl,
                data: vars,
                dataType: 'json',
                success: function (response) {
                    cwCheckResponse(response);
                    //Setup the next poll recursively
                    poll();
                    jQuery('#cw-loading').removeClass( 'fa-spin' );

                }
            });
        }, 16500);
    })();

    // Our countdown plugin takes a duration, and an optional message
    jQuery.fn.countdown = function (duration, message) {

        // Maybe hide countdown and progress bar
        if (CryptoWoo.show_countdown == 0) {
            jQuery('#countdown-timer').hide();
            jQuery('#progress').hide();
        }

        // Progress bar
        var options = {
            bg: '#666',
            // leave target blank for global nanobar
            target: document.getElementById('progress'),
            // id for new nanobar
            id: 'timeoutBar'
        };
        var nanobar = new Nanobar(options);

        // If no message is provided, we use an empty string
        message = message || "";
        // Get reference to container, and set initial content
        var container = jQuery(this[0]).html(duration + message);
        // Get reference to the interval doing the countdown
        var countdown = setInterval(function () {
            // If seconds remain
            if (--duration) {
                // Update our container's message
                if (duration < 2) {
                    setTimeout(function () {
                        container.html("<span class='cryptowoo-warning'>" + CryptoWoo.please_wait + " <i class='fa fa-refresh fa-spin'></i>");
                    }, 3000);
                    // Wait 3 seconds to make sure the order has really timed out, then force processing of orders in database
                    var vars = {
                        'action': 'cw_force_processing'
                    };
                    var ajaxurl = CryptoWoo.admin_url;
                    jQuery.ajax({
                        type: 'POST',
                        url: ajaxurl,
                        data: vars,
                        dataType: 'json',
                        success: function () {
                            window.location.href = CryptoWoo.redirect;
                            clearInterval(countdown);
                        }
                    });
                } else {
                    // Calculate percentage
                    var progress = (duration / CryptoWoo.total_time) * 100; //
                    // Update progressbar with percentage
                    nanobar.go(progress);
                    // Update countdown time
                    container.html(secondsTimeSpanToHMS(duration));
                }
                // Otherwise
            } else {
                // Clear the countdown interval
                clearInterval(countdown);
            }
            // Run interval every 1000ms (1 second)
        }, 1000);
    };

    // Format seconds to hh:mm:ss
    function secondsTimeSpanToHMS(s) {
        var h = Math.floor(s/3600); //Get whole hours
        s -= h*3600;
        var m = Math.floor(s/60); //Get remaining minutes
        s -= m*60;
        return h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
    }

    // Use .countdown as container, duration, and optional message
    jQuery(".countdown").countdown(CryptoWoo.time_left, '');

    // Create QR code
    var qrcode = new QRCode(document.getElementById("qrcode"), {
        text: CryptoWoo.qr_data,
        width: 250,
        height: 250,
        colorDark : "#000000",
        colorLight : "#ffffff",
        correctLevel : QRCode.CorrectLevel.M
    });
});
// Select full content on payment page
function selectText(containerid) {
    if (document.selection) {
        var range = document.body.createTextRange();
        range.moveToElementText(document.getElementById(containerid));
        range.select();
    } else if (window.getSelection) {
        var range = document.createRange();
        range.selectNode(document.getElementById(containerid));
        window.getSelection().removeAllRanges();
        window.getSelection().addRange(range);
    }
    jQuery('#addr_img').removeClass('hidden');
}
