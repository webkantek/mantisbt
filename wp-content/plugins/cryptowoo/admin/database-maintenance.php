<?php
if (!defined('ABSPATH')) {die();}// Exit if accessed directly
$updateSuccess = null;
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['submit'])) {
    $admin_main = new CW_AdminMain();
    // if form has been posted process data
    if(isset($_GET['update_exchange_data'])) {
        $updateSuccess = $admin_main->update_exchange_data();
        if ($updateSuccess) {

            $updateMessage = print_r($updateSuccess, true);
            $type = 'notice notice-info';
        } else {
            $updateMessage = __('There was a problem inserting the live data. Please try again.', 'cryptowoo');
            $type = 'error';
        }
    }
}
?>
<div class="wrap">
<h2><?php echo __('CryptoWoo Database Maintenance', 'cryptowoo'); ?></h2>
<div class="wrap postbox cw-postbox">
    <div class="wrap postbox cw-postbox"><h3><?php echo __('Update exchange rates in database', 'cryptowoo'); ?></h3>
    <form id="update_exchange_data" action="" method="GET">
            <input type="hidden" name="update_exchange_data" id="update_exchange_data" value="true" />
            <input type="hidden" name="page" id="page" value="cryptowoo_database_maintenance" />
            <input id="update_exchange_data" type="submit" name="submit" class="button" value="<?php echo __('Update Exchange Rate Data', 'cryptowoo'); ?>" onClick="" />
    </form>
    <p><div class="button" id="reset-error-counter" href="#"><?php echo __('Reset Error Counter', 'cryptowoo'); ?></div><div id="reset-error-counter-response"></div></p>
    <?php if ($updateSuccess !== null):?>
        <div id='message' class='<?php echo $type; ?> fade'><p class="flash_message"><pre><?php echo $updateMessage ?></pre></p></div><br>
    <?php endif;
    /*
    // 19.10.2015 Manual rate update via AJAX collides with WooCommerce Currency Switcher Plugin - TODO Revisit later

        <a class="button" id="update-exchange-rates" href="#"><?php echo __('Update Exchange Rates', 'cryptowoo'); ?></a>
        <div id="cw-rates-loading"></div>
        <div id="cw-rates-response"></div>
    */
    // Display current exchange rates in database
    $rates = new CW_ExchangeRates();
    echo $rates->get_exchange_rates();
    ?>
 <p>Current time: <?php echo date('Y-m-d H:i:s', current_time( 'timestamp' )); ?></p>
 </div>
     <?php
     // Ajax url
     $admin_url = admin_url('admin-ajax.php');

     // Register script
     wp_register_script( 'cw_admin', CWOO_PLUGIN_PATH.'/assets/js/admin-js.js' );

     // Localize the script with new data
     $php_vars_array = array('admin_url' => $admin_url);
     wp_localize_script('cw_admin', 'CryptoWooAdmin', $php_vars_array);

     // Enqueued script with localized data.
     wp_enqueue_script('cw_admin');

     // Maybe include error visualization via Google charts
    $charts_file = sprintf('%sadmin/error-charts.php', CWOO_PLUGIN_DIR);
    if(file_exists($charts_file)) {
        include_once($charts_file);
    }
    ?>
    
    <div class="wrap postbox cw-postbox">
        <h3><?php echo __('Process open orders', 'cryptowoo'); ?></h3>
        <p><a class="button" id="update-tx-details" href="#"><?php echo __('Update incoming payments for open orders', 'cryptowoo'); ?></a> <?php echo __('This will make a request to the selected block chain API, so better wait a while before clicking again!', 'cryptowoo'); ?></p>
        <p><a class="button" id="process-open-orders" href="#"><?php echo __('Process order data', 'cryptowoo'); ?></a> <?php echo __('Check if any open orders have been paid or timed out.', 'cryptowoo'); ?></p>
        <p><div id="cw-processing-response"></div></p>
    </div>
    <div class="wrap postbox cw-postbox">
    <h3><?php echo __('Table reset', 'cryptowoo'); ?></h3>
        <div class="button" id="reset-exchange-rate-table" href="#"><?php echo __('Reset Exchange Rate Table', 'cryptowoo'); ?></div>
        <div class="button" id="reset-payments-table" href="#"><?php echo __('Reset Payments Table', 'cryptowoo'); ?></div> <span style="color:#B94A48; font-size:85%;"><?php echo __('This will remove all currently open orders from the queue.', 'cryptowoo'); ?></span>
        <div id="reset-table-response"></div>
    </div>
    <div class="wrap postbox cw-postbox">
        <h3>Cronjob Setup Info</h3>
        <?php
            $options = get_option('cryptowoo_payments');
            // Display cronjob setup info according to the selected cron interval
            echo CW_AdminMain::get_cronjob_info(in_array($options['soft_cron_interval'], array('seconds_60', 'seconds_120', 'seconds_300')));
        ?>
    </div>
    <?php
    // Include debug information
    include CWOO_PLUGIN_DIR.'admin/debug.php';
    ?>
</div>

