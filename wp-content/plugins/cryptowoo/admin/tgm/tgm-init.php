<?php

/**
 * TGM Init Class
 */
include_once ('class-tgm-plugin-activation.php');

function cryptowoo_payments_register_required_plugins() {



    $plugins = array(
            array(
                'name' => 'WooCommerce',
                'slug' => 'woocommerce',
                'required' => true,
                'version'            => '3.0',
                'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
                'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            )
    );

    // If the plugin does not include the Redux Framework, require it
    if ( !file_exists( WP_PLUGIN_DIR.'/cryptowoo/admin/redux-framework/framework.php' ) ) {
        $plugins[] = array(
            array(
                'name'     => 'Redux Framework',
                'slug'     => 'redux-framework',
                'required' => true,
            ));
    }
    if(WP_DEBUG) {
        $plugins[] = array(
            'name' => 'Advanced Cron Manager',
            'slug' => 'advanced-cron-manager',
            'required' => false,
        );
    }

    $config = array(
        'id'           => 'cryptowoo',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'domain'            => 'cryptowoo',             // Text domain - likely want to be the same as your theme.
        'default_path'      => '',                          // Default absolute path to pre-packaged plugins
        'parent_slug'   => 'cryptowoo',                 // Default parent menu slug
        'menu'              => 'install-required-plugins',  // Menu slug
        'has_notices'       => true,                        // Show admin notices or not
        'is_automatic'      => true,                        // Automatically activate plugins after installation or not
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        /*
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'cryptowoo' ),
			'menu_title'                      => __( 'Install Plugins', 'cryptowoo' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'cryptowoo' ),
			'notice_can_install_required'     => _n_noop(
				/* translators: 1: plugin name(s). * /
				'CryptoWoo requires the following plugin: %1$s.',
				'CryptoWoo requires the following plugins: %1$s.',
				'cryptowoo'
			),
			'notice_can_install_recommended'  => _n_noop(
				/* translators: 1: plugin name(s). * /
				'CryptoWoo recommends the following plugin: %1$s.',
				'CryptoWoo recommends the following plugins: %1$s.',
				'cryptowoo'
			),
			'notice_ask_to_update'            => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with CryptoWoo: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with CryptoWoo: %1$s.',
				'cryptowoo'
			),
		),
	*/

    );

	tgmpa( $plugins, $config );

}
add_action( 'tgmpa_register', 'cryptowoo_payments_register_required_plugins' );

