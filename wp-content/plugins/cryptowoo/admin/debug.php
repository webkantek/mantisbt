<?php
if(!isset($options)) {
    $options = get_option('cryptowoo_payments');
}

if(is_array($options) && ((bool)$options['logging']['debug'] || WP_DEBUG)) { ?>
    <div class="wrap postbox cw-postbox">
        <h3><?php esc_html_e('Debug Information', 'cryptowoo'); ?></h3>
        <p><?php esc_html_e('Please copy/paste the information below into your ticket when contacting support.', 'cryptowoo'); ?> </p>
<table>
<?php
$rates = new CW_ExchangeRates();
printf('<tr><td>PHP version</td><td>%s</td></tr>', phpversion());
printf('<tr><td>WooCommerce Version</td><td>%s</td></tr>', WC_VERSION);
printf('<tr><td>CryptoWoo Version</td><td>%s</td></tr>', CWOO_VERSION);

// Plugin license status
if(file_exists(CWOO_PLUGIN_DIR . 'includes/class.api.php')) {
    // license status in database
    printf('<tr><td>Local License</td><td>%s</td></tr>', get_option('cryptowoo_activated'));
    $cw_update      = new CWoo_MENU();
    $remote_license = $cw_update->license_key_status();
}
if(isset($remote_license) && is_array($remote_license)) {
  foreach($remote_license as $key => $value) {
    if($key === 'active') {
      printf('<tr><td>Remote License</td><td>%s</td></tr>', $value);
    }
  }
} else {
  printf('<tr><td>%s</td></tr>', esc_html__('Unable to retrieve license info from server.', 'cryptowoo'));
}

$unpaid_addresses = CW_OrderProcessing::get_unpaid_addresses();
printf('<tr><td>Unpaid addresses</td><td>%s</td></tr>', count($unpaid_addresses));
// Uncomment to see the details for all unpaid addresses
// print_r($unpaid_addresses);

echo '<tr><td><b>PHP Extensions</b></td></tr>';
        $required_extensions = array('curl', 'mcrypt', 'gmp');
        $loaded_extensions = get_loaded_extensions();
foreach($required_extensions as $required_extension) {
    if(in_array($required_extension, $loaded_extensions)) {
        printf('<tr><td>%s</td><td><span style="font-weight: bold; color: green;"><i class="fa fa-check"></i></span> enabled</td></tr>', $required_extension);
    } else {
        printf('<tr><td>%s</td><td><span style="font-weight: bold; color: red;"><i class="fa fa-warning"></i></span> not found | <a href="https://www.cryptowoo.com/enable-required-php-extensions/?ref=cw_status" target="_blank">More Info</a></td></tr>', $required_extension);
    }
}
// exchange rate update error info
$error_transient = get_transient('cryptowoo_rate_errors');
$rate_error_info = $error_transient ? str_replace('Array', '<span style="font-weight: bold; background-color: yellow;"><i class="fa fa-warning"></i></span> Errors:', print_r($error_transient, true)) : sprintf(esc_html__('%s None', 'cryptowoo'), '<span style="font-weight: bold; color: green;"><i class="fa fa-check"></i></span>');
printf(esc_html__('%sExchange rate errors%s%s%s','cryptowoo'), '<tr><td><b>', '</b></td><td><pre>', $rate_error_info, '</pre></td></tr>');

$rates->get_exchange_rates();

echo '<tr><td><b>Plugin Settings</b></td></tr>';

foreach ($options as $key => $value) {

	// Don't display API keys and MPKs
	$secrets = array(
                'cryptowoo_btc_api',
                'cryptowoo_btc_test_api',
                'cryptowoo_ltc_api',
                'cryptowoo_doge_api',
                'cryptowoo_doge_test_api',
                'cryptowoo_btc_mpk',
                'cryptowoo_btc_test_mpk',
                'cryptowoo_ltc_mpk',
                'cryptowoo_ltc_mpk_xpub',
                'cryptowoo_blk_mpk',
                'cryptowoo_blk_mpk_xpub',
                'cryptowoo_doge_mpk',
                'cryptowoo_doge_mpk_xpub',
                'cryptowoo_doge_test_mpk',
                'cryptoid_api_key',
                'blockcypher_token'
                );

    $option_value = $value !== '' ? $value : 'not set';
	$debug_array[$key] = $option_value === 'not set' || !in_array($key, $secrets)  ? $option_value : '********';

}
//print_r($debug_array);
// Maybe include HD Wallet Add-on Settings
if(file_exists(WP_PLUGIN_DIR . '/cryptowoo-hd-wallet-addon/cryptowoo-hd-wallet-addon.php')) {

    // Add HD wallet info
    $index_keys = array(
        'BTC' => 'cryptowoo_btc_index',
        'BTCTEST' => 'cryptowoo_btc_test_index',
        'DOGE' => 'cryptowoo_doge_index',
        'DOGETEST' => 'cryptowoo_doge_test_index',
        'LTC' => 'cryptowoo_ltc_index',
        'BLK' => 'cryptowoo_blk_index'
    );

    $title = '<b>HD Wallet Index</b>';
    $hdwallet_info[$title] = '';

    foreach ($index_keys as $coin => $index_key) {
        $current_index = get_option($index_key);
        $hdwallet_info[$coin] = $current_index !== false && $current_index !== '' ? $current_index : 'not set';
    }

} else {
    $title = '<b>HD Wallet Index</b>';
    $hdwallet_info[$title] = 'CryptoWoo HD Wallet Add-on not found. <a href="http://www.cryptowoo.com/shop/cryptowoo-hd-wallet-addon/?ref=cw_status" target="_blank">Get it here!</a>';
}
$debug_array = array_merge($debug_array, $hdwallet_info);
            foreach($debug_array as $option_name => $option_value) {
                if(is_array($option_value)) {
                    $option_value = sprintf('<pre>%s</pre>', print_r($option_value, true));
                }
                printf('<tr><td>%s</td><td>%s</td></tr>', $option_name, $option_value);
            }
        ?>
    </table></div>
   <?php } 
   



