<?php
if (!defined('ABSPATH')) {die();}// Exit if accessed directly
/**
 * Admin helper functions
 *
 * @package CryptoWoo
 * @subpackage Admin
 */

class CW_AdminMain {

    /**
    * Get Block.io API key, optionally force corresponding livenet currency API key
    *
    * @param $options
    * @param $currency
    * @param $force_livenet
    * @return mixed
    */
    public static function get_blockio_api_key($options, $currency, $force_livenet = false) {
			$stripped = strpos($currency, 'TEST') ? strtolower(str_replace('TEST', $force_livenet ? '' : '_test', $currency)) : strtolower($currency);
			return $options["cryptowoo_{$stripped}_api"];
	}

	 /**
	  * Show plugin changes on plugins overview page, adapted from W3 Total Cache Plugin
	  *
	  * @return void
	  */
	 function cw_in_plugin_update_message() {

		 $plugin_data = get_plugin_data(CWOO_PLUGIN_DIR.'/cryptowoo.php');

		 // only return for the current & later versions
		 $args['Version'] = $plugin_data['Version'];

		 $transient_name = 'cw_upgrade_notice_' . $args['Version'];

		 if (false === ( $upgrade_notice = get_transient( $transient_name ) ) ) {

			 $args = array(
				 'timeout'     => 20,
				 'redirection' => 5,
				 'httpversion' => '1.0',
				 'user-agent'  => 'CryptoWoo'.$plugin_data['Version'].'/' . get_bloginfo( 'version' ) . '; ' . get_bloginfo( 'url' ),
				 'blocking'    => true,
				 'headers'     => array(),
				 'cookies'     => array(),
				 'body'        => null,
				 'compress'    => false,
				 'decompress'  => true,
				 'sslverify'   => true,
				 'stream'      => false,
				 'filename'    => null
			 );

			 $response = wp_remote_get('https://www.cryptowoo.com/docs.php?action=update', $args);
			 //$response = wp_remote_get('http://local.wordpress.dev/text.txt');

			 if (!is_wp_error($response) && is_array($response)) {
				 set_transient( $transient_name, $response['body'], DAY_IN_SECONDS );
			 }
		 } else {
			 $response['body'] = get_transient( $transient_name );
		 }
		 if(isset($response['body'])) {
			 $matches = null;
			 $regexp = '~==\s*Changelog\s*==\s*=\s*[0-9.]+\s*=(.*)(=\s*' . preg_quote(CWOO_VERSION) . '\s*=|$)~Uis';

			 if (preg_match($regexp, $response['body'], $matches)) {
				 $changelog = (array) preg_split('~[\r\n]+~', trim($matches[1]));

				 echo '<div style="color: #f00;">Take a minute to update, here\'s why:</div><div style="font-weight: normal;">';
				 $ul = false;

				 foreach ($changelog as $index => $line) {
					 if (preg_match('~^\s*\*\s*~', $line)) {
						 if (!$ul) {
							 echo '<ul style="list-style: disc; margin-left: 15px;">';
							 $ul = true;
						 }
						 $line = preg_replace('~^\s*\*\s*~', '', htmlspecialchars($line));
						 echo '<li style="width: 48%; margin: 0; padding: 0 5 0 5; float: left; ' . ($index % 2 == 0 ? 'clear: left;' : '') . '">' . $line . '</li>';
					 } else {
						 if ($ul) {
							 echo '</ul><div style="clear: left;"></div>';
							 $ul = false;
						 }
						 echo '<p style="margin: 5px 0;">' . htmlspecialchars($line) . '</p>';
					 }
				 }

				 if ($ul) {
					 echo '</ul><div style="clear: left;"></div>';
				 }

				 echo '</div>';
			 }
		 }
	 }

	 /**
	  * On activation, set a time, frequency and name of an action hook to be scheduled.
	  */
	 function cryptowoo_cron_activation_schedule() {

		 $options = get_option('cryptowoo_payments');

		 if(is_array($options)) {
			 $starttime =  time();//current_time( 'timestamp' );

			 $interval = isset($options['soft_cron_interval']) ? $options['soft_cron_interval'] : 'seconds_30';

			 if(!wp_next_scheduled('cryptowoo_cron_action')) {
				wp_schedule_event($starttime+2, $interval, 'cryptowoo_cron_action');
			 } elseif($interval !== wp_get_schedule( 'cryptowoo_cron_action' )) {
			    // Interval has changed - reset hook
				wp_clear_scheduled_hook('cryptowoo_cron_action');
				wp_schedule_event($starttime+2, $interval, 'cryptowoo_cron_action');
			 }
			 if(!wp_next_scheduled('cryptowoo_integrity_check')) {
				wp_schedule_event($starttime+20, 'hourly', 'cryptowoo_integrity_check');
			 }
			 if(!wp_next_scheduled('cryptowoo_archive_addresses') && isset($options['auto_archive_addresses']) && (bool)$options['auto_archive_addresses']) {
			    // Maybe archive Block.io addresses
			 	wp_schedule_event($starttime+60, 'daily', 'cryptowoo_archive_addresses');
			 }
		 }
		 return true;
	 }

	 /**
	  * Add custom intervals to WP Cron
	  *
	  * @param $schedules
	  * @return mixed
	  */
	 function cron_add_schedules($schedules) {

        $custom_schedules = array(
        'seconds_15' => array(
			 'interval' => 15,
			 'display' => sprintf(__('Once every %s seconds', 'cryptowoo'), 15)
			 ),
		'seconds_30' => array(
			 'interval' => 30,
			 'display' => sprintf(__('Once every %s seconds', 'cryptowoo'), 30)
			 ),
		'seconds_60' => array(
			 'interval' => 60,
			 'display' => sprintf(_n('Once every minute', 'Once every %s minutes', 1, 'cryptowoo'), 1),
			 ),
	    'seconds_120' => array(
			 'interval' => 120,
			 'display' => sprintf(_n('Once every minute', 'Once every %s minutes', 2, 'cryptowoo'), 2)
			 ),
		'seconds_300' => array(
			 'interval' => 300,
			 'display' => sprintf(_n('Once every minute', 'Once every %s minutes', 5, 'cryptowoo'), 5)
			 )
        );
        return array_merge($custom_schedules, $schedules);
	 }

	 /**
	  * On the scheduled action hook, process open orders and update exchange rates.
      * @todo Add log verbosity options
      * @param bool $return
      * @return array
      */
	 function do_cryptowoo_cron_action($return = false) {

		 $time_begin = microtime(true);

		 $options = get_option('cryptowoo_payments');

         /*
		 // Check cron lock
		 $lock = get_option('cryptowoo_cron_running');
		 $interval = (int)str_replace('seconds_', '', $options['soft_cron_interval']);
         if(false !== $lock && (($lock + $interval) < $time_begin)) {
            return 'Cron already running';
         } else {
            update_option('cryptowoo_cron_running', $time_begin);
         }
         */

		 // Update transaction details for open orders
		 $tx_update = CW_OrderProcessing::update_tx_details($options);
		 if((bool)$options['logging']['transactions']) {
		    $data['order-processing']['tx-update'] = isset($tx_update['info']) ? $tx_update['info'] : $tx_update;
		 } else {
		    $data['order-processing']['tx-update'] = isset($tx_update['info']) ? $tx_update['info'] : array('status' => 'success');
		 }
		 $tx_update_time = microtime(true);

		 // Maybe process open orders
		 if(!is_string($data['order-processing']['tx-update'])) { // TODO Revisit force order update
			 $processing_result = CW_OrderProcessing::process_open_orders($options);
			 if((bool)$options['logging']['orders']) {
			    // Full logging
			    $data['order-processing']['order-update'] = $processing_result;
			 } else {
                $data['order-processing']['order-update'] = array(__('unpaid_addresses', 'cryptowoo') => $processing_result['unpaid_addresses']);
			 }
		 } else {
			 $data['order-processing']['order-update'] = __('Transaction data unchanged - processing skipped', 'cryptowoo');
		 }
		 $order_update_time = microtime(true);

		 // Update exchange rates
		 $btc = CW_ExchangeRates::update_btc_fiat_rates($options);
 		 $ltc = CW_ExchangeRates::update_altcoin_fiat_rates('LTC',$options);
 		 $doge = CW_ExchangeRates::update_altcoin_fiat_rates('DOGE', $options);
 		 $blk = CW_ExchangeRates::update_altcoin_fiat_rates('BLK', $options);

 		 // Add-on hook for altcoin rate updates
         $data = apply_filters('cw_cron_update_exchange_data', $data, $options);

         // Maybe log exchange rate updates
		 if((bool)$options['logging']['rates']) {
            if($btc['status'] === 'not updated' || strpos($btc['status'], 'disabled')) {
                $data['btc'] = strpos($btc['status'], 'disabled') ? $btc['status'] : $btc['last_update'];
            } else {
                $data['btc'] = $btc;
            }

            if($ltc['status'] === 'not updated' || strpos($ltc['status'], 'disabled') ) {
                $data['ltc'] = strpos($ltc['status'], 'disabled') ? $ltc['status'] : $ltc['last_update'];
            } else {
                $data['ltc'] = $ltc;
            }

            if($doge['status'] === 'not updated' || strpos($doge['status'], 'disabled') ) {
                $data['doge'] = strpos($doge['status'], 'disabled') ? $doge['status'] : $doge['last_update'];
            } else {
                $data['doge'] = $doge;
            }

            if($blk['status'] === 'not updated' || strpos($blk['status'], 'disabled') ) {
                $data['blk'] = strpos($blk['status'], 'disabled') ? $blk['status'] : $blk['last_update'];
            } else {
                $data['blk'] = $blk;
            }
		}
        if((bool)$options['logging']['debug']) {
            $data['durations']['tx-update-duration'] = round($tx_update_time - $time_begin, 4).'sec';
            $data['durations']['order-update-duration'] = round($order_update_time - $tx_update_time, 4).'sec';
            $data['durations']['rate-updates'] = round(microtime(true) - $order_update_time, 4).'sec';

            if(isset($options['blockcypher_token']) && !empty($options['blockcypher_token'])) {
               $data['rate_limit'] = CW_AdminMain::get_blockcypher_limit($options);
            }
        }
		if(WP_DEBUG || in_array('1', $options['logging'])) {
		    CW_AdminMain::cryptowoo_log_data($time_begin, 'do_cryptowoo_cron_action', $data);
		}
		do_action('cwrc_catch_request', $data);
		// Remove lock
		//delete_option('cryptowoo_cron_running');
		return $return ? $data : true;
	 }

	/**
    * Log data to file
    *
    * @param $time_begin
    * @param $function
    * @param $data
    * @param $filename
    */
    static function cryptowoo_log_data($time_begin, $function, $data, $filename = false) {
	    // Check filesize
	    if(!$filename) {
	        $filename = 'cryptowoo-cron.log';
	    }
	    $file = sprintf('%s/%s',CW_LOG_DIR, $filename);
		if(file_exists($file)) {
		    $size =  filesize($file);
			 // Rename if larger than 2MB
				 if($size > 2000000) {
					 rename($file, sprintf('%s%s-%s', CW_LOG_DIR, $filename, date('Y-m-d_H_i')));
				 }
		}
		if($time_begin > 0) {
		    $data['durations']['full-runtime'] = round(microtime(true) - $time_begin, 4).'sec';
		} else {
		    $time_begin = time();
		}
		file_put_contents($file, sprintf("%s BEGIN - %s\r\n%s\r\n%s END\r\n", date('Y-m-d H:i:s', $time_begin), $function, var_export($data, true), date('Y-m-d H:i:s')), FILE_APPEND);
	 }

	/**
     * Log data as JSON
     *
     * @param $to_json
     */
     public static function cryptowoo_log_json($to_json) {
        $file = sprintf('%srate-errors.json', CW_LOG_DIR);
        if (is_file($file)) {
            $data = json_decode(file_get_contents($file), true);
        } else {
            $data = array();
        }
        $data[] = $to_json;

        // Purge data older than a week
		for ($i = 0; $i < count($data); $i++) {
		    if ($data[$i]['time'] < (time() - WEEK_IN_SECONDS)) {
		        array_splice($data, $i, 1);
		    }
		}
        file_put_contents($file, json_encode($data));
    }

    /**
    * Get BlockCypher API limit
    *
    * @todo use neutral token to get current free tier limits
    *
    * @param $options
    * @param bool $force
    * @return array|mixed
    */
    static function get_blockcypher_limit($options, $force = false) {

        if(empty($options['blockcypher_token'])) {
            return array(
                    'limit_hour' => 200,
                    'limit_sec' => 3
                    );
        }
        $keep_until = CW_AdminMain::seconds_to_next_hour();
        if($force || false === ($bc_status = get_transient('blockcypher_token_limit'))) {
            $result = wp_safe_remote_get("https://api.blockcypher.com/v1/tokens/{$options['blockcypher_token']}");
            if(!is_wp_error($result)) {

                $result = json_decode($result['body']);
                $limits = isset($result->limits) ? (array)$result->limits : array('api/hour' => 200, 'api/second' => 3);

                $bc_status = array(
                    'limit_hour' => $limits['api/hour'],
                    'limit_sec' => $limits['api/second']
                    );

                if(isset($result->hits)) {
                    $hits = (array)$result->hits;
                    $bc_status['hits_hour'] = $hits['api/hour'];
                    $bc_status['hits_sec']  = isset($hits['api/second']) ? $hits['api/second'] : 'not_set';
                }
                $bc_status['time_to_reset'] = $keep_until;

                // Set transient, keep for 2 minutes
                set_transient('blockcypher_token_limit', $bc_status, 120);
            }
        }
        $bc_status['time_to_reset'] = $keep_until;
        return $bc_status;
    }


   /**
    * Calculate seconds left to the next full hour
    *
	* Since we're dividing by 3600, it will give seconds since the last full hour increment (1:00, 2:00, 3:00, etc...).
    * If it's 1:30:01, the modulo operation will return 1 and $prev will be 1:00. Adding 3600 seconds will make $next equal to 2:00.
    *
    * @return int
    */
    static function seconds_to_next_hour() {
		$now = time();
		$prev = $now - ($now % 3600);
		$next = $prev + 3600;
		return absint($now - $next);
    }

	/**
	 * On the scheduled action hook, archive Block.io addresses
	 */
	function cryptowoo_archive_addresses() {
	 	$time_begin = microtime(true);
		$options = get_option('cryptowoo_payments');
		$data = CW_Address::archive_addresses($options);

		// Get status
		if(isset($data['status']) && isset($data['message']) && $data['status'] === 'alert') {
			// Send email to admin
			CW_AdminMain::send_blockio_address_archive_warning($data);
		}

		if((defined('WP_DEBUG') && WP_DEBUG) || (bool)$options['logging']['debug']) {
            CW_AdminMain::cryptowoo_log_data($time_begin, 'do_archive_addresses', $data);
		}
	 }

	/**
     * Send "Block.io Account threshold reached" e-Mail
    * @param $data
    */
    static function send_blockio_address_archive_warning($data) {

        $to       = get_option( 'admin_email' );
        $blogname = get_bloginfo( 'name', 'raw' );
        $subject = sprintf('%s: %s', $blogname, __('Block.io address archival warning', 'cryptowoo'));
        $message = CW_Formatting::cw_get_template_html('email-header', $subject);
        $message .= sprintf(__( "Hello Admin,%sCryptoWoo at %s has found an issue while archiving your addresses at Block.io:%s%s", 'cryptowoo' ), '<br>', $blogname, '<br>', $data['message']);
        $message .= CW_Formatting::cw_get_template_html('email-footer');

        $headers = array("From: CryptoWoo Plugin <{$to}>",'Content-Type: text/html; charset=UTF-8');

        wp_mail( $to, $subject, $message, $headers );

    }


/*  Order table sorting functions */

function cryptowoo_order_data($columns) {
	$new_columns = (is_array($columns)) ? $columns : array();
	unset($new_columns['order_actions']);

	//edit this for you column(s)
	//all of your columns will be added before the actions column
	$new_columns['crypto_amount'] = 'Amount due';
	$new_columns['received_confirmed'] = 'Amount received';
	$new_columns['payment_currency'] = 'Payment Currency';
	$new_columns['payment_address'] = 'Payment Address';

	//stop editing

	$new_columns['order_actions'] = $columns['order_actions'];

	return $new_columns;
}

function cryptowoo_columns_values_function($column) {
	global $post;
	$data = get_post_meta($post->ID);

	//start editing, I was saving my fields for the orders as custom post meta
	//if you did the same, follow this code
	if ($column == 'crypto_amount') {
		echo (isset($data['crypto_amount'][0]) ? CW_Formatting::fbits($data['crypto_amount'][0]) : '0');
	}
	if ($column == 'received_confirmed') {
		echo (isset($data['received_confirmed'][0]) ? CW_Formatting::fbits($data['received_confirmed'][0]) : '0');
	}
	if ($column == 'payment_currency') {
		echo (isset($data['payment_currency'][0]) ? $data['payment_currency'][0] : '');
	}
	if ($column == 'payment_address') {
		echo (isset($data['payment_address'][0]) && isset($data['payment_currency'][0]) && !empty($data['payment_currency'][0]) ? CW_Formatting::link_to_address($data['payment_currency'][0], $data['payment_address'][0], false, true) : '');
	}

	//stop editing
}

function cryptowoo_columns_sort_function($columns) {
	$custom = array(
		//start editing

		'crypto_amount'    => 'crypto_amount',
		'received_confirmed' => 'received_confirmed',
		'payment_address'    => 'payment_address',
		'payment_currency' => 'payment_currency'
		//stop editing
	);

	return wp_parse_args($custom, $columns);
}

	 /**
	  * If WooCommerce is not active deactivate cronjobs and disable CryptoWoo payments.
	  *
	  */
	 function cw_is_woocommerce_active() {
		if (is_admin()) {

			include_once (ABSPATH . 'wp-admin/includes/plugin.php');

			$options = get_option('cryptowoo_payments');

			// Add "Extension Disabled" notice
			add_action('admin_notices', array($this, 'cryptowoo_extension_notice'));
			if (!is_plugin_active('woocommerce/woocommerce.php')) {

				// deactivate cronjobs
				cryptowoo_plugin_deactivate();

				// Disable gateway

				//$options['enabled'] = false;
				$options['cryptowoo_price_rewrite'] = 'disable';
				$options['cryptowoo_currency_switch_position'] = 'disable';
				$options['enable_soft_cron'] = '0';
				update_option('cryptowoo_payments', $options);
				/*
				Inactive/Not installed WooCommerce is handled by TGMPA

				if(!file_exists(WP_PLUGIN_DIR.'woocommerce/woocommerce.php')) {
					// Add WooCommerce not installed notice
					add_action('admin_notices', array($this, ' cryptowoo_wc_notinstalled_notice'));
				} else {
					// Add "WooCommerce Disabled" notice
					add_action('admin_notices', array($this, 'cryptowoo_wc_inactive_notice'));
				}
				*/
				return;

			}
			if(!strpos($_SERVER['REQUEST_URI'], 'cryptowoo') && $options['enabled'] == false) {

				// Add "Gateway Disabled" notice
				add_action('admin_notices', array($this, 'cryptowoo_cw_inactive_notice'));
			}

			// Maybe display missing integrity check file notice
			if(!isset($options['cw_filename'])) {
				$filename = $options['cw_filename'] = urlencode(sha1(NONCE_SALT));
				update_option('cryptowoo_payments', $options);
			} else {
				$filename = $options['cw_filename'];
			}
			$path = WP_CONTENT_DIR.'/uploads/'.$filename;
			if(!file_exists($path) && false == strpos($_SERVER['REQUEST_URI'], 'cryptowoo')) {
				add_action('admin_notices', array($this, 'cryptowoo_integrity_notice'));
			}

			// Maybe display cronjob setup info notice on options page
			if(strpos($_SERVER['REQUEST_URI'], 'cryptowoo') && false === get_option('cryptowoo_cronjob_notice', false)) {
				add_action('admin_notices', array($this, 'create_cronjob_notice'));
			}

			// Maybe display exchange rate error notice
			if(isset($options['display_rate_error_notice']) && (bool)$options['display_rate_error_notice']) {
                $rate_errors = get_transient('cryptowoo_rate_errors');
                $error_count = (int)CW_Validate::check_if_unset('error_count', $rate_errors, '0');
                if($error_count >= 1) {
                    add_action('admin_notices', array($this, 'cryptowoo_rate_notice'));
                }
			}

			// Maybe display license expiration notice
			$license_expiration = get_option('cryptowoo_license_expiration');
			if($license_expiration  && false === get_option('cryptowoo_license_notice', false)) {
			    $now = time();
			    $days_before = 4 * WEEK_IN_SECONDS;
			    $expiration_timestamp = $license_expiration['access_expires'];
			    if(($expiration_timestamp - $now) < $days_before ) {
			        add_action('admin_notices', array($this, 'cryptowoo_license_expiration_notice'));
			    }
			}

			// Maybe display option reset notice on options page
			if(CWOO_VERSION === '0.12.1' && false === get_option('cryptowoo_option_reset_notice', false)) {
				add_action('admin_notices', array($this, 'cryptowoo_option_reset_notice'));
			}

	        if('dismissed' !== get_option('cryptowoo_save_options_notice')) {
	            add_action('admin_notices', array($this, 'cryptowoo_please_save_options_notice'));
	        }
		}
	}

   /**
    * Render cronjob setup info depending on the set interval and if we are in a WP multisite network
    *
    * @param bool $single
    * @return string
    */
    static function get_cronjob_info($single = true) {

    	$cronjob_info = sprintf(__('%sWe recommend you use an external cron via cPanel/crontab/Task Scheduler to trigger WP Cron to make sure that the orders are being processed and the rates are updated.%s', 'cryptowoo'), '<p>', '<br>');
		$cronjob_info .= '<div style="padding: 1em; background-color: #ccc;">';
		$url =  get_bloginfo('url');
		if(!defined('CWOO_MULTISITE') || (defined('CWOO_MULTISITE') && !CWOO_MULTISITE)) {
			$cronjob_info .= sprintf('%s* * * * * cd %s; php -q wp-cron.php > /dev/null 2>&1 # Trigger WP Cron once per minute%s', '<pre>', ABSPATH, '</pre>');
		   if(!$single) {
		        $intervals = array('15','30','45');
		        $cronjob_info .= sprintf(__('%s# Additionally for intervals < 1 minute:%s', 'cryptowoo'), '<pre>', '</pre>');
		        foreach($intervals as $interval) {
		          $cronjob_info .= sprintf('<pre>* * * * * sleep %s; cd %s; php -q wp-cron.php > /dev/null 2>&1 # Sleep for %ssec before running </pre>', $interval, ABSPATH, $interval);
		        }
          }
          $cronjob_info .= sprintf('%s<pre>* * * * * wget -O - "%s/wp-cron.php?doing_wp_cron" > /dev/null 2>&1 # Use wget to trigger WP Cron once per minute</pre>', __('# Alternative command: '), $url);
		} else {
			$cronjob_info .= sprintf('%s* * * * * wget -q -O - "http://%s/wp-cron-multisite.php" > /dev/null 2>&1 # Trigger WP Cron once per minute%s', '<pre>', DOMAIN_CURRENT_SITE, '</pre>');
		   if(!$single) {
		       $intervals = array('15','30','45');
		       $cronjob_info .= sprintf(__('%s# Additionally for intervals < 1 minute:%s', 'cryptowoo'), '<pre>', '</pre>');
			    foreach($intervals as $interval) {
		          $cronjob_info .= sprintf('<pre>* * * * * sleep %s; wget -q -O - "http://%s/wp-cron-multisite.php" > /dev/null 2>&1 # Sleep for %ssec before running </pre>', $interval, DOMAIN_CURRENT_SITE, $interval);
		        }
            }
            $cronjob_info .= sprintf(__('# Make sure you copy the file %swp-multisite-cron.php%s to %s!', 'cryptowoo'), '<code>', '</code>', DOMAIN_CURRENT_SITE);
        }
		$cronjob_info .= $single ? '</div></p>' : __('</div></p><p>Explanation:<br>The example above consists of <strong>4 crontasks that are each executed every minute</strong>.<br>The 2nd, 3rd, and 4th cronjob will sleep for 15, 30, and 45 seconds before they trigger WP cron.<br>This results in a <strong>15 second interval</strong> between each WP cron execution <strong>instead of the smallest possible interval of one minute</strong> when using only one crontask.</p>', 'cryptowoo');
		$cronjob_info .= sprintf(__('<p>The first crontask is enough if you set the cron interval to 60 seconds or higher. Refer to %sthe tutorial%s on our Help Desk for further information.</p><p><i class="fa fa-exclamation-triangle"></i> After setting up the crontask(s) add <code>define(\'DISABLE_WP_CRON\', true);</code> to your <code>/wp-config.php</code>.</p>', 'cryptowoo'), '<a href="https://www.cryptowoo.com/cron-issues/?utm_source=plugin&utm_medium=link&utm_campaign=settings-tooltip" title="Visit CryptoWoo Help Desk" target="_blank">', '</a>');
		return wp_kses_post($cronjob_info);
	}

	/**
 	 * Cronjob setup info admin notice
 	 */
 	function create_cronjob_notice() {
	 	if (isset($_POST['dismiss_cronjob_notice'])){
	 		update_option('cryptowoo_cronjob_notice', 'dismissed');
    	} else {

			$cronjob_info = CW_AdminMain::get_cronjob_info();

			$dismiss_cronjob_notice = '<p><form id="delete_payment_data" action="" method="post">
        							<fieldset>
            							<input type="hidden" name="dismiss_cronjob_notice" value="dismiss_cronjob_notice" />
            							<input id="dismiss_cronjob_notice" type="submit" name="submit" class="button" value="'.__('Dismiss', 'cryptowoo').'" onClick="" />
            						</fieldset>
            					</form></p>';
		 	?>
			<div class="update-nag redux-messageredux-notice notice is-dismissible redux-notice">
			<?php
				echo $cronjob_info;
				echo $dismiss_cronjob_notice.'</div>';
			}
 	}
	 /**
	  * WooCommerce inactive notice
	  */
	 function cryptowoo_wc_inactive_notice() {
		 $update_actions =  array(
			 'activate_plugin_wc' => '<a class="button" href="' . wp_nonce_url('plugins.php?action=activate&amp;plugin='.urlencode('woocommerce/woocommerce.php'), 'activate-plugin_' . 'woocommerce/woocommerce.php') . '" title="' . esc_attr__('Activate WooCommerce Plugin') . '" target="_parent">' . __('Activate WooCommerce') . '</a>',
			 'cw_settings' => '<a class="button" href="'.self_admin_url('admin.php?page=cryptowoo').'" title="' . esc_attr__('Go to CryptoWoo Checkout Settings') . '" target="_parent">' . __('Enable CryptoWoo Payment Gateway') . '</a>'
			 /*'plugins_page' => '<a href="' . self_admin_url('plugins.php') . '" title="' . esc_attr__('Go to plugins page') . '" target="_parent">' . __('Return to Plugins page') . '</a>'*/
		 );
		 ?>
		 <div class="error">
			 <p><?php _e('<b>CryptoWoo payment gateway has been disabled!</b> It seems like WooCommerce has been deactivated or is not installed.<br>Go to the Plugins menu and make sure that the WooCommerce plugin is installed and activated. Then visit the CryptoWoo checkout settings to re-enable the CryptoWoo payment gateway.', 'cryptowoo');?></p>
			 <?php foreach ($update_actions as $key => $value) {
				 echo $value . ' ';
			 }
			 ?>
		 </div>
		 <?php
	 }

	 /**
	  * CryptoWoo payment gateway disabled notice
	  */
	 function cryptowoo_cw_inactive_notice() {
		 $checkout_settings =  '<a class="button" href="'.self_admin_url('admin.php?page=cryptowoo').'" title="' . esc_attr__('Go to CryptoWoo Checkout Settings') . '" target="_parent">' . __('Enable CryptoWoo Payment Gateway') . '</a>';
		 ?>
		 <div class="error">
			 <p><?php _e('<b>CryptoWoo payment gateway has been disabled!</b><br>Go to the CryptoWoo checkout settings to make sure the settings are correct and re-enable the CryptoWoo payment gateway.', 'cryptowoo');?></p>
			 <?php echo $checkout_settings;	?>
		 </div>
		 <?php
	 }

	 /**
	  * CryptoWoo payment gateway disabled notice
	  */
	 function cryptowoo_integrity_notice() {
		 $checkout_settings =  '<a class="button" href="'.self_admin_url('admin.php?page=cryptowoo').'" title="' . esc_attr__('Go to CryptoWoo Checkout Settings') . '" target="_parent">' . __('Go to CryptoWoo Checkout Settings') . '</a>';
		 ?>
		 <div class="error">
			 <p><?php _e('<b>CryptoWoo can\'t find the integrity check file!</b><br>Go to the CryptoWoo checkout settings and click the "Save Changes" button to re-create it.', 'cryptowoo');?></p>
			 <?php
				 if(!strpos($_SERVER['REQUEST_URI'], 'section=wc_cryptowoo')) {
					 echo $checkout_settings;
				 }
			 ?>
		 </div>
		 <?php
	 }

	 /**
	  * CryptoWoo exchange rate error notice
	  */
	 function cryptowoo_rate_notice() {
		 $rate_errors = get_transient('cryptowoo_rate_errors');

		 $error_count = (int)CW_Validate::check_if_unset('error_count', $rate_errors, 0);
		 $counter_start = (int)CW_Validate::check_if_unset('counter_start', $rate_errors, 0);

		 $counting_since_date = date('l jS \of F Y h:i:s',$counter_start);

		 $buttons =  '<a class="button" href="'.self_admin_url('admin.php?page=cryptowoo_database_maintenance').'" title="' . esc_attr__('Go to Database Maintenance Page') . '" target="_parent">1. ' . __('Go to Database Maintenance Page') . '</a>';
		 $buttons .=  ' <a class="button" href="'.self_admin_url('admin.php?page=cryptowoo').'" title="' . esc_attr__('Go to CryptoWoo Options') . '" target="_parent">2. ' . __('Go to CryptoWoo Options') . '</a>';
		 $buttons .=' <a class="button" href="http://cryptowoo.zendesk.com/" target="_blank" title="'.__('Visit Help Desk', 'cryptowoo').'">3. '.__('Visit Help Desk', 'cryptowoo').'</a>';
		 ?>
		 <div class="error redux-messageredux-notice notice is-dismissible redux-notice">
			 <p><?php printf(__('<b>CryptoWoo has detected %s exchange rate update errors since %s.</b><br>Go to the CryptoWoo Database Maintenance page, reset the error counter and try to update the rates manually.<br>If this message is still being displayed, please select a different preferred exchange API on the options page and create a support ticket in our HelpDesk.<p>%s</p>', 'cryptowoo'), $error_count, $counting_since_date, $buttons);?></p>
		 </div>
		 <?php
	 }

	 /**
	  * WooCommerce not installed notice
	  */
	 function cryptowoo_wc_notinstalled_notice() {
		 $update_actions =  array(
			 'activate_plugin_wc' => '<a class="button" href="' . wp_nonce_url( urlencode('update.php?action=install-plugin&plugin=woocommerce') ) . '" title="' . esc_attr__('Activate WooCommerce Plugin') . '" target="_parent">' . __('Install WooCommerce') . '</a>',
			 'cw_settings' => '<a class="button" href="'.self_admin_url('admin.php?page=cryptowoo').'" title="' . esc_attr__('Go to CryptoWoo Checkout Settings') . '" target="_parent">' . __('Enable CryptoWoo Payment Gateway') . '</a>'
			 /*'plugins_page' => '<a href="' . self_admin_url('plugins.php') . '" title="' . esc_attr__('Go to plugins page') . '" target="_parent">' . __('Return to Plugins page') . '</a>'*/
		 );
		 ?>
		 <div class="error">
			 <p><?php _e('<b>CryptoWoo payment gateway has been disabled!</b> It seems like WooCommerce is not installed.<br>Click here to install and activate the WooCommerce plugin. Then visit the CryptoWoo checkout settings to re-enable the CryptoWoo payment gateway.', 'cryptowoo');?></p>
			 <?php foreach ($update_actions as $key => $value) {
				 echo $value . ' ';
			 }
			 ?>
		 </div>
		 <?php
	 }

	/**
     * License expiration notice
     */
    function cryptowoo_license_expiration_notice() {
	    if (isset($_POST['dismiss_license_notice'])){
	 		update_option('cryptowoo_license_notice', 'dismissed');
	 	} else {
            $expiration_info = get_option('cryptowoo_license_expiration');
		 	?>
			<div class="updated redux-messageredux-notice notice is-dismissible redux-notice">
			<?php printf(__('<h4>Your CryptoWoo License Key will expire on <strong>%s</strong></h4> Renew your license now for a discount!<br>Your discount code: <strong>%s</strong>', 'cryptowoo'), date('l jS \of F Y', $expiration_info['access_expires']), $expiration_info['order_key']); ?>
				<p><form id="dismiss_license_notice" action="" method="post">
        			<fieldset>
            			<input type="hidden" name="dismiss_license_notice" value="dismiss_license_notice" />
            			<input id="dismiss_license_notice" type="submit" name="submit" class="button" value="<?php echo __('Dismiss', 'cryptowoo'); ?>" onClick="" />
            		</fieldset>
            	</form></p></div>
            <?php
			}
	 	}

	/**
     * "Please save options" notice
     */
    function cryptowoo_please_save_options_notice() {
	    if (isset($_POST['dismiss_save_options_notice'])){
	 		update_option('cryptowoo_save_options_notice', 'dismissed');
	 	} else {
		 	?>
			<div class="updated redux-messageredux-notice notice redux-notice">
			<?php printf('<h4>%s</h4>',  __('Thanks for updating CryptoWoo! Please verify that your settings are correct and click the "Save settings" button.', 'cryptowoo')); ?>
				<p><form id="dismiss_save_options_notice" action="" method="post">
        			<fieldset>
            			<input type="hidden" name="dismiss_save_options_notice" value="dismiss_save_options_notice" />
            			<input id="dismiss_save_options_notice_button" type="submit" name="submit" class="button" value="<?php echo __('Dismiss', 'cryptowoo'); ?>" onClick="" />
            		</fieldset>
            	</form></p></div>
            <?php
			}
	 	}

	 /**
	  * PHP extensions missing notice
	  */
	 function cryptowoo_extension_notice() {

		 $version = phpversion();
		 $message = '';

		 $not_installed = __('<b>%s extension</b> seems not to be installed.<br>', 'cryptowoo');

		 if (version_compare(PHP_VERSION, '5.6.0', '<')) {
			 $message .= sprintf(__('Your current <b>PHP Version is %s</b>.<br>', 'cryptowoo'), $version);
		 }
		 if (!extension_loaded('gmp')) {
			 $message .= sprintf($not_installed, 'GMP');
		 }

		 if (!extension_loaded('curl')) {
			 $message .= sprintf($not_installed, 'cURL');
		 }

		 if (!extension_loaded('mcrypt')) {
			 $message .= sprintf($not_installed, 'mCrypt');
		 }

		 if($message !== '') {
			 ?>
			 <div class="error">
				 <p><?php printf(__('%sYou need to have at least PHP version 5.6 with <a href="http://php.net/manual/en/mcrypt.installation.php" target="_blank">mCrypt</a>, <a href="http://php.net/manual/en/gmp.installation.php" target="_blank">GMP</a>, and <a href="http://php.net/manual/en/curl.installation.php" target="_blank">cURL</a> extensions enabled to use CryptoWoo. <a href="https://www.cryptowoo.com/enable-required-php-extensions/?ref=cw_ext_error" target="_blank">More Info</a>', 'cryptowoo'), $message); ?></p>
			 </div>
			 <?php
		 }
	 }

     /**
      * Cronjob setup info admin notice
      */
     function cryptowoo_option_reset_notice() {
         if (isset($_POST['dismiss_reset_notice'])){
             update_option('cryptowoo_option_reset_notice', 'dismissed');
         } else {
            $cw_settings = sprintf(__('%s%s%sCryptoWoo settings page%s', 'cryptowoo'), '<a href="', admin_url('admin.php?page=cryptowoo'), '">','</a>');
            $message = sprintf(__('%sImportant notice:%s During this CryptoWoo update we had to set the %s"Payment Processing" > "Order Expiration Time"%s back to the default value.%sIf you customized this setting before the update, please go to the %s and set it again.', 'cryptowoo'), '<strong>', '</strong><br>', '<code>', '</code>', '<br>', $cw_settings);
            ?>
            <div class="update-nag redux-messageredux-notice notice is-dismissible redux-notice">
                <p><?php echo $message; ?> <form id="dismiss_reset_notice" action="" method="post">
                    <fieldset>
                        <input type="hidden" name="dismiss_reset_notice" value="dismiss_reset_notice" />
                        <input id="dismiss_notice" type="submit" name="submit" class="button" value="<?php echo __('Done', 'cryptowoo'); ?>" onClick="" />
                    </fieldset>
                </form></p>
            </div>
            <?php
         }
     }

    /**
     * Misconfiguration notice
     */
    function cryptowoo_misconfig_notice() {
        $options = get_option('cryptowoo_payments');
        if((bool)$options['enabled']) {
             $enabled['BTC'] = $options['processing_api_btc'] === 'disabled' && ((bool)CW_Validate::check_if_unset('cryptowoo_btc_api', $options) || (bool)CW_Validate::check_if_unset('cryptowoo_btc_mpk', $options) );
             $enabled['LTC'] = $options['processing_api_ltc'] === 'disabled' && ((bool)CW_Validate::check_if_unset('cryptowoo_ltc_api', $options) || ((bool)CW_Validate::check_if_unset('cryptowoo_ltc_mpk', $options) || (bool)CW_Validate::check_if_unset('cryptowoo_ltc_mpk_xpub', $options)) );
             $enabled['DOGE'] = $options['processing_api_doge'] === 'disabled' && ((bool)CW_Validate::check_if_unset('cryptowoo_doge_api', $options) || ((bool)CW_Validate::check_if_unset('cryptowoo_doge_mpk', $options) || (bool)CW_Validate::check_if_unset('cryptowoo_doge_mpk_xpub', $options) ) );
             $enabled['BLK'] = $options['processing_api_blk'] === 'disabled' && ((bool)CW_Validate::check_if_unset('cryptowoo_blk_mpk_xpub', $options) || (bool)CW_Validate::check_if_unset('cryptowoo_blk_mpk', $options) );
             $enabled = apply_filters('cw_misconfig_notice', $enabled, $options);
             $message = '';
             $nicenames = cw_get_woocommerce_currencies();
             foreach($enabled as $currency => $status) {
                if($status) {
                    $message .= sprintf(__('%s%s payment processing configuration error%s', 'cryptowoo'), '<p><strong>', $nicenames[$currency], '</strong></p>');
                }
             }
             if(!empty($message)) {
                $message .= sprintf('<p>%s</p>',__('Go to "Payment Processing" > "Block Chain Access" and make sure you have selected a "Processing API" provider. Otherwise CryptoWoo can not look up transactions in the block chain.', 'cryptowoo'));
                printf('<div class="error redux-messageredux-notice notice is-dismissible redux-notice">%s</div>', $message);
            }
        }
    }


	 function do_cryptowoo_integrity_check() {

		 $validate = new CW_Validate();
		 $api_keys_valid = $validate->cryptowoo_api_check();

		 $result = !isset($api_keys_valid['valid']) || !$api_keys_valid['valid'] ? false : true;
		 if(!$result) {

			 // Disable gateway
			 $options = get_option('cryptowoo_payments');
			 $options['enabled'] = false;
			 $options['btc_enabled'] = '0';
			 $options['doge_enabled'] = '0';
			 $options['ltc_enabled']= '0';
			 $options['cryptowoo_currency_table_on_single_products'] = false;
			 $options['cryptowoo_price_rewrite'] = 'disable';
			 $options['cryptowoo_currency_switch_position'] = 'disabled';

			 update_option('cryptowoo_payments', $options);

			 // Disable cron
			 wp_clear_scheduled_hook('cryptowoo_cron_action');

			 $admin_ip = false !== get_transient( 'admin_ip' ) ? get_transient( 'admin_ip' ) : false;
			 $admin_useragent = false !== get_transient( 'admin_useragent' ) ? get_transient( 'admin_useragent' ) : false;

			 $print_result = var_export($api_keys_valid, true);
			 $to = get_option( 'admin_email' );
			 $blogname = get_bloginfo( 'name', 'raw' );
			 $subject = sprintf('%s: %s', $blogname, __('Unauthorized changes detected', 'cryptowoo'));

             $message = CW_Formatting::cw_get_template_html('email-header', $subject);
			 $message .= sprintf(__('Hello Admin,%s on %s, CryptoWoo has detected an unauthorized change of your settings at %s.%s', 'cryptowoo'), '<br>', date('l jS \of F Y h:i:s'), $blogname, '<br>');
			 $message .= $admin_ip ? sprintf('%s: %s<br>',__('Last known admin IP: ', 'cryptowoo'), $admin_ip) : '';
			 $message .= $admin_useragent ? sprintf('%s: %s<br>',__('Last known admin user agent: ', 'cryptowoo'), $admin_useragent) : '';

			 $message .= sprintf(
			 __( 'The CryptoWoo payment gateway has been disabled. From now on, open WooCommerce orders using CryptoWoo will not be completed and the payment method will not be available for new orders.%sPLEASE LOG IN AT %s TO CHANGE YOUR PASSWORDS AND API KEYS AS SOON AS POSSIBLE!%sSOMEONE ELSE MAY HAVE ACCESS TO THEM.%sValidation result:%s%s', 'cryptowoo'), '<br>', strtoupper($blogname), '<br>', '<br>', '<br>',$print_result,'<br>');

			 $message .= __('If you need help, please submit a ticket at http://cryptowoo.zendesk.com', 'cryptowoo');

             $message .= CW_Formatting::cw_get_template_html('email-footer');

             $headers = array("From: CryptoWoo Plugin <{$to}>",
                                                 'Content-Type: text/html; charset=UTF-8');

			 wp_mail( $to, $subject, $message, $headers );

			 // Write to log
			 file_put_contents(CW_LOG_DIR . 'cryptowoo-cron.log', "\r\n==========-cryptowoo_integrity_check-==========\r\nBEGIN " . date("Y-m-d H:i:s") . "\r\n" . $print_result . "\r\nEND\r\n====================\r\n", FILE_APPEND);
		 }
		 return $result;
	 }

	 /**
	  * Retrieves the best guess of the client's actual IP address.
	  * Takes into account numerous HTTP proxy headers due to variations
	  * in how different ISPs handle IP addresses in headers between hops.
	  * Stolen from https://gist.github.com/cballou/2201933#file-get-ip-address-optimized-php
	  */
	 function get_ip() {
		 $ip_keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
		 foreach ($ip_keys as $key) {
			 if (array_key_exists($key, $_SERVER) === true) {
				 foreach (explode(',', $_SERVER[$key]) as $ip) {
					 // trim for safety measures
					 $ip = trim($ip);
					 // attempt to validate IP
					 if ($this->validate_ip($ip)) {
						 return $ip;
					 }
				 }
			 }
		 }
		 return CW_Validate::check_if_unset('REMOTE_ADDR', $_SERVER);

	 }

	 /**
	  * Ensures an ip address is both a valid IP and does not fall within a private network range.
	  * Taken from https://gist.github.com/cballou/2201933#file-get-ip-address-optimized-php
      * @param $ip
      * @return bool
      */
	 function validate_ip($ip) {
		 if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
			 return false;
		 }
		 return true;
	 }

	 function database_maintenance() {

		 // Only get balances if WooCommerce is active
		 include_once (ABSPATH . 'wp-admin/includes/plugin.php');
		 if (is_plugin_active('woocommerce/woocommerce.php')) {
			 //BEGIN Database maintenance

			 // include database maintenance processing & form

			 include 'database-maintenance.php';
		 } else {
			 add_action('admin_notices', array($this, 'cryptowoo_wc_inactive_notice'));
		 }
		 //END Database maintenance
	 }


	 function delete_payment_data() {
		 global $wpdb;

		 $payments = $wpdb->get_results("DELETE FROM `" . $wpdb->prefix . "cryptowoo_payments_temp` WHERE 1", ARRAY_A);

		 $message = (bool)$payments ? __('Payment data deleted', 'cryptowoo') : __('Error deleting payment data', 'cryptowoo');

		 return $message;

	 }

	 function create_plugin_table($blog_id) {
		 global $wpdb;
		 $charset_collate = '';

		 if (!empty($wpdb->charset)) {
			 $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
		 }

		 if (!empty($wpdb->collate)) {
			 $charset_collate .= " COLLATE {$wpdb->collate}";
		 }

		 // Create payments table
		 $sql = "CREATE TABLE " . $wpdb->prefix . "cryptowoo_payments_temp (
						id int(11) NOT NULL AUTO_INCREMENT,
						amount decimal(17,8) NOT NULL,
						crypto_amount bigint(20) NOT NULL,
						address varchar(50) NOT NULL DEFAULT '',
						payment_currency varchar(20) NOT NULL DEFAULT '',
						customer_reference varchar(50) NOT NULL DEFAULT '',
						invoice_number int(11) NOT NULL,
						email varchar(100) NOT NULL DEFAULT '',
						order_id int(11) NOT NULL,
						received_confirmed bigint(20) NOT NULL DEFAULT '0',
						received_unconfirmed bigint(20) NOT NULL DEFAULT '0',
						created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
						last_update timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
						timeout_value int(11) NOT NULL,
						timeout tinyint(1) NOT NULL DEFAULT '0',
						txids text NOT NULL DEFAULT '',
						paid tinyint(1) NOT NULL DEFAULT '0',
						is_archived tinyint(1) NOT NULL DEFAULT '0',
						PRIMARY KEY  (id),
						KEY address (address),
						KEY order_id (order_id),
						KEY invoice_number (invoice_number),
						KEY ticks (timeout,paid)
					) $charset_collate;";

		 require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		 dbDelta($sql);

		 // Create exchange rate table
		 $sql = "DROP TABLE IF EXISTS " . $wpdb->prefix . "cryptowoo_exchange_rates;
			  CREATE TABLE " . $wpdb->prefix . "cryptowoo_exchange_rates (
					coin_type varchar(8) NOT NULL,
					exchange_rate decimal(17,8) NOT NULL,
					exchange varchar(20) NOT NULL,
					status varchar(40) NOT NULL,
					method varchar(40) NOT NULL,
					last_update timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
					api_timestamp timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
					PRIMARY KEY  (coin_type),
					KEY last_update (last_update)
				) $charset_collate;";

		 require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		 dbDelta($sql);

	 }

	 function recreate_table($table = 'cryptowoo_exchange_rates', $backup = false) {
		 global $wpdb;
		 $charset_collate = '';

		 if (!empty($wpdb->charset)) {
			 $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
		 }

		 if (!empty($wpdb->collate)) {
			 $charset_collate .= " COLLATE {$wpdb->collate}";
		 }

		$table_name = $wpdb->prefix.$table;

		if($table === 'cryptowoo_exchange_rates' || $backup === false) {
			$wpdb->query("DROP TABLE IF EXISTS $table_name");
		} else {
			$backup_table_name = $table_name.'_'.str_replace('.', '_', CWOO_VERSION).'_bak';
			$rename = $wpdb->query("RENAME TABLE $table_name TO $backup_table_name");
			if(false === $rename) {
				$wpdb->query("DROP TABLE IF EXISTS $table_name");
			}
			update_option('cryptowoo_payments_table_backup', $backup_table_name);
		}

		 switch($table) {
			 case('cryptowoo_payments_temp'):
				 $sql = "CREATE TABLE " . $table_name . " (
						id int(11) NOT NULL AUTO_INCREMENT,
						amount decimal(17,8) NOT NULL,
						crypto_amount bigint(20) NOT NULL,
						address varchar(50) NOT NULL DEFAULT '',
						payment_currency varchar(20) NOT NULL DEFAULT '',
						customer_reference varchar(50) NOT NULL DEFAULT '',
						invoice_number int(11) NOT NULL,
						email varchar(100) NOT NULL DEFAULT '',
						order_id int(11) NOT NULL,
						received_confirmed bigint(20) NOT NULL DEFAULT '0',
						received_unconfirmed bigint(20) NOT NULL DEFAULT '0',
						created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
						last_update timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
						timeout_value int(11) NOT NULL,
						timeout tinyint(1) NOT NULL DEFAULT '0',
						txids text NOT NULL DEFAULT '',
						paid tinyint(1) NOT NULL DEFAULT '0',
						is_archived tinyint(1) NOT NULL DEFAULT '0',
						PRIMARY KEY  (id),
						KEY address (address),
						KEY order_id (order_id),
						KEY invoice_number (invoice_number),
						KEY ticks (timeout,paid)
					) $charset_collate;";
				 break;
			 case('cryptowoo_exchange_rates') :
			 default:
				 // recreate exchange rate table
				 $sql = "CREATE TABLE " . $table_name . " (
					coin_type varchar(8) NOT NULL,
					exchange_rate decimal(17,8) NOT NULL,
					exchange varchar(20) NOT NULL,
					status varchar(40) NOT NULL,
					method varchar(40) NOT NULL,
					last_update timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
					api_timestamp timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
					PRIMARY KEY  (coin_type),
					KEY last_update (last_update)
				) $charset_collate;";
				 break;
		 }

		 require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		 $dbdelta = dbDelta($sql);

		 return empty($dbdelta) ? true : $dbdelta;
	 }

	/**
     * Force update exchange rates for all enabled currencies
     *
     * @return array
     */
    function update_exchange_data() {
	    $results = array(
	        'btc' => CW_ExchangeRates::update_btc_fiat_rates(false, true),
            'doge' => CW_ExchangeRates::update_altcoin_fiat_rates('DOGE', false, true),
            'ltc' => CW_ExchangeRates::update_altcoin_fiat_rates('LTC', false, true),
            'blk' => CW_ExchangeRates::update_altcoin_fiat_rates('BLK', false, true),
	    );
	    return apply_filters('cw_force_update_exchange_rates', $results);
	 }

	/**
	 * Update exchange rates callback
	 */
 	function cw_exchange_rates_callback() {
		$admin_main = new CW_AdminMain();
		echo '<div class="update-nag">';
		echo 'Request time: '.date('Y-m-d H:i:s').'<pre>';
		$rate_updates = $admin_main->update_exchange_data();
		print_r($rate_updates);
		echo '</pre>';
		echo '<a class="button" href="javascript: window.location.reload(true)">Reload Page</a></div>';
		wp_die(); // this is required to terminate immediately and return a proper response
	}

	/**
 	 * Reset exchange rate error counter callback
 	 */
 	function cw_reset_error_counter_callback() {
		// Reset transient
		delete_transient('cryptowoo_rate_errors');
		echo '<div id="message" class="success fade" style="color: green;"><i class="fa fa-check"></i>' . __('Rate error counter has been reset.', 'cryptowoo') . '</div>';
		wp_die(); // this is required to terminate immediately and return a proper response
	}

	/**
	 * Reset exchange rate table callback
	 */
 	function cw_reset_exchange_rate_table_callback() {
		$admin_main = new CW_AdminMain();
		echo '<div class="update-nag">';
		echo 'Request time: '.date('Y-m-d H:i:s').'<pre>';
		$truncate_success = $admin_main->recreate_table('cryptowoo_exchange_rates');
		if($truncate_success) {
			$message = __('Exchange rate table truncated successfully!', 'cryptowoo');
		} else {
			$message = __('There was a problem preparing the plugin table. Please try again.', 'cryptowoo');
		}
		echo $message;
		echo '<br><a class="button" href="javascript: window.location.reload(true)">Reload Page</a></div>';
		wp_die(); // this is required to terminate immediately and return a proper response
	}

	/**
	 * Recreate payments table callback
	 */
 	function cw_reset_payments_table_callback() {
		$admin_main = new CW_AdminMain();
		echo '<div class="update-nag">';
		echo 'Request time: '.date('Y-m-d H:i:s');
		$delete_success = $admin_main->recreate_table('cryptowoo_payments_temp');
		if ($delete_success) {
			$message =  __('Payments table data has been reset!', 'cryptowoo');
		} else {
			$message = __('There was a problem resetting the payments table. Please try again.', 'cryptowoo');
		}
		echo $message;
		echo '</div>';
		wp_die(); // this is required to terminate immediately and return a proper response
	}

	/**
	 * Update tx details callback
	 */
 	function cw_update_tx_details_callback() {
        $data = CW_OrderProcessing::update_tx_details();
        CW_AdminMain::cryptowoo_log_data(time(), 'manual tx update', $data);
        echo json_encode($data);
		wp_die(); // this is required to terminate immediately and return a proper response
	}

	/**
     * Admin process open orders callback
     */
    function cw_process_open_orders_callback() {
        $data = CW_OrderProcessing::process_open_orders();
        echo json_encode($data);
        wp_die();
    }

    /**
     * Frontend force process open orders callback
     */
    function cw_front_process_open_orders_callback() {
        $data = CW_OrderProcessing::process_open_orders();
        CW_AdminMain::cryptowoo_log_data(time(), 'cw_front_process_open_orders_callback', $data);
        wp_die();
    }

	/**
     * Maybe redirect the user to the options page and trigger notice that instructs him to click the  "Save settings" button
     */
    function cw_maybe_redirect_to_options_page() {
	    $version = get_option('cryptowoo_version');
	    if($version && version_compare($version , '0.16.1', '<')) {
	        add_option('cryptowoo_save_options_notice', 'display');
	        add_option('cryptowoo_plugin_do_activation_redirect', true);
	        update_option('cryptowoo_version', CWOO_VERSION);
	    }
	}
}

