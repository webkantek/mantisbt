<?php
if (!defined('ABSPATH')) {die();}// Exit if accessed directly

/**
 * Get the exchange rates from supported exchanges:
 *
 * - Bitcoinaverage.com
 * - BTC-e.com
 * - Bitfinex.com
 * - BitPay.com
 * - Block.io (authenticated only)
 * - Chain.so
 * - Dogecoinaverage.com
 * - Poloniex.com
 * - Shapeshift.com
 * - Bittrex.com
 * - Bitcoincharts
 * - Coinbase (GDAX.com)
 * - Bitstamp.com
 * - Blockchain.info
 * - Coindesk BPI
 * - Luno.com
 * - OKCoin.com
 * - OKCoin.cn
 * - Kraken
 * - Livecoin.net
 *
 * Some functions modified from "Bitcoin Payments for WooCommerce"
 * Author: BitcoinWay
 * URI: http://www.bitcoinway.com/
 *
 * @category CryptoWoo
 * @package CryptoWoo
 * @subpackage ExchangeRates
 * @author DRDoGE
 * @todo refactor
 *
 */
class CW_ExchangeRates
{
    /**
     * Get the Altcoin/Fiat exchange_rate
     *
     * @package CW_ExchangeRates
     * @param $coin
     * @param bool $options
     * @param bool $force
     * @return null|string
     */
    public static function update_altcoin_fiat_rates($coin, $options = false, $force = false) {
        global $wpdb;

        if (!$options) {
            $options = get_option('cryptowoo_payments');
        }

        $query = $wpdb->get_results("SELECT last_update, coin_type
                                    FROM {$wpdb->prefix}cryptowoo_exchange_rates
                                       WHERE coin_type = '{$coin}';");
        date_default_timezone_set('UTC');
        if (!$query) {
            $timeago = 60;
        } else {
            $last_update = $query[0]->last_update;
            $timeago     = time() - strtotime($last_update);
        }

        if ($timeago >= 59 || $force) { // only update price if data is older than 1 minute

            $lc_coin                = strtolower($coin);
            $preferred_exchange_key = sprintf('preferred_exchange_%s', $lc_coin);

            $preferred_exchange = CW_Validate::check_if_unset($preferred_exchange_key, $options);

            $enabled = array(sprintf('blockio_%s', $lc_coin) => CW_Validate::check_if_unset(sprintf('cryptowoo_%s_api', $lc_coin), $options),
                             sprintf('blockio_%s_test_enabled', $lc_coin) => CW_Validate::check_if_unset(sprintf('cryptowoo_%s_test_api', $lc_coin), $options),
                             sprintf('hd_%s_enabled', $lc_coin) => CW_Validate::check_if_unset(sprintf('cryptowoo_%s_mpk', $lc_coin), $options) || CW_Validate::check_if_unset(sprintf('cryptowoo_%s_mpk_xpub', $lc_coin), $options),
                             sprintf('hd_%s_test_enabled', $lc_coin) => CW_Validate::check_if_unset(sprintf('cryptowoo_%s_test_mpk', $lc_coin), $options));

            $status = null;

            if ($preferred_exchange && in_array(true, $enabled) && (bool)$options['enabled']) {

                $method = $preferred_exchange; // TODO this changes with fallback usage
                /*
                if ($preferred_exchange === 'dogecoinaverage') {
                    $prices = CW_ExchangeRates::get_dogecoinaverage_price('BTC', 'vwap', 20); // Dogecoinaverage.com
                } elseif ($preferred_exchange === 'blockio') {
                    $prices = CW_ExchangeRates::get_block_io_price($coin, $options); // Block.io
                } elseif ($preferred_exchange === 'shapeshift') {
                    $prices = CW_ExchangeRates::get_shapeshift_price($coin); // Shapeshift.io
                } elseif ($preferred_exchange === 'poloniex') {
                    $prices = CW_ExchangeRates::get_poloniex_price($coin); // Poloniex.com
                } elseif ($preferred_exchange === 'chain_so') {
                    $prices = CW_ExchangeRates::get_chain_so_price($coin, $options); // Chain.so
                } elseif ($preferred_exchange === 'bitfinex') {
                    $prices = CW_ExchangeRates::get_bitfinex_price($coin); // Bitfinex.com
                } elseif ($preferred_exchange === 'btc_e') {
                    $prices = CW_ExchangeRates::get_bitfinex_price($coin); // BTC-e.com
                } */

                $function = sprintf('get_%s_price', $preferred_exchange);
                if ($preferred_exchange !== 'dogecoinaverage') {
                    $prices = CW_ExchangeRates::$function($coin, $options);
                } else {
                    // 1 DOGE = 1 DOGE, no need for an API call
                    $prices = CW_ExchangeRates::get_dogecoinaverage_price('BTC', 'vwap', 20); // Dogecoinaverage.com
                }
                $coin_fiat = isset($prices[$method][$coin]['price']) ? $prices[$method][$coin]['price'] : 0;
                $status    = $prices[$method]['status'];
                $time      = isset($prices[$method][$coin]) ? gmdate('Y-m-d H:i:s', $prices[$method][$coin]['timestamp']) : gmdate('Y-m-d H:i:s');

                // Maybe save rate
                $maybe_save = CW_ExchangeRates::maybe_save_rate($coin, $coin_fiat, $preferred_exchange, $status, $method, $time, $prices, $options);

                if (is_array($maybe_save)) {
                    return $maybe_save;
                } else {

                    $prices['errors'][] = sprintf('%s|%s|%s', $preferred_exchange, $coin, json_encode($prices));

                    // If the other API calls failed try again with chain_so
                    if (false === strpos($status, 'success')) {


                        // Get the price from chain.so or bittrex (if BLK)
                        $fallback_1_id = $coin !== 'BLK' ? 'chain_so' : 'bittrex';
                        $fallback_1 = "get_{$fallback_1_id}_price";
                        $fallback_prices_1 = CW_ExchangeRates::$fallback_1($coin, $options);
                        $coin_fiat         = isset($fallback_prices_1[$fallback_1_id][$coin]['price']) ? $fallback_prices_1[$fallback_1_id][$coin]['price'] : 0;

                        $status = $fallback_prices_1[$fallback_1_id]['status'];
                        $time   = gmdate('Y-m-d H:i:s', $fallback_prices_1[$fallback_1_id][$coin]['timestamp']);
                        // Maybe save rate
                        $maybe_save = CW_ExchangeRates::maybe_save_rate($coin, $coin_fiat, $preferred_exchange, $status, $fallback_1_id, $time, $fallback_prices_1, $options);

                        if (is_array($maybe_save)) {
                            return $maybe_save;
                        } else {
                            $fallback_prices_2           = CW_ExchangeRates::get_poloniex_price($coin); // Poloniex.com
                            $fallback_prices_2['errors'] = $prices['errors'];
                            $coin_fiat                   = isset($fallback_prices_2['poloniex'][$coin]['price']) ? $fallback_prices_2['poloniex'][$coin]['price'] : 0;
                            $status                      = $fallback_prices_2['poloniex']['status'];
                            $time                        = gmdate('Y-m-d H:i:s', $fallback_prices_2['poloniex'][$coin]['timestamp']);
                            $maybe_save                  = CW_ExchangeRates::maybe_save_rate($coin, $coin_fiat, $preferred_exchange, $status, 'poloniex-fallback', $time, $fallback_prices_2, $options);

                            if (is_array($maybe_save)) {
                                return $maybe_save;
                            } else {
                                $prices['errors']['fallback_2'] = sprintf('Poloniex fallback from %s|%s|%s', $preferred_exchange, $coin, json_encode($prices));
                            }
                        }
                    }
                    if (false === strpos($status, 'success')) {
                        // Inform admin about failure
                        CW_ExchangeRates::maybe_warn_admin('error', false, $prices, gmdate('Y-m-d H:i:s'), $coin, $coin_fiat, $options);
                    }
                    return $prices;
                }
            } else {// if DOGE is enabled

                $status = "skipped: {$coin} is disabled";

                $wpdb->query("UPDATE {$wpdb->prefix}cryptowoo_exchange_rates
								                   SET 	status = '{$status}'
								                 WHERE coin_type = '{$coin}';");

                return array('status' => $status,
                             'last_update' => "{$timeago}s ago",
                             'time' => date('Y-m-d H:i:s'));
            }
        } else {
            $next_update = 60 - $timeago;

            return array('status' => 'not updated',
                         'last_update' => "{$timeago}s ago - next update in {$next_update}s",
                         'time' => date('Y-m-d H:i:s'));
        }
    }

    /**
     * Get the BTC/Fiat exchange_rate
     *
     * @package CW_ExchangeRates
     * @param bool $options
     * @param bool $force
     * @return array|null|string
     */
    public static function update_btc_fiat_rates($options = false, $force = false) {
        global $wpdb;
        $coin = 'BTC';

        if (!is_array($options)) {
            $options = get_option('cryptowoo_payments');
        }

        $query = $wpdb->get_results("SELECT last_update, coin_type, exchange_rate
                                   FROM {$wpdb->prefix}cryptowoo_exchange_rates
                                     WHERE coin_type = '{$coin}';");
        date_default_timezone_set('UTC');
        if (!$query) {
            $timeago = 60; // Force update
        } else {
            $last_update = $query[0]->last_update;
            $timeago     = time() - strtotime($last_update);
	        // Set old rate
	        $prices = array('price' => $query[0]->exchange_rate);
        }

        if ($timeago >= 59 || $force) { // only update price if data is older than 1 minute

            $base_currency = get_option('woocommerce_currency');

            $method = $preferred_exchange = $options['preferred_exchange_btc'];

	        // Return 1 if WooCommerce currency = BTC
	        if($base_currency === 'BTC') {
	            $prices = array($method => array($coin => array('price' => 1)));
	            $maybe_save = CW_ExchangeRates::maybe_save_rate( $coin, 1, 'constant', 'success', 'constant', gmdate('Y-m-d H:i:s'), $prices, $options );
	            return $maybe_save;
            }

            $status = null;

                if ($preferred_exchange === 'bitcoinaverage') {
                    // @todo add option for rate_type
                    $prices = CW_ExchangeRates::get_bitcoinaverage_price($base_currency, 30);
                } elseif ($preferred_exchange === 'blockio') {
                    $prices = CW_ExchangeRates::get_blockio_price($coin, $options);
                } elseif($preferred_exchange === 'bitpay') {
                    $prices = CW_ExchangeRates::get_bitpay_price($base_currency, 'realtime', 60);
                } elseif(in_array($preferred_exchange, array('bitstamp', 'coinbase', 'blockchain_info', 'bitcoincharts', 'coindesk', 'luno'))) {
                    // Use fiat currency for Bitstamp, Coinbase, Blockchain.info, Bitcoincharts.com, CoinDesk, and Luno.com
                    $function = sprintf('get_%s_price', $preferred_exchange);
                    $prices = CW_ExchangeRates::$function($base_currency);
                }  elseif($preferred_exchange === 'kraken') {
                    // Use coin and base currency fiat currency for Kraken
                    $prices = CW_ExchangeRates::get_kraken_price($coin, $base_currency);
                } else {
                    // All others
                    $function = sprintf('get_%s_price', $preferred_exchange);
                    $prices = CW_ExchangeRates::$function($coin, $options);
                }

                $coin_fiat = isset($prices[$method][$coin]['price']) ? $prices[$method][$coin]['price'] : 0;
                $status    = $prices[$method]['status'];
                $time      = gmdate('Y-m-d H:i:s', $prices[$method][$coin]['timestamp']);

                // Maybe save rate
                $maybe_save = CW_ExchangeRates::maybe_save_rate($coin, $coin_fiat, $preferred_exchange, $status, $method, $time, $prices, $options);

                if (is_array($maybe_save)) {
                    return $maybe_save;
                } else {
                    $prices['errors'][] = sprintf('%s|%s|%s', $preferred_exchange, $coin, json_encode($prices));
                }

                if (false === strpos($status, 'success')) { // If the API call failed try again with bitcoinaverage
                    // Bitcoinaverage Fallback  TODO: refactor

                    // Get the 24 average BTC price from bitcoinaverage.com
                    $prices = CW_ExchangeRates::get_bitcoinaverage_price($base_currency, 30);

                    $method = 'bitcoinaverage-fallback';

                    $coin_fiat = isset($prices['bitcoinaverage'][$coin]['price']) ? $prices['bitcoinaverage'][$coin]['price'] : 0;
                    $status    = $prices['bitcoinaverage']['status'];
                    $time      = gmdate('Y-m-d H:i:s', $prices['bitcoinaverage'][$coin]['timestamp']);

                    // Maybe save rate
                    $maybe_save = CW_ExchangeRates::maybe_save_rate($coin, $coin_fiat, $preferred_exchange, $status, $method, $time, $prices, $options);

                    if (is_array($maybe_save)) {
                        return $maybe_save;
                    } else {
                        $prices['errors'][] = sprintf('%s|%s|%s', $preferred_exchange, $coin, json_encode($prices));

                        $chain_so_currencies = array('USD', 'EUR', 'CNY', 'AUD', 'CAD');
                        if (in_array($base_currency, $chain_so_currencies)) { // Only fall back to chain.so if the base currency is supported at all (which doesn't mean there actually are rates).

                            // Try chain.so
                            // Get the price from chain.so
                            $prices = CW_ExchangeRates::get_chain_so_price('BTC', $options);
                            $method = 'chain_so-fallback';

                            $coin_fiat = isset($prices['chain_so'][$coin]['price']) ? $prices['chain_so'][$coin]['price'] : 0;
                            $status    = $prices['chain_so']['status'];
                            $time      = gmdate('Y-m-d H:i:s', $prices['chain_so'][$coin]['timestamp']);

                            // Maybe save rate
                            $maybe_save = CW_ExchangeRates::maybe_save_rate($coin, $coin_fiat, $preferred_exchange, $status, $method, $time, $prices, $options);

                            if (is_array($maybe_save)) {
                                return $maybe_save;
                            } else {
                                $prices['errors'] = sprintf('chain_so-fallback-BTC|%s', json_encode($prices));
                            }
                        } else {
                            $prices['errors'][] = sprintf('bitcoinaverage-BTC|%s', json_encode($prices));
                        }

                        // Everything goes to shit if we reach this point and don't get that exchange rate right now -  try bitstamp

                        $chain_so_currencies = array('USD', 'EUR');
                        if (in_array($base_currency, $chain_so_currencies)) { // Only fall back to bitstamp if the base currency is supported

                            // Last resort bitstamp
                            $prices = CW_ExchangeRates::get_bitstamp_price($base_currency);
                            $method = 'bitstamp-fallback';

                            $coin_fiat = isset($prices['bitstamp'][$coin]['price']) ? $prices['bitstamp'][$coin]['price'] : 0;
                            $status    = $prices['bitstamp']['status'];
                            $time      = gmdate('Y-m-d H:i:s', $prices['bitstamp'][$coin]['timestamp']);

                            // Maybe save rate
                            $maybe_save = CW_ExchangeRates::maybe_save_rate($coin, $coin_fiat, $preferred_exchange, $status, $method, $time, $prices, $options);

                            if (is_array($maybe_save)) {
                                // Phew - this one finally worked
                                return $maybe_save;
                            } else {
                                // We're out of options. Brace yourselves, shit is coming...
                                $prices['errors'] = sprintf('bitstamp-fallback-BTC|%s', json_encode($prices));
                            }
                        } else {
                            $prices['errors'] = sprintf('fallback-BTC|%s', json_encode($prices));
                        }

                    }
                }
                if (false === strpos($status, 'success')) {
                    // Inform admin about total failure
                    CW_ExchangeRates::maybe_warn_admin('error', false, $prices, gmdate('Y-m-d H:i:s'), $coin, $coin_fiat, $options);
                }
        } else {
            $next_update = 60 - $timeago;

            return array('status' => 'not updated',
                         'last_update' => "{$timeago}s ago - next update in {$next_update}s",
                         'time' => date('Y-m-d H:i:s'));
        }
        return $prices;
    }

    /**
     * Maybe save the new exchange rate
     *
     * @param $coin
     * @param $coin_fiat
     * @param $preferred_exchange
     * @param $status
     * @param $method
     * @param $time
     * @param $prices
     * @param $options
     * @return array|bool
     */
    static function maybe_save_rate($coin, $coin_fiat, $preferred_exchange, $status, $method, $time, $prices, $options) {

        if (is_numeric($coin_fiat) && $coin_fiat > 0) {

            $result = CW_ExchangeRates::save_rate($coin, $coin_fiat, $preferred_exchange, $status, $method, $time);

            $update_success = $result ? true : false;

            // Maybe inform admin about fallback
            CW_ExchangeRates::maybe_warn_admin($method, $update_success, $prices, $time, $coin, $coin_fiat, $options);

            return array('exchange' => $preferred_exchange,
                         'price' => $coin_fiat,
                         'status' => $status,
                         'method' => $method,
                         'time' => $time);
        } else {
            return false;
        }
    }

    /**
     * cURL helper function
     *
     * @param $url
     * @param bool $json
     * @param bool $user_agent
     * @param int $timeout
     * @param string $proxy // 'localhost:9050'
     * @return bool|mixed
     */
    static function curl_it($url, $json = true, $user_agent = false, $timeout = 10, $proxy = '') {

        /*
        modified from Bitcoin Payments for WooCommerce
        URI: http://www.bitcoinway.com/
        Version: 3.12
        Author: BitcoinWay
        */

        if (!function_exists('curl_init')) {
            $ret_val = @file_get_contents($url);

            return $ret_val;
        }

        // Initiate cURL and set headers/options
        $options = array(CURLOPT_URL => $url,
                         CURLOPT_RETURNTRANSFER => true,
                         // return web page
                         CURLOPT_HEADER => false,
                         // don't return headers
                         CURLOPT_ENCODING => "",
                         // handle compressed
                         CURLOPT_USERAGENT => $user_agent ? $user_agent : urlencode("Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.12 (KHTML, like Gecko) Chrome/9.0.576.0 Safari/534.12"),
                         // who am i
                         CURLOPT_AUTOREFERER => true,
                         // set referer on redirect
                         CURLOPT_CONNECTTIMEOUT => $timeout,
                         // timeout on connect
                         CURLOPT_TIMEOUT => $timeout,
                         // timeout on response in seconds.
                         CURLOPT_FOLLOWLOCATION => true,
                         // follow redirects
                         CURLOPT_MAXREDIRS => 10,
                         // stop after 10 redirects
                         CURLOPT_SSL_VERIFYPEER => true,
        );

        $ch = curl_init();

        if (function_exists('curl_setopt_array')) {
            curl_setopt_array($ch, $options);
        } else {
            // To accomodate older PHP 5.0.x systems
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     // return web page
            curl_setopt($ch, CURLOPT_HEADER, false);    // don't return headers
            curl_setopt($ch, CURLOPT_ENCODING, "");       // handle compressed
            curl_setopt($ch, CURLOPT_USERAGENT, $user_agent ? $user_agent : urlencode("Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.12 (KHTML, like Gecko) Chrome/9.0.576.0 Safari/534.12")); // who am i
            curl_setopt($ch, CURLOPT_AUTOREFERER, true);     // set referer on redirect
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);       // timeout on connect
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);       // timeout on response in seconds.
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);     // follow redirects
            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);       // stop after 10 redirects
            //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER  , false);    // Disable SSL verifications
        }


		if(!empty($proxy)) {
			curl_setopt ($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_PROXY, $proxy);
			curl_setopt ($ch, CURLOPT_PROXYTYPE, 7);
		}

        // Execute the cURL request
        $result = curl_exec($ch);

        $err    = curl_errno($ch);
        $header = curl_getinfo($ch);

        curl_close($ch);

        if ($err || $header['http_code'] != 200) {
            if (WP_DEBUG) {
                file_put_contents(CW_LOG_DIR . 'cryptowoo-curl-error.log', date("Y-m-d H:i:s") . 'curl header: ' . var_export($header, true) . ' response: ' . var_export($result, true) . var_export($err, true) . "\r\n", FILE_APPEND);
            }
            return false;
        }

        $json_result = json_decode($result);

        // Spit back the response object or fail
        if (!$json && !$err && $header['http_code'] == 200) {
            return $result ? $json_result : false;
        } else {
            //file_put_contents( CW_LOG_DIR . 'cryptowoo-rate-error.log', date( "Y-m-d H:i:s" ) . 'curl header: ' . var_export( $header, true ) . ' err: ' . var_export( $err, true ) . "\r\n", FILE_APPEND );
            return $err ? false : $result;
        }
    }

    /**
     * Get the exchange rate from shapeshift, cross-calculate Altcoin/Fiat values via BTC/USD
     *
     * https://shapeshift.io/rate/
     *
     * @param $currency
     * @return mixed
     */
    public static function get_shapeshift_price($currency) {
        $shapeshift_server = 'https://shapeshift.io/rate/';

        // Get the BTC/USD exchange rate from the database
        $btc_usd = self::get_exchange_rate('BTC');
        // Make sure $btc_usd amount cannot be 0 or less, return error if it is
        if (!is_numeric($btc_usd) || $btc_usd <= 0) {
            $prices['shapeshift']['DOGE']['price'] = 'error';
            $status['shapeshift']['DOGE']          = $prices['shapeshift']['status'] = 'error';
            return $prices;
        }

        // Set coin pair as $currency_btc, for example doge_btc
        $coin_pair = strtolower($currency . '_btc');

        //Get $currency/BTC market data
        $url        = $shapeshift_server . $coin_pair;
        $price_data = CW_ExchangeRates::curl_it($url, false, false, 8);

        // Verify that api returned correct pair, a rate, and no error
        $status['shapeshift'][$currency] = isset($price_data->pair) && isset($price_data->rate) && !isset($price_data->error) ? $price_data->pair == $coin_pair : false;

        // If getting pair rate from api was successful
        if ($status['shapeshift'][$currency]) {
            // Calculate $currency/USD
            $price = $prices['shapeshift'][$currency]['price'] = $btc_usd * $price_data->rate;

            // shapeshift has no timestamp, use current time instead
            $prices['shapeshift'][$currency]['timestamp'] = time();

            $prices['shapeshift']['status'] = 'success';

            // If getting pair rate from api failed
        } else {
            // return error
            $prices['shapeshift'][$currency]['price']     = 'error';
            $status['shapeshift'][$currency]              = isset($price_data->error) ? $price_data->error : 'shapeshift API error';
            $prices['shapeshift']['status']               = isset($price_data->error) ? $price_data->error : 'shapeshift API error';
            $prices['shapeshift'][$currency]['timestamp'] = time();
        }

        $validate_prices['shapeshift']['coin'] = $prices['shapeshift']['coin'] = $currency;
        $validate_prices['shapeshift']['coin'] = $prices['shapeshift'][$currency]['price'];

        // @todo validate exchange rate and data age
        $is_valid = $prices['shapeshift']['status'] && isset($price) ? true : false;//CW_ExchangeRates::validate_exchange_rate($price, $currency, 'shapeshift') : 0;

        $prices['shapeshift']['status'] = $is_valid ? 'success' : $prices['shapeshift']['status'];

        if (isset($prices['shapeshift']['status']) && strpos($prices['shapeshift']['status'], 'error')) {
            file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', date("Y-m-d H:i:s") . ' shapeshift error: ' . print_r($prices, true) . ' $status: ' . print_r($status, true) . "\r\n", FILE_APPEND);
        }

        return $prices;
    }

    /**
     * Get exchange rate from poloniex, cross-calculate fiat calculate Altcoin/Fiat values via BTC/Fiat
     *
     * @param bool $currency
     * @return mixed
     */
    static function get_poloniex_price($currency = false) {

    	// Maybe change coin name
	    $search_currency = apply_filters('cw_get_poloniex_price_coin', $currency);

	    $pair    = (bool)$currency ? sprintf('BTC_%s', strtoupper($search_currency)) : 'ALL';

        // Get the BTC/Fiat exchange rate from the database
        $btc_usd = self::get_exchange_rate('BTC');

        $request = wp_remote_get("https://poloniex.com/public?command=returnTicker");
        if (is_wp_error($request)) {
            $error = $request->get_error_message();
        } else {
            $return = json_decode($request['body']);
            if ($pair === 'ALL') {
                foreach ($return as $currency_pair => $data) {
                    $prices['poloniex'][$currency_pair] = $data;
                }
                $error                        = false;
            } else {
                $pair = strtoupper($pair);
                if (isset($return->$pair)) {
                    $prices['poloniex'][$currency]['price'] = isset($return->$pair->last) ? $return->$pair->last * $btc_usd : 'error';
                    $error                        = false;
                } else {
                    $error = true;
                }
            }
        }
        if ((bool)$error) {
            $prices['poloniex'][$pair]    = false;
            $prices['poloniex']['status'] = "{$error}|Exchange rate not found";
            file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', sprintf("%sPoloniex error: %s\n", date("Y-m-d H:i:s"), print_r($prices, true)), FILE_APPEND);
        } else {
            $prices['poloniex']['status'] = 'success';
        }
        $prices['poloniex'][$currency]['timestamp'] = time(); // No timestamp in Poloniex API response
        return $prices;
    }

    /**
     * Get exchange rate from bittrex.com, cross-calculate fiat calculate Altcoin/Fiat values via BTC/Fiat
     *
     * @param bool $currency
     * @return mixed
     */
    static function get_bittrex_price($currency) {

        // Get the BTC/Fiat exchange rate from the database
        $btc_usd = self::get_exchange_rate('BTC');

        $request = wp_remote_get("https://bittrex.com/api/v1.1/public/getticker/?market=BTC-{$currency}");
        if (is_wp_error($request)) {
            $message = $request->get_error_message();
            $error = !empty($message) ? $message : true;
        } else {
            $return = json_decode($request['body']);
            if (isset($return->success) && $return->success === true) {
                $result = $return->result;
                $prices['bittrex'][$currency]['price'] = isset($result->Last) ? $result->Last * $btc_usd : 'error';
                $error                                  = isset($return->message) && !empty($return->message) ? $return->message : false;
            } else {
                $error = true;
            }
        }
        if ((bool)$error) {
            $prices['bittrex'][$currency]    = false;
            $prices['bittrex']['status'] = "{$error}|Exchange rate not found";
            file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', sprintf("%sBittrex error: %s\n", date("Y-m-d H:i:s"), print_r($prices, true)), FILE_APPEND);
        } else {
            $prices['bittrex']['status'] = 'success';
        }
        $prices['bittrex'][$currency]['timestamp'] = time(); // No timestamp in Bittrex API response
        return $prices;
    }

    /**
     * Return the fiat exchange rate for $currency cross-calculate Altcoin/Fiat values via BTC/USD
     *
     * @param $currency
     * @param bool $force
     * @param bool $is_woocs
     * @return string
     */
    static function get_exchange_rate($currency, $force = false, $is_woocs = false) {

        global $wpdb;

        //if ($currency !== cw_get_woocommerce_currency()) { removed since switcher 1.1.5

        $begin = time();

        $cached = wp_cache_get($currency, 'cryptowoo-rates'); // get rates from WP object cache

        if (!$cached) {

            $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}cryptowoo_exchange_rates WHERE coin_type = '{$currency}'", ARRAY_A);
            $rates   = $results ? $results[0] : array();

            $rate = CW_Validate::check_if_unset('exchange_rate', $rates, false);

            // Maybe update rate
            $last_update = isset($rates['last_update']) ? strtotime($rates['last_update']) : 0;
            $time_diff   = $rates ? time() - $last_update : 1201;

            $updated = defined("UPDATED") ? UPDATED : false;
            $running = wp_cache_get('running', 'cryptowoo-rates'); // get status from WP object cache

            // Force update exchange rates if they are not in the database or older than 20 minutes
            if (!$is_woocs && !$updated && !$running && ($force || $time_diff > 1200 || !$rate)) {

                // Prevent query nesting
                wp_cache_set('running', true, 'cryptowoo-rates', 10);

                $time_diff = "updated - {$time_diff}";

                if($currency === 'BTC' || $currency === 'BTCTEST') {
                    $result = self::update_btc_fiat_rates(false, true);
                } else {
                    $result = self::update_altcoin_fiat_rates($currency);
                }
                define("UPDATED", true);

                if (isset($result['price']) && is_numeric($result['price'])) {
                    // Use new price from update result
                    $rate = $result['price'];
                } else {
                    $rate = 0;
                }

                if (UPDATED && WP_DEBUG) {
                    $updated = constant("UPDATED");
                    $data    = var_export($rates, true);
                    file_put_contents(CW_LOG_DIR . 'cryptowoo-rates.log', "\r\n====================\r\nBEGIN " . date("Y-m-d H:i:s", $begin) . "\r\nExchange Rate Status\r\nUpdated: " . $updated . "\r\n" . $data . "\r\nTime Difference: " . $time_diff . "\r\nEND " . date("Y-m-d H:i:s") . "\r\n====================\r\n", FILE_APPEND);
                }
            }

            // Save rate to WP object cache
            wp_cache_set($currency, $rate, 'cryptowoo-rates', 60);

                return $rate;
            } else {
                // Use rates from WP cache
                return $cached;
            }
    }

    /**
     * Return the fiat exchange rate for all currencies in the database
     *
     * @param $force
     * @return string
     */
    static function get_all_exchange_rates($force = true) {

        global $wpdb;

        $cached = wp_cache_get('all_rates', 'cryptowoo-rates'); // get rates from WP object cache

        if (!$cached) {

            $results = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'cryptowoo_exchange_rates', ARRAY_A);

            $rates = array();
            foreach ($results as $result) {
                if (array_key_exists($result['coin_type'], $rates)) {
                    //- we want the lowest exchange rate for now
                    if ($result['exchange_rate'] >= $rates[$result['coin_type']]['exchange_rate']) {
                        continue;
                    }
                }
                $rates[$result['coin_type']] = $result;
            }
            unset($results);
            // Save rate to WP object cache
            wp_cache_set('all_rates', $rates, 'cryptowoo-rates', 60);
            $cached = $rates;
        }
        return $cached;
    }

    /**
     * Return the fiat exchange rate for all currencies in the database
     *
     * @param $enabled_currencies
     * @return array
     */
    static function filter_enabled_currencies_without_rates($enabled_currencies) {

        global $wpdb;

        // Get rates from WP object cache
        $cached = wp_cache_get('all_rates', 'cryptowoo-rates');

        if (!$cached) {
            $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}cryptowoo_exchange_rates", ARRAY_A);

            $rates = array();
            foreach ($results as $result) {
                if (array_key_exists($result['coin_type'], $rates)) {
                    //- we want the lowest exchange rate for now
                    if ($result['exchange_rate'] >= $rates[$result['coin_type']]['exchange_rate']) {
                        continue;
                    }
                }
                $cached[$result['coin_type']] = $result;
            }
            unset($results);
            // Save rates to WP object cache
            wp_cache_set('all_rates', $cached, 'cryptowoo-rates', 60);
        }
        // Create currency => price and last_update array from enabled currencies without testnet coins
        $norates = array();
        foreach ($enabled_currencies as $key => $value) {
            if ($key !== 'BTCTEST' && $key !== 'DOGETEST' && !isset($cached[$key]['exchange_rate'])) {
                // Collect currencies that are enabled but have no rates in the DB
                $norates[$key] = $value;
                unset($enabled_currencies[$key]);
            }
        }
        if (count($norates)) {
            // Make currency unavailable on checkout if there are no rates. Keep for 30 seconds.
            set_transient('cryptowoo_norates', $norates, 30); // TODO tweak transient time
        }
        return $enabled_currencies;
    }

    /**
     * Save exchange rate to DB
     * @param $coin
     * @param $coin_fiat
     * @param $preferred_exchange
     * @param $status
     * @param $method
     * @param $time
     * @return false|int
     */
    static function save_rate($coin, $coin_fiat, $preferred_exchange, $status, $method, $time) {
        global $wpdb;
        date_default_timezone_set('UTC');
        $last_update = date("Y-m-d H:i:s");

        $result = $wpdb->query($wpdb->prepare("REPLACE INTO {$wpdb->prefix}cryptowoo_exchange_rates (coin_type, exchange_rate, exchange, status, method, last_update, api_timestamp) VALUES (%s, %f, %s, %s, %s, %s, %s)", $coin,//string
                                              $coin_fiat,//float
                                              $preferred_exchange,//string
                                              $status,//string
                                              $method,//string
                                              $last_update,//string
                                              $time)//string
        //s,f,s,s,s,s
        );

        // Save rate to WP object cache
        wp_cache_set($coin, $coin_fiat, 'cryptowoo-rates', 60); // wp_cache_set( $currency, $rate, 'cryptowoo-rates', $expire );

        return $result;
    }

    /**
     * Maybe display backend notice and send e-Mail
     * @todo revisit error stats, maybe attach logfile upon rotation?
     *
     * @param $used_method
     * @param $update_success
     * @param $prices
     * @param $time
     * @param $coin
     * @param $coin_fiat
     * @param bool $options
     */
    public static function maybe_warn_admin($used_method, $update_success, $prices, $time, $coin, $coin_fiat, $options = false) {

        $rate_error_transient = get_transient('cryptowoo_rate_errors');

        if (!$options) {
            $options = get_option('cryptowoo_payments');
        }

        $exchange_key = sprintf('preferred_exchange_%s', strtolower(str_replace('TEST', '', $coin)));
        $preferred_exchange = isset($options[$exchange_key]) ? $options[$exchange_key] : $exchange_key ;

        date_default_timezone_set('UTC');
        $api_time                                 = strtotime($time);
        $prices[$used_method]['api_time']         = $time;
        $prices[$used_method]['time_lag_seconds'] = time() - $api_time;
        $prices[$used_method]['time_lag_minutes'] = round($prices[$used_method]['time_lag_seconds'] / 60, 2);

        if ($preferred_exchange !== $used_method || !$update_success || $prices[$used_method]['time_lag_seconds'] > 1800) {

            // Start counting if this is the first email in 60 minutes or use time from transient
            $rate_errors['counter_start'] = isset($rate_error_transient['error_count']) && (int)$rate_error_transient['error_count'] >= 1 ? $rate_error_transient['counter_start'] : time();

            // Increase error counter
            $rate_errors['error_count'] = isset($rate_error_transient['error_count']) ? (int)$rate_error_transient['error_count'] + 1 : 1;

            // Sent count
            $rate_errors['sent_count'] = isset($rate_error_transient['sent_count']) ? (int)$rate_error_transient['sent_count'] : 0;

            /*
            // Human readable time
            $rate_errors['counter_start_date'] = date( "Y-m-d H:i:s", $rate_errors['counter_start'] );

            // Calculate time since we started counting
            $last_error = time() - $rate_errors['counter_start'];

            // Reset counter after 60 minutes
            if ( (int) $last_error >= 3600 ) {
                $rate_errors['error_count_previous_period'] = $rate_errors['error_count'];
                $rate_errors['error_count']                 = 0;
            }
            */

            // Add error stats to email data
            $prices['error_stats'] = $rate_errors;

            $status = var_export($prices, true); //print_r($prices, true);
            //$update_success = print_r($update_success, true);
            $update_success = $update_success ? 'success' : 'error';

            // Maybe prepare JSON data
            if((bool)$options['rate_error_charts']) {
                $json_log = array('time' => $api_time,
                                  'preferred_exchange' => $preferred_exchange,
                                  'used_exchange' => $used_method,
                                  'coin' => $coin,
                                  'coin_fiat' => $coin_fiat,
                                  'status' => $update_success,
                                  'time_lag_seconds' => time() - $api_time,
                                  'time_lag_minutes' => round($prices[$used_method]['time_lag_seconds'] / 60, 2),
                                  'counter_start' => $rate_errors['counter_start'],
                                  'error_count' => $rate_errors['error_count']);

                CW_AdminMain::cryptowoo_log_json($json_log);
            }

            $full_log_file = $file = sprintf('%scryptowoo-rate-error.log', CW_LOG_DIR);
            $logrotate = false;
            // Check filesize
            if (file_exists($file)) {
                //$data['sizestats'] = 'Size of logfile is '.cw_human_filesize(filesize($file));
                $size = filesize($file);

                // Rename if larger than 10MB
                if ($size > 10000000) {
                    $full_log_file = sprintf('%scryptowoo-rate-errors-until-%s.log', CW_LOG_DIR, date('Y-m-d'));
                    rename($file, $full_log_file);
                    $logrotate = true;
                }
            }

            if ((int)$rate_errors['error_count'] <= 1 || $logrotate) {

                $to       = get_option('admin_email');
                $blogname = get_bloginfo('name', 'raw');
                $subject  = sprintf(__('%s: CryptoWoo exchange rate update errors', 'cryptowoo'), $blogname);

                //$error_count = isset( $prices['error_stats']['error_count'] ) ? $prices['error_stats']['error_count'] : '%undefined%';
                $date = isset($prices['error_stats']['counter_start_date']) ? $prices['error_stats']['counter_start_date'] : date('d. M Y H:i:s');

                $db_actions_page = sprintf('<a href="%1$s">%1$s</a><br>', admin_url('admin.php?page=cryptowoo_database_maintenance'));
                $text = __("Hello Admin,<br>CryptoWoo has detected %s error(s) since %s while updating the exchange rates via %s:<br>
                Please log in at %s, check your settings, reset the error counter via the button on the database maintenance page, and try to update the rates manually.<br>%s", 'cryptowoo');
                $message = CW_Formatting::cw_get_template_html('email-header', $subject);
                $message .= sprintf($text, $rate_errors['error_count'], $date, $preferred_exchange, $blogname, $db_actions_page);

                // Maybe add logrotation info
                $message .= $logrotate ? sprintf('%s%s%s%s', '<br>', __('All errors up to now have been logged to', 'cryptowoo'), $full_log_file, __('You should inspect that file to find out what is causing the rate updates to fail.', 'cryptowoo'), '<br>') : '';

                $text_2 = __("All subsequent errors will be logged to %s until you log in and reset the error counter.<br>
                If the manual update fails try to select a different exchange rate API.<br>
                If you continue to receive this e-Mail after selecting another exchange rate API, please submit a ticket at http://cryptowoo.zendesk.com <br>
                Preferred method: %s <br>Used method: %s<br>Update Data: <br>%s<br>
                <br>Update status: %s<br>", 'cryptowoo');
                $message .= sprintf($text_2, $file, $preferred_exchange, $used_method, $status, $update_success);

                $message .= CW_Formatting::cw_get_template_html('email-footer');

                $headers = array("From: CryptoWoo Plugin <{$to}>",
                                 'Content-Type: text/html; charset=UTF-8');

                wp_mail($to, $subject, $message, $headers);
	            $rate_errors['sent_count'] = isset($rate_errors['sent_count']) ? $rate_errors['sent_count']++ : 1;
            }

            file_put_contents($file, sprintf("%s %s rate update: %s <br>status: %s", date("Y-m-d H:i:s"), $used_method, $update_success, $status), FILE_APPEND);
            // Keep transient for one week after the last error
            set_transient('cryptowoo_rate_errors', $rate_errors, (DAY_IN_SECONDS * 7));
        }
    }

    /**
     * Get Bitcoinaverage exchange rate (BTC)
     *
     * $rate_type: 'vwap' | 'realtime' | 'bestrate'
     *
     * @param string $currency_code
     * @param $rate_type
     * @param $timeout
     * @return bool
     */
    static function v1_get_bitcoinaverage_price($currency_code = 'USD', $rate_type, $timeout) {
        date_default_timezone_set('UTC');

        $source_url = "https://api.bitcoinaverage.com/ticker/global/{$currency_code}/";
        $result     = CW_ExchangeRates::curl_it($source_url, true, $timeout); //@BWWC__file_get_contents ($source_url, false, $bwwc_settings['exchange_rate_api_timeout_secs']);

        /*	{
          "24h_avg": 260.05,
          "ask": 254.55,
          "bid": 254.18,
          "last": 254.55,
          "timestamp": "Wed, 28 Jan 2015 18:06:47 -0000",
          "volume_btc": 94095.19,
          "volume_percent": 89.98
        }
        */

        $rate_obj = @json_decode(trim($result), true);

        if (!is_array($rate_obj)) {
            return false;
        }

        $prices['bitcoinaverage']['BTC']['timestamp'] = isset($rate_obj['timestamp']) ? strtotime($rate_obj['timestamp']): time();
        $prices['bitcoinaverage']['coin']      = 'BTC';

        $timediff = time() - $prices['bitcoinaverage']['BTC']['timestamp'];

        if ($timediff > 666) { // if data is older than 11.1 minutes
            $prices['bitcoinaverage']['status'] = "success - timediff: {$timediff}";
        } else {
            $prices['bitcoinaverage']['status'] = 'success';
        }

        if (@$rate_obj['24h_avg']) {
            $rate_24h_avg = @$rate_obj['24h_avg'];
        } else {
            if (@$rate_obj['last'] && @$rate_obj['ask'] && @$rate_obj['bid']) {
                $rate_24h_avg = ($rate_obj['last'] + $rate_obj['ask'] + $rate_obj['bid']) / 3;
            } else {
                $rate_24h_avg = @$rate_obj['last'];
            }
        }

        switch ($rate_type) {
            case 'vwap'    :
                $price = $prices['bitcoinaverage']['BTC']['price'] = $rate_24h_avg;
                break;
            case 'realtime'    :
                $price = $prices['bitcoinaverage']['BTC']['price'] = @$rate_obj['last'];
                break;
            case 'bestrate'    :
            default:
                $price = $prices['bitcoinaverage']['BTC']['price'] = min($rate_24h_avg, @$rate_obj['last']);
            break;
        }


        $is_valid = isset($price) && false !== strpos($prices['bitcoinaverage']['status'], 'success') ? CW_ExchangeRates::validate_exchange_rate($price, 'BTC', 'bitcoinaverage') : 0;

        if ($is_valid && $price > 0) {
            $prices['bitcoinaverage']['status'] = 'success';

            return $prices;
        } else {
            if (!$is_valid) {
                $prices['bitcoinaverage']['status'] = 'error-notvalid';
            } else {
                $prices['bitcoinaverage']['status'] = 'error';
            }

            if (isset($prices['bitcoinaverage']['status']) && strpos($prices['bitcoinaverage']['status'], 'error')) {
                file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', date("Y-m-d H:i:s") . ' Bitcoinaverage error: ' . print_r($prices, true) . "\r\n", FILE_APPEND);
            }

            return $prices;
        }
    }

    /**
     * Get Bitcoinaverage exchange rate (BTC) via API v2
     * Request limit 5,000/month (~6.9) per hour -> cache for 8.5 minutes
     *
     * @param string $currency_code
     * @param $timeout
     * @return bool
     */
    static function get_bitcoinaverage_price($currency_code = 'USD', $timeout) {
        date_default_timezone_set('UTC');

        $rate_transient = 'bitcoinaverage-rates';
        if(false !== ($prices = get_transient($rate_transient))) {
            return $prices;
        }
        $market = sprintf('BTC%s', $currency_code);
        $source_url = "https://apiv2.bitcoinaverage.com/indices/global/ticker/short?crypto=BTC&fiats={$currency_code}";
        // TODO enable different rate types via https://apiv2.bitcoinaverage.com/indices/global/ticker/BTCUSD
        $result     = CW_ExchangeRates::curl_it($source_url, true, $timeout);

        /*	{
                "BTCUSD": {
                    "averages": {
                        "day": 730.06
                    },
                    "last": 727.53
                }
            }
        */

        $rates = json_decode(trim($result));

        if (!is_object($rates)) {
            return false;
        }

        $prices['bitcoinaverage']['BTC']['timestamp'] = time(); //isset($rates->time) ? strtotime($rates->time): time();
        $prices['bitcoinaverage']['coin']      = 'BTC';

        $timediff = time() - $prices['bitcoinaverage']['BTC']['timestamp'];

        if ($timediff > 666) { // if data is older than 11.1 minutes
            $prices['bitcoinaverage']['status'] = "success - timediff: {$timediff}";
        } else {
            $prices['bitcoinaverage']['status'] = 'success';
        }

        $prices['bitcoinaverage']['BTC']['price'] = isset($rates->$market->last) ? $rates->$market->last : 0;

        $is_valid = false !== strpos($prices['bitcoinaverage']['status'], 'success') ? CW_ExchangeRates::validate_exchange_rate($prices['bitcoinaverage']['BTC']['price'], 'BTC', 'bitcoinaverage') : 0;

        if ($is_valid && $prices['bitcoinaverage']['BTC']['price'] > 0) {
            $prices['bitcoinaverage']['status'] = 'success';
            set_transient($rate_transient, $prices, 610); // Cache for 10:10 min to prevent rate limiting
            return $prices;
        } else {
            if (!$is_valid) {
                $prices['bitcoinaverage']['status'] = 'error-notvalid';
            } else {
                $prices['bitcoinaverage']['status'] = 'error';
            }

            if (isset($prices['bitcoinaverage']['status']) && strpos($prices['bitcoinaverage']['status'], 'error')) {
                file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', date("Y-m-d H:i:s") . ' Bitcoinaverage error: ' . print_r($prices, true) . "\r\n", FILE_APPEND);
            }

            return $prices;
        }
    }

    /**
     * Compare the exchange rate from the API response with previous rates
     *
     * @param $price
     * @param $currency
     * @param $exchange
     * @return bool
     * @todo review float difference calculation via epsilon
     */
    static function validate_exchange_rate($price, $currency, $exchange) {

        return is_numeric($price) && $price > 0;
        /*
        global $wpdb;

        $result = array();
        $result['is_valid'] = true;

        $query = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}cryptowoo_exchange_rates
                                       WHERE `coin_type` = '$currency';");

        if (!empty($query)) {
            $exchange_rate = $result['exchange_rate'] = $query[0]->exchange_rate;

            // Calculate maximum rate difference of 15%
            $epsilon = (float)$exchange_rate * 0.15;

            // Invalid if difference is larger than 15% -> use fallback
            if(abs((float)$exchange_rate-(float)$price) > $epsilon) {
                 $result['is_valid'] = false;
            }
        }
        if(!$result['is_valid']) {
            file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', date("Y-m-d H:i:s") . " ==Rate validation error==\n". print_r($result, true) . "\r\n", FILE_APPEND);
        }
        return $result['is_valid'];
        */
    }

    /**
     * Get Bitpay exchange rate (BTC)
     * @param $currency_code
     * @param $rate_type
     * @param $timeout
     * @return mixed
     */
    static function get_bitpay_price($currency_code, $rate_type, $timeout) {
        $source_url = "https://bitpay.com/api/rates";
        $result     = CW_ExchangeRates::curl_it($source_url, true, $timeout);

        $prices['bitpay']['coin']   = 'BTC';
        $prices['bitpay']['status'] = 'error';

        $rate_objs = @json_decode(trim($result), true);
        if (!is_array($rate_objs)) {
            $prices['bitpay']['status'] = 'error';
        }//false;

        foreach ($rate_objs as $rate_obj) {
            if (@$rate_obj['code'] == $currency_code) {
                // Only realtime rate is available
                $price = $prices['bitpay']['BTC']['price'] = @$rate_obj['rate'];

                $is_valid = isset($price) ? true : false; //CW_ExchangeRates::validate_exchange_rate($price, 'BTC', 'bitpay' ) : 0;

                if ($is_valid && $prices['bitpay']['BTC']['price'] > 0) {
                    $prices['bitpay']['status'] = 'success';
                } else {
                    $prices['bitpay']['status'] = 'error-notvalid';
                }
            }
        }
        if (isset($prices['bitpay']['status']) && strpos($prices['bitpay']['status'], 'error')) {
            file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', sprintf("%s Bitpay error: %s\n", date("Y-m-d H:i:s"), print_r($prices, true)), FILE_APPEND);
        }
        if(!isset($prices['bitpay']['BTC']['timestamp'])) {
            $prices['bitpay']['BTC']['timestamp'] = time();
        }

        return $prices;
    }

    /**
     * Get Bitfinex exchange rate (BTC, LTC)
     *
     * @param $currency
     * @param string $base
     * @return mixed
     */
    public static function get_bitfinex_price($currency, $base = 'USD') {
        date_default_timezone_set('UTC');
        switch ($currency) {
            default:
            case ('BTC'):
                // DEFAULT: Get BTC/USD market data
                $url      = 'https://api.bitfinex.com/v1/pubticker/BTCUSD';
                $btc_data = CW_ExchangeRates::curl_it($url, false);

                $status['bitfinex']['BTC'] = 1;

                if ($status['bitfinex']['BTC'] == 1) {
                    $prices['bitfinex']['BTC']['price']     = $btc_data->bid;
                    $prices['bitfinex']['BTC']['timestamp'] = $btc_data->timestamp;
                    $prices['bitfinex']['BTC']['data_age']  = time() - $prices['bitfinex']['BTC']['timestamp'];
                    $prices['bitfinex']['status']           = 'success';
                } else {
                    // return error
                    $prices['bitfinex']['BTC']['price'] = 'error';
                    $status['bitfinex']['BTC']          = $btc_data->error;
                    $prices['bitfinex']['status']       = $btc_data->error;

                }
                break;
            case ('LTC'):

                /*
                //Get BTC/USD market data
                $url = 'https://api.bitfinex.com/v1/pubticker/BTCUSD';
                $btc_data = CW_ExchangeRates::curl_it($url, false);

                $status['bitfinex']['BTC'] = 1;

                if($status['bitfinex']['BTC'] == 1) {
                    $prices['bitfinex']['BTC']['price'] = $btc_data->bid;
                    $prices['bitfinex']['BTC']['timestamp'] = $btc_data->timestamp;
                    $prices['bitfinex']['BTC']['data_age'] = time() - $prices['bitfinex']['BTC']['timestamp'];
                    $prices['bitfinex']['status'] = 'success';
                } else {
                    // return error
                    $prices['bitfinex']['BTC']['price'] = 'error';
                    $status['bitfinex']['BTC'] = $btc_data->error;
                    $prices['bitfinex']['status'] = $btc_data->error;
                }
                */

                // Get the BTC/USD exchange rate from the database
                $btc_usd = self::get_exchange_rate('BTC');

                if (is_numeric($btc_usd) && $btc_usd > 0) {

                    // Get LTC/USD market data
                    $marketid = '3';

                    $url      = 'https://api.bitfinex.com/v1/pubticker/LTCBTC';
                    $ltc_data = CW_ExchangeRates::curl_it($url, false);

                    $status['bitfinex']['LTC']              = 1;
                    $prices['bitfinex']['LTC']['timestamp'] = $ltc_data->timestamp;

                    // @todo get timezone difference
                    $prices['bitfinex']['LTC']['data_age'] = time() - $prices['bitfinex']['LTC']['timestamp'];

                    if ($status['bitfinex']['LTC'] == 1) {
                        // Calculate LTC/USD
                        $prices['bitfinex']['LTC']['price'] = (float)$btc_usd * (float)$ltc_data->ask;
                        $prices['bitfinex']['status']       = 'success';
                    } else {
                        // return error
                        $prices['bitfinex']['LTC']['price'] = 'error';
                        $status['bitfinex']['LTC']          = $ltc_data->error;
                        $prices['bitfinex']['status']       = $ltc_data->error;
                    }
                } else {
                    $prices['bitfinex']['LTC']['price'] = 'error';
                    $status['bitfinex']['LTC']          = $prices['bitfinex']['status'] = 'error';
                }
                break;
        }
        if (isset($prices['bitfinex']['status']) && strpos($prices['bitfinex']['status'], 'error')) {
            file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', date("Y-m-d H:i:s") . ' Bitfinex error: ' . print_r($prices, true) . ' $status: ' . print_r($status, true) . "\r\n", FILE_APPEND);
        }
        if(!isset($prices['bitfinex'][$currency]['timestamp'])) {
            $prices['bitfinex'][$currency]['timestamp'] = time();
        }

        return $prices;
    }

    /**
     * Get BTC-e exchange rates (BTC, LTC)
     *
     * @param $currency
     * @param string $base
     * @return mixed
     */
    public static function get_btc_e_price($currency, $base = 'USD') {
        date_default_timezone_set('UTC');
        switch ($currency) {
            default:
            case ('BTC'):
                // DEFAULT: Get BTC/USD market data
                $url      = 'https://btc-e.com/api/3/ticker/btc_usd';
                $btc_data = CW_ExchangeRates::curl_it($url, false);

                $status['btc_e']['BTC'] = $btc_data ? 1 : 0;

                if ($status['btc_e']['BTC'] == 1) {
                    $prices['btc_e']['BTC']['price']     = $btc_data->btc_usd->buy;
                    $prices['btc_e']['BTC']['timestamp'] = $btc_data->btc_usd->updated;
                    $prices['btc_e']['BTC']['data_age']  = time() - $prices['btc_e']['BTC']['timestamp'];
                    $prices['btc_e']['status']           = 'success';

                } else {
                    // return error
                    $prices['btc_e']['BTC']['timestamp'] = time();
                    $prices['btc_e']['BTC']['price']     = 'error';
                    $status['btc_e']['BTC']              = isset($btc_data['error']) ? $btc_data['error'] : 'invalid API response';
                    $prices['btc_e']['status']           = isset($btc_data['error']) ? $btc_data['error'] : 'error';

                }
                break;
            case ('LTC'):

                /*
                //Get BTC/USD market data
                $url = 'https://btc-e.com/api/3/ticker/btc_usd';
                $btc_data = CW_ExchangeRates::curl_it($url);

                $status['btc_e']['BTC'] = 1;

                if($status['btc_e']['BTC'] == 1) {
                    $prices['btc_e']['BTC']['price'] = $btc_data->btc_usd->buy;
                    $prices['btc_e']['BTC']['timestamp'] = $btc_data->btc_usd->updated;
                    $prices['btc_e']['BTC']['data_age'] = time() - $prices['btc_e']['BTC']['timestamp'];
                    $prices['btc_e']['status'] = 'success';
                } else {
                    // return error
                    $prices['btc_e']['BTC']['price'] = 'error';
                    $status['btc_e']['BTC'] = $btc_data->error;
                    $prices['btc_e']['status'] = $btc_data->error;
                }
                */
                // Get the BTC/USD exchange rate from the database
                $btc_usd = self::get_exchange_rate('BTC');

                if (is_numeric($btc_usd) && $btc_usd > 0) {

                    // Get LTC/BTC market data
                    $marketid = '3';
                    $url      = 'https://btc-e.com/api/3/ticker/ltc_btc';
                    $ltc_data = CW_ExchangeRates::curl_it($url, false);

                    $status['btc_e']['LTC']              = 1;
                    $prices['btc_e']['LTC']['timestamp'] = $ltc_data->ltc_btc->updated;

                    // @todo get timezone difference
                    $prices['btc_e']['LTC']['data_age'] = time() - $prices['btc_e']['LTC']['timestamp'];

                    if ($status['btc_e']['LTC'] == 1) {
                        // Calculate LTC/USD
                        $prices['btc_e']['LTC']['price'] = $btc_usd * $ltc_data->ltc_btc->sell;
                        $prices['btc_e']['status']       = 'success';
                    } else {
                        // return error
                        $prices['btc_e']['LTC']['price'] = 'error';
                        $status['btc_e']['LTC']          = $ltc_data->error;
                        $prices['btc_e']['status']       = $ltc_data->error;
                    }
                } else {
                    $prices['btc_e']['LTC']['price'] = 'error';
                    $status['btc_e']['LTC']          = $prices['btc_e']['status'] = 'error';
                }
                break;
        }
        if (isset($prices['btc_e']['status']) && strpos($prices['btc_e']['status'], 'error')) {
            file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', date("Y-m-d H:i:s") . ' BTC-e error: ' . print_r($prices, true) . ' $status: ' . print_r($status, true) . "\r\n", FILE_APPEND);
        }
        if(!isset($prices['btc_e'][$currency]['timestamp'])) {
            $prices['btc_e'][$currency]['timestamp'] = time();
        }

        return $prices;
    }

    /**
     * Return the lowest exchange rate from Block.io
     *
     * @param $currency
     * @param bool $options
     * @return mixed
     */
    public static function get_blockio_price($currency, $options = false) {

        date_default_timezone_set('UTC');

        if (!$options) {
            $options = get_option('cryptowoo_payments');
        }

        $woocommerce_currency = get_option('woocommerce_currency'); // unfiltered base currency

        $base_currency = $currency === 'BTC' ? $woocommerce_currency : 'BTC';

        // Get the BTC/USD exchange rate from the database
        $btc_db = self::get_exchange_rate('BTC');

        // Set to 1 if currency is BTC
        $btc_usd = $currency === 'BTC' ? 1 : $btc_db;

        /*
        // Get the exchange rates from all available exchanges

            $api_key = WC_CryptoWoo::get_address_api_key($currency);
            $blockio = new BlockIo($api_key, '');

            $btc_fiat = $blockio->get_current_price(array('price_base' => $base_currency));
            $status = $btc_fiat->{'status'};

            if ($btc_fiat) {
                foreach ($btc_fiat->data->prices as $price) {

                    $wpdb->query(
                        "REPLACE INTO {$wpdb->prefix}cryptowoo_exchange_rates " .
                        "(`coin_type`, `exchange_rate`, `exchange`, `status`, `last_update`) " .
                        "VALUES " .
                        "('$coin', '" . $price->price . "', '" . $price->exchange . "', '$status', NOW())"
                    );
                }

                return $status;
            }
        */
        // Get only the lowest exchange rate

        $api_key   = CW_AdminMain::get_blockio_api_key($options, $currency, true);
        $blockio   = new BlockIo($api_key, '');
        try {
            $coin_fiat = $blockio->get_current_price(array('price_base' => $base_currency));
            $error_message = '';
        } catch(Exception $e) {
            $coin_fiat = new stdClass();
            $coin_fiat->status = 'error';
            $error_message = $e->getMessage();
        }

        $prices = isset($coin_fiat->data->prices) ? (array)$coin_fiat->data->prices : array();
        if (!empty($prices)) {
            // Create exchange => price array
            foreach ($prices as $price) {
                if (isset($price->price) && $price->price > 0) {
                    $price_array[$price->exchange] = $price->price;
                    $time_array[$price->exchange]  = $price->time;
                }
            }

            // sort price array: lowest exchange rate to highest
            // @todo create option to select rate from specific exchange
            $sort_prices = asort($price_array, SORT_NUMERIC);

            if ($sort_prices) {
                // pick first value in sorted array (= best rate for merchant) and multiply with BTC/USD from DB
                $prices['blockio'][$currency]['price']    = reset($price_array) * $btc_usd;
                $exchange                      = key($price_array);
                $prices['blockio']['exchange'] = $exchange;
                $prices['blockio'][$currency]['timestamp']     = isset($time_array[$exchange]) ? $time_array[$exchange] : time();
            }
        }
        if (isset($coin_fiat->status) && $coin_fiat->status !== 'success') {
            $prices['blockio']['status'] = isset($coin_fiat->data->error_message) ? sprintf('%s|%s', $coin_fiat->data->error_message, $error_message) : $error_message;
        } else {
            $prices['blockio']['status'] = isset($coin_fiat->status) ? $coin_fiat->status : 'no response';
            $prices['blockio']['coin']   = $currency;
        }

        // @todo validate exchange rate and data age
        $is_valid = isset($prices['blockio'][$currency]['price']) && $prices['blockio']['status'] === 'success' ? true : false; //CW_ExchangeRates::validate_exchange_rate($rate, $currency, 'blockio') : 0;

        $prices['blockio']['status'] = $is_valid ? 'success' : $prices['blockio']['status'] . '-error-notvalid';

        if (isset($prices['blockio']['status']) && strpos($prices['blockio']['status'], 'error')) {
            file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', date("Y-m-d H:i:s") . ' Block.io error: ' . print_r($prices, true) . "\r\n", FILE_APPEND);
        }
        if(!isset($prices['blockio'][$currency]['timestamp'])) {
            $prices['blockio'][$currency]['timestamp'] = time();
        }

        return $prices;
    }

    /**
     * Return the lowest exchange rate from chain.so
     *
     * @param $currency
     * @param bool $options
     * @return mixed
     */
    public static function get_chain_so_price($currency, $options = false) {

        date_default_timezone_set('UTC');

        if (!$options) {
            $options = get_option('cryptowoo_payments');
        }

        $woocommerce_currency = get_option('woocommerce_currency'); // unfiltered base currency

        $base_currency = $currency === 'BTC' ? $woocommerce_currency : 'BTC';

        // Get the BTC/USD exchange rate from the database
        $btc_db = self::get_exchange_rate('BTC');

        // Set to 1 if currency is BTC
        $btc_usd = $currency === 'BTC' ? 1 : $btc_db;

        // Get only the lowest exchange rate
        $url       = $currency === 'BTC' ? 'https://chain.so/api/v2/get_price/BTC/' . $woocommerce_currency : 'https://chain.so/api/v2/get_price/' . $currency . '/BTC';
        $coin_fiat = CW_ExchangeRates::curl_it($url, false);

	    $all_prices = isset($coin_fiat->data->prices) && is_array($coin_fiat->data->prices) ? (array)$coin_fiat->data->prices : array();

        // Create exchange => price array
        $price_array = array();
        foreach ($all_prices as $price) {
            if (isset($price->price) && $price->price > 0) {
                $price_array[$price->exchange] = $price->price;
                $time_array[$price->exchange]  = $price->time;
            }
        }
        $prices['chain_so']['status'] = 'error';
        // sort price array: lowest exchange rate to highest
        // @todo create option to select rate from specific exchange
        $sort_prices = asort($price_array, SORT_NUMERIC);

        if ($sort_prices && (bool)$all_prices) {
            // pick first value in sorted array (= best rate for merchant) and multiply with BTC/USD from DB
	        $lowest = reset($price_array);
            $prices['chain_so'][$currency]['price']    = $lowest > 0 ? $lowest * $btc_usd : 0;
            $exchange                       = key($price_array);
            $prices['chain_so']['exchange'] = $exchange;
            $prices['chain_so'][$currency]['timestamp']     = isset($time_array[$exchange]) ? $time_array[$exchange] : time();
        }
        if (isset($coin_fiat->status) && $coin_fiat->status != 'success') {
            $prices['chain_so']['status'] = $coin_fiat->data->error_message;
        } else {
            $prices['chain_so']['status'] = isset($coin_fiat->status) ? $coin_fiat->status : 'no response';
            $prices['chain_so']['coin']   = $currency;
        }

        // @todo validate exchange rate and data age
        $is_valid = isset($prices['chain_so'][$currency]['price']) && isset($prices['chain_so'][$currency]['price']) > 0 && $prices['chain_so']['status'] === 'success' ? true : false; //CW_ExchangeRates::validate_exchange_rate($rate, $currency, 'chain_so') : 0;

        $prices['chain_so']['status'] = $is_valid ? 'success' : $prices['chain_so']['status'] . '-error-notvalid';

        if (strpos($prices['chain_so']['status'], 'error')) {
            file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', date("Y-m-d H:i:s") . ' Chain.so error: ' . print_r($prices, true) . "\r\n", FILE_APPEND);
        }
        if(!isset($prices['chain_so'][$currency]['timestamp'])) {
            $prices['chain_so'][$currency]['timestamp'] = time();
        }

        return $prices;
    } // function get_chain_so_price

    /**
     * Get Dogecoinaverage exchange rate (DOGE)
     *
     * @param $currency_code
     * @param string $rate_type
     * @param int $timeout
     * @return mixed
     */
    public static function get_dogecoinaverage_price($currency_code = 'BTC', $rate_type = 'vwap', $timeout = 15) {
        date_default_timezone_set('UTC');

        // Get the BTC/USD exchange rate from the database
        $btc_db = self::get_exchange_rate('BTC');

        $source_url = 'http://dogecoinaverage.com/BTC.json';
        $result     = CW_ExchangeRates::curl_it($source_url, true, $timeout);

        //$prices['dogecoinaverage']['coin'] = $currency_code;

        $rate_objs = @json_decode(trim($result), true);
        if (!is_array($rate_objs)) {
            $prices['dogecoinaverage']['status'] = 'error';//false;
            return $prices;
        } else {
            $prices['dogecoinaverage']['coin']      = $rate_objs['currency_code'];
            $prices['dogecoinaverage']['DOGE']['timestamp'] = $rate_objs['ts'];
            $prices['dogecoinaverage']['DOGE']['data_age']  = time() - $rate_objs['ts'];
            $price                                  = $prices['dogecoinaverage']['DOGE']['price'] = $rate_objs['vwap'] * $btc_db;

            $is_valid = isset($price) ? true : false; //CW_ExchangeRates::validate_exchange_rate($price, 'BTC', 'dogecoinaverage' ) : 0;

            if ($is_valid && $prices['dogecoinaverage']['DOGE']['price'] > 0) {
                $prices['dogecoinaverage']['status'] = 'success';
            } else {
                $prices['dogecoinaverage']['status'] = 'error-notvalid';
            }
        }
        if (isset($prices['dogecoinaverage']['status']) && strpos($prices['dogecoinaverage']['status'], 'error')) {
            file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', date("Y-m-d H:i:s") . ' Dogecoinaverage error: ' . print_r($prices, true) . "\r\n", FILE_APPEND);
        }

        return $prices;
    }
    /**
     * Get Blockchain.info exchange rate (Fiat/BTC)
     *
     * @param $currency_code
     * @param string $rate_type
     * @param int $timeout
     * @return mixed
     */
    public static function get_blockchain_info_price($currency_code = 'USD', $rate_type = 'last', $timeout = 15) {
	    date_default_timezone_set( 'UTC' );

	    $options = get_option('cryptowoo_payments');
	    $source_url = 'https://blockchain.info/ticker';
	    $proxy = false;

	    if(isset($options['bc_info_tor']) && !empty($options['bc_info_tor'])) {
		    $source_url = 'https://blockchainbdgpzk.onion/ticker';
		    $proxy      = $options['bc_info_tor'];
	    }

	    $result     = CW_ExchangeRates::curl_it( $source_url, true, false, $timeout, $proxy );

	    $rate_obj = @json_decode( trim( $result ), true );

	    $prices['blockchain_info']['BTC']['timestamp'] = time(); // Not provided
	    $prices['blockchain_info']['coin']             = 'BTC'; // BTC only

	    $price = $prices['blockchain_info']['BTC']['price'] = isset( $rate_obj[ $currency_code ][ $rate_type ] ) ? $rate_obj[ $currency_code ][ $rate_type ] : false;

	    $is_valid = CW_ExchangeRates::validate_exchange_rate( $price, 'BTC', 'blockchain_info' );

	    if ( $is_valid ) {
		    $prices['blockchain_info']['status'] = 'success';

		    return $prices;
	    } else {
		    $prices['blockchain_info']['status'] = 'error';
		    file_put_contents( CW_LOG_DIR . 'cryptowoo-rate-error.log', date( 'Y-m-d H:i:s' ) . ' Blockchain.info error: ' . print_r( $prices, true ) . "\r\n", FILE_APPEND );
	    }

	    return $prices;
    }

    /**
     * Display exchange rates from DB
     *
     * @package CW_ExchangeRates
     */
    public function get_exchange_rates() {
        global $wpdb;
        $query = $wpdb->get_results("SELECT last_update, api_timestamp, coin_type, exchange_rate, exchange, status, method FROM {$wpdb->prefix}cryptowoo_exchange_rates;");

        $rates = __('<div id="message" class="error fade"><p class="flash_message">No exchange rates found in database. Please update manually.</p></div>', 'cryptowoo');

        //$currencies = array();
        if (!empty($query)) {

            date_default_timezone_set('UTC');
            $woocommerce_currency = cw_get_woocommerce_currency();
            $schedule             = wp_get_schedule('cryptowoo_cron_action');

            $schedule_seconds = str_replace('seconds_', '', $schedule);

            $rates = '<br><div id="dvData"><table class="cw-table" style="border: 1px solid black;">
					<thead>
					<tr><th colspan="9" style="text-align:center;">Current Rates in Database</th></tr>
						<tr><th>Rate</th>
							<th>Currency</th>
							<th>Exchange</th>
							<th>Method</th>
							<th>Status</th>
							<th>Last Run</th>
							<th>API Time</th>
							<th>Data Lag</th>
							<th>API URL</th>
						</tr></thead>' . "\n";

            foreach ($query as $currency) {

                /*
                $last_update = $currency->last_update;
                $exchange_rate = $currency->exchange_rate;
                $coin = $currency->coin_type;
                $exchange = $currency->exchange;
                $method = $currency->method;
                $status = $currency->status;
                $api_timestamp = $currency->api_timestamp;
                */
                // Get API URL
                $api_url = CW_ExchangeRates::get_rate_api_url($currency);
                $last_update      = strtotime($currency->last_update);
                $last_run_sec_ago = time() - $last_update;
                $data_lag         = $last_update - strtotime($currency->api_timestamp);

                // take cron schedule into account when evaluating last run status
                $execution_status = (int)$last_run_sec_ago - (int)$schedule_seconds;

                if ((int)$execution_status <= 65) {
                    // all good: last run timestamp within 15 seconds on schedule
                    $last_run_color = 'color:green;';
                } elseif ((int)$execution_status > 65 && (int)$execution_status < 135) {
                    // last rate update more than 60 seconds after current schedule: warn
                    $last_run_color = 'background-color:yellow; font-weight: bold;';
                } else {
                    // error: ĺast update is more than 60 seconds after schedule
                    $last_run_color = !strpos($currency->status, 'disabled') ? 'color:#B94A48; font-weight: bold;' : '';
                }

                // API response status colour
                $td_color_status = $currency->status == 'success' || strpos($currency->status, 'disabled') ? 'color:green;' : 'background-color:yellow; font-weight: bold;';

                // API Lag colour
                if ($data_lag >= 300 && $data_lag < 666) {
                    $td_color_time = 'background-color:yellow; font-weight: bold;';
                } elseif ($data_lag < 300) {
                    $td_color_time = 'color:green;';
                } else {
                    $td_color_time = false === strpos($currency->status, 'disabled') ? 'color:#B94A48; font-weight: bold;' : '';
                }

                $rates .= '<tr>';
                $rates .= '<td class="cw-bold">' . CW_Formatting::fbits($currency->exchange_rate, false) . '</td>';

                $print_currency = $woocommerce_currency . '/' . $currency->coin_type;

                $rates .= '<td class="bits" style="text-align: left;">' . $print_currency . '</td>';
                $rates .= '<td class="bits" style="text-align: center;">' . $currency->exchange . '</td>';
                $rates .= '<td class="bits" style="text-align: center;">' . $currency->method . '</td>';
                $rates .= '<td class="cw-bold" style="text-align: center;' . $td_color_status . '">' . $currency->status . '</td>';
                $rates .= '<td class="bits" style="border: 1px solid black; ' . $last_run_color . '">' . $last_run_sec_ago . ' sec ago (' . $currency->last_update . ')</td>';
                $rates .= '<td class="bits" style="border: 1px solid black;">' . $currency->api_timestamp . '</td>';
                $rates .= '<td class="bits" style="' . $td_color_time . '">' . (int)$data_lag . ' sec</td>';
                $rates .= '<td class="bits" style="text-align: left;">';
                $rates .= $api_url ? '<a href="' . $api_url . '" target="_blank" title="Open API URL">' . $api_url . '</a>' : 'N/A';
                $rates .= '</td>';
                $rates .= '</tr>';
            }
            $rates .= '</table></div>';
        } else {
            $rates = '<div id="message" class="error fade"><p class="flash_message">No exchange rates found in database. Please update manually.</p></div>';
        }

        return $rates;
    }

    static function get_rate_api_url($currency) { // TODO refactor

        $api_url = false;
        $base_currency = cw_get_woocommerce_currency();

        // Get API URL
        switch ($currency->method) {
            case('shapeshift') :
                $lc_coin_type = strtolower($currency->coin_type);
                $api_url = "https://shapeshift.io/rate/{$lc_coin_type}_btc";
                break;
            case('bitcoinaverage') :
            case('bitcoinaverage-fallback') :
                $api_url = "https://api.bitcoinaverage.com/ticker/global/{$base_currency}/";
                break;
            case('bitpay') :
                $api_url = 'https://bitpay.com/api/rates';
                break;
            case('bitfinex') :
                if ($currency->coin_type === 'BTC') {
                    $api_url = 'https://api.bitfinex.com/v1/pubticker/BTCUSD';
                } elseif ($currency->coin_type === 'LTC') {
                    $api_url = 'https://api.bitfinex.com/v1/pubticker/LTCBTC';
                }
                break;
            case('btc_e') :
                if ($currency->coin_type === 'BTC') {
                    $api_url = 'https://btc-e.com/api/3/ticker/btc_usd';
                } elseif ($currency->coin_type === 'LTC') {
                    $api_url = 'https://btc-e.com/api/3/ticker/ltc_btc';
                }
                break;
            case('blockio') :
                $api_url = 'http://status.block.io/';
                break;
            case('chain_so') :
            case('chain_so-fallback') :
                if ($currency->coin_type === 'BTC') {
                    $api_url = 'https://chain.so/api/v2/get_price/BTC/USD';
                } else {
                    $api_url = "https://chain.so/api/v2/get_price/{$currency->coin_type}/BTC";
                }
                break;
            case('dogecoinaverage') :
            case('dogecoinaverage-fallback') :
                $api_url = 'http://dogecoinaverage.com/BTC.json';
                break;
            case('poloniex') :
            case('poloniex-fallback') :
                $api_url = 'https://poloniex.com/public?command=returnTicker';
                break;
            case('bittrex') :
            case('bittrex-fallback') :
                $api_url = "https://bittrex.com/api/v1.1/public/getticker/?market=BTC-{$currency->coin_type}";
                break;
            case('coinbase') :
            case('coinbase-fallback') :
                $api_url = "https://api.gdax.com/products/BTC-{$base_currency}/ticker";
                break;
            case('bitstamp') :
            case('bitstamp-fallback') :
                $api_url = sprintf('https://www.bitstamp.net/api/v2/ticker/btc%s', strtolower($base_currency));
                break;
            case('blockchain_info') :
            case('blockchain_info-fallback') :
                $api_url = 'https://blockchain.info/ticker';
                break;
            case('bitcoincharts') :
            case('bitcoincharts-fallback') :
                $api_url = 'http://api.bitcoincharts.com/v1/weighted_prices.json';
                break;
            case('coindesk') :
            case('coindesk-fallback') :
                $api_url = "http://api.coindesk.com/v1/bpi/currentprice/{$base_currency}.json";
                break;
            case('luno') :
            case('luno-fallback') :
                $api_url = "https://api.mybitx.com/api/1/ticker?pair=XBT{$base_currency}";
                break;
            case('okcoin') :
            case('okcoin-fallback') :
                $api_url = sprintf('https://www.okcoin.com/api/v1/ticker.do?symbol=%s_usd', strtolower($currency->coin_type));
                break;
            case('okcoincn') :
            case('okcoincn-fallback') :
                $api_url = sprintf('https://www.okcoin.cn/api/v1/ticker.do?symbol=%s_cny', strtolower($currency->coin_type));
                break;
            case('kraken') :
            case('kraken-fallback') :
                $api_url = sprintf('https://api.kraken.com/0/public/Ticker?pair=%s%s', $currency->coin_type, $base_currency);
                break;
            case('livecoin') :
            case('livecoin-fallback') :
                $api_url = sprintf('https://api.livecoin.net/exchange/ticker?currencyPair=%s/BTC', $currency->coin_type);
                break;
        }
        return $api_url;
    }

    /**
     * Exchange API nice name
     *
     * @param $exchange_id
     * @return string
     */
    static function get_exchange_nicename($exchange_id) { // TODO refactor
        switch ($exchange_id) {
            case('shapeshift') :
                $exchange = 'ShapeShift';
                break;
            case('bitcoinaverage') :
            case('bitcoinaverage-fallback') :
                $exchange = 'Bitcoinaverage';
                break;
            case('bitpay') :
                $exchange = 'BitPay';
                break;
            case('bitfinex') :
                $exchange = 'Bitfinex';
                break;
            case('btc_e') :
                $exchange = 'BTC-E';
                break;
            case('blockio') :
                $exchange = 'Block.io';
                break;
            case('chain_so') :
            case('chain_so-fallback') :
                $exchange = 'Chain.so';
                break;
            case('dogecoinaverage') :
            case('dogecoinaverage-fallback') :
                $exchange = 'Dogecoinaverage';
                break;
            case('poloniex') :
            case('poloniex-fallback') :
                $exchange = 'Poloniex';
                break;
            case('bittrex') :
            case('bittrex-fallback') :
                $exchange = 'Bittrex';
                break;
            case('coinbase') :
            case('coinbase-fallback') :
                $exchange = 'GDAX';
                break;
            case('bitstamp') :
            case('bitstamp-fallback') :
                $exchange = 'Bitstamp';
                break;
            case('blockchain_info') :
            case('blockchain_info-fallback') :
                $exchange = 'Blockchain.info';
                break;
	        case('bitcoincharts') :
            case('bitcoincharts-fallback') :
                $exchange = 'Bitcoincharts.com';
                break;
	        case('coindesk') :
	        case('coindesk-fallback') :
                $exchange = 'CoinDesk';
                break;
	        case('luno') :
	        case('luno-fallback') :
                $exchange = 'Luno';
                break;
	        case('okcoin') :
	        case('okcoin-fallback') :
                $exchange = 'OKCoin.com';
                break;
	        case('okcoincn') :
	        case('okcoincn-fallback') :
                $exchange = 'OKCoin.cn';
                break;
	        case('kraken') :
	        case('kraken-fallback') :
                $exchange = 'Kraken';
                break;
	        case('livecoin') :
	        case('livecoin-fallback') :
                $exchange = 'Livecoin.net';
                break;
            default :
                $exchange = '';
                break;
        }
        return $exchange;
    }

	/**
	 * Get all exchanges
	 *
	 * @param $cryptocurrency
	 * @param string $base_currency
	 *
	 * @return array
	 */
	static function get_exchanges($cryptocurrency, $base_currency = 'USD') { // TODO refactor
		$lc_coin_type = strtolower( $cryptocurrency );
		$lc_base_currency = strtolower( $base_currency );

		$exchanges    = array(
			'shapeshift'      => array(
				'nicename' => 'ShapeShift',
				'api_url'  => "https://shapeshift.io/rate/{$lc_coin_type}_btc",
			),
			'bitcoinaverage'  => array(
				'nicename' => 'Bitcoinaverage',
				'api_url'  => "https://api.bitcoinaverage.com/ticker/global/{$base_currency}/",
			),
			'bitpay'          => array(
				'nicename' => 'Shapeshift',
				'api_url'  => "https://bitpay.com/api/rates",
			),
			'bitfinex'        => array(
				'nicename' => 'Bitfinex',
				'api_url'  => $cryptocurrency === 'LTC' ? 'https://api.bitfinex.com/v1/pubticker/LTCBTC' : 'https://api.bitfinex.com/v1/pubticker/BTCUSD'
			),
			'btc_e'           => array(
				'nicename' => 'BTC-E',
				'api_url'  => $cryptocurrency === 'LTC' ? 'https://btc-e.com/api/3/ticker/ltc_btc' : 'https://btc-e.com/api/3/ticker/ltc_btc',
			),
			'blockio'         => array(
				'nicename' => 'Block.io',
				'api_url'  => 'http://status.block.io/',
			),
			'chain_so'        => array(
				'nicename' => 'Chain.so',
				'api_url'  => $cryptocurrency === 'BTC' ? 'https://chain.so/api/v2/get_price/BTC/USD' : "https://chain.so/api/v2/get_price/{$cryptocurrency}/BTC",
			),
			'dogecoinaverage' => array(
				'nicename' => 'Dogecoinaverage',
				'api_url'  => 'http://dogecoinaverage.com/BTC.json',
			),
			'poloniex'        => array(
				'nicename' => 'Poloniex',
				'api_url'  => 'https://poloniex.com/public?command=returnTicker',
			),
			'bittrex'         => array(
				'nicename' => 'Bittrex',
				'api_url'  => "https://bittrex.com/api/v1.1/public/getticker/?market=BTC-{$cryptocurrency}"
			),
			'coinbase'        => array(
				'nicename' => 'GDAX (Coinbase)',
				'api_url'  => "https://api.gdax.com/products/BTC-{$base_currency}/ticker",
			),
			'bitstamp'        => array(
				'nicename' => 'Bitstamp',
				'api_url'  => "https://www.bitstamp.net/api/v2/ticker/btc{$lc_base_currency}",
			),
			'blockchain_info' => array(
				'nicename' => 'Blockchain.info',
				'api_url'  => "https://blockchain.info/ticker",
			),
			'bitcoincharts'   => array(
				'nicename' => 'Bitcoincharts.com',
				'api_url'  => 'http://api.bitcoincharts.com/v1/weighted_prices.json',
			),
			'coindesk'        => array(
				'nicename' => 'CoinDesk',
				'api_url'  => "http://api.coindesk.com/v1/bpi/currentprice/{$base_currency}.json",
			),
			'luno'        => array(
				'nicename' => 'Luno',
				'api_url'  => "https://api.mybitx.com/api/1/ticker?pair=XBT{$base_currency}",
			),
			'okcoin'        => array(
				'nicename' => 'OKCoin.com',
				'api_url'  => "https://www.okcoin.com/api/v1/ticker.do?symbol={$lc_coin_type}_usd",
			),
			'okcoincn'        => array(
				'nicename' => 'OKCoin.cn',
				'api_url'  => "https://www.okcoin.cn/api/v1/ticker.do?symbol={$lc_coin_type}_cny",
			),
			'kraken'        => array(
				'nicename' => 'Kraken',
				'api_url'  => "https://api.kraken.com/0/public/Ticker?pair={$cryptocurrency}{$base_currency}",
			),
			'livecoin'        => array(
				'nicename' => 'Livecoin.net',
				'api_url'  => "https://api.livecoin.net/exchange/ticker?currencyPair={$cryptocurrency}/BTC",
			),
		);

		return $exchanges;

	}

    /**
     * Get Bitcoincharts exchange rate
     *
     * Max 4 Requests / hour -> only use as fallback
     * $rate_type: 'vwap' | 'realtime' | 'bestrate'
     *
     * @param $currency_code
     * @param $timeout
     * @return bool
     */
    static function get_bitcoincharts_price($currency_code, $timeout = 10) {
        $prices['bitcoincharts']['coin'] = 'BTC';
	    $error = $currency_code;

	    $rate_transient = 'bitcoincharts-rates';
	    if(false && false !== ($prices = get_transient($rate_transient))) {
		    return $prices;
	    }

        $source_url = 'http://api.bitcoincharts.com/v1/weighted_prices.json'; //"http://api.bitcoincharts.com/v1/markets.json";
	    $rates      = CW_ExchangeRates::curl_it($source_url, true, $timeout);
	    $rates      = json_decode($rates, true);

        if(is_array($rates)) {

	        $prices['bitcoincharts']['BTC']['price'] = isset($rates[$currency_code]['24h']) ? $rates[$currency_code]['24h'] : false;

        } else {
        	$error .= '|cURL error';
        }
	    $prices['bitcoincharts']['BTC']['timestamp'] = time();


	    if ($prices['bitcoincharts']['BTC']['price']) {
            $prices['bitcoincharts']['status'] = 'success';
	        set_transient($rate_transient, $prices, 910); // Cache for 15:10 min to prevent rate limiting
        } else {
	        $prices['bitcoincharts']['status'] = "{$error}|Exchange rate not found";
	        file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', sprintf("%s Bitcoincharts error: %s\n", date("Y-m-d H:i:s"), print_r($prices, true)), FILE_APPEND);
            return false;
        }
	    return $prices;
    }

    /**
     * Get exchange rate from coinbase
     * @param $fiat_currency
     * @return mixed
     */
    static function get_coinbase_price($fiat_currency = 'USD') {
        $error = true;
        $return = false;
        $currency = strtoupper($fiat_currency);
        $request = self::curl_it("https://api.gdax.com/products/BTC-{$currency}/ticker", true);
        if (is_wp_error($request)) {
            $error = $request->get_error_message();
        } else {
            $return = json_decode($request);
            if (is_object($return)) {
                $prices['coinbase']['BTC']['price'] = $return->price;
                $error = false;
            }
        }
        if ((bool)$error) {
            $prices['coinbase']['BTC']    = false;
            $prices['coinbase']['status'] = "{$error}|Exchange rate not found";
            file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', sprintf("%s Coinbase error: %s\n", date("Y-m-d H:i:s"), print_r($prices, true)), FILE_APPEND);
        } else {
            $prices['coinbase']['status'] = 'success';
        }
        $prices['coinbase']['BTC']['timestamp'] = is_object($return) && isset($return->time) ? strtotime($return->time) : time();
        return $prices;
    }

    /**
     * Get exchange rate from bitstamp
     * @param $fiat_currency
     * @return mixed
     */
    static function get_bitstamp_price($fiat_currency = 'USD') {
        $error = true;
        $return = false;
        $currency = strtolower($fiat_currency);
        $request = self::curl_it("https://www.bitstamp.net/api/v2/ticker/btc{$currency}/", true);
        if (is_wp_error($request)) {
            $error = $request->get_error_message();
        } else {
            $return = json_decode($request);
            if (is_object($return)) {
                $prices['bitstamp']['BTC']['price'] = isset($return->last) ? $return->last : false;
                $error = false;
            }
        }
        if ((bool)$error || (isset($prices['bitstamp']['BTC']['price']) && !(bool)$prices['bitstamp']['BTC']['price'])) {
            $prices['bitstamp']['BTC']    = false;
            $prices['bitstamp']['status'] = "{$error}|Exchange rate not found";
            file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', sprintf("%s Bitstamp error: %s\n", date("Y-m-d H:i:s"), print_r($prices, true)), FILE_APPEND);
        } else {
            $prices['bitstamp']['status'] = 'success';
        }
        $prices['bitstamp']['BTC']['timestamp'] = is_object($return) && isset($return->timestamp) ? $return->timestamp : time();
        return $prices;
    }

	/**
	 * Get exchange rate from Coindesk BPI
	 * @param $fiat_currency
	 * @return mixed
	 */
	static function get_coindesk_price($fiat_currency = 'USD') {
		$error = true;
		$return = false;
		$currency = strtoupper($fiat_currency);
		$request = self::curl_it("http://api.coindesk.com/v1/bpi/currentprice/{$currency}.json", true);
		if (is_wp_error($request)) {
			$error = $request->get_error_message();
		} else {
			$return = json_decode($request);
			if (is_object($return) && isset($return->bpi)) {
				$bpi = $return->bpi;
				if(isset($bpi->$fiat_currency)) {
					$prices['coindesk']['BTC']['price'] = $bpi->$fiat_currency->rate_float;
					$error                              = false;
				}
			}
		}
		if ((bool)$error) {
			$prices['coindesk']['BTC']    = false;
			$prices['coindesk']['status'] = "{$error}|Exchange rate not found";
			file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', sprintf("%s Coindesk error: %s\n", date("Y-m-d H:i:s"), print_r($prices, true)), FILE_APPEND);
		} else {
			$prices['coindesk']['status'] = 'success';
		}
		$time = is_object($return) && isset($return->time) ? isset($return->time->updatedISO) ? strtotime($return->time->updatedISO) : time() : false;
		$prices['coindesk']['BTC']['timestamp'] =  $time ? : time();
		return $prices;
	}

	/**
	 * Get exchange rate from Luno exchange (luno.com)
	 * @param $fiat_currency
	 * @return mixed
	 */
	static function get_luno_price($fiat_currency = 'ZAR') {
		$error = true;
		$currency = strtoupper($fiat_currency);
		$request = wp_remote_get("https://api.mybitx.com/api/1/ticker?pair=XBT{$currency}");
		if (is_wp_error($request) || !isset($request['body'])) {
			$error = $request->get_error_message();
		} else {
			$return = json_decode($request['body']);
			if (is_object($return) && isset($return->last_trade)) {
					$prices['luno']['BTC']['price'] = $return->last_trade;
					$error                              = false;
					$time = isset($return->timestamp) ? absint($return->timestamp / 1000) : time();
			}
		}
		if ((bool)$error) {
			$prices['luno']['BTC']    = false;
			$prices['luno']['status'] = "{$error}|Exchange rate not found";
			file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', sprintf("%s Luno error: %s\n", date("Y-m-d H:i:s"), print_r($prices, true)), FILE_APPEND);
		} else {
			$prices['luno']['status'] = 'success';
		}
		$prices['luno']['BTC']['timestamp'] =  isset($time) ? $time : time();
		return $prices;
	}

	/**
	 * Get exchange rate from okcoin.com
	 * @param $coin
	 * @param $options
	 * @return mixed
	 */
	static function get_okcoin_price($coin = 'BTC', $options = array()) {
		$error = true;
		$lc_currency = strtolower($coin);
		$request = wp_remote_get("https://www.okcoin.com/api/v1/ticker.do?symbol={$lc_currency}_usd");
		if (is_wp_error($request) || !isset($request['body'])) {
			$error = $request->get_error_message();
		} else {
			$return = json_decode($request['body']);
			if (is_object($return) && isset($return->ticker)) {
					$prices['okcoin'][$coin]['price'] = $return->ticker->last;
					$error                              = false;
					$time = isset($return->date) ? $return->date : time();
			}
		}
		if ((bool)$error) {
			$prices['okcoin'][$coin]    = false;
			$prices['okcoin']['status'] = "{$error}|Exchange rate not found";
			file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', sprintf("%s OKCoin.com error: %s\n", date("Y-m-d H:i:s"), print_r($prices, true)), FILE_APPEND);
		} else {
			$prices['okcoin']['status'] = 'success';
		}
		$prices['okcoin'][$coin]['timestamp'] =  isset($time) ? $time : time();
		return $prices;
	}

	/**
	 * Get exchange rate from okcoin.cn
	 * @param $coin
	 * @return mixed
	 */
	static function get_okcoincn_price($coin = 'BTC', $options = array()) {
		$error = true;
		$lc_currency = strtolower($coin);
		$request = wp_remote_get("https://www.okcoin.cn/api/v1/ticker.do?symbol={$lc_currency}_usd");
		if (is_wp_error($request) || !isset($request['body'])) {
			$error = $request->get_error_message();
		} else {
			$return = json_decode($request['body']);
			if (is_object($return) && isset($return->ticker)) {
					$prices['okcoincn'][$coin]['price'] = $return->ticker->last;
					$error                              = false;
					$time = isset($return->date) ? $return->date : time();
			}
		}
		if ((bool)$error) {
			$prices['okcoincn'][$coin]    = false;
			$prices['okcoincn']['status'] = "{$error}|Exchange rate not found";
			file_put_contents(CW_LOG_DIR . 'cryptowoo-rate-error.log', sprintf("%s OKCoin.cn error: %s\n", date("Y-m-d H:i:s"), print_r($prices, true)), FILE_APPEND);
		} else {
			$prices['okcoincn']['status'] = 'success';
		}
		$prices['okcoincn'][$coin]['timestamp'] =  isset($time) ? $time : time();
		return $prices;
	}

	/**
	 * Get exchange rate from kraken.com
	 * @param $coin
	 * @param $base_currency
	 * @return mixed
	 */
	static function get_kraken_price($coin = 'BTC', $base_currency) {
		$error = true;

		$xcoin = $coin === 'BTC' ? 'XBT' : $coin;

		$ticker_pair = sprintf( 'X%sZ%s', $xcoin, $base_currency );

		$url     = sprintf( 'https://api.kraken.com/0/public/Ticker?pair=X%sZ%s', strtoupper( $xcoin ), $base_currency );
		$request = wp_remote_get( $url );
		if ( is_wp_error( $request ) || ! isset( $request['body'] ) ) {
			$error = $request->get_error_message();
		} else {
			$return = json_decode( $request['body'] );
			if ( isset( $return->result ) && isset( $return->result->$ticker_pair ) ) {
				$last = $return->result->$ticker_pair->c;
				if ( isset( $last[0] ) ) {
					$prices['kraken'][ $coin ]['price'] = $last[0];
					$error                              = ! empty( $return->error ) ? $return->error : false;
				}
				$time = time();
			}
		}
		if ( (bool) $error ) {
			$prices['kraken'][ $coin ]  = false;
			$prices['kraken']['status'] = "{$error}|Exchange rate not found";
			file_put_contents( CW_LOG_DIR . 'cryptowoo-rate-error.log', sprintf( "%s Kraken error: %s\n", date( "Y-m-d H:i:s" ), print_r( $prices, true ) ), FILE_APPEND );
		} else {
			$prices['kraken']['status'] = 'success';
		}
		$prices['kraken'][ $coin ]['timestamp'] = isset( $time ) ? $time : time();

		return $prices;
	}

	/**
	 * Get exchange rate from livecoin.net
	 * @param $coin
	 * @param $base_currency
	 * @return mixed
	 */
	static function get_livecoin_price($coin = 'DIBC', $base_currency = 'BTC') {
		$error = true;

		if($coin === 'OMNI') {
			$coin = 'DIBC';
		}

		// Get the BTC/USD exchange rate from the database
		$btc_usd = self::get_exchange_rate('BTC');

		$url     = sprintf( 'https://api.livecoin.net/exchange/ticker?currencyPair=%s/BTC', strtoupper( $coin ), $base_currency );
		$request = wp_remote_get( $url );

		if ( is_wp_error( $request ) || ! isset( $request['body'] ) ) {
			$error = $request->get_error_message();
		} else {
			$return = json_decode( $request['body'] );
			if ( isset( $return->last ) && $return->last ) {
					$prices['livecoin'][ $coin ]['price'] = $btc_usd * $return->last;
			}
			$time = time();
			$error                              = isset( $return->errorMessage ) ? $return->errorMessage : false;
		}
		if ( (bool) $error ) {
			$prices['livecoin'][ $coin ]  = false;
			$prices['livecoin']['status'] = "{$error}|Exchange rate not found";
			file_put_contents( CW_LOG_DIR . 'cryptowoo-rate-error.log', sprintf( "%s Livecoin error: %s\n", date( "Y-m-d H:i:s" ), print_r( $prices, true ) ), FILE_APPEND );
		} else {
			$prices['livecoin']['status'] = 'success';
		}
		$prices['livecoin'][ $coin ]['timestamp'] = isset( $time ) ? $time : time();

		return $prices;
	}

}// END Class CW_ExchangeRates

