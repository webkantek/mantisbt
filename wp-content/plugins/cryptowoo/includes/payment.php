<?php if (!defined('ABSPATH')) {die();}// Exit if accessed directly
if (!isset($payment_address) || !isset($crypto_amount) || $crypto_amount <= 0) {
    $redirect = CW_OrderProcessing::decline_order('error', $order_id);
    wp_safe_redirect($redirect);
    exit;
}
?>
<div class="cryptowoo-pay cw-row">
    <div class="cw-row">
        <div class="cw-col-12">
        <?php // Notices
        if(isset($message) && !empty($message)) {
	        printf('<span class="cryptowoo-message">%s</span>', $message );
        }?>
        </div>
    </div>
    <div class="cw-row">
        <div class="cw-col-12">
        <?php // Custom payment page instructions
        if(isset($options['payment_page_text']) && !empty($options['payment_page_text'])) {
            printf('<span style="clear: both;">%s</span>',$options['payment_page_text']);
        }?>
        </div>
    </div>
    <div class="cw-row">
        <?php $colsize = isset($options['payment_page_width']) && is_numeric($options['payment_page_width']) ? $options['payment_page_width'] : 8; ?>
        <div class="cw-col-<?php echo $colsize; ?>">
            <div class="cw-row">
                <!-- Send -->
                <div class="cw-col-2 cw-bold">
	                <?php esc_html_e('Send:', 'cryptowoo'); ?>
                </div>
                <div class="cw-col-5">
                    <span class="cryptowoo-blocktext copywrap-amount" id="amount" onclick="selectText('amount')" style="display:inline;"><?php echo CW_Formatting::fbits($crypto_amount, true, $wallet_config['decimals'], true); ?></span> <?php esc_html_e($payment_currency); ?>
                </div>
            </div>
            <div class="cw-row">
                <!-- To -->
                <div class="cw-col-2 cw-bold">
	                <?php esc_html_e('To:', 'cryptowoo'); ?>
                </div>
                <div class="cw-col-10 cw-label">
                    <span class="cryptowoo-blocktext copywrap-address" id="payment-address" onclick="selectText('payment-address')" ><?php esc_html_e($payment_address); ?></span>
                    <div class="cryptowoo-smalltext nojs">
                        <!-- Open wallet client -->
                        <a class="cw-tooltip" href="<?php echo esc_url($qr_data, $coin_protocols, false); ?>">
                            <span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x" aria-hidden="true"></i><i class="fa fa-external-link fa-stack-1x fa-inverse" aria-hidden="true"></i></span>
                            <span class="cw-tt-info"> <?php printf(esc_html__('Open %s client', 'cryptowoo'),  $wallet_config['coin_client']); ?> </span>
                        </a>
                        <!-- Link to block chain -->
                        <a class="cw-tooltip" href="<?php echo esc_url($url); ?>" target="_blank">
                            <span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x" aria-hidden="true"></i><i class="fa fa-cube fa-stack-1x fa-inverse" aria-hidden="true"></i></span>
                            <span class="cw-tt-info"> <?php printf(esc_html__('View address on %s block chain.', 'cryptowoo'), $wallet_config['coin_client']); ?> </span>
                        </a>
                    </div>
                    <div id="addr_img" class="hidden">
                        <?php
                        // Security image
                        if(isset($options['sec_image']) && $options['sec_image'] !== 'disabled') {
                            if( $order->get_total() >= (float)$options['sec_image']) {
	                           echo CW_Formatting::generate_sec_image($payment_address);
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="cw-row">
                <div class="cw-col-7 cw-label">
                    <!-- Unconfirmed -->
                    <div class="cw-row">
                        <div class="cw-col-6 cw-bold">
					        <?php esc_html_e('Unconfirmed:', 'cryptowoo'); ?>
                        </div>
                        <div class="cw-col-6">
                            <span class="cryptowoo-blocktext copywrap-amount"><span id="cw-unconfirmed" >0<?php echo $decimal_sep; ?>00</span></span>
                        </div>
                    </div>
                    <!-- Confirmed -->
                    <div class="cw-row">
                        <div class="cw-col-6 cw-bold">
					        <?php esc_html_e('Confirmed:', 'cryptowoo'); ?>
                        </div>
                        <div class="cw-col-6">
                            <span class="cryptowoo-blocktext copywrap-amount"><span id="cw-confirmed" >0<?php echo $decimal_sep; ?>00</span></span>
                        </div>
                    </div>
                    <!-- Countdown -->
                    <div class="cw-row" id="countdown-timer">
                        <div class="cw-col-6 cw-bold">
					        <?php esc_html_e('Time Left:', 'cryptowoo'); ?>
                        </div>
                        <div class="cw-col-6">
                            <span class="cryptowoo-blocktext copywrap-amount"><span class="countdown"></span></span>
                        </div>
                    </div>
                </div>
                <div class="cw-col-5 cw-label">
                    <!-- Check Payment -->
                    <a id="check" href="#" class="btn-check-payment"><span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x" aria-hidden="true"></i><i id="cw-loading" class="fa fa-refresh fa-stack-1x fa-inverse" aria-hidden="true"></i></span></a> <?php esc_html_e('Check Status', 'cryptowoo'); ?>
                </div>
                <div class="cw-col-12">
                    <!-- Amount incoming -->
                    <div id="amount-incoming" class="woocommerce-message hidden">
                        <i class="fa fa-check" aria-hidden="true"></i> <?php esc_html_e('Incoming transaction detected. You will receive an email when your payment is fully confirmed.', 'cryptowoo'); ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- QR Code -->
        <div class="cw-col-4">
            <div style="width:230px;">
                <a href="<?php echo esc_url($qr_data, $coin_protocols, false); ?>" title="Scan QR code or click to open wallet client."><div class="cryptowoo-qr" id="qrcode"></div></a>
            </div>
        </div>
    </div>
<?php if($shifty['enabled']) { ?>
    <div class="cw-row">
        <div class="cw-col-6 nojs ss-button">
				<!--  Shapeshift Button -->
				<script>function shapeshift_click(a,e){e.preventDefault();var link=a.href;window.open(link,'1418115287605','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=0,left=0,top=0');return false;}</script>
				<a class="cw-tooltip-large-top" onclick="shapeshift_click(this, event);" href="<?php echo esc_url($shifty['data']); ?>"><img src="<?php echo CWOO_PLUGIN_PATH; ?>assets/images/small_light_altcoins.png">
					<?php printf(__('%sMake sure that the "Destination" address and the %s amount in the Shapeshift.io popup match the details of this order!', 'cryptowoo'), '<span class="cw-tt-info-large-alt-pos-top">',  $wallet_config['coin_client'], '</span>'); ?>
				</a>
		</div>
    </div>
<?php } ?>
    <!--  Show Receipt Button -->
	<?php if(isset($options['cw_display_pay_later_button']) && (bool)$options['cw_display_pay_later_button'] ) { ?>
		<div class="cw-row">
			<div class="cw-col-6">
					<form action="<?php echo $order->get_checkout_order_received_url();?>" method="POST">
						<input type="hidden" name="order_id" value="<?php echo esc_attr($order->get_id());?>">
						<input type="submit" class="button medium" value="<?php esc_html_e('I have sent the payment - please process my order', 'cryptowoo'); ?>">
					</form>
			</div>
		</div>
	<?php } ?>
    <!-- Progress Bar -->
    <div class="cw-row nojs"><div class="cw-col-<?php echo $colsize <= 9 ? $colsize + 3 : 12; ?>" id="progress"><div id="timeoutBar"></div></div></div>
	<?php if('Activated' !== get_option('cryptowoo_activated')) {
		printf('<div class="cw-branding cryptowoo-smalltext">
                    <div class="cw-row">
                        <div class="cw-col-5"><a href="https://www.cryptowoo.com" target="_blank" title="Powered by CryptoWoo"><img src="%sassets/images/cryptowoo-checkout.png"/></a></div>
                    </div>
				</div>', CWOO_PLUGIN_PATH);
	} ?>
</div>

