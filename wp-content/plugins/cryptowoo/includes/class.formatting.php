<?php
if (!defined('ABSPATH')) {die();}// Exit if accessed directly
/**
 * WooCommerce template modifications and their processing functions
 * @category  CryptoWoo
 * @package CryptoWoo
 * @subpackage Address
 *
 */
class CW_Formatting
{

    /**
     * Calculate BTC, DOGE, and LTC amounts from their lowest divisible unit and format them according to WooCommerce settings
     *
     * @param $amount
     * @param bool|true $divide
     * @param int $decimals
     * @param bool|false $easy_copy
     * @param bool $is_qr
     * @return string
     */
    static function fbits($amount, $divide = true, $decimals = 8, $easy_copy = false, $is_qr = false) {

        $decimal_separator  = $is_qr ? '.' : wc_get_price_decimal_separator();
        $thousand_separator = wc_get_price_thousand_separator();
        $float_amount       = $divide && (float)$amount > 0 ? (float)($amount / 1e8) : (float)$amount;

        if($decimal_separator === '.') {
            if ($decimals === 8) {
                $string = sprintf('%.8f', $float_amount);
            } elseif ($decimals === 6) {
                $string = sprintf('%.6f', $float_amount);
            } elseif ($decimals === 4) {
                $string = sprintf('%.4f', $float_amount);
            } else {
                $string = sprintf('%.2f', $float_amount);
            }
        } else {
            $string_result      = $easy_copy ? rtrim(number_format(floatval($float_amount), $decimals, $decimal_separator, ''), '0') : rtrim(number_format(floatval($float_amount), $decimals, $decimal_separator, $thousand_separator), '0');

            if (strcmp(substr((string)$string_result, -1), $decimal_separator) === 0) {
                // The last character is the decimal seperator - add 2 zeros
                $string = str_replace($decimal_separator, $decimal_separator . '00', $string_result);
            } else {
                $string = $string_result;
            }
        }

        return $string;
    }

    /**
     * Return link to address in block explorer, default to explorer that belongs to processing API
     *
     * Blockcypher:     BTC, DOGE, LTC, BTCTEST
     * Chain.so:        BTC, DOGE, LTC, BTCTEST, DOGETEST
     * Blockr.io:       BTC, LTC, BTCTEST
     * Smartbit.com.au: BTC, BTCTEST
     * Blocktrail.com:  BTC, BTCTEST
     *
     *
     * @param $currency
     * @param $address
     * @param bool|false $options
     * @param $format
     * @return string
     */
    static function link_to_address($currency, $address, $options = false, $format = false) {

	    $v = new CW_Validate();
	    if(!$v->offline_validate_address($address, $currency)) {
		    return esc_html( $address );
	    }

        if (!$options) {
            $options = get_option('cryptowoo_payments');
        }

        $lc_currency = strtolower($currency);
        if ($currency === 'BLK') {
            $url = "https://chainz.cryptoid.info/blk/search.dws?q={$address}";
            return $format ? sprintf('<a href="%s" title="%s" target="_blank">%s</a>',$url, esc_html__('View address in block explorer', 'cryptowoo'), $address) : $url;
            //return "https://bitinfocharts.com/blackcoin/address/{$address}";
        }

        $testnet = strpos($lc_currency, 'test');

        // Prepare block chain API identifier
        $stripped = $testnet ? str_replace('test', '', $lc_currency) : $lc_currency;


	    // Check if we have a block explorer for this currency
	    $to_use = isset($options["preferred_block_explorer_{$stripped}"]) ? $options["preferred_block_explorer_{$stripped}"]: 'none';

	    // Maybe autoselect block explorer for links by the current processing API
	    if($to_use === 'autoselect') {

		    // Get the current processing API
		    $api_identifier = sprintf('processing_api_%s', $stripped);
		    $processing_api = isset($options[$api_identifier]) ? $options[$api_identifier] : '';

		    // Block.io processing API uses chain.so for links
		    $force_chain = array('block_io', 'chain_so');

		    // DOGETEST mandates chain.so
		    $to_use      = !in_array($options[$api_identifier], $force_chain) && $currency !== 'DOGETEST' ? $processing_api : 'chain_so';

	    }
	    // Set to "none" if we don't have an address
		$to_use = strlen($address) < 5 ? 'none' : $to_use;
	    switch ($to_use) {
		    case 'none' :
			    $url     = "#";
			    $address = '';
			    $format = false;
			    break;
            case 'smartbit' :
                $network = $testnet ? 'sandbox' : 'www';
                $url     = "https://{$network}.smartbit.com.au/address/{$address}";
                break;
            case 'blockcypher' :
                $network = $currency === 'BTCTEST' ? 'btc-testnet' : $lc_currency;
                $url     = "https://live.blockcypher.com/{$network}/address/{$address}";
                break;
            case 'blockr_io' :
                $network = $testnet ? 'tbtc.' : '';
                $url = "https://{$network}blockr.io/address/info/{$address}";
                break;
            case 'blocktrail' :
                $network = $currency === 'BTCTEST' ? 'tBTC' : $currency;
                $url = "https://www.blocktrail.com/{$network}/address/{$address}";
                break;
            case 'custom' :
	            $url = preg_replace('/{{ADDRESS}}/', $address, $options["custom_block_explorer_{$stripped}"]);
	            // Fail on invalid URL
	            if( !wp_http_validate_url($url) ) {
		            $url     = "#";
		            //$address = '';
		            $format = false;
	            }
                break;
            default :
            case 'chain_so' :
                $url = "https://chain.so/address/{$lc_currency}/{$address}";
                break;
        }
		$url = apply_filters('cw_link_to_address', $url, $address, $currency, $options);
        return $format ? sprintf('<a href="%s" title="%s" target="_blank">%s</a>',$url, esc_html__('View address in block explorer', 'cryptowoo'), $address) : $url;
    }

    /**
     * Format Insight API URL, force trailing slash, defaults to genesis block
     *
     * @param $value
     * @param $endpoint
     * @return mixed
     */
    static function format_insight_api_url($value, $endpoint = 'block-index/0') {
        $url = parse_url($value);
        if (isset($url['path'])) {
            $has_slash = strpos(substr($url['path'], -1), '/');
            $url['fixed_path'] = $has_slash  === 0 ? $url['path'] : sprintf('%s/', $url['path']);
            $url['path']       = $has_slash  === 0 ? sprintf('%s%s', $url['path'], $endpoint) :  sprintf('%s/%s', $url['path'], $endpoint);
        } else {
            $url['path'] = $url['fixed_path'] = '/';
        }
        $urls['surl']      = isset($url['port']) ? sprintf('%s://%s:%s%s', $url['scheme'], $url['host'], $url['port'], $url['path']) : sprintf('%s://%s%s', $url['scheme'], $url['host'], $url['path']);
        $urls['fixed_url'] = isset($url['port']) ? sprintf('%s://%s:%s%s', $url['scheme'], $url['host'], $url['port'], $url['fixed_path']) : sprintf('%s://%s%s', $url['scheme'], $url['host'], $url['fixed_path']);
        return $urls;
    }

	/**
	 * Exchange rate table for single product and checkout page
	 *
	 * @param $price
	 * @return string
	 */
	static function cryptowoo_crypto_rates_list($price) {

		$dbrates = CW_ExchangeRates::get_all_exchange_rates();
		$rates = $norates = array();
		if(empty($dbrates)) {
			return 'No exchange rates found in database!'; // TODO fail gracefully
		}

		$enabled_currencies   = cw_get_enabled_currencies();
		$woocommerce_currency = cw_get_woocommerce_currency();

		// Create currency => price and last_update array from enabled currencies without testnet coins
		foreach ($enabled_currencies as $key => $value) {
			if ($key != 'BTCTEST' && $key != 'DOGETEST') {
				if(!isset($dbrates[$key]['exchange_rate'])) {
					// Collect currencies that are enabled but have no rates in the DB
					$norates[$key] = $key;
				} else {
					$rates[$key]       = $dbrates[$key]['exchange_rate'];
					$exchange[$key]    = $dbrates[$key]['exchange'];
					$last_update[$key] = $dbrates[$key]['last_update'];
				}
			}
		}
		if(count($norates)) {
			// Make currency unavailable on checkout if there are no rates. Keep for 30 seconds.
			set_transient('cryptowoo_norates', $norates, 30); // TODO tweak transient time
			$message = esc_html__('Exchange rates not found in database. Please refresh this page.', 'cryptowoo');
		} else {
			// Clear transient if we have rates
			delete_transient('cryptowoo_norates');
			$message = '';
		}
		$options       = get_option('cryptowoo_payments');
		$currencytable = sprintf('%s<div class="cw-row crypto-price-table">', $message);
		$i = 0;
		foreach ($rates as $coin_type => $rate) {

			$i++;
			$multiplier_key = sprintf('multiplier_%s', strtolower($coin_type));
			$multiplier = (float)CW_Validate::check_if_unset($multiplier_key, $options, 1);

			// Don't display exchange names if one currency has a multiplier enabled
			$using_multiplier = $options['multiplier_btc'] != 1 || $options['multiplier_ltc'] != 1 || $options['multiplier_doge'] != 1 ? true : false;

			$value = ($price * $multiplier / $rate) * 100000000;

			$t_stamp         = strtotime($last_update[$coin_type]);
			$dec_places      = $coin_type != 'BTC' ? 4 : 8; // TODO define decimals for currencies in one place
			$fiat_dec_places = $coin_type === 'DOGE' ? 8 : 2;

			$crypto_symbol = CW_Formatting::get_coin_icon($coin_type, 'small');//cw_get_currency_symbol($coin_type);
			$display_rate  = ($price / ($price * $multiplier / $rate)) * 100000000;

			// Column count
			$col_count = isset($options['estimation_col_count']) ? (int)$options['estimation_col_count'] : 3;

			if (is_product()) {
				// small fonts and no date if on single product pages
				$fontsize = '75%'; //  TODO create styling settings for currency table
				if(CW_Validate::check_if_unset('display_fiat_rate', $options)) {
					$td            = '<div class="cw-col-%1$d" id="price-%2$s"><span style="font-size:%3$s;" class="priceinfo">%4$s %5$s</span><br><span style="font-size:%3$s;" class="exchangeinfo">%6$s %7$s/%2$s';
					$currencytable .= sprintf( $td, $col_count, esc_attr( $coin_type ), $fontsize, $crypto_symbol, CW_Formatting::fbits( $value, true, $dec_places ), CW_Formatting::fbits( $display_rate, true, $fiat_dec_places ), esc_html( $woocommerce_currency ) );
				} else {
					$td = '<div class="cw-col-%1$d" id="price-%2$s"><span style="font-size:%3$s;" class="priceinfo">%4$s %5$s';
					$currencytable .= sprintf( $td, $col_count, esc_attr( $coin_type ), $fontsize, $crypto_symbol, CW_Formatting::fbits( $value, true, $dec_places ) );
				}
			} else {
				if(CW_Validate::check_if_unset('display_fiat_rate', $options)) {
					// Show more details if we are not on a product page
					$td            = '<div class="cw-col-%1$d" id="price-%2$s"><span class="priceinfo">%3$s %4$s</span><br><span class="exchangeinfo">%5$s %6$s/%2$s';
					$currencytable .= sprintf( $td, $col_count, esc_attr( $coin_type ), $crypto_symbol, CW_Formatting::fbits( $value, true, $dec_places ), CW_Formatting::fbits( $display_rate, true, $fiat_dec_places ), esc_html( $woocommerce_currency ) );
				} else {
					$td            = '<div class="cw-col-%1$d" id="price-%2$s"><span class="priceinfo">%3$s %4$s</span><span class="exchangeinfo">';
					$currencytable .= sprintf( $td, $col_count, esc_attr( $coin_type ), $crypto_symbol, CW_Formatting::fbits( $value, true, $dec_places ));
				}
			}
			$currencytable .= !$using_multiplier && (bool)$options['display_rate_source'] ? sprintf('<br>%s </span><span class="cw-rate-ts">%s</span></div>', CW_ExchangeRates::get_exchange_nicename($exchange[$coin_type]), date('H:i:s', $t_stamp)) : '</span></div>';

		}
		$currencytable .= '</div>';
		return $currencytable;
	}

    /**
     * Add cryptocurrency price list to single product pages
     * Triggered in display settings: add_action('woocommerce_single_product_summary', 'CW_Formatting::display_crypto_price', 15);
     */
    static function display_crypto_price() {
	    global $product;
	    if ($price = $product->get_price()) {
            echo CW_Formatting::cryptowoo_crypto_rates_list($price);
        }
    }

    /**
     * Display currency settings hook
     */
    static function display_currency_settings() {

        $options = get_option('cryptowoo_payments');

        // Cryptocurrency price table on single product pages
        if (isset($options['cryptowoo_currency_table_on_single_products']) && $options['cryptowoo_currency_table_on_single_products'] == true) {
            // Add cryptocurrency price list to short description on single product pages
            add_action('woocommerce_single_product_summary', 'CW_Formatting::display_crypto_price', 15);
        }
    }

    /**
     * Add the field for the payment currency to the checkout page
     *
     **/
    static function cryptowoo_payment_currency_checkout_field() {

        $options = get_option('cryptowoo_payments');

        // Check if the gateway is enabled
        if (!(bool)$options['enabled']) {
            return;
        }
        echo '<div id="cryptowoo_checkout_field">';

        $total_amount = WC()->cart->total;

        if ($total_amount > 0 && (!isset($options['display_order_total_estimation']) || (bool)$options['display_order_total_estimation'])) {
            printf('<div id="estd_order_total_checkout"><p>%s</p></div>', CW_Formatting::cryptowoo_crypto_rates_list($total_amount));
        }

        // Get enabled currencies that have exchange rates
        $currencies = CW_ExchangeRates::filter_enabled_currencies_without_rates(cw_get_enabled_currencies());

        // Maybe use preselected payment currency
        $preselected = array();
        if ( count($currencies) > 1 ) {
            if (($options['default_payment_currency'] === 'disabled' || $currencies === array() || !isset($currencies[$options['default_payment_currency']]))) {
                $preselected['please_choose'] = esc_html__('Please select a currency', 'cryptowoo');
            } else {
                $preselected[$options['default_payment_currency']] = $currencies[$options['default_payment_currency']];
            }
            $hide = '';
        } else {
            $hide = ' cw-hide';
        }


        $currencies = array_merge($preselected, $currencies);

        woocommerce_form_field('payment_currency', array('type' => 'select',
                                                         'class' => array(sprintf('payment-currency-select form-row-full%s', $hide)),
                                                         'label' => esc_html__('Payment Currency', 'cryptowoo'),
                                                         'required' => true,
                                                         'options' => $currencies));
        // Maybe add refund address field
        if ($options['collect_refund_address'] !== 'disabled') {
            $required = $options['collect_refund_address'] === 'required' ? true : false;
            woocommerce_form_field('refund_address', array('type' => 'text',
                                                           'class' => array('refund-address form-row-full'),
                                                           'label' => esc_html__('Refund Address', 'cryptowoo'),
                                                           'required' => $required,
                                                           'placeholder' => '',
                                                           'clear' => true));
        }
        if ('Activated' !== get_option('cryptowoo_activated')) {
            printf('<p class="form-row form-row-full"><div class="cw-branding cryptowoo-smalltext"><a href="%s" class="about-cryptowoo" target="_blank" title="%s"><img alt="CryptoWoo" src="%sassets/images/cryptowoo-checkout-x15.png" /></a></div></p>', esc_url('https://www.cryptowoo.com/'), esc_html__('powered by CryptoWoo', 'cryptowoo'), CWOO_PLUGIN_PATH);
        }
        echo '</div>';
    }

    /**
     * Process the checkout
     */
    static function payment_currency_checkout_validation() {
        // Check if set, if its not set add an error.
        if (isset($_POST['payment_currency']) && $_POST['payment_method'] === 'cryptowoo') {
            if ($_POST['payment_currency'] === 'please_choose') {
                wc_add_notice(esc_html__('Please select your payment currency from the dropdown list.', 'cryptowoo'), 'error');
            }
            $options = get_option('cryptowoo_payments');
            // Is an address required or filled out?
            if (isset($_POST['refund_address'])) {
                $validate = new CW_Validate();
                if (empty($_POST['refund_address']) && $options['collect_refund_address'] === 'required') {
                    wc_add_notice(esc_html__('Please enter a refund address.', 'cryptowoo'), 'error');
                } elseif(!empty($_POST['refund_address']) && !$validate->offline_validate_address($_POST['refund_address'], $_POST['payment_currency'])) {
                    wc_add_notice(esc_html__('Your refund address seems to be invalid. Make sure you copy and paste the whole address.', 'cryptowoo'), 'error');
                }
            }
        }
    }

    /**
     * Update the order meta payment_currency and refund_address
     *
     * @param $order_id
     */
    static function cryptowoo_payment_currency_update_order_meta($order_id) {
        //check if $_POST has our custom fields and if payment with digital currencies has been selected
        if (isset($_POST['payment_currency'])) {
	        $meta_data = array();
            if ($_POST['payment_currency'] === 'please_choose' || $_POST['payment_method'] !== 'cryptowoo') {
	            // Update WooCommerce order details
	            $meta_data = array(
		            'payment_currency' => cw_get_woocommerce_currency(),
	            );
	            CW_OrderProcessing::cwwc_update_order_meta($order_id, $meta_data);
            } else {
	            // Update WooCommerce order details
	            $meta_data = array(
		            'payment_currency' => $_POST['payment_currency'],
	            );
	            CW_OrderProcessing::cwwc_update_order_meta($order_id, $meta_data);
            }
            if (isset($_POST['refund_address'])) {
	            $meta_data['refund_address'] = $_POST['refund_address'];
            }
	        CW_OrderProcessing::cwwc_update_order_meta($order_id, $meta_data);
        }
    }

    /**
     * Display cryptocurrency payment info on order received page
     * Doesn't differentiate between unconfirmed and confirmed transactions
     *
     * @param $id
     * @return bool
     */
    static function display_crypto_payment_info($id) {

	    // WooCommerce order
	    $order = wc_get_order($id);
	    if(!$order || !('cryptowoo' === $order->get_payment_method())) {
	    	return;
	    }

        $options = get_option('cryptowoo_payment');
        $data       = CW_OrderProcessing::get_payment_details($id, 'order_id');
	    $amount_due = 0;

        if ($data) {

            // Use data in temp table
            $amount_due       = $data->crypto_amount;
            $payment_currency = $data->payment_currency;
            $payment_address  = $data->address;
            $timeout          = (int)$data->timeout;

            $paid               = (bool)$data->paid;
            $received_confirmed = $data->received_confirmed;
            $amount_unconfirmed = $data->received_unconfirmed;

            // Calculate missing amount
            $amount_missing = ($received_confirmed + $amount_unconfirmed) > 0 ? $amount_due - $received_confirmed - $amount_unconfirmed : $amount_due;

        } else {

            // Entry does not exist in temp table, get from WooCommerce order
            $amount_due       = (int)$order->get_meta('crypto_amount');
            $payment_currency = $order->get_meta('payment_currency');
            $payment_address  = $order->get_meta('payment_address');
            $timeout          = strpos($order->get_meta('tx_confirmed'), 'timeout') ? 1 : 0;

            $received_confirmed = (int)$order->get_meta('received_confirmed');
            $amount_unconfirmed = (int)$order->get_meta('received_confirmed');;

            // Calculate missing amount and check payment status
            $amount_missing = ($received_confirmed + $amount_unconfirmed) > 0 ? ($amount_due - $received_confirmed - $amount_unconfirmed) : $amount_due;
            $percentage_paid = $amount_due > 0 ? (($received_confirmed + $amount_unconfirmed) / $amount_due) * 100 : 0;
            $paid           = $percentage_paid >= (float)$options['underpayment_notice_range'][2];
        }
        if ( $amount_due >= 0 && (bool)$payment_address ) {

            // Get nice currency names
            $wc_currencies = cw_get_woocommerce_currencies();

            printf(esc_html__('%s%s Payment Details%s', 'cryptowoo'), '<div class="thankyou-cryptowoo"><h3>', $wc_currencies[$payment_currency], '</h3>');
            printf(esc_html__('Your order total: %s %s%s', 'cryptowoo'), CW_Formatting::fbits($amount_due), $payment_currency, '<br>');
            printf(esc_html__('Amount received: %s %s%s', 'cryptowoo'), CW_Formatting::fbits($received_confirmed), $payment_currency, '<br>');

            // Maybe display unconfirmed amount
            if ($amount_unconfirmed > 0 && !$paid) {
                printf(esc_html__('Amount unconfirmed: %s %s%s', 'cryptowoo'), CW_Formatting::fbits($amount_unconfirmed), $payment_currency, '<br>');
            }

            // Link to payment address on block chain
            $url = CW_Formatting::link_to_address($payment_currency, $payment_address);
            printf('%s: <a title="%s" target="_blank" href="%s">%s</a><br>', esc_html__('Payment address', 'cryptowoo'), sprintf(esc_html__('Payment address on the %s blockchain', 'cryptowoo'), $wc_currencies[$payment_currency]), $url, $payment_address);

            // WooCommerce order status
	        $on_hold = $order->has_status('on-hold');

            // Amount missing
            if ($on_hold || ($amount_missing > ($amount_due * ((float)$options['underpayment_notice_range'][2] / 100)) && $amount_due > 0 && !$paid)) {
                printf(esc_html__('Amount missing: %s %s%s', 'cryptowoo'), CW_Formatting::fbits($amount_missing), $payment_currency, '<br>');


                if ($timeout !== 1 && !$on_hold && ($received_confirmed > 0 || $amount_unconfirmed > 0)) {

                    // Request customer to send the missing amount
                    wc_print_notice(esc_html__('Please send the missing amount to the address above. You will receive an email when your payment is fully confirmed.', 'cryptowoo'), 'notice');

                } elseif ($timeout === 1 || $on_hold) {

                    $hold_notice = $on_hold ? esc_html__('and has been put on hold. Please contact us.', 'cryptowoo') : esc_html__('. Please try again or contact us if you need assistance.', 'cryptowoo');
                    // Inform customer about timeout and maybe that his order is on hold.
                    wc_print_notice(sprintf(esc_html__('Your order is expired %s', 'cryptowoo'), $hold_notice), 'error');
                }
            } elseif ($paid && $amount_due >= 0 && $received_confirmed > 0 && $amount_unconfirmed <= 0) {
                // Maybe use custom thank you page text
                $options = get_option('cryptowoo_payments');
                $text    = !empty($options['thankyou_page_text']) ? $options['thankyou_page_text'] : esc_html__('Your payment has been received. Thank you for shopping with us!', 'cryptowoo');

                wc_print_notice($text, 'success');

            } elseif (!$paid && $timeout !== 1 && $order->has_status('pending')) {
                wc_print_notice(esc_html__('You will receive an email when your payment is fully confirmed.', 'cryptowoo'), 'notice');
            }
            echo '</div>';
        }
    }

    /**
     * Add cryptocurrency payment info to WooCommerce emails
     *
     * @param $order
     * @return string
     */
    static function display_order_email_info($order, $sent_to_admin, $plain_text, $email) {

        if ('cryptowoo' === $order->get_payment_method()) {

            $data       = CW_OrderProcessing::get_payment_details($order->get_id(), 'invoice_number');

            if ($data) {
                $amount_due         = $data->crypto_amount;
                $paid               = (int)$data->paid;
                $timeout            = (int)$data->timeout;
                $amount_unconfirmed = $data->received_unconfirmed;
                $full_amount        = $data->received_confirmed + $data->received_unconfirmed;
                $payment_currency   = $data->payment_currency;
                $payment_address    = $data->address;
                $amount_missing     = $data->crypto_amount - $data->received_confirmed - $data->received_unconfirmed;

                // Get currency nicenames
                $wc_currencies = cw_get_woocommerce_currencies();

                $data = sprintf(esc_html__('%s%s Payment Details%s', 'cryptowoo'), '<div class="thankyou-cryptowoo"><h3>', $wc_currencies[$payment_currency], '</h3>');
                $data .= sprintf(esc_html__('Your order total: %s %s%s', 'cryptowoo'), CW_Formatting::fbits($amount_due), $payment_currency, '<br>');
                $data .= sprintf(esc_html__('Amount received: %s %s%s', 'cryptowoo'), CW_Formatting::fbits($full_amount), $payment_currency, '<br>');

                // Maybe display unconfirmed amount
                if ($amount_unconfirmed > 0) {
                    $data .= sprintf(esc_html__('Unconfirmed: %s %s %s', 'cryptowoo'), CW_Formatting::fbits($amount_unconfirmed, true), $payment_currency, '<br>');
                }

                // Link to payment address on block chain
                $url = CW_Formatting::link_to_address($payment_currency, $payment_address);
                $data .= sprintf('%s: <a title="%s" target="_blank" href="%s">%s</a><br>', esc_html__('Payment address', 'cryptowoo'), sprintf(esc_html__('Payment address on the %s blockchain', 'cryptowoo'), $wc_currencies[$payment_currency]), $url, $payment_address);

                // Amount missing
                $options = get_option('cryptowoo_payment');
                if ($amount_missing > ($amount_due * ((float)$options['underpayment_notice_range'][2] / 100)) && $amount_due > 0 && $paid !== 1) {
                    $data .= sprintf(esc_html__('%sAmount missing: %s %s%s', 'cryptowoo'), '<span style="font-weight: bold; color: red;">', CW_Formatting::fbits($amount_missing), $payment_currency, '</span><br>');
                }

                // Status message
                if ($paid === 1 && $timeout !== 1) {
                    $data .= sprintf(esc_html__('%sYour payment of %s %s has been received.%s', 'cryptowoo'), '<p>', CW_Formatting::fbits($full_amount), $order->get_meta('payment_currency'), '</p>');
                } elseif ($paid !== 1 && $timeout === 1) {
                    $data .= esc_html__('Your order is expired. Please try again or contact us if you need assistance.', 'cryptowoo');
                } elseif ($paid !== 1 && $timeout === 3) {
                    // Maybe tell customer to contact the shop owner
                    //$order = wc_get_order($order->get_id());
                    if ($order->has_status('on-hold')) {
                        $data .= sprintf('<span style="font-weight: bold; color: red;">%s</span>',esc_html__('Your order has been put on hold. Please get in touch with us.', 'cryptowoo'));
                    } else {
                        $data .= sprintf('<p>%s</p>',esc_html__('You will receive another email when your payment is fully confirmed.', 'cryptowoo'));
                    }
                }

                $data .= '</div>';
                echo $data;
            }
        }
    }

    /**
     * Prepare overpayment message for customer
     *
     * @param $order_data
     * @param $refund_address
     * @param $options
     * @return string
     */
    static function prepare_overpayment_message($order_data, $refund_address, $options) {
        if (empty($options['overpayment_message'])) {
            $options['overpayment_message'] = esc_html__('You paid {{AMOUNT_DIFF}} {{PAYMENT_CURRENCY}} too much. Please get in touch with us.', 'cryptowoo');
        }
        $cnote = preg_replace_callback("/\{\{([a-zA-Z-0-9\ \_]+?)\}\}/", function ($match) use ($order_data, $refund_address, $options) {
            switch (strtolower($match[1])) {
                // faucet information:
                case "refund_address":
                    return (bool)$refund_address ? CW_Formatting::link_to_address($order_data->payment_currency, $refund_address, $options, true) : 'n/a';
                case "percentage_paid":
                    return round($order_data->percentage_paid - 100, 3);
                case "amount_diff":
                    return CW_Formatting::fbits($order_data->amount_diff);
                case "payment_currency":
                    return $order_data->payment_currency;
                default:
                    return $match[1];
            }
        }, $options['overpayment_message']);

        return $cnote;
    }

    /**
     * Get Shapeshift "Shifty" Button setting for currency
     *
     *
     * @param $payment_currency
     * @param $payment_address
     * @param $crypto_amount
     * @param $options
     * @return array
     */
    static function cw_shifty_enabled($payment_currency, $payment_address, $crypto_amount, $options) {

	    // Maybe add Shapeshift affiliate key
	    if((bool)$options['support_cryptowoo_ss']) {
	    	$cwkey = 'f07fe6b593ad35d47105ca8f106b36b1e30070aad535bea69482f60d7b0f670d64d82c068111344a171997acb55664ee9b253df77652ef773d269c026f8798fe';
	    } else {
		    $cwkey =  isset( $options['shapeshift_affiliate_id'] ) && !empty($options['shapeshift_affiliate_id']) && ctype_alnum($options['shapeshift_affiliate_id']) ? $options['shapeshift_affiliate_id'] : '';
	    }

        $key = !empty($cwkey) ? sprintf('&amp;apiKey=%s', esc_attr($cwkey)) : '';
        $data['data'] = sprintf('https://shapeshift.io/shifty.html?destination=%s&amp;output=%s&amp;amount=%s%s', $payment_address, $payment_currency, CW_Formatting::fbits( $crypto_amount, true, 8, true, true), $key);
        $data['enabled'] = false === strpos($payment_currency, 'TEST') && ($payment_currency === $options['shapeshift_button'] || $options['shapeshift_button'] === 'global') ? true : false;
       return $data;
    }

    /**
     * Like wc_get_template_html, returns the HTML
     * @see wc_get_template
     * @since 2.5.0
     * @param string $template_name
     * @return string
     */
    static function cw_get_template_html( $template_name, $email_heading = '', $args = array(), $template_path = '', $default_path = '' ) {
        $file = sprintf('%stemplates/%s.php',CWOO_PLUGIN_DIR,$template_name);
        if(file_exists($file)) {
            $template = self::cw_ob($file);
            return str_replace('{{EMAIL_HEADING}}', $email_heading, $template);
        } else {
            return '';
        }
    }

    /**
     * Output buffer
     * @param $file
     * @return string
     */
    private static function cw_ob($file) {
        ob_start();
        include($file);
        return ob_get_clean();
    }

	/**
	 * Get coin icon
	 *
	 * @param $currency
	 *
	 * @return string
	 */
	static function get_coin_icon($currency, $size = 'normal') {
		if($currency === 'BLK') {
			$icon_html = '<i class="cc BC aw-cryptoicon"';
		} else {
			$icon_html = sprintf('<i class="cc %s"', $currency);
		}
		$icon_html .= $size === 'normal' ? '></i> ' : ' style="font-size: 120%;"></i> ';
		return $icon_html;
	}

	/**
	 * Create GIF image from payment address
	 * @param $payment_address
	 *
	 * @return string
	 */
	static function generate_sec_image($payment_address) {
		$text  = sprintf(esc_html__('Please compare the destination address with the one below%1$sbefore you send the payment:%1$s', 'cryptowoo'), PHP_EOL);
		$im    = imagecreate( 800, 120 );
		ImageColorAllocate( $im, 0, 0, 0 ); // Black background // TODO maybe use pre-defined image that makes OCR more difficult
		$white = ImageColorAllocate( $im, 255, 255, 255 );
		ImageTTFText( $im, 15, 0, 10, 25, $white, CWOO_PLUGIN_DIR.'assets/fonts/dejavu/DejaVuSansMono.ttf', $text );
		$red = ImageColorAllocate( $im, 255, 0, 0 );
		ImageTTFText( $im, 15, 0, 200, 90, $red, CWOO_PLUGIN_DIR.'assets/fonts/dejavu/DejaVuSansMono.ttf', $payment_address );
		ob_start();
		ImageGif( $im );
		$image_data = ob_get_clean();
		ImageDestroy( $im );

		return sprintf( '<h3><i class="fa fa-lock" aria-hidden="true"></i> Security Check</h3> <img src="data:image/gif;base64,%s" />', base64_encode( $image_data ));
	}
} // End Class CW_Formatting

/*
	Functions for enabling cryptos as base currency.
	USE AT YOUR OWN RISK

	Using crypto as base currency can create problems with calculating exchange rates
	depending on shop and product configuration.

*/
/*
add_filter( 'woocommerce_currencies', 'cw_add_btc' );

function cw_add_btc( $currencies ) {
	$currencies['BTC'] = __( 'Bitcoin', 'woocommerce' );
	return $currencies;
}

add_filter('woocommerce_currency_symbol', 'cw_add_btc_symbol', 10, 2);

function cw_add_btc_symbol( $currency_symbol, $currency ) {
	return $currency === 'BTC' ? '฿' : $currency_symbol;
}

add_filter( 'woocommerce_currencies', 'cw_add_ltc' );

function cw_add_ltc( $currencies ) {
	$currencies['LTC'] = __( 'Litecoin', 'woocommerce' );
	return $currencies;
}

add_filter('woocommerce_currency_symbol', 'cw_add_ltc_symbol', 10, 2);

function cw_add_ltc_symbol( $currency_symbol, $currency ) {
	return $currency === 'LTC' ? 'Ł' : $currency_symbol;
}

add_filter( 'woocommerce_currencies', 'cw_add_doge' );

function cw_add_doge( $currencies ) {
	$currencies['DOGE'] = __( 'Dogecoin', 'woocommerce' );
	return $currencies;
}

add_filter('woocommerce_currency_symbol', 'cw_add_doge_symbol', 10, 2);

function cw_add_doge_symbol( $currency_symbol, $currency ) {

	return $currency === 'DOGE' ? 'Ð' : $currency_symbol;
}
*/

