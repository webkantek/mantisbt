<?php
if (!defined('ABSPATH')) {die();}// Exit if accessed directly
/**
 * Sorting of order objects
 *
 */
class CW_OrderSorting
{

    static function cw_order_by_id($a, $b) {
        return strcmp($a->id, $b->id);
    }

    static function cw_order_by_date($a, $b) {
        return strcmp(strtotime($a->order_date), strtotime($b->order_date));
    }

    static function cw_order_by_last_update($a, $b) {
        return strcmp(strtotime($a->last_update), strtotime($b->last_update));
    }

    static function cw_order_by_timeout($a, $b) {
        return $a->timeout_value < $b->timeout_value;
    }

    static function cw_order_by_received_confirmed($a, $b) {
        return strcmp($a->received_confirmed, $b->received_confirmed);
    }

    static function cw_order_by_received_unconfirmed($a, $b) {
        return strcmp($a->received_unconfirmed, $b->received_unconfirmed);
    }

    static function cw_order_by_amount_due($a, $b) {
        return strcmp($a->amount_due, $b->amount_due);
    }

    static function cw_order_by_crypto_amount($a, $b) {
        return strcmp($a->crypto_amount, $b->crypto_amount);
    }

    static function cw_order_by_email($a, $b) {
        return strcmp($a->_billing_email, $b->_billing_email);
    }

    static function cw_sort_orders($cryptowoo_orders, $orderby, $sort) {

        switch ($orderby) {
            default:
            case 'id' :
                $function = 'cw_order_by_id';
                break;
            case 'created_at' :
                $function = 'cw_order_by_date';
                break;
            case 'last_update' :
                $function = 'cw_order_by_last_update';
                break;
            case 'received_confirmed' :
                $function = 'cw_order_by_received_confirmed';
                break;
            case 'received_unconfirmed' :
                $function = 'cw_order_by_received_unconfirmed';
                break;
            case 'amount_due' :
                $function = 'cw_order_by_amount_due';
                break;
            case 'crypto_amount' :
                $function = 'cw_order_by_crypto_amount';
                break;
            case 'email' :
                $function = 'cw_order_by_email';
                break;
        }

        uasort($cryptowoo_orders, "CW_OrderSorting::{$function}");

        // Maybe reverse order
        if (strcasecmp($sort, 'ASC')) {
            $cryptowoo_orders = array_reverse($cryptowoo_orders);
        }
        return $cryptowoo_orders;
    }

    /**
     * Sort unpaid addresses by average blocktime (DOGE > BLK > LTC > BTC) and prepare address batches
     *
     * array(
     *  'batches' =>
     *        array(
     *            'BTC' => array( 0 => 'ADDRESS1', 1 => 'ADDRESS2', 3 => 'ADDRESS3'),
     *            'LTC' => array( 0 => ...
     *
     *        ),
     *    'DOGE' => $unpaid_addresses_doge,
     *  'LTC' => $unpaid_addresses_ltc,
     *  'BTC' => $unpaid_addresses_btc
     *  'BLK' => $unpaid_addresses_blk
     *    );
     *
     * @param $unpaid_addresses_raw
     * @return mixed
     */
    public static function sort_unpaid_addresses($unpaid_addresses_raw) {
        $address_batch = array();
        $top_n         = array(array(), array(), array(), array());
        //batches,  [0] DOGE, [1] BLK, [2] LTC, [3] BTC

        // Order the items according to their currencies' average blocktime
        foreach ($unpaid_addresses_raw as $address) {
            $payment_currency = $address->payment_currency;
            if (strcmp($payment_currency, 'BTC') === 0) {
                $top_n[3]['BTC'][]      = $address;
                $address_batch['BTC'][] = $address->address;
            } elseif (strcmp($payment_currency, 'DOGE') === 0) {
                $top_n[0]['DOGE'][]      = $address;
                $address_batch['DOGE'][] = $address->address;
            } elseif (strcmp($payment_currency, 'LTC') === 0) {
                $top_n[2]['LTC'][]      = $address;
                $address_batch['LTC'][] = $address->address;
            } elseif (strcmp($payment_currency, 'BLK') === 0) {
                $top_n[1][$payment_currency][]      = $address;
                $address_batch[$payment_currency][] = $address->address;
            } elseif (strcmp($payment_currency, 'BTCTEST') === 0) {
                $top_n[2]['BTCTEST'][]      = $address;
                $address_batch['BTCTEST'][] = $address->address;
            } elseif (strcmp($payment_currency, 'DOGETEST') === 0) {
                $top_n[0]['DOGETEST'][]      = $address;
                $address_batch['DOGETEST'][] = $address->address;
            } else {
            	$top_n = apply_filters('cw_sort_unpaid_addresses', $top_n, $address);
	            $address_batch = apply_filters('cw_filter_batch', $address_batch, $address);
            }
        }

        $unpaid_addresses = array_merge(array('batches' => $address_batch), $top_n[0], $top_n[1], $top_n[2], $top_n[3]);

        return $unpaid_addresses;
    }

    /**
     * Return max. $number of unpaid addresses sorted by average blocktime (DOGE > BLK > LTC > BTC)
     *
     *
     * @param $unpaid_addresses_raw
     * @param $number
     * @return mixed
     */
    public static function prioritize_unpaid_addresses($unpaid_addresses_raw, $number = 10) {
	    $top_n       = $top_n_sorted = array( array(), array(), array(), array() );
	    //[0] DOGE, [1] BLK, [2] LTC, [3] BTC

	    // Order the items according to their currencies' average blocktime
	    foreach ( $unpaid_addresses_raw as $address ) {
		    $payment_currency = $address->payment_currency;
		    if ( strcmp( $payment_currency, 'BTC' ) === 0 || strcmp( $payment_currency, 'BTCTEST' ) === 0 ) {
			    $top_n[3][] = $address;
		    } elseif ( strcmp( $payment_currency, 'DOGE' ) === 0 || strcmp( $payment_currency, 'DOGETEST' ) === 0 ) {
			    $top_n[0][] = $address;
		    } elseif ( strcmp( $payment_currency, 'LTC' ) === 0 || strcmp( $payment_currency, 'LTCTEST' ) === 0 ) {
			    $top_n[2][] = $address;
		    } elseif ( strcmp( $payment_currency, 'BLK' ) === 0 ) {
			    $top_n[1][] = $address;
		    } else {
			    $top_n = apply_filters('cw_prioritize_unpaid_addresses', $top_n, $address);
		    }
	    }

        foreach ($top_n as $n => $currency_orders) {
            $top_n_sorted[$n] = CW_OrderSorting::cw_sort_orders($currency_orders, 'last_update', 'ASC');
        }

        $result           = array_merge($top_n_sorted[0], $top_n_sorted[1], $top_n_sorted[2], $top_n_sorted[3]);
        $unpaid_addresses = array_slice($result, 0, $number);
        return $unpaid_addresses;
    }
}
