<?php

/**
 * Blockcypher API Helper
 */
class CW_Blockcypher
{

    /**
     * Prepare blockcypher API for currency
     *
     *
     * @param $currency
     * @param $options
     * @return \BlockCypher\Rest\ApiContext
     * @throws \BlockCypher\Exception\BlockCypherConfigurationException
     */
    public static function prepare_blockcypher_api($currency, $options) {

        // Provide API Token. Maybe replace the given one with the token set by the merchant.
        $token = isset($options['blockcypher_token']) ? $options['blockcypher_token'] : '';

        // Determine network
        $network = strpos($currency, 'TEST') ? 'test3' : 'main';
        $coin    = strtolower(str_replace('TEST', '', $currency));

        // SDK config
        $config = array('log.LogEnabled' => isset($options['logging']['transactions']) && (bool)$options['logging']['transactions'] ? true : false,
                        'log.FileName' => CW_LOG_DIR . 'BlockCypher.log',
                        'log.LogLevel' => defined('WP_DEBUG') && WP_DEBUG ? 'DEBUG' : 'INFO',
                        'validation.level' => 'log');

        $api_context = BlockCypher\Rest\ApiContext::create($network, $coin, 'v1', new BlockCypher\Auth\SimpleTokenCredential($token), $config);

        $api_context->setConfig($config);

        return $api_context;
    }

    /**
     * Query Blockcypher multiple full address endpoint
     * @param $batch
     * @param $currency
     * @param $api_context
     * @param $options
     * @return \BlockCypher\Api\FullAddress|bool
     */
    public static function blockcypher_full_address($batch, $currency, $api_context, $options) {

        $error = $fullAddress = false;
        $addressClient = new BlockCypher\Client\AddressClient($api_context);

        // Rate limit transient
        $limit_transient = get_transient('cryptowoo_limit_rates');

        try {
            $fullAddress = $addressClient->getMultipleFullAddresses($batch);  // getFullAddress($order_data->address);

            //$fullAddress = $addressClient->getMultipleFullAddresses($batch, array('confidence' => 90));  // TODO exclude low confidence transactions
        } catch (Exception $ex) {
            $error = $ex->getMessage();
            // Action hook for Blockcypher API error
            do_action('cryptowoo_api_error', 'Blockcypher API error: '.$error);

            // Update rate limit transient
            $limit_transient[$currency] = isset($limit_transient[$currency]['count']) ? array('count' => (int)$limit_transient[$currency]['count'] + 1,
                                                                                                'api' => 'blockcypher') : array('count' => 1, 'api' => 'blockcypher');
            // Keep error data until the next full hour (rate limits refresh every full hour). We'll try again after that time.
            set_transient('cryptowoo_limit_rates', $limit_transient, CW_AdminMain::seconds_to_next_hour());

            //if ((bool)$options['logging']) {
            file_put_contents(CW_LOG_DIR . 'cryptowoo-tx-update.log', date('Y-m-d H:i:s') . " Blockcypher full address error {$error}\r\n", FILE_APPEND);
            //}

        }
        // Delete rate limit transient if the last call was successful
        if (false !== $limit_transient && false === $error) {
            delete_transient('cryptowoo_limit_rates');
        }
        return false !== $error ? $error : $fullAddress;
    }

    /**
     * Get block height via blockcypher
     *
     * @param $currency
     * @param $api_context
     * @param $return_error
     * @return int
     */
    public static function blockcypher_block_height($currency, $api_context, $return_error = false) {

        $error = $blockchain = false;

        // Prepare API
        $blockchainClient = new BlockCypher\Client\BlockchainClient($api_context);

        $network = strpos($currency, 'TEST') ? '.test3' : '.main';

        try {
            $blockchain = $blockchainClient->get($currency . $network);
        } catch (Exception $ex) {
            $error['message'] = 'Blockcypher API error: '.$ex->getMessage();
            // Action hook for Blockcypher API error
            do_action('cryptowoo_api_error', 'Blockcypher API error: '.$error);
        }
        return $error || !$blockchain ? !$return_error ? false : $error : (int)$blockchain->height;
    }

    /**
     * Get transaction confidence via blockcypher
     *
     * @param $transactions
     * @param $currency
     * @param $api_context
     * @return float|int
     */
    public static function blockcypher_tx_confidence(array $transactions, $currency, $api_context) {

        $error = $tx_confidence = false;
        $result = array();
        usleep(700000); // sleep 0.7 seconds to stay below max requests/second TODO proper rate limits
        // Prepare API
        $txClient = new BlockCypher\Client\TXClient($api_context);

        foreach ($transactions as $transaction) {
            // Only include unconfirmed transactions
            if ((int)$transaction->confirmations === 0 || !isset($transaction->confirmations)) { // TODO Filter transactions that are spending from the payment address
                $data[] = isset($transaction->hash) ? $transaction->hash : $transaction->txid;
            }
        }
        if (isset($data)) {
            $options = get_option('cryptowoo_payments');
            try {
                $multiple_tx_confidence = $txClient->getMultipleConfidences($data, array(), $api_context);
            } catch (Exception $ex) {
                $error = $ex->getMessage();
                // Action hook for Blockcypher API error
                do_action('cryptowoo_api_error', 'Blockcypher API error: '.$error);
                // Rate limit transient
                $limit_transient = get_transient('cryptowoo_limit_rates');
                // Update rate limit transient
                $limit_transient[$currency] = isset($limit_transient[$currency]['count']) ? array('count' => (int)$limit_transient['count'] + 1,
                                                                                                    'api' => 'blockcypher') : array('count' => 1,'api' => 'blockcypher');
                // Keep error data for the rest of the current hour
                set_transient('cryptowoo_limit_rates', $limit_transient, CW_AdminMain::seconds_to_next_hour());
                file_put_contents(CW_LOG_DIR . 'cryptowoo-tx-update.log', date('Y-m-d H:i:s') . " blockcypher tx confidence error {$error} " . var_export($transactions, true) . "\r\n", FILE_APPEND);

            }

            if (isset($multiple_tx_confidence) && is_array($multiple_tx_confidence)) {
                foreach ($multiple_tx_confidence as $tx_confidence) {
                    $result[$tx_confidence->txhash] = $tx_confidence->confidence;
                }
            }
            if ((bool)$options['logging']['transactions']) {
                CW_AdminMain::cryptowoo_log_data(0, __FUNCTION__, $result, 'cryptowoo-tx-update.log');
            }
        }
        return $error ? false : $result; // TODO better handling of response {"error": "Transaction hash not found or transaction hasalready been confirmed: 93f44720befff58abe4e73cd782402bb5e8af227e2b581ae282a1ec938b3ae7c."}
    }

    /**
     * Get amounts paid to addresses in the current batch via Blockcypher
     *
     * @todo Getting the block height for each currency on each cron execution wastes lots of resources -> refactor and check max. once per smallest avg. block time of the currency we're dealing with
     *
     * @param $batch_currency
     * @param $batch
     * @param $orders
     * @param $options
     * @return mixed
     */
    public static function blockcypher_batch_tx_update($batch_currency, $batch, $orders, $options) {

        $api_data = array();

        // Prepare API
        $api_context = self::prepare_blockcypher_api($batch_currency, $options);

        // Maybe get block height (for checking the locktime) TODO Move somewhere else
        /*
        if ($min_conf === 0) {
            $chain_height = CW_OrderProcessing::blockcypher_block_height($batch_currency, $api_context);
        }
        */

        // Get data for currency batch
        $full_address_batch = self::blockcypher_full_address($batch, $batch_currency, $api_context, $options);

        // Maybe return error message
        if (is_string($full_address_batch)) {
            return $full_address_batch;
        }

        // Prepare data for each order
        foreach ($full_address_batch as $batch_order_data) {
            if (isset($batch_order_data->address)) {
                $api_data[$batch_order_data->address] = $batch_order_data;
            }
        }
        // Analyze Blockcypher response
        $tx_data = self::blockcypher_tx_analysis($orders, $api_data, $options, $api_context); // $chain_height);

        return $tx_data;
    }

    /**
     * Calculate amounts paid to each order matching an address in the Blockcypher response
     *
     * @todo Pass $chain_height to check locktime
     *
     * @param $batch_orders
     * @param $api_data
     * @param $options
     * @param bool|false $api_context
     * @return mixed
     */
    public static function blockcypher_tx_analysis($batch_orders, $api_data, $options, $api_context = false) {

        $dbupdate      = 0;
        $payment_array = array();
        /*
        if(WP_DEBUG) {
            file_put_contents(CW_LOG_DIR . 'cryptowoo-tx-update.log', date('Y-m-d H:i:s') . ' blockcypher tx analysis ' . var_export($api_data, true) . "\r\n", FILE_APPEND);
        } */

        foreach ($batch_orders as $order_data) {

            if (!isset($api_data[$order_data->address]) || (isset($api_data[$order_data->address]->final_n_tx) && $api_data[$order_data->address]->final_n_tx < 1)) {
                $payment_array[$order_data->order_id] = array('status' => !is_string($api_data) ? 'Blockcypher success | no transactions for address' : sprintf('BlockCypher error: %s', $api_data),
                                                              'force_update' => 'no',
                                                              'address' => $order_data->address,
                                                              'order_id' => $order_data->order_id,
                                                              'total_received_confirmed' => 0,
                                                              'total_received_unconfirmed' => 0,
                                                              'tx_count' => 0);
	            CW_OrderProcessing::bump_last_update($order_data->address, $order_data->order_id);
                continue; // Go to next order if we did not receive data for this address
            }

            if (!$api_context) {
                $api_context = self::prepare_blockcypher_api($order_data->payment_currency, $options);
            }

            // Get processing configuration
            $pc_conf = CW_OrderProcessing::get_processing_config($order_data->payment_currency, $order_data->amount, $options);

            $status = is_array($api_data) && isset($full_address['message']) ? $full_address['message'] : 'success';

            // Prepare transaction data for the payment address of this order
            $txs_to_address = $api_data[$order_data->address];
            $transactions   = isset($txs_to_address->txs) ? $txs_to_address->txs : array();

            $txids                          = array();
            $total_received_unconfirmed_sat = $total_received_confirmed_sat = 0;
            $double_spend                   = false;

            // Get confidence for tx batch if the merchant accepts zeroconf, else use 0/1 value from address/full endpoint
            $batch_confidence = $pc_conf['min_confidence'] > 0 && $pc_conf['min_conf'] < 1 ? self::blockcypher_tx_confidence($transactions, $order_data->payment_currency, $api_context) : array();

            $count = count($transactions);
            // Only calculate tx amounts if there are txs in the API response
            foreach ($transactions as $transaction) {

                //$payment_array[$order_data->order_id]['result'] = sprintf('%s|%s', strtotime($order_data->created_at), strtotime($transaction->received));
                // Only consider the transaction output if the lock time is ok and it is either unconfirmed or the order has been created before the transaction

                // Skip if address reuse (more than one tx or already confirmed tx was sent to the address before the order existed)
                $tx_ts = strtotime($transaction->received);
                $is_fresh = (bool)(!isset($transaction->received) || !$tx_ts || strtotime($order_data->created_at) < ($tx_ts + 3600));
                if (!$is_fresh) {
                    $data = array(sprintf('possible address reuse detected - ignoring transaction %s', $transaction->hash) => array('order_created_at' => $order_data->created_at, 'order_ts' => strtotime($order_data->created_at), 'tx_ts' => $tx_ts));
                    CW_AdminMain::cryptowoo_log_data(0, __FUNCTION__, $data, 'cryptowoo-tx-update.log');
                    continue;
                }

                // Block height = -1 if unconfirmed
                $is_unconfirmed = (bool)((isset($transaction->confirmations) && (int)$transaction->confirmations < 1) || (int)$transaction->block_height == -1);

                // Maybe check lock time
                $locktime_ok = $is_unconfirmed && isset($chain_height) ? CW_OrderProcessing::check_tx_lock_time($transaction, $chain_height) : true;

                if ($locktime_ok) {

	                if($pc_conf['min_confidence'] > 0) {
		                // Get tx confidence from the batch if the merchant accepts zeroconf, else use 0/1 value from address/full endpoint
		                $c          = isset( $batch_confidence[ $transaction->hash ] ) && is_numeric( $batch_confidence[ $transaction->hash ] ) ? (float) $batch_confidence[ $transaction->hash ] : (float) $transaction->confidence;
		                $confidence = $pc_conf['min_confidence'] > 0 ? $c : 1;
	                } else {
	                	// Raw zeroconf
	                	$confidence = $transaction->confirmations = 1;
	                }
                    // Determine age of the transaction
                    //$time = strtotime($transaction->received);
                    //$tx_age = time() - $time;

                    // Add all outputs of the tx that go to the payment address
                    $amount_received = self::get_sum_outputs($order_data, $transaction->outputs);

                    // If the transaction is unconfirmed check tx sequence number (to prevent RBF)
                    $is_rbf = $is_unconfirmed ? CW_OrderProcessing::check_input_sequences($transaction) : false;

                    // Add tx amount to total amount received
                    // if transaction confidence is good or it has more than the required minimum confirmations
                    if (((!$is_rbf && $confidence >= (float)$pc_conf['min_confidence']) || !$is_unconfirmed) && (int)$transaction->confirmations >= $pc_conf['min_conf']) {

                        // Add tx amount to total amount received
                        $total_received_confirmed_sat += $amount_received;
                        /*
                        if ((int)$transaction->confirmations < 1) {
                            // Calculate the amount that is not spendable yet
                            $not_spendable_sat += $amount_received;
                        } */
                    } else {
                        if($is_rbf) {
                            if ((bool)$options['logging']['transactions']) {
                                CW_AdminMain::cryptowoo_log_data(0, __FUNCTION__, sprintf('replace-by-fee flag detected - no zeroconf for %s', $transaction->hash), 'cryptowoo-tx-update.log');
                            }
                            $transaction->hash .= '-RBF';
                        }
                        $total_received_unconfirmed_sat += $amount_received;
                    }

                    if ((bool)$transaction->double_spend) {
                        $txids[$transaction->hash] = "DOUBLESPEND-{$confidence}|{$amount_received}"; // Add notice about double spend to txid array
                        $double_spend              = true;
                        // Action hook for double spend alert
                        do_action('cryptowoo_doublespend', $txids);
                    } else {
                        $txids[$transaction->hash] = $amount_received;
                    }
                } // Check block height
            } // Foreach transaction
            $total_received_unconfirmed = !(bool)$double_spend ? $total_received_unconfirmed_sat : 0; /// 100000000 : 0;
            $total_received_confirmed   = !(bool)$double_spend ? $total_received_confirmed_sat : 0; /// 100000000 : 0;
            $txids_serialized           = serialize($txids);

            // Prepare tx update result for order
            $payment_array[$order_data->order_id] = array('status' => is_array($api_data) ? "Blockcypher: {$status}" : sprintf('Blockcypher error: %s %s', $status, var_export($api_data, true)),
                                                          'address' => $order_data->address,
                                                          'order_id' => $order_data->order_id,
                                                          'total_received_confirmed' => $total_received_confirmed,
                                                          'total_received_unconfirmed' => $total_received_unconfirmed,
                                                          'tx_confidence' => isset($confidence) ? (float)$confidence : 'none',
                                                          'tx_count' => $count,
                                                          'txids_serialized' => $txids_serialized,);
            if ((bool)$options['logging']['transactions']) {
                file_put_contents(CW_LOG_DIR . 'cryptowoo-tx-update.log', date('Y-m-d H:i:s') . " Blockcypher|#{$order_data->order_id}|{$order_data->address}|{$txids_serialized}\r\n", FILE_APPEND);
            }
            //$timeago = time() - (int)$order_data->last_update;

            // Force order processing if the order will time out within the next 5.5 minutes
            if (!isset($payment_array[$order_data->order_id]['force_update']) || $payment_array[$order_data->order_id]['force_update'] !== 'yes') {
                $time                                                 = time();
                $long_ago                                             = $time - (int)$order_data->last_update > 60 ? true : false;
                $payment_array[$order_data->order_id]['force_update'] = $long_ago || ((int)$order_data->timeout_value - $time) < 330 ? 'yes' : 'no';
            }

            // Calculate order age
            $payment_array[$order_data->order_id]['order_age'] = time() - strtotime($order_data->created_at);

            // Maybe update order data // TODO move to update_tx_details()
            if (strpos($payment_array[$order_data->order_id]['status'], 'success') || $payment_array[$order_data->order_id]['force_update'] === 'yes') {

                // Force order processing since we have new tx data
                $payment_array[$order_data->order_id]['force_update'] = 'yes'; // TODO Revisit force order update

                // Update payments table TODO batch up and update in one query
                $dbupdate += CW_OrderProcessing::update_address_info($order_data->address, $payment_array[$order_data->order_id]['total_received_confirmed'], $payment_array[$order_data->order_id]['total_received_unconfirmed'], $txids_serialized, $order_data->order_id);

                // Update order meta
                $order_meta = array('received_confirmed' => $payment_array[$order_data->order_id]['total_received_confirmed'],
                                    'received_unconfirmed' => $payment_array[$order_data->order_id]['total_received_unconfirmed'],
                                    'txids' => $txids_serialized,
                                    'has_txids' => !empty($txids),);

                CW_OrderProcessing::cwwc_update_order_meta($order_data->order_id, $order_meta);

            } else {
                $payment_array[$order_data->order_id] = array_merge($payment_array[$order_data->order_id], array('timeout_in' => $order_data->timeout_value - time(),
                                                                                                                 'timeout' => $order_data->timeout));
            }
            unset($txids_serialized);
        }
        $payment_array['dbupdate'] = $dbupdate;
        return $payment_array;
    }

	/**
	 * Add up all outputs for a payment address
	 *
	 * @param $order_data
	 * @param array $outputs
	 * @return int
	 */
	static function get_sum_outputs($order_data, $outputs = array()) {
		$amount_received = 0;

		foreach ($outputs as $output) {
			$output_addresses = isset($output->addresses) && is_array($output->addresses) ? $output->addresses : array();
			if(in_array($order_data->address, $output_addresses)) {
				$amount_received += (int)$output->value;
			}
		}
		return (int)$amount_received;
	}
}