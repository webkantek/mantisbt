<?php
if (!defined('ABSPATH')) {die();}// Exit if accessed directly
/**
 * Process orders
 *
 * @category  CryptoWoo
 * @package CryptoWoo
 * @subpackage OrderProcessing
 */
class CW_OrderProcessing
{

    /**
     *
     * Convert currency amounts from float to integer to use lowest non-divisible unit for calculations.
     *
     * There are 10^8 satoshis in a single bitcoin (100,000,000s = 1BTC), 10^8 base units per litecoin, and 10^8 koinus per dogecoin (100,000,000k = 1DOGE).
     * Display amounts are calculated afn formatted in CW_Formatting::fbits($amount, $dec_places).
     *
     * @param $value
     * @return int
     *
     */
    static function cw_float_to_int($value) {
        return (int)round($value * 1e8);
    }

    /**
     * Get paid status of address in table payments
     *
     * @package OrderProcess
     * @param $address
     * @return bool
     */
    static function paid($address) {
        global $wpdb;
	    $sql = $wpdb->prepare("SELECT paid FROM {$wpdb->prefix}cryptowoo_payments_temp WHERE address = '%s';", $address);
        $query = $wpdb->get_var($sql);

        if ($query === 1) {
            return true;
        } else {
            return $query;
        }

    }

    /**
     * Return payment details of an order via order ID or payment address, newest first
     *
     * @package OrderProcess
     * @param $column
     * @param string $where
     * @return null
     */
    static function get_payment_details($column, $where = 'address') {
        global $wpdb;
	    $sql = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}cryptowoo_payments_temp WHERE {$where} = %s ORDER BY id DESC;", $column);
	    $query = $wpdb->get_results($sql);
        return $query ? $query[0] : null;
    }

    /**
     * Return all orders in table cryptowoo_payments_temp that are
     *        - have not been paid yet
     *    - are older than min_order_age seconds
     *        - have not timed out (being 0 or 3)
     *        - have an order amount > 0
     *
     * Orders that weren't updated lately take priority
     *
     * @package OrderProcess
     * @param $currency
     * @return array
     *
     */
    static function get_unpaid_addresses($currency = false) {
        global $wpdb;

        $data = array();
        if (!$currency) {
	        $sql = "SELECT * FROM {$wpdb->prefix}cryptowoo_payments_temp WHERE paid = '0' AND timeout != '1' AND timeout != '4' AND crypto_amount > 0 ORDER BY date(last_update) ASC ;";
        } else {
	        $sql = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}cryptowoo_payments_temp WHERE paid = '0' AND timeout != '1' AND timeout != '4' AND crypto_amount > 0 AND payment_currency = %s ORDER BY date(last_update) ASC ;", $currency);
        }
	    $query = $wpdb->get_results($sql);
        foreach ($query as $res) {
            $data[] = $res;
        }

        return $data;
    }

    /**
     * Get orders from table cryptowoo_payments_temp for a specific currency that are marked as paid and contain only confirmed coins
     * Return orders for all currencies if $currency_name is not specified
     *
     * @package OrderProcess
     * @param $currency
     * @return array
     */
    static function get_paid_orders($currency = false) {
        global $wpdb;

        $data = array();
        if (!$currency) {
	        $sql = "SELECT * FROM {$wpdb->prefix}cryptowoo_payments_temp WHERE received_confirmed > 0 AND received_unconfirmed = '0' AND paid = '1' AND crypto_amount > 0 AND is_archived= '0';";
        } else {
            // No currency specified -> Return all unarchived addresses hat are marked as paid and contain only confirmed coins
	        $sql = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}cryptowoo_payments_temp WHERE received_confirmed > 0 AND received_unconfirmed = '0' AND paid = '1' AND crypto_amount > 0 AND is_archived = '0' AND payment_currency = %s;", $currency);
        }
	    $query = $wpdb->get_results($sql);
        foreach ($query as $res) {
            $data[] = $res;
        }
        return $data;
    }

    /**
     * Return orders from table cryptowoo_payments_temp that have been marked as paid but still contain unconfirmed transactions
     * Orders that weren't updated lately take priority
     *
     * @package OrderProcess
     * @param bool $currency
     * @return array
     */
    static function get_unconfirmed_addresses($currency = false) {
        global $wpdb;

        $data = array();
	    if (!$currency) {
            $sql = "SELECT * FROM {$wpdb->prefix}cryptowoo_payments_temp WHERE received_unconfirmed > 0 AND crypto_amount > 0 AND paid > 0 AND timeout != '1' AND timeout < '4' ORDER BY date(last_update) ASC;";
        } else {
            $sql = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}cryptowoo_payments_temp WHERE received_unconfirmed > 0 AND crypto_amount > 0 AND paid > 0 AND timeout != '1' AND timeout < '4' AND payment_currency = %s ORDER BY date(last_update) ASC;", $currency);
        }
	    $query = $wpdb->get_results($sql);
        foreach ($query as $res) {
            $data[] = $res;
        }
        return $data;
    }

    /**
     * Update payment address info in database
     *
     * @package OrderProcess
     *
     * @param $address
     * @param $amount_received
     * @param $amount_unconfirmed
     * @param $txids_serialized
     * @param $invoice_number
     * @return mixed
     */
    static function update_address_info($address, $amount_received, $amount_unconfirmed, $txids_serialized, $invoice_number) {

        global $wpdb;

        return $wpdb->update(// Table
            "{$wpdb->prefix}cryptowoo_payments_temp", // Data
            array('received_confirmed' => $amount_received, // int
                  'received_unconfirmed' => $amount_unconfirmed, // int
                  'txids' => $txids_serialized, // string
                  'last_update' => gmdate('Y-m-d H:i:s') // string
            ), // WHERE clause
            array('address' => $address, // string
                  'invoice_number' => $invoice_number // int
            ), array('%d',    // value1
                     '%d',    // value2
                     '%s',    // value3
                     '%s'    // value4
            ), // WHERE clause format
            array('%s',
                  '%d'));
    }

    /**
     * Update last_update timestamp for address
     *
     * @param $address
     * @param $invoice_number
     * @return mixed
     */
    static function bump_last_update($address, $invoice_number) {

	    global $wpdb;

	    return $wpdb->update(// Table
		    "{$wpdb->prefix}cryptowoo_payments_temp",
		    array(
			    'last_update' => gmdate( 'Y-m-d H:i:s' ) // string
		    ), array(
			    'address'        => $address, // string
			    'invoice_number' => $invoice_number // int
		    ), array(
			    '%s' // string
		    ), // WHERE clause format
		    array(
			    '%s',
			    '%d'
		    ) );
    }

    /**
     * Update order status info in database
     *
     * @package OrderProcess
     *
     * @param $address
     * @param $paid
     * @param $timeout
     * @param $invoice_number
     * @return mixed
     */
    static function update_order_info($address, $paid, $timeout, $invoice_number) {
        global $wpdb;

        return $wpdb->update(// Table
            "{$wpdb->prefix}cryptowoo_payments_temp", // Data
            array('paid' => (int)$paid, // int
                  'timeout' => (int)$timeout // int
            ), // WHERE clause
            array('address' => $address, // string
                  'invoice_number' => $invoice_number // int
            ), // Data format
            array('%d',    // value1
                  '%d'    // value2
            ), // WHERE clause format
            array('%s',
                  '%d'));
    }

    /**
     * Flatten array
     *
     * @param array $array
     * @param $nokeys bool
     * @return array
     */
    static function flatten_array(array $array, $nokeys = false) {
        $return = array();
        if ($nokeys) {
            array_walk_recursive($array, function ($a) use (&$return) {
                $return[] = $a;
            });
        } else {
            array_walk_recursive($array, function ($a, $b) use (&$return) {
                $return[$b] = $a;
            });
        }
        return $return;
    }

    /**
     * Maybe override tx update time interval based on rate limit error transient and number of open orders.
     *
     * Checks BlockCypher API limit by checking https://api.blockcypher.com/v1/tokens/$YOURTOKEN
     *
     * @param $address_count
     * @param $currency
     * @param $global_limit_transient
     * @param $options
     * @return int
     */
    static function maybe_override_interval($address_count, $currency, $global_limit_transient, $options) {

        $bc_status = CW_AdminMain::get_blockcypher_limit($options);
        if (is_array($bc_status) && isset($bc_status['limit_hour'])) {
            $interval = $bc_status['limit_hour'] / 6;
            $hits     = isset($bc_status['hits_hour']) ? $bc_status['hits_hour'] : 'not_set';
        } else {
            $hits = 'not_set';
        }

        $global_limit    = isset($global_limit_transient[$currency]['count']) ? (int)$global_limit_transient[$currency]['count'] : 1;
        $interval_option = (int)str_replace('seconds_', '', $options['soft_cron_interval']);
        $interval        = isset($interval) && $interval >= $interval_option ? $interval : $interval_option;

        if ($address_count > 1) {
            $interval += intval($interval * 0.25) * (int)$address_count * $global_limit;
        }

        if ((bool)$options['logging']['transactions']) {
            $result = $interval_option === $interval ? "Interval unchanged {$interval}" : sprintf('Interval override: %d to %d', $interval_option, intval($interval));
            file_put_contents(CW_LOG_DIR . 'cryptowoo-tx-update.log', date('Y-m-d H:i:s') . " {$result}|addr_count:{$address_count}|hits/hour:{$hits}\r\n", FILE_APPEND);
        }

        return $interval;
    }

    /**
     * Figure out how to connect to the block chain for this currency
     *
     * @param $currency
     * @param $limit_transient
     * @param $address_count
     * @param $options
     * @return stdClass
     */
    static function get_tx_api_config($currency, $limit_transient, $address_count, $options) {

        $api_config                  = new stdClass();
        $api_config->skip_this_round = false;

        // Check if we have a Block.io API key for this currency
        $is_testnet                     = strpos($currency, 'TEST');
        $stripped                       = $is_testnet ? strtolower(str_replace('TEST', '_test', $currency)) : strtolower($currency);
        $api_config->api_key_identifier = "cryptowoo_{$stripped}_api";
        $api_config->block_io_api_key   = isset($options[$api_config->api_key_identifier]) && $options[$api_config->api_key_identifier] !== '' ? $options[$api_config->api_key_identifier] : false;

        $failing_api               = isset($limit_transient[$currency]['api']) ? $limit_transient[$currency]['api'] : false;
        $api_config->address_count = $address_count;

        // Determine which blockexplorer to use: If we don't have Block.io API keys and unless something else is specified, use Blockcypher.com
        $processing_api_id = sprintf('processing_api_%s', strtolower(str_replace('TEST', '', $currency)));
        //if (isset($options[$processing_api_id]) && !empty($options[$processing_api_id])) {
        $api_config->tx_update_api = $options[$processing_api_id] === 'block_io' && !$api_config->block_io_api_key ? 'blockcypher' : $options[$processing_api_id];

        if ($options[$processing_api_id] === 'custom' && $is_testnet === false) {
            // Check if we have a custom API URL
            $has_custom_url            = CW_Validate::check_if_unset("custom_api_{$stripped}", $options, false);
            $api_config->tx_update_api = $has_custom_url ? 'insight' : $api_config->tx_update_api;
        }
        // TODO Fall back if there is a configuration error and we have an alternative

        // Maybe limit Blockcypher API calls based on the number of open orders
        if ($api_config->tx_update_api === 'blockcypher' && (bool)$options['limit_blockcypher_rate']) {
            //$rate_limit = self::get_rate_limit($address_count, $limit_reached, $options);
            $estimated_min_interval = self::maybe_override_interval($address_count, $currency, $limit_transient, $options);
            $last_update            = get_option('cryptowoo_last_tx_update');
            if (isset($last_update['blockcypher'])) {
                // Skip if the last update has happened within the estimated minimum interval
                $api_config->skip_this_round = (time() - (int)$last_update['blockcypher']) < (int)$estimated_min_interval ? true : false;
                $api_config->time_diff       = (time() - (int)$last_update['blockcypher']) . ' < ' . (int)$estimated_min_interval;
            }
        }

        // If the fallback is enabled
        // If we run into API limits while using Blockcypher, maybe use Block.io API as fallback.
        // If we don't have Block.io API keys for this currency or Block.io is failing, use single-address polling via chain.so
        // If chain.so fails and the currency is bitcoin, try smartbit
        // If smartbit fails and the currency is bitcoin or bitcoin testnet, use chain.so
        if ((bool)$options['processing_fallback'] && ((bool)$failing_api || $api_config->skip_this_round)) {
	        if ( $api_config->tx_update_api === 'blockcypher' || $api_config->tx_update_api === 'block_io' ) {

		        if ( in_array( $currency, CW_BlockIo::get_supported_currencies() ) ) {
			        $api_config->tx_update_api = $api_config->block_io_api_key && $failing_api !== 'block_io' ? 'block_io' : 'chain_so';

			        // We want to use chain.so as fallback - check if our current error transient has a chain.so error, if it does, try smartbit
			        $api_config->skip_this_round = $api_config->tx_update_api === 'chain_so' && $failing_api === 'chain_so';

			        // Smartbit fallback for BTC - else skip this round
			        if ( false !== strpos( $currency, 'BTC' ) && $api_config->skip_this_round && $failing_api !== 'smartbit' ) {
				        $api_config->tx_update_api   = 'smartbit';
				        $api_config->skip_this_round = false;
			        }
		        } else {
			        // This currency is not supported by Block.io or chain.so
			        // apply_filters or skip this round
			        $api_config->skip_this_round = true;
			        $api_config                  = apply_filters( 'cw_get_tx_api_config', $api_config, $currency );

		        }

	        } elseif ( $api_config->tx_update_api === 'insight' ) {
		        // Catch failing insight API
		        if ( in_array( $currency, CW_BlockIo::get_supported_currencies() ) ) {
			        $api_config->tx_update_api = (bool) $api_config->block_io_api_key ? 'block_io' : 'chain_so';

			        // If we don't have a block.io key and chain.so fails
			        $api_config->skip_this_round = $api_config->tx_update_api === 'chain_so' && $failing_api === 'chain_so' || $api_config->tx_update_api === 'block_io' && $failing_api === 'block_io';

			        // Smartbit fallback for BTC - else skip this round
			        if ( false !== strpos( $currency, 'BTC' ) && $api_config->skip_this_round && $failing_api !== 'smartbit' ) {
				        $api_config->tx_update_api   = 'smartbit';
				        $api_config->skip_this_round = false;
			        }
		        } elseif ( $currency === 'BLK' ) {
			        $api_config->tx_update_api = 'cryptoid';
			        // Skip this round if we don't have a cryptoID API key
			        $api_config->skip_this_round = (bool) CW_Validate::check_if_unset( 'cryptoid_api_key', $options, false ) ? false : true;
		        } else {
			        // Custom filter last resort
			        // apply_filters or skip this round
			        $api_config->skip_this_round = true;
			        $api_config                  = apply_filters( 'cw_get_tx_api_config', $api_config, $currency );
		        }
	        } elseif ( $api_config->tx_update_api === 'smartbit' && $failing_api !== 'chain_so' ) {
		        $api_config->tx_update_api   = 'chain_so';
		        $api_config->skip_this_round = false;
	        }
        }
        if ((bool)$api_config->skip_this_round) {
            $api_config->block_io_api_key = 'not_logged';
            CW_AdminMain::cryptowoo_log_data(0, __FUNCTION__, array($api_config), 'cryptowoo-tx-update.log');
        }

        //$api_config->tx_update_api = 'smartbit';
        return $api_config;
    }

    /**
     * Update transaction data for addresses belonging to open orders during cron
     * Orders with a smaller average block time and the nearest timeout value take priority.
     * If we run into API limits, maybe use Block.io API as fallback.
     * If we don't have Block.io API keys for this currency or Block.io is failing, use single-address polling via chain.so.
     *
     * @package OrderProcess
     * @param $options
     * @return string
     */
    static function update_tx_details($options = false) {

        date_default_timezone_set('UTC');
        if (!$options) {
            $options = get_option('cryptowoo_payments');
        }

        // Batch up addresses for each currency
        $orders = self::batch_up_orders_per_currency();
        //$force_update_block_io = $force_update_blockcypher = $force_update_chain_so = 'no';

        // Get universal rate limit transient
        $limit_transient = get_transient('cryptowoo_limit_rates');

        $last_tx_update_db = get_option('cryptowoo_last_tx_update');
        $last_tx_update    = $last_tx_update_db ? $last_tx_update_db : array();

        if (isset($orders['count']) && $orders['count'] > 0) {

            $batch_data = array();

            if (isset($orders['batches'])) {

                // Split order data from prepared batch addresses
                $batches       = $orders['batches'];
                $address_count = $orders['count'];
                unset($orders['batches']);
                unset($orders['count']);

                // Loop through each currency batch
                foreach ($batches as $batch_currency => $batch) {

                    if (!empty($batch)) {

                        // Check which processing API we'll be using
                        $processing = self::get_tx_api_config($batch_currency, $limit_transient, $address_count, $options);

                        if ($processing->skip_this_round) {
                            $batch_data[$batch_currency] = "Skipped this round to prevent too many {$processing->tx_update_api} requests.";
                            continue;
                        }

                        if ($processing->tx_update_api === 'block_io' && $processing->block_io_api_key) {
                            // Use Block.io API TODO check get_transactions request limit
                            $batch_data[$batch_currency] = CW_BlockIo::block_io_batch_tx_update($batch, $batch_currency, $orders[$batch_currency], $options, $processing->block_io_api_key);
                            //$force_update_block_io = self::maybe_force_order_update('yes', $batch_data[$batch_currency]);
                            usleep(250000); // Max ~4 requests/second with Block.io
                        } elseif ($processing->tx_update_api === 'blockcypher' && $batch_currency !== 'DOGETEST') {
                            // if ($timeago > $rate_limit) {// only update balance if data is older than x seconds TODO revisit tx update rate limiting
                            // Use Blockcypher API
                            // Batch requests count against the limits -> max 3 addresses for free account users
                            $prio_orders = CW_OrderSorting::prioritize_unpaid_addresses($orders[$batch_currency], 3); // TODO increase for paid BlockCypher accounts
							// Get prioritized batch of addresses
	                        $addr_batch = array();
	                        foreach ($prio_orders as $order) {
	                        	$addr_batch[] = $order->address;
	                        }

	                        //$prio_orders =  isset($options['blockcypher_token']) && !empty($options['blockcypher_token']) ? $orders[$batch_currency] : self::prioritize_unpaid_addresses($orders[$batch_currency], 5);
                            $batch_data[$batch_currency] = CW_Blockcypher::blockcypher_batch_tx_update($batch_currency, $addr_batch, $prio_orders, $options);
                            //$force_update_blockcypher = self::maybe_force_order_update('yes', $batch_data[$batch_currency]);
                            sleep(1); // Max ~3 requests/second TODO remove when we have proper rate limiting
                            //}
                        } elseif ($processing->tx_update_api === 'insight') {
                            // Insight API: Batch up 5 addresses
                            $prio_orders = CW_OrderSorting::prioritize_unpaid_addresses($orders[$batch_currency], 5); // TODO admin setting for orders/currency/interval
                            //$prio_orders =  isset($options['blockcypher_token']) && !empty($options['blockcypher_token']) ? $orders[$batch_currency] : self::prioritize_unpaid_addresses($orders[$batch_currency], 5);
                            $batch_data[$batch_currency] = CW_Insight::insight_batch_tx_update($batch_currency, $batch, $prio_orders, $options);
                            //$force_update_blockcypher = self::maybe_force_order_update('yes', $batch_data[$batch_currency]);
                            usleep(333333); // Max ~3 requests/second TODO remove when we have proper rate limiting
                        } elseif ($processing->tx_update_api === 'cryptoid') {
                            // cryptoID.info API: Single address polling
                            $prio_orders                 = CW_OrderSorting::prioritize_unpaid_addresses($orders[$batch_currency], 1);
                            $batch_data[$batch_currency] = CW_CryptoID::batch_tx_update($batch_currency, $batch, $prio_orders, $options);
                            //$force_update_cryptoid = self::maybe_force_order_update('yes', $batch_data[$batch_currency]);
                            usleep(333333); // Max ~3 requests/second
                        } elseif ($processing->tx_update_api === 'chain_so') {
                            // Chain.so fallback: Single-address polling, only 1 address per currency per interval
                            $fallback_orders             = CW_OrderSorting::prioritize_unpaid_addresses($orders[$batch_currency], 1);
                            $batch_data[$batch_currency] = CW_ChainSo::chain_so_single_tx_update($batch_currency, $fallback_orders, $options);
                            //$force_update_chain_so = self::maybe_force_order_update('yes', $batch_data[$batch_currency]);
                            usleep(250000); // Max ~4 requests/second with Chain.so
                        } elseif ($processing->tx_update_api === 'smartbit') {
                            //  Smartbit.com.au: Single-address polling, only 1 address per currency per interval
                            $fallback_orders             = CW_OrderSorting::prioritize_unpaid_addresses($orders[$batch_currency], 1);
                            $batch_data[$batch_currency] = CW_Smartbit::smartbit_single_tx_update($batch_currency, $fallback_orders, $options);
                            //$force_update_chain_so = self::maybe_force_order_update('yes', $batch_data[$batch_currency]);
                            usleep(250000); // Max ~4 requests/second with Smartbit.com.au
                        } else {
	                        $batch_data = apply_filters('cw_update_tx_details', $batch_data, $batch_currency, $orders[$batch_currency], $processing, $options);
	                        if(!isset($batch_data[$batch_currency]) || !count($batch_data[$batch_currency])) {
		                        $batch_data[ $batch_currency ] = array( "{$batch_currency}_API" => sprintf('Nothing new from %s', $processing->tx_update_api) );
		                        CW_AdminMain::cryptowoo_log_data( 0, __FUNCTION__, $batch_data[ $batch_currency ], 'cryptowoo-tx-update.log' );
	                        }
                        }
                        $last_tx_update[$processing->tx_update_api] = time();
                    }
                }
                /*
                if(WP_DEBUG) {
                    file_put_contents(CW_LOG_DIR . 'cryptowoo-tx-update.log', date('Y-m-d H:i:s') . ' batch_tx_update ' . var_export($batch_data, true) . "\r\n", FILE_APPEND);
                }
                */
            }
            $update_stats = array('api_error_transient' => $limit_transient,
                                  // TODO Revisit force order update
                                  //'execute_order_update' => 'yes',//in_array('yes', array($force_update_block_io, $force_update_blockcypher, $force_update_chain_so)) ? 'yes' : 'no',
                                  'payment_data' => $batch_data);
            //$update_stats = $payment_array;
        } else {
            // We don't have unpaid addresses
            $update_stats['info'] = esc_html__('No unpaid addresses found', 'cryptowoo');
        }
        update_option('cryptowoo_last_tx_update', $last_tx_update);
        return $update_stats;
    }

    /**
     * Check the tx update response for force_update
     *
     * @todo Revisit later
     *
     * @param $id
     * @param $array
     * @return int|null|string
     */
    static function maybe_force_order_update($id, $array) {
        $return = 'no';
        foreach ($array as $key => $val) {
            if ($val['force_update'] === $id) {
                $return = $val['force_update'];
            }
        }
        return $return;
    }

    /**
     * Update transaction data for addresses belonging to open orders during cron
     *
     * @return array
     */
    static function batch_up_orders_per_currency() {

        date_default_timezone_set('UTC');

        // Get unpaid order's payment addresses
        $unpaid_addresses_raw = self::get_unpaid_addresses(false);
        $address_count        = count($unpaid_addresses_raw);

        if ($address_count <= 0) {

            // If we currently don't have any open unpaid orders,
            // get the addresses that are marked as paid but still have unconfirmed amounts in the DB and update their status
            $unpaid_addresses_raw = self::get_unconfirmed_addresses();
            $address_count        = count($unpaid_addresses_raw);
        }
        // If we have more than one unpaid address, prioritize and sort them by average block time
        $unpaid_addresses          = !empty($unpaid_addresses_raw) && $address_count > 0 ? CW_OrderSorting::sort_unpaid_addresses($unpaid_addresses_raw) : $unpaid_addresses_raw;
        $unpaid_addresses['count'] = $address_count;
        return $unpaid_addresses;
    }


    /**
     * Prevent acceptance of Replace-by-Fee and timelocked transactions https://github.com/bitcoin/bitcoin/pull/6871
     *
     * A nSequence below (MAX-1) represents opt-in RBF. https://github.com/bitcoin/bips/blob/master/bip-0125.mediawiki
     * A nSequence below MAX represents a transaction with a locktime.
     *
     * Thus, as long as the locktime isn't preventing next block acceptance, the sequence number of a transaction input has to be >= MAX-1 (0xFFFFFFFE = 4294967294) to be taken into account for zeroconf.
     *
     * Bitcoin Core 0.12.0 uses nLocktime to help prevent fee sniping https://github.com/bitcoin/bitcoin/pull/6216/files
     *
     *    The reason you see a lot of (MAX-1) nSequences on the network below (MAX) on at least one input is required for the nLocktime field to be consensus enforced.
     *
     *      This is not a serious issue today with high block subsidies to transaction fee ratios, but a good measure to implement now so that it becomes standard policy as the subsidy drops, average fees per bytes increases, and block sizes grow)
     *    and because more transactions are spending OP_CLTV-protected outputs which require the spending transaction set nLocktime.
     *    Opt-in RBF's signalling criteria was specifically designed to allow those other users of locktime to be able to continue using it without signalling RBF,
     *    which is why opt-in RBF is below (MAX-1) rather than less than or equal to (MAX-1). https://www.reddit.com/r/Bitcoin/comments/47upgx/nsequence_and_optin_replacebyfee_difference/d0fsno0
     *
     * Read more about fee sniping at self::check_tx_lock_time($transaction, $chain_height)
     *
     * @param $transaction
     * @return bool
     */
    static function check_input_sequences($transaction) {

        $input_is_rbf = array();
        if(!isset($transaction->inputs) || !is_array($transaction->inputs)) {
            // No inputs present - in dubio pro reo
            return false;
        }
        foreach ($transaction->inputs as $input) {
            $input_is_rbf[] = (int)$input->sequence < 4294967294; // 0xFFFFFFFE === UINT_MAX - 1 === 4294967295 - 1 ;
        }
        return in_array(true, $input_is_rbf);
    }

    /**
     * Prevent acceptance of timelocked transactions by checking against the current block height
     *
     *  // Discourage fee sniping. https://github.com/bitcoin/bitcoin/blob/master/src/wallet/wallet.cpp#L1975
     *
     *    For a large miner the value of the transactions in the best block and
     *    the mempool can exceed the cost of deliberately attempting to mine two
     *    blocks to orphan the current best block. By setting nLockTime such that
     *    only the next block can include the transaction, we discourage this
     *    practice as the height restricted and limited blocksize gives miners
     *    considering fee sniping fewer options for pulling off this attack.
     *
     *    A simple way to think about this is from the wallet's point of view we
     *    always want the blockchain to move forward. By setting nLockTime this
     *    way we're basically making the statement that we only want this
     *    transaction to appear in the next block; we don't want to potentially
     *    encourage reorgs by allowing transactions to appear at lower heights
     *    than the next block in forks of the best chain.
     *
     *    Of course, the subsidy is high enough, and transaction volume low
     *    enough, that fee sniping isn't a problem yet, but by implementing a fix
     *    now we ensure code won't be written that makes assumptions about
     *    nLockTime that preclude a fix later.
     *
     *    txNew.nLockTime = chainActive.Height();
     *
     *    Secondly occasionally randomly pick a nLockTime even further back, so
     *    that transactions that are delayed after signing for whatever reason,
     *    e.g. high-latency mix networks and some CoinJoin implementations, have
     *    better privacy.
     *
     *    if (GetRandInt(10) == 0)
     *          txNew.nLockTime = std::max(0, (int)txNew.nLockTime - GetRandInt(100));
     *
     * @param $transaction
     * @param $chain_height
     * @return bool
     */
    static function check_tx_lock_time($transaction, $chain_height) {
        return (int)$transaction->lock_time <= (int)$chain_height + 1; // Height +1 for the next block from now
    }

    /**
     * Get the confidence for a given transaction ID via wp_remote_get() from blockcypher.com or chain.so
     *
     *
     * @param $currency
     * @param $txid
     * @param string $method
     * @return int
     */
    static function get_tx_confidence($currency, $txid, $method = 'blockcypher') {

        $args = array('timeout' => 5,
                      'redirection' => 5,
                      'httpversion' => '1.0',
                      'user-agent' => 'localhost',
                      'blocking' => true,
                      'headers' => array(),
                      'cookies' => array(),
                      'body' => null,
                      'compress' => false,
                      'decompress' => true,
                      'sslverify' => true,
                      'stream' => false,
                      'filename' => null);

        $network = $currency === 'BTCTEST' ? 'btc/test3' : strtolower($currency) . '/main';
        $url     = $method === 'blockcypher' ? "http://api.blockcypher.com/v1/{$network}/txs/{$txid}/confidence" : "https://chain.so/api/v2/get_confidence/{$currency}/{$txid}";

        $response     = wp_remote_get($url, $args);
        $api_response = isset($response['body']) ? json_decode($response['body']) : false;
        $confidence   = 0;
        if (!is_wp_error($api_response)) {
            if ($method === 'blockcypher' && isset($api_response->confidence)) {
                $confidence = $api_response->confidence;
            } else {
                if (isset($api_response->data->confirmations) && (int)$api_response->data->confirmations < 1) {
                    $confidence = isset($api_response->data->confidence) ? (float)$api_response->data->confidence : 0;
                } else {
                    $confidence = !isset($api_response->data->confirmations) ? 0 : 1;
                }
            }
        } else {
            // Rate limit transient
            $limit_transient = get_transient('cryptowoo_limit_rates');
            // Update rate limit transient
            $limit_transient[$currency] = isset($limit_transient[$currency]['count']) ? array($currency => array('count' => (int)$limit_transient[$currency]['count'] + 1,
                                                                                                'api' => $method)) : array($currency => array('count' => 1,
                                                                                                                                              'api' => $method));
            // Keep error data for 30 minutes. We'll try again after that time.
            set_transient('cryptowoo_limit_rates', $limit_transient, 1800);
            $options = get_option('cryptowoo_payments');
            if ((bool)$options['logging']['transactions']) {
                CW_AdminMain::cryptowoo_log_data(0, __FUNCTION__, $api_response, 'cryptowoo-tx-update.log');
            }
        }
        usleep(333333); // ~3 req/sec
        return $confidence;
    }


    /**
     * Get minimum number of confirmations for $currency while honoring max order amount threshold for zeroconf acceptance
     *
     * @todo More fine grained confirmation requirements for different order amounts
     *
     * @package OrderProcess
     *
     * @param $currency
     * @param $order_amount
     * @param bool|false $options
     * @return array
     */
    static function get_processing_config($currency, $order_amount, $options = false) {

        if (!$options) {
            $options = get_option('cryptowoo_payments');
        }

        $lc_currency = strtolower(str_replace('TEST', '', $currency));

        $min_conf_key       = "cryptowoo_{$lc_currency}_min_conf"; // Confirmations
        $max_amount_key     = "cryptowoo_max_unconfirmed_{$lc_currency}"; // Order amount threshold
        $min_confidence_key = "min_confidence_{$lc_currency}"; // Transaction confidence (if applicable for currency)

        $pc_conf['min_conf'] = isset($options[$min_conf_key]) && $options[$min_conf_key] !== '' ? (int)$options[$min_conf_key] : 1;

        // Force minimum one confirmation at BLK // TODO Refactor
        if ($currency === 'BLK' && $pc_conf['min_conf'] < 1) {
            $pc_conf['min_conf'] = 1;
        }

        $max_amount                = CW_Validate::check_if_unset($max_amount_key, $options, 100);
        $pc_conf['min_confidence'] = false !== CW_Validate::check_if_unset($min_confidence_key, $options, false) ? (float)$options[$min_confidence_key] / 100 : 0.9895;

        $pc_conf = apply_filters('cw_get_processing_config', $pc_conf, $currency, $options);

        // Require at least one confirmation if the order amount is above the threshold TODO Add more options
        if ($pc_conf['min_conf'] < 1 && $max_amount > 0 && (float)$order_amount >= $max_amount) {
            $pc_conf['min_conf']++;
        }
        //CW_AdminMain::cryptowoo_log_data(0, __FUNCTION__,  $options[$min_conf_key], 'cryptowoo-cron.log');
        return $pc_conf;
    }

    /**
     * Process and update order data for open orders during cron
     *
     * @package OrderProcess
     * @param $options
     * @return string
     */
    static function process_open_orders($options = false) {

        if (!$options) {
            $options = get_option('cryptowoo_payments');
        }

        $dbupdate              = 0;
        $double_spend_detected = $address_in_use = false;
        $payment_array         = array();

        // Get unpaid order's payment addresses
        $unpaid_addresses = self::get_unpaid_addresses();
		$address_count = count($unpaid_addresses);
        if ($address_count) {

            foreach ($unpaid_addresses as $address) {
                $timeago = time() - (int)$address->last_update;

                if ($timeago > 14) {// Only update balance if data is older than x seconds

	                // Timeout status: Orders with an amount larger than the specified percentage but lower than the order amount will have $timeout = 3 before they are set to $timeout = 1
	                $timeout = (int)$address->timeout;

	                // Time in seconds until the order expires
	                $times_out_in  = (int)$address->timeout_value - time();

	                // Amounts
	                $amount_due         = (int)$address->crypto_amount;
	                $amount_unconfirmed = (int)$address->received_unconfirmed;
	                $amount_received    = (int)$address->received_confirmed;


	                // Order expiration time in seconds
	                $order_timeout = (int)$options['order_timeout_min'] * 60;

	                // Has the address already received a payment?
	                $address_in_use = ($amount_received + $amount_unconfirmed) > 0 ? true : false;

	                // Calculate percentage_paid of amount_due
	                $address->percentage_paid = (($amount_received + $amount_unconfirmed) / $amount_due) * 100;


	                // Check if CryptoWoo is still the payment method for this order
	                $order_id = $address->order_id;
	                $order = wc_get_order($order_id);

	                $status = __('Undefined Error', 'cryptowoo');

	                if( !is_object( $order ) ) {
		                $status  = sprintf( esc_html__( 'Error: Order #%s with address %s not found', 'cryptowoo' ), (int) $address->order_id, $address->address );
		                $timeout = 4;
	                } else {
		                $payment_method = $order->get_payment_method();
		                if ( $payment_method !== 'cryptowoo' ) {
			                $timeout = 4;
			                $status  = sprintf( esc_html__( 'Payment method has been changed to %s - removing address from queue', 'cryptowoo' ), $payment_method );
			                // Add order note about the changed payment method
			                if ( $address->timeout !== 4 && $timeout === 4 ) {
				                $order->add_order_note( $status );
			                }
		                }
	                }

	                // Maybe skip address
	                if($timeout > 3) {
		                $payment_array[$address->order_id] = array('status' => $status,
		                                                           'address' => $address->address,
		                                                           'paid' => 0,
		                                                           'address_in_use' => $address_in_use,
		                                                           'timeout' => $timeout,
		                                                           'times_out_in' => $times_out_in,
		                                                           'percentage_paid' => $address->percentage_paid);
		                // Keep address in table but ignore it from now on
		                CW_OrderProcessing::update_order_info($address->address, 0, 4, $order_id);
		                continue;
	                }

                    // Is the received amount larger than the amount due?
                    $address->amount_diff = $amount_received - $amount_due;
                    
                    // Underpayment notice range
                    $in_underpayment_range = (float)$address->percentage_paid <= (float)$options['underpayment_notice_range'][2] && (float)$address->percentage_paid >= (float)$options['underpayment_notice_range'][1];
                    
                    // Payment complete? Also honour upper limit of underpayment_notice_range to ignore slight underpayments
                    $paid = ((bool)$address->paid || $address->amount_diff >= 0) && $amount_due > 0 && $amount_received > 0 ? true : $amount_unconfirmed == 0 && $address->percentage_paid >= (float)$options['underpayment_notice_range'][2] && $amount_due > 0;

	                // Get Woocommerce order
	                $order = wc_get_order($address->order_id);

                    if (!$paid) {

                        // Timeout reached?
                        if ($times_out_in <= 0 && !(bool)$address_in_use) {
                            // Order has been abandoned -> cancel and remove from queue
                            $timeout = 1;
                        }
                        if ((bool)$address_in_use) {

                            // Get underpayment trigger event
                            if(is_numeric($options['underpayment_notice_trigger'])) {
                                // Expiration time based notice trigger
                                //$time_before_expiration = str_replace('timeout_', '', $options['underpayment_notice_trigger']);
                                $mispayment_trigger = $times_out_in <= (int)$options['underpayment_notice_trigger'] + 16;
                            } else {
                                // Order is still unpaid because the amount is too low but either the transaction is confirmed or time is running out
                                $mispayment_trigger = ($amount_unconfirmed == 0 && $times_out_in >= 40) || $times_out_in <= 40;
                            }

                            if (((float)$address->percentage_paid >= (float)$options['underpayment_notice_range'][2] || $in_underpayment_range) && $mispayment_trigger) {

                                // ~5 minutes before order expiration or insufficient amount confirmed
                                // a) underpayment -> notify customer and keep in update queue
                                // b) unusually long network confirmation duration -> redirect customer but keep in queue

                                // Only notify customer if the incoming amounts are in the underpayment notice range
                                if ((float)$address->percentage_paid < 100 && $in_underpayment_range && (int)$timeout != 3) {

                                    // We have only a partial payment
                                    //    either 5 minutes before $options['order_timeout_min'] is reached
                                    //    or the insufficient amount has received the required number of confirmations

                                    // "Partial" meaning maximum 100 - (float)$options['underpayment_notice_range'][1] is missing when considering all confirmed and unconfirmed txs

                                    // Prepare order note
                                    $note      = array('is_customer_note' => true,
                                                       'txnreference' => sprintf(esc_html__('Your payment is incomplete. This order has not been paid in full. Please send the missing amount within the next %d minutes.', 'cryptowoo'), round((($order_timeout + $times_out_in) / 60), 2)));

                                    // Add a note for the customer
                                    self::cw_update_wc_order($order_id, array('update' => false), $note);

                                    //  Set timeout to 3
                                    $timeout = 3;

                                } elseif ((float)$address->percentage_paid >= (float)$options['underpayment_notice_range'][2]) {

                                    // The full amount (= more than (float)$options['underpayment_notice_range'][2]) is seen on the network but has not yet received the required confirmations.
                                    if ($times_out_in <= 30) {
                                        // Redirect customer to "Order Received" page but keep order in queue
                                        $timeout    = 3;
                                        $kill_after = $options['kill_unconfirmed_after'] > 0 ? (int)$options['kill_unconfirmed_after'] * 60 * 60 : 0;

                                        //$data = array('kill_unconfirmed_after' => $options['kill_unconfirmed_after'], 'times_out_in' => absint($times_out_in), 'kill_after' => $kill_after);
                                        //CW_AdminMain::cryptowoo_log_data(0, 'kill unconfirmed after', array('data' => $data), 'cryptowoo-tx-update.log');
                                        if ((bool)$kill_after && absint($times_out_in) >= absint($times_out_in + $kill_after)) {
                                            // Transaction did not reach the required confirmations for more than kill_unconfirmed_after - manual interaction needed
                                            $timeout = 1;
                                        }
                                    } else {
                                        // Wait
                                        $timeout = 0;
                                    }
                                }
                            }

                            if ( (bool)$options['kill_unconfirmed_after'] && (int)$address->timeout == 3 && $times_out_in <= -$order_timeout && $address->percentage_paid < (float)$options['underpayment_notice_range'][2]) {

                                // Incoming transactions don't cover the order amount (but are higher than $options['underpayment_notice_range'][1])
                                // and we're already waiting for double the specified timeout $options['order_timeout_min']
	                            // and the congestion handling is not unlimited -> cancel order and request manual intervention
                                $timeout = 1;

                            } elseif ( (bool)$options['kill_unconfirmed_after'] && $address->percentage_paid < (float)$options['underpayment_notice_range'][1] && (int)$address->timeout < 3 && $times_out_in <= 0) {

                                // Customer paid less than $options['underpayment_notice_range'][1] of the order amount for longer than $options['order_timeout_min']
                                $timeout = 1;
                            }
                        }

                        // Change order status if order has timed out and notify customer and admin
                        if ((int)$timeout == 1) {

                            if ($address->percentage_paid >= (float)$options['underpayment_notice_range'][1]) {

                                $order_update = array('update' => true,
                                                      'status' => 'on-hold',
                                                      'notice' => esc_html__('Payment timeout - ', 'cryptowoo'));

                                $note = array('is_customer_note' => false,
                                              'txnreference' => sprintf(esc_html__('CryptoWoo payment failed - Percentage of order paid: %s%s. Manual interaction needed.', 'cryptowoo'), $address->percentage_paid, '%'),);

                                // Change the status to on-hold and add a note for the admin
                                self::cw_update_wc_order($order_id, $order_update, $note);

                                // Update order meta
	                            self::cwwc_update_order_meta($address->order_id, array('tx_confirmed' => 'failed - timeout'));

                                // Action hook for refund necessary
                                do_action('cryptowoo_refund_required', $address);

                            } else {

                                $order_update = array('update' => true,
                                                      'status' => 'cancelled',
                                                      'notice' => esc_html__('Payment timeout - ', 'cryptowoo'));

                                $note = array('is_customer_note' => true,
                                              'txnreference' => sprintf(esc_html__('This order has expired. Please try again or contact us if you have already sent a payment. Reference: %d', 'cryptowoo'), $order_id));

                                // Change the status to cancelled and add a note to the order
                                self::cw_update_wc_order($order_id, $order_update, $note);

                                // Update order meta
                                self::cwwc_update_order_meta($address->order_id, array('tx_confirmed' => 'cancelled - timeout'));

                            }
                        } else {

                            // Order is still unpaid but has not timed out yet - check for double spend or other issues
                            $double_spend_detected = strpos($address->txids, 'DOUBLESPEND') ? true : false;


                            if ($double_spend_detected || $amount_due <= 0 || empty($address->address)) {

                                if($double_spend_detected) {
	                                $order_update = array(
		                                'update' => false,
		                                'status' => '',
		                                'notice' => esc_html__( 'Doublespend detected - ', 'cryptowoo' )
	                                );

	                                $note = array(
		                                'is_customer_note' => false,
		                                'txnreference'     => sprintf(esc_html__( 'CryptoWoo detected a replace-by-fee or doublespend attempt. Old txid: %s', 'cryptowoo' ), $address->txids),
	                                );
                                } else {
	                                $order_update = array(
		                                'update' => true,
		                                'status' => 'cancelled',
		                                'notice' => esc_html__( 'Payment error - ', 'cryptowoo' )
	                                );

	                                $note = array(
		                                'is_customer_note' => true,
		                                'txnreference'     => esc_html__( 'We detected an error and had to cancel this order. Please try again or contact us if you already sent a payment. Sorry for the inconvenience.', 'cryptowoo' ),
	                                );
                                }
                                // Change the status to cancelled and add a note to the order
                                self::cw_update_wc_order($order_id, $order_update, $note);

                                // Update order meta
	                            self::cwwc_update_order_meta($address->order_id, array('tx_confirmed' => 'failed - error'));

                            }

                            // Maybe add order note about incoming payment
                            if ($amount_unconfirmed > 0 || $amount_received > 0) {

                                $tx_confirmed = get_post_meta($address->order_id, 'tx_confirmed', true);

                                if (empty($tx_confirmed) || $tx_confirmed === '0') {

	                                $full_amount_pending = $amount_received + $amount_unconfirmed;

	                                // Payment received notice
	                                $order->add_order_note( sprintf( esc_html__( 'Incoming Payment: %s%s%s%s', 'cryptowoo' ), CW_Formatting::fbits( $full_amount_pending ), $address->payment_currency, PHP_EOL, $address->txids ) );

	                                // Update order meta
	                                $order_meta = array(
		                                'tx_confirmed'         => 'pending', // Only add note once
		                                'received_unconfirmed' => CW_Formatting::fbits( $amount_unconfirmed ),
		                                'txids'                => $address->txids
	                                );

	                                self::cwwc_update_order_meta( $address->order_id, $order_meta );
                                }
                            }
                        } // Unpaid & not timed out
                    } // Order is unpaid

                    // Update database if status has changed
                    //if ($paid != $address->paid || $timeout != $address->timeout) { // TODO Maybe revisit to save resources

                    $payment_array[$address->order_id] = array('address' => $address->address,
                                                               'paid' => $paid,
                                                               'address_in_use' => $address_in_use,
                                                               'timeout' => $timeout,
                                                               'times_out_in' => $times_out_in,
                                                               'percentage_paid' => $address->percentage_paid,
                                                               'underpayment_range_begin' => (float)$options['underpayment_notice_range'][1],
                                                               'min_perc_needed_to_complete' => (float)$options['underpayment_notice_range'][2]);
                    $dbupdate += self::update_order_info($address->address, $paid, $timeout, $address->invoice_number);

                    // Complete payment
                    if ($paid == 1 && $amount_due > 0 && !$double_spend_detected) {
                        self::cryptowoo_complete_payment($address->order_id);

                        // Maybe mark as overpayment and add order note
                        if($address->percentage_paid > 100) {

	                        $order_meta = array(
		                        'amount_difference' =>  $address->amount_diff,
		                        'percentage_paid' => $address->percentage_paid,
	                        );

	                        self::cwwc_update_order_meta($address->order_id, $order_meta);

                            $order_update = array('update' => false,
                                                  'status' => false,
                                                  'notice' => esc_html__('Overpayment - ', 'cryptowoo'));

                            // Check if we have a refund address for this order
                            //$refund_address = get_post_meta($address->order_id, 'refund_address', true);
	                        $refund_address = $order->get_meta('refund_address');

                            // Is the payment larger than the overpayment buffer?
                            if(((float)$address->percentage_paid - (float)$options['overpayment_buffer']) > 100 && $amount_due < $amount_received) {
                                $note = array('is_customer_note' => true,
                                              'txnreference' => CW_Formatting::prepare_overpayment_message($address, $refund_address, $options));

                                // Add order note and notify customer
                                self::cw_update_wc_order($order_id, $order_update, $note);

                                // Send email to admin
                                $to         = get_option('admin_email');
                                $blogname   = get_bloginfo('name', 'raw');
                                $subject    = sprintf('%s %s%s', $blogname, esc_html__(' - Overpayment for order #', 'cryptowoo'), $address->order_id);
                                $order_url  = admin_url("post.php?post={$address->order_id}&action=edit");
                                $view_order = sprintf('<p><a href="%s">%s #%s</a></p>', $order_url, esc_html__('View Order', 'cryptowoo'), $address->order_id);

                                $message = CW_Formatting::cw_get_template_html('email-header', $subject);

                                $message    .= sprintf(esc_html__('The customer paid %d%s (%s %s) too much.', 'cryptowoo'), round($address->percentage_paid - 100, 3), '%', CW_Formatting::fbits($address->amount_diff), $address->payment_currency);
                                $message    .= sprintf('<p>%s: %s</p>%s', esc_html__('Customer e-Mail address','cryptowoo'), $address->email, $view_order);

                                // Maybe add refund address

                                if((bool)$refund_address) {
                                    $message .= sprintf('<p>%s %s</p>', esc_html__('Refund address: ', 'cryptowoo'), CW_Formatting::link_to_address($address->payment_currency, $refund_address, $options, true));
                                    $label = rawurlencode(sprintf('%s %s', esc_html__('Refund Overpayment for Order', 'cryptowoo'), $address->order_id));
                                    $wallet_config = CW_Address::get_wallet_config($address->payment_currency, $address->crypto_amount, $options);

                                    $qr_data = sprintf('%s:%s?amount=%s&label=%s', $wallet_config['coin_client'], $refund_address, CW_Formatting::fbits($address->amount_diff, true, 8, true, true), $label);

                                    // Maybe create QR Code TODO always create QR code when library added support for PHP7
                                    if(defined('CWOO_SHOW_REFUND_QR')) {
                                        $message .= esc_html__('Scan or click the QR code to refund the excess amount.', 'cryptowoo');
                                        $qr = QRCode::getMinimumQRCode($qr_data, QR_ERROR_CORRECT_LEVEL_L);
                                        $im = $qr->createImage(2, 4);
                                        ob_start();
                                        // Generate the byte stream
                                        imagejpeg($im, null, 100);
                                        // Retrieve the byte stream
                                        $rawImageBytes = ob_get_clean();
                                        $qr_code       = "<img src='data:image/jpeg;base64," . base64_encode($rawImageBytes) . "' />";
                                    } else {
                                        $qr_code = esc_html__('Click here to open your wallet and refund the excess amount.', 'cryptowoo');
                                    }
                                    $message .= sprintf('<br><br><a href="%s">%s</a>', esc_url($qr_data, $wallet_config['coin_protocols'], false), $qr_code);

                                } else {
                                    $message .= sprintf('<p>%s</p>', esc_html__('No refund address available.', 'cryptowoo'));
                                }
                                $message .= CW_Formatting::cw_get_template_html('email-footer');

                                $headers = array("From: CryptoWoo Plugin <{$to}>",
                                                 'Content-Type: text/html; charset=UTF-8');
                                wp_mail($to, $subject, $message, $headers);
                            }

                            // Action hook for refund necessary
                            do_action('cryptowoo_refund_required', $address);
                        }
                    }
                    /*} else {
                        $payment_array[$address->order_id] = array('DBstatus' => 'not updated',
                                                                   'address_in_use' => $address_in_use,
                                                                   'timeout' => $timeout,
                                                                   'times_out_in' => $times_out_in,
                                                                   'percentage_paid' => $percentage_paid,
                                                                   'underpayment_range_begin' => (float)$options['underpayment_notice_range'][1],
                                                                    'min_perc_needed_to_complete' => (float)$options['underpayment_notice_range'][2]);
                    } */
                } // If last update is longer than x minutes ago
            } // Loop through unpaid addresses

            $update_stats = array('unpaid_addresses' => $address_count,
                                  'status' => $dbupdate,
                                  'payment_requests' => $payment_array);

            return $update_stats;
        }
        return esc_html__('No unpaid addresses found', 'cryptowoo');
    }


    /**
     * Update WooCommerce order status and/or add a note to the order.
     *
     * @param $order_id
     * @param $order_status
     * @param bool|false $note
     */
    private static function cw_update_wc_order($order_id, $order_status, $note = false) {

        $order = wc_get_order($order_id);
	    // Prevent updating of orders that aren't paid with CryptoWoo
	    // maybe the customer switched to another method after submitting his order
		// and it did not get removed from the processing queue yet
	    if($order->get_payment_method() !== 'cryptowoo') { return; }

        if (is_array($note) && isset($note['txnreference'])) {
            // Add a note
            $order->add_order_note($note['txnreference'], $note['is_customer_note']);
        }
        if ($order_status['update'] === true) {
            // Update order status
            $order->update_status($order_status['status'], $order_status['notice']);
        }

    }

    /**
     * Update WooCommerce order meta
     *
     * @depecated
     * @param $order_meta
     */
    static function cw_update_order_meta($order_meta) {
        // Update order meta
        update_post_meta($order_meta['order_id'], 'received_confirmed', $order_meta['received_confirmed']);
        update_post_meta($order_meta['order_id'], 'received_unconfirmed', $order_meta['received_unconfirmed']);
        // Only overwrite txids in order meta if there are transactions in this response
        if ((bool)$order_meta['has_txids']) {
            update_post_meta($order_meta['order_id'], 'txids', $order_meta['txids']);
        }
    }

   /**
	* Update WooCommerce order meta
	*
    * @param $order_id
	* @param $order_meta
	*
	* @return int|WC_Order
	*/
    static function cwwc_update_order_meta($order_id, $order_meta) {
    	if(!count($order_meta)) {
    		return 0;
	    }
    	$order = wc_get_order($order_id);
    	if(is_object($order)) {
		    foreach ( $order_meta as $meta_key => $meta_value ) {
			    $order->update_meta_data( $meta_key, $meta_value );
		    }

		    return $order->save();
	    }
    }

    /**
     * Add order status notice to frontend according to reason and return redirect url.
     *
     * @param $reason
     * @param $order_id
     * @return bool|string
     */
    static function decline_order($reason, $order_id) {

        global $woocommerce;
        $txnreference = $failed = $redirect = false;

        // Create message and update order status
        if ($reason === 'timeout') {
            $txnreference = sprintf(__('This order has expired. Please try again or contact us if you have already sent a payment. Reference: %d', 'cryptowoo'), $order_id);
        } elseif ($reason === 'hold') {
            $order    = wc_get_order($order_id);
            $redirect = $order->get_checkout_order_received_url();
        } elseif ($reason === 'address_error') {
            $txnreference = __('An error occurred while creating the payment address. Please try again.', 'cryptowoo');
            $delete       = true;
        } elseif ($reason === 'amount_error') {
            $txnreference = __('An error occurred while calculating the digital currency amount. Please try again.', 'cryptowoo');
            $delete       = true;
        } else {
            $txnreference = sprintf(__('An unexpected error occurred and we had to cancel your order. Please contact us if you need assistance. Reference: %s', 'cryptowoo'), $order_id);
            // Empty cart and clear session
            $woocommerce->cart->empty_cart();
            $failed = true;
        }

        if ($txnreference) {
            // Add notice for the customer when we return back to the cart
            wc_add_notice(esc_html(sprintf(__('TRANSACTION DECLINED: %s', 'cryptowoo'), $txnreference)), 'error');
        }

        if ($failed) {
            // Delete order from cryptowoo_temp table
            //self::delete_order_payment_request($order_id);

            // Set WC order status to "failed"
            $order    = wc_get_order($order_id);
	        if(is_object($order)) {
		        $order->update_status( 'failed', 'CryptoWoo Error', true );
	        }
        }
        // Redirect back to the cart
        if (!$redirect) {
            $redirect = $woocommerce->cart->get_cart_url();
        }
        return $redirect;
    }

    /**
     * Delete a payment request from DB
     *
     * @param $order_id
     * @return string
     */
    static function delete_order_payment_request($order_id) {
        global $wpdb;

        // Delete the record from the database where the ID column has the value 9999
        $delete_order_payment_request = $wpdb->delete($wpdb->prefix . 'cryptowoo_payments_temp', array('order_id' => $order_id), array('%d'));

        if (empty($delete_order_payment_request)) {

            $message = 'Payment request has been deleted.';

        } else {

            $message = $delete_order_payment_request;

        }

        return $message;
    }

    /**
     * Check basic order validity
     *
     * @param $payment_details
     * @return array
     */
    static function cw_check_order_validity($payment_details) {

        $is_valid = array('status' => true);

        if ($payment_details->crypto_amount <= 0) {
            $is_valid['status'] = false;
            $is_valid['type']   = 'amount_error';
        }
        if (!(bool)CW_Validate::check_if_unset($payment_details->address, false)) {
            $is_valid['status'] = false;
            $is_valid['type']   = 'address_error';
        }
        if ((int)$payment_details->timeout == 1) {
            $is_valid['status'] = false;
            $is_valid['type']   = 'timeout';
        }
        if ((int)$payment_details->timeout == 3) {
            $is_valid['status'] = false;
            $is_valid['type']   = 'hold';
        }
        if ((bool)strpos($payment_details->txids, 'DOUBLESPEND') || empty($payment_details->payment_currency)) {
            $is_valid['status'] = false;
            $is_valid['type']   = 'error';
        }
        return $is_valid;
    }

    /**
     * Save payment details to DB
     *
     * @param $payment_address
     * @param $amount
     * @param $customer_reference
     * @param $invoice_number
     * @param string $email
     * @param $payment_currency
     * @param $crypto_amount
     * @param $order_id
     * @param $options
     */
    static function cryptowoo_save_payment_details($payment_address, $amount, $customer_reference, $invoice_number, $email = 'na', $payment_currency, $crypto_amount, $order_id, $options) {
        global $wpdb;

        $gmdate  = gmdate('Y-m-d H:i:s');
        $created = strtotime($gmdate);

        $timeout_value = $created + ((int)$options['order_timeout_min'] * 60); // Timeout in seconds

        /* Insert entry into table payments */
        $wpdb->insert($wpdb->prefix . 'cryptowoo_payments_temp', array('amount' => $amount,
                                                                       'crypto_amount' => $crypto_amount,
                                                                       'payment_currency' => $payment_currency,
                                                                       'address' => $payment_address,
                                                                       'customer_reference' => $customer_reference,
                                                                       'invoice_number' => $invoice_number,
                                                                       'email' => $email,
                                                                       'order_id' => $order_id,
                                                                       'timeout_value' => $timeout_value,
                                                                       'created_at' => $gmdate,
                                                                       'last_update' => $gmdate, // string
                                                                       'is_archived' => 0), array('%s',
                                                                                                  '%d',
                                                                                                  '%s',
                                                                                                  '%s',
                                                                                                  '%s',
                                                                                                  '%d',
                                                                                                  '%s',
                                                                                                  '%d',
                                                                                                  '%d',
                                                                                                  '%s',
                                                                                                  '%s',
                                                                                                  '%d'));

        // Update WooCommerce order details
	    $meta_data = array(
		    'payment_address' => $payment_address,
		    'crypto_amount' => $crypto_amount,
		    'amount_due' => $amount,
		    'payment_currency' => $payment_currency,
		    'tx_confirmed' => '0'
	    );
	    CW_OrderProcessing::cwwc_update_order_meta($order_id, $meta_data);

    }

    /**
     * Update payment details
     *
     * @param $payment_address
     * @param $amount
     * @param $customer_reference
     * @param $invoice_number
     * @param string $email
     * @param $payment_currency
     * @param $crypto_amount
     * @param $order_id
     * @param bool $options
     * @return bool
     */
    static function cryptowoo_update_payment_details($payment_address, $amount, $customer_reference, $invoice_number, $email = 'na', $payment_currency, $crypto_amount, $order_id, $options = false) {
        global $wpdb;

        if (!$options) {
            $options = get_option('cryptowoo_payments');
        }

        $created = strtotime(gmdate('Y-m-d H:i:s'));

        $timeout_option = isset($options['order_timeout_min']) && !empty($options['order_timeout_min']) ? (int)$options['order_timeout_min'] * 60 : 1800; // Default to 30 minutes
        $timeout_value  = $created + $timeout_option;// add address timeout

        // Update the payment_request for the order_id
        $update_payment_request = $wpdb->update($wpdb->prefix . 'cryptowoo_payments_temp', array('amount' => $amount,
                                                                                                 'crypto_amount' => $crypto_amount,
                                                                                                 'payment_currency' => $payment_currency,
                                                                                                 'address' => $payment_address,
                                                                                                 'customer_reference' => $customer_reference,
                                                                                                 'invoice_number' => $invoice_number,
                                                                                                 'email' => $email,
                                                                                                 'order_id' => $order_id,
                                                                                                 'received_confirmed' => 0,
                                                                                                 'received_unconfirmed' => 0,
                                                                                                 'timeout_value' => $timeout_value,
                                                                                                 'is_archived' => 0), array('order_id' => $order_id), array('%s',
                                                                                                                                                            '%d',
                                                                                                                                                            '%s',
                                                                                                                                                            '%s',
                                                                                                                                                            '%s',
                                                                                                                                                            '%d',
                                                                                                                                                            '%s',
                                                                                                                                                            '%d',
                                                                                                                                                            '%d',
                                                                                                                                                            '%d',
                                                                                                                                                            '%d',
                                                                                                                                                            '%d'), array('%d'));

        if (false !== $update_payment_request) {

            // Update WooCommerce order details
	        $meta_data = array(
		        'payment_address' => $payment_address,
		        'crypto_amount' => $crypto_amount,
		        'amount_due' => $amount,
		        'payment_currency' => $payment_currency,
		        'tx_confirmed' => '0'
	        );

            $update_payment_request = (int)self::cwwc_update_order_meta($order_id, $meta_data);

        }

	    return $update_payment_request;

    }

    /**
     * Set WooCommerce order status, add order note, and update order meta
     *
     * @param $order_id
     */
    static function cryptowoo_complete_payment($order_id) {

        $options = get_option('cryptowoo_payments');

        $order           = wc_get_order($order_id);
        $payment_details = CW_OrderProcessing::get_payment_details($order_id, 'order_id');

        // Add/Update payment metadata to woocommerce order
	    $order->update_meta_data('payment_address', $payment_details->address);
	    $order->update_meta_data('received_confirmed', $payment_details->received_confirmed);
	    $order->update_meta_data('received_unconfirmed', $payment_details->received_unconfirmed);
	    $order->update_meta_data('crypto_amount', $payment_details->crypto_amount);
	    $order->update_meta_data('payment_currency', $payment_details->payment_currency);
	    $order->update_meta_data('txids', $payment_details->txids);

        if ($payment_details->received_confirmed >= $payment_details->crypto_amount && $payment_details->received_unconfirmed == 0 && $payment_details->paid == 1) {
            // Payment confirmed note
            $order->add_order_note(sprintf(esc_html('%s Payment Complete - Amount Confirmed: %s', 'cryptowoo'), $payment_details->payment_currency, CW_Formatting::fbits($payment_details->received_confirmed)), $payment_details->timeout == 3);
	        $order->update_meta_data('tx_confirmed', 'confirmed');

            // Action hook for payment confirmed
            do_action('cryptowoo_confirmed', $payment_details);
        }

        // Save our updated order meta data
	    $order->save();

        // Complete WooCommerce order
        $order->payment_complete();

        // Maybe set final order status
        if (!$order->has_status($options['final_order_status'])) {
            $order->update_status($options['final_order_status'], esc_html__('Payment Complete - ', 'cryptowoo'));
        }
    }

    /**
     * Fires when a WooCommerce order is set to "cancelled", removes the payment address for the order from the queue
     *
     * @param $order_id
     * @return bool
     */
    static function cw_woocommerce_cancelled_order($order_id) {
        global $wpdb;

        $is_crypto_order = self::get_payment_details($order_id, 'order_id');
        if (!(bool)$is_crypto_order) {
            return;
        }

        // Update the payment request for the order_id
        $updated = $wpdb->update(
            $wpdb->prefix . 'cryptowoo_payments_temp',
            array(
                'timeout' => 1,
            ),
            array('order_id' => $order_id),
            array('%d'),
            array('%d')
        );

        $note = sprintf(esc_html__('%s: Order #%d has been cancelled - removing address from queue.', 'cryptowoo'), date('Y-m-d H:i:s'), $order_id);

        CW_AdminMain::cryptowoo_log_data(0, __FUNCTION__, array('message' => $note, 'db_had_to_be_updated' => $updated), 'order-status.log');

        // Add note to order
        $order = wc_get_order($order_id);
        $order->add_order_note($note, false);
    }


    /**
     * Fires when a WooCommerce order is set to "completed", removes the payment address for the order from the queue
     *
     * @param $order_id
     */
    static function cw_order_status_completed($order_id) {
        global $wpdb;

        $is_crypto_order = self::get_payment_details($order_id, 'order_id');
        if (!(bool)$is_crypto_order) {
            return;
        }

        // Update the payment request for the order_id
        $updated = $wpdb->update(
            $wpdb->prefix . 'cryptowoo_payments_temp',
            array(
                'paid' => 1,
            ),
            array('order_id' => $order_id),
            array('%d'),
            array('%d')
        );

        $note = sprintf(esc_html__('%s: Order #%d has been completed - removing address from queue.', 'cryptowoo'), date('Y-m-d H:i:s'), $order_id);

        CW_AdminMain::cryptowoo_log_data(0, __FUNCTION__, array('message' => $note, 'db_had_to_be_updated' => $updated), 'order-status.log');

        // Add note to order
        $order = wc_get_order($order_id);
        $order->add_order_note($note, false);
    }

    /**
     * Send processing API error to admin email
     * @param $error
     */
    static function cw_processing_api_error_action($error) {
        $last_sent = get_transient('cryptowoo_last_sent_processing_error_email');

        // Max 1 email every 15 minutes
        if(!$last_sent || (time() - $last_sent) > 900) {
            // Send email to admin
            $to       = get_option('admin_email');
            $blogname = get_bloginfo('name', 'raw');
            $subject  = sprintf('%s %s', $blogname, esc_html__(' - Payment processing API error', 'cryptowoo'));

            $message = CW_Formatting::cw_get_template_html('email-header', $subject);
            $message .= sprintf(esc_html__('CryptoWoo has detected an issue during payment processing%s %s', 'cryptowoo'), '<br>', $error);
            $message .= CW_Formatting::cw_get_template_html('email-footer');

            $headers = array("From: CryptoWoo Plugin <{$to}>",
                             'Content-Type: text/html; charset=UTF-8');
            wp_mail($to, $subject, $message, $headers);
            set_transient('cryptowoo_last_sent_processing_error_email', time(), 900);
        }
    }
}

