<?php

// Block.io wrapper
require_once CWOO_PLUGIN_DIR.'includes/block_io.php';

// Blockcypher wrapper
require_once CWOO_PLUGIN_DIR.'vendor/autoload.php';

if(version_compare(phpversion(), '7.0.0', '<')) {
    define('CWOO_SHOW_REFUND_QR', true);
    // kazuhikoarase PHP QR Code
    require_once(CWOO_PLUGIN_DIR . 'includes/qrcode.php');
}

// Addresses
require_once CWOO_PLUGIN_DIR.'includes/class.address.php';

// Maybe include HD Wallet Address class
if(file_exists(WP_PLUGIN_DIR . '/cryptowoo-hd-wallet-addon/class.hdwallet.php')) {
    require_once WP_PLUGIN_DIR . '/cryptowoo-hd-wallet-addon/class.hdwallet.php';
}

// Exchange rates
require_once CWOO_PLUGIN_DIR.'includes/class.exchange-rates.php';

// Validations
require_once CWOO_PLUGIN_DIR.'includes/class.validate.php';

// Order Sorting
require_once CWOO_PLUGIN_DIR.'includes/class.order-sorting.php';

// Order Processing
require_once CWOO_PLUGIN_DIR.'includes/class.order-processing.php';

// Admin backend
require_once CWOO_PLUGIN_DIR.'admin/class.admin-main.php';
require_once CWOO_PLUGIN_DIR.'admin/admin-menus.php';

// Internationalization
cryptowoo_textdomain(); // Run this without hook to load before Redux Framework https://github.com/reduxframework/redux-framework/issues/1546#issuecomment-51727596

// Redux Framework via TGM
require_once CWOO_PLUGIN_DIR.'admin/admin-init.php';

// Template modifications
require_once CWOO_PLUGIN_DIR.'includes/class.formatting.php';

// API Helpers
$apis = array(
    'blockio',
    'blockcypher',
    'chainso',
    'insight',
    'smartbit',
);

function cryptowoo_load_api_helpers($apis) {
    foreach($apis as $api) {
        $file = sprintf('%sincludes/processing/class.%s.php',CWOO_PLUGIN_DIR,$api);
        if(file_exists($file)) {
            require_once $file;
        }
    }
}

cryptowoo_load_api_helpers($apis);

do_action('cw_include_all');

