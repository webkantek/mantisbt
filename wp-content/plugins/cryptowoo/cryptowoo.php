<?php
if (!defined('ABSPATH')) {exit;}// Exit if accessed directly
/**
 * Plugin Name: CryptoWoo
 * Plugin URI: https://www.cryptowoo.com/
 * Description: Digital Currency Payment Gateway for WooCommerce
 * Version: 0.20.2
 * Author: flxstn
 * Author URI: https://www.cryptowoo.com
 * License: GPLv2
 * Text Domain: cryptowoo
 * Domain Path: /lang
 * WC tested up to: 3.2.3
 *
 */
/*
Copyright 2015 flxstn

CryptoWoo is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

CryptoWoo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with CryptoWoo; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
define('CWOO_VERSION', '0.20.2');
define('CWOO_FILE', 'cryptowoo/cryptowoo.php');
define('CWOO_PLUGIN_PATH', plugins_url(basename(plugin_dir_path(__FILE__)), basename(__FILE__)) . '/');
define('CWOO_PLUGIN_DIR', plugin_dir_path(__FILE__));

// Determine log file directory
$log_dir = defined('WC_LOG_DIR') ? WC_LOG_DIR : WP_CONTENT_DIR.'/uploads/wc-logs/';
define('CW_LOG_DIR', $log_dir);

// Includes
require_once(__DIR__ . '/includes/include-all.php');

// License manager
if ( ! class_exists( 'CWoo_License_Menu' ) && file_exists(plugin_dir_path( __FILE__ ) . 'am-license-menu.php')) {
	require_once( plugin_dir_path( __FILE__ ) . 'am-license-menu.php' );
	CWoo_License_Menu::instance( __FILE__, 'CryptoWoo', CWOO_VERSION, 'plugin', 'https://www.cryptowoo.com/' );
}

// Remove Redux Demo Mode
function removeCryptoWooDemoModeLink() {
	if ( class_exists('ReduxFrameworkPlugin') ) {
		remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
	}
	if ( class_exists('ReduxFrameworkPlugin') ) {
		remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
	}
}
add_action('init', 'removeCryptoWooDemoModeLink');

$admin_main = new CW_AdminMain();

register_activation_hook(__FILE__, array($admin_main,'cryptowoo_cron_activation_schedule'));

// Multisite compatible plugin activation hook
register_activation_hook( __FILE__, 'cw_multisite_activate' );

/**
 * Multisite compatible plugin activation
* @param $networkwide
*/
function cw_multisite_activate($networkwide) {
	global $wpdb;
	// Check if it is a multisite network
	if ( is_multisite() ) {
		// Check if the plugin has been activated on the network or on a single site
		if ( $networkwide ) {
			// TODO maybe add multisite activation routine that honors CWOO_MULTISITE
			deactivate_plugins( plugin_basename( __FILE__ ), TRUE, TRUE );
			wp_die(esc_html__('Network Activation failed: You have to activate CryptoWoo on each site seperately.', 'cryptowoo'));
		} else {
			// Activated on a single site, in a multi-site
			cryptowoo_plugin_activate( $wpdb->blogid );
		}
	} else {
		// Activated on a single site
		cryptowoo_plugin_activate( $wpdb->blogid );
	}
}

// Remove scheduled crontasks
register_deactivation_hook(__FILE__, 'cryptowoo_plugin_deactivate');

// Internationalization
	//add_action('plugins_loaded', 'cryptowoo_textdomain');

// Plugin init
	add_action('plugins_loaded', 'woocommerce_cryptowoo_init', 1);
	add_action('admin_init', 'cryptowoo_plugin_init');

// Plugin requirements check
	add_action('admin_init', array($admin_main, 'cw_is_woocommerce_active'));

// Misconfiguration notice
	add_action('admin_notices', array($admin_main, 'cryptowoo_misconfig_notice'));

// Maybe force redirect to options page
	add_action('admin_init', array($admin_main, 'cw_maybe_redirect_to_options_page'));

// Custom WP cron schedules
	add_filter('cron_schedules', array($admin_main,'cron_add_schedules'));
	add_action('wp', array($admin_main,'cryptowoo_cron_activation_schedule'));

// Exchange rate update and payment processing function executed by cron
	add_action('cryptowoo_cron_action', array($admin_main, 'do_cryptowoo_cron_action'));

// Display settings
	add_action('wp', array('CW_Formatting','display_currency_settings'));
	// Hook into WooCommerce Currency Switcher plugin
	add_filter( 'woocs_currency_data_manipulation', 'add_currencies_to_woocs', 9999, 1 );

// Checkout field validation & cart update handling
	add_action('woocommerce_checkout_process', array('CW_Formatting', 'payment_currency_checkout_validation'));
	add_action('woocommerce_checkout_update_order_meta', array('CW_Formatting', 'cryptowoo_payment_currency_update_order_meta'));
	add_action('woocommerce_after_cart_item_quantity_update', array('CW_Formatting', 'cryptowoo_payment_currency_update_order_meta'));
	add_action('woocommerce_checkout_init', array('CW_Formatting', 'cryptowoo_payment_currency_update_order_meta'));

// Order details for cryptocurrency orders
	add_action('woocommerce_view_order', array('CW_Formatting', 'display_crypto_payment_info'));
	add_action('woocommerce_thankyou_cryptowoo', array('CW_Formatting', 'display_crypto_payment_info'));

// Order details in WooCommerce emails
	add_action('woocommerce_email_order_details', array('CW_Formatting', 'display_order_email_info'), 99, 4);

// Order status update for payment page background polling
	add_action('wp_ajax_nopriv_poll', 'poll_callback');
	add_action('wp_ajax_poll', 'poll_callback');

// Order status update for button on payment page
	add_action('wp_ajax_nopriv_check_receipt', 'check_receipt_callback');
	add_action('wp_ajax_check_receipt', 'check_receipt_callback');

// API and Master Public Key integrity
	add_action('cryptowoo_integrity_check', array($admin_main, 'do_cryptowoo_integrity_check'));

// Order table sorting
	add_filter('manage_edit-shop_order_columns', array($admin_main,'cryptowoo_order_data'), 11);
	add_action('manage_shop_order_posts_custom_column', array($admin_main,'cryptowoo_columns_values_function'), 2);
	add_filter('manage_edit-shop_order_sortable_columns', array($admin_main,'cryptowoo_columns_sort_function'));

// Scripts and styles
	add_action('admin_enqueue_scripts', 'cw_enqueue_admin_scripts');
	add_action('wp_enqueue_scripts', 'cryptowoo_scripts');

// Ajax functions for admin backend
	add_action( 'wp_ajax_cw_reset_error_counter', array($admin_main,'cw_reset_error_counter_callback' ));
	add_action( 'wp_ajax_cw_reset_exchange_rate_table', array($admin_main,'cw_reset_exchange_rate_table_callback' ));
	add_action( 'wp_ajax_cw_reset_payments_table', array($admin_main,'cw_reset_payments_table_callback' ));
	add_action( 'wp_ajax_cw_update_exchange_rates', array($admin_main,'cw_exchange_rates_callback' ));
	add_action( 'wp_ajax_cw_update_tx_details', array($admin_main,'cw_update_tx_details_callback'));
	//add_action( 'wp_ajax_cw_process_open_orders', array($admin_main,'cw_process_open_orders_callback'));

// Process open orders on timeout (not update tx details via API)
	add_action('wp_ajax_nopriv_cw_force_processing', array($admin_main, 'cw_front_process_open_orders_callback'));
	add_action('wp_ajax_cw_force_processing', array($admin_main,'cw_front_process_open_orders_callback'));
	// Admin manual processing (has ouptput)
	add_action('wp_ajax_nopriv_cw_process_open_orders', array($admin_main, 'cw_process_open_orders_callback'));
	add_action('wp_ajax_cw_process_open_orders', array($admin_main,'cw_process_open_orders_callback'));

// Order cancellation
    add_action( 'woocommerce_cancelled_order', array('CW_OrderProcessing','cw_woocommerce_cancelled_order'), 10, 1 );
    add_action( 'woocommerce_order_status_cancelled', array('CW_OrderProcessing','cw_woocommerce_cancelled_order'));

// Order completion
    add_action( 'woocommerce_order_status_completed', array('CW_OrderProcessing','cw_order_status_completed'), 10, 1);

// Payment processing failures
	add_action( 'cryptowoo_api_error', array('CW_OrderProcessing','cw_processing_api_error_action'), 10, 1);

	/**
	 * Load cryptowoo textdomain.
	 *
	 * @since 0.14.0
	 */
	function cryptowoo_textdomain() {
		load_plugin_textdomain( 'cryptowoo', false, dirname( plugin_basename(__FILE__ ) ) . '/lang/' );
	}

    /**
     * Redirect the user to the setup page after installation
     * @param $blog_id
     */
    function cryptowoo_plugin_activate($blog_id) {

        $admin_main = new CW_AdminMain();
        $admin_main->create_plugin_table($blog_id);

        // Maybe rewrite old order meta
        /*
        $current_meta_ver = get_option('cryptowoo_order_meta_ver', '0.1');
        if(version_compare(CWOO_VERSION, $current_meta_ver, '>')) {
            $admin_main->cryptowoo_update_ordermeta_to_satoshi();
        } */

        // Only redirect to settings if WooCommerce is active
        include_once (ABSPATH . 'wp-admin/includes/plugin.php');

        if(validate_file('woocommerce/woocommerce.php') || !file_exists(WP_PLUGIN_DIR . '/woocommerce/woocommerce.php') ) {

            // If WooCommerce is not installed then show installation notice
            add_action('admin_notices', 'cryptowoo_wc_notinstalled_notice');
            return;
        } elseif (!is_plugin_active('woocommerce/woocommerce.php')) {
            add_action('admin_notices', 'cryptowoo_wc_inactive_notice');
            return;
        } else {
            add_option('cryptowoo_plugin_do_activation_redirect', true);
        }
    }

function cryptowoo_plugin_init() {
	if (get_option('cryptowoo_plugin_do_activation_redirect', false)) {
		delete_option('cryptowoo_plugin_do_activation_redirect');
		wp_safe_redirect(admin_url() . 'admin.php?page=cryptowoo');
		exit;
	}

	$admin_main = new CW_AdminMain();

	if(file_exists(file_exists(CWOO_PLUGIN_DIR . 'includes/class.api.php'))) {
		// Add update notification to plugins.php page
		add_action('in_plugin_update_message-cryptowoo/cryptowoo.php', array($admin_main, 'cw_in_plugin_update_message'), 10, 2);
	}
	// Log admin IPs
	$user = wp_get_current_user();
	if(in_array('administrator', $user->roles)) {

		// Keep info for two days
		set_transient('admin_ip', $admin_main->get_ip(), 2 * DAY_IN_SECONDS);
		set_transient('admin_useragent', (string)$_SERVER['HTTP_USER_AGENT'], 2 * DAY_IN_SECONDS);
	}
}

function check_receipt_callback() {
	global $woocommerce;

	$redirect = $redirect_on_unconfirmed = false;
	$address = strval($_REQUEST['address']);

	if ($address == '') {
		echo json_encode(array(
			'address' => $address,
			'received' => CW_Formatting::fbits(0),
			'unconfirmed' => CW_Formatting::fbits(0),
			'redirect' => false,
		));
		die();
	}

	// Get order data
	$cw_order_data = CW_OrderProcessing::get_payment_details($address);

	$amount_due = $cw_order_data->crypto_amount;
	$order_id = $cw_order_data->order_id;
	$paid =  (int)$cw_order_data->paid;

	// Check basic order validity
	$is_valid = CW_OrderProcessing::cw_check_order_validity($cw_order_data);
	if($is_valid['status'] === false) {
		$redirect = CW_OrderProcessing::decline_order($is_valid['type'], $order_id);
	}

	$amount_unconfirmed = (int)CW_Validate::check_if_unset($cw_order_data->received_unconfirmed, false, '0');
	$amount_received = (int)CW_Validate::check_if_unset($cw_order_data->received_confirmed, false, '0');

	// Maybe redirect when we see an unconfirmed amount
	if($amount_unconfirmed > 0) {
		$options = get_option('cryptowoo_payments');
		$redirect_on_unconfirmed = isset( $options['cw_redirect_on_unconfirmed'] ) && (bool) $options['cw_redirect_on_unconfirmed'];
    }

	if (($redirect_on_unconfirmed || $paid > 0) && $amount_due > 0) {

		$order = wc_get_order($order_id);

		$redirect = $order->get_checkout_order_received_url(); //$woocommerce->get_return_url($order);

		// Empty cart and clear session
		$woocommerce->cart->empty_cart();

	}
	$result = json_encode(array(
		'address' => $address,
		'received' => $amount_received > 0 ? CW_Formatting::fbits($amount_received) : '0.00',
		'unconfirmed' => $amount_unconfirmed > 0 ? CW_Formatting::fbits($amount_unconfirmed) : '0.00',
		'redirect' => $redirect,
	));
	echo $result;
	die(); // This is required to terminate immediately and return a proper response
}


function poll_callback() {
	global $woocommerce;

	$redirect = $redirect_on_unconfirmed = false;
	$address = strval($_REQUEST['address']);

	if ($address == '') {
		echo json_encode(array(
			'address' => $address,
			'received' => CW_Formatting::fbits(0),
			'unconfirmed' => CW_Formatting::fbits(0),
			'redirect' => false,
		));
		die();
	}

	// Get order data
	$cw_order_data = CW_OrderProcessing::get_payment_details($address);

	$amount_due = $cw_order_data->crypto_amount;
	$order_id = $cw_order_data->order_id;
	$paid =  (int)$cw_order_data->paid;

	// Check basic order validity
	$is_valid = CW_OrderProcessing::cw_check_order_validity($cw_order_data);
	if($is_valid['status'] === false) {
		$redirect = CW_OrderProcessing::decline_order($is_valid['type'], $order_id);
	}

	$amount_unconfirmed = (int)CW_Validate::check_if_unset($cw_order_data->received_unconfirmed, false, '0');
	$amount_received = (int)CW_Validate::check_if_unset($cw_order_data->received_confirmed, false, '0');

	// Maybe redirect when we see an unconfirmed amount
	if($amount_unconfirmed > 0) {
		$options = get_option('cryptowoo_payments');
		$redirect_on_unconfirmed = isset( $options['cw_redirect_on_unconfirmed'] ) && (bool) $options['cw_redirect_on_unconfirmed'];
	}

	if (($redirect_on_unconfirmed || $paid > 0) && $amount_due > 0) {

		$order = wc_get_order($order_id);

		$redirect = $order->get_checkout_order_received_url();

		// Empty cart and clear session
		$woocommerce->cart->empty_cart();

	}

	$result = json_encode(array(
		'address' => $address,
		'received' => $amount_received > 0 ? CW_Formatting::fbits($amount_received) : '0.00',
		'unconfirmed' => $amount_unconfirmed > 0 ? CW_Formatting::fbits($amount_unconfirmed) : '0.00',
		'redirect' => $redirect,
	));
	echo $result;
	die(); // This is required to terminate immediately and return a proper response
}

/**
 * On deactivation, remove all functions from the scheduled action hook.
 */
function cryptowoo_plugin_deactivate() {
	wp_clear_scheduled_hook('cryptowoo_cron_action');
	wp_clear_scheduled_hook('cryptowoo_integrity_check');
	wp_clear_scheduled_hook('cryptowoo_archive_addresses');
}

/**
 * Enqueue scripts
 */
function cryptowoo_scripts() {


	// Progress bar
	wp_enqueue_script('nanobar', CWOO_PLUGIN_PATH.'assets/js/nanobar.js', array('jquery'));

	// davidshimjs QR Code
	wp_enqueue_script('QRCode', CWOO_PLUGIN_PATH.'assets/js/qrcodejs-master/qrcode.js', array('jquery'));

	// Register payment page script
	wp_register_script( 'cw_polling', CWOO_PLUGIN_PATH.'/assets/js/polling.js', array('jquery'));

	// Plugin Styles
	wp_enqueue_style('cryptowoo', CWOO_PLUGIN_PATH.'assets/css/cryptowoo-plugin.css');

	// AW Cryptocoins
	wp_enqueue_style('aw-cryptocoins', CWOO_PLUGIN_PATH.'assets/fonts/aw-cryptocoins/cryptocoins.css');

	// Fontawesome icon font
	wp_enqueue_style('fontawesome', CWOO_PLUGIN_PATH.'assets/font-awesome/css/font-awesome.min.css', __FILE__);
}

function cw_enqueue_admin_scripts() {
	//wp_enqueue_script('cw-admin-js', CWOO_PLUGIN_PATH.'assets/js/admin-js.js');
    // Plugin Styles
    wp_enqueue_style('cryptowoo', CWOO_PLUGIN_PATH.'assets/css/cryptowoo-plugin.css');
    // AW Cryptocoins
    wp_enqueue_style('aw-cryptocoins', CWOO_PLUGIN_PATH.'assets/fonts/aw-cryptocoins/cryptocoins.css');
    // Fontawesome icons
	wp_enqueue_style('fontawesome', CWOO_PLUGIN_PATH.'assets/font-awesome/css/font-awesome.min.css', __FILE__);
}

/**
 * Import gateway class extends
 */
function woocommerce_cryptowoo_init() {
	if (!class_exists('WC_Payment_Gateway')) {
		return;
	};

	/**
	 * Constructor for the gateway.
	 *
	 * @access public
	 * @return void
	 */
	class WC_CryptoWoo extends WC_Payment_Gateway {

		public function __construct() {

			$redux = get_option('cryptowoo_payments');

			$this->id = 'cryptowoo';
			$this->method_title = 'CryptoWoo Payment';
			$this->has_fields = true;

			if( 'Activated' !==  get_option('cryptowoo_activated') || (isset($redux['remove_checkout_branding']) && !$redux['remove_checkout_branding'])) {
				$this->icon = apply_filters('woocommerce_cryptowoo_icon', CWOO_PLUGIN_PATH . 'assets/images/cryptowoo-checkout-coins.png');
			}
			// Load the form fields.
			$this->init_form_fields();

			// Load the settings.
			$this->init_settings();

			// Define user set variables
			$this->enabled = $this->get_option('enabled');
			$this->title = $this->get_option('title');
			$this->description = $this->get_option('description');

			/*
			// Define user set variables
			$this->enabled = $redux['enabled'];
			$this->title = $redux['title'];
			$this->description = $redux['description'];
			*/

			add_action('woocommerce_api_wc_cryptowoo', array($this, 'check_response'));
			add_action('woocommerce_receipt_cryptowoo', array(&$this, 'receipt_page'));

			if (version_compare(WOOCOMMERCE_VERSION, '2.0', '<')) {
				/* 1.6.6 */
				add_action('woocommerce_update_options_payment_gateways', array(&$this, 'process_admin_options'));
			} else {
				/* 2.0.0 */
				add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
			}

			add_action('woocommerce_update_options_payment_gateways_' . $this->id, 'cryptowoo_gateway_activation');
		}

		/**
		 * get_icon - filter for woocommerce_gateway_icon to add the enabled currencies to the payment gateway title on the checkout page
		 *
		 * @return string
		 */
		public function get_icon() {
			$icon_html = '';
			$enabled_currencies = cw_get_enabled_currencies();

			foreach ($enabled_currencies as $enabled_currency => $nice_name) {
				if(!strpos($enabled_currency, 'TEST')) {
					$icon_html .= CW_Formatting::get_coin_icon($enabled_currency);
				}
			}
			/*
			Only display logo below custom CryptoWoo checkout fields
			if(CW_DISPLAY_BRANDING|| !isset($redux['remove_checkout_branding']) || !$redux['remove_checkout_branding']) {
				$icon_html .= sprintf('<a href="%1$s" class="about-cryptowoo" target="_blank" title="' . esc_attr__('powered by CryptoWoo', 'cryptowoo') . '"><img alt="CryptoWoo" src="' . CWOO_PLUGIN_PATH . 'assets/images/cryptowoo-checkout-x15.png" />' . '</a>', esc_url('https://www.cryptowoo.com/'));
			}
			*/
			return apply_filters('woocommerce_gateway_icon', $icon_html, $this->id);
		}

		/**
		 * Integrity check file helper
         */
		function cryptowoo_hash_keys() {
           $this->do_cryptowoo_hash_keys();
        }

		/**
		 * Create integrity check file
		 *
		 * Create a hash containing API keys and master public keys
		 * Save to /wp-content/uploads/ directory
         */
		private function do_cryptowoo_hash_keys() {

			$options = get_option('cryptowoo_payments');

			$keys = array(NONCE_SALT);

			$foo = array( // @todo refactor
			    'cryptowoo_btc_api',
				'cryptowoo_doge_api',
				'cryptowoo_ltc_api',
				'cryptowoo_btc_test_api',
				'cryptowoo_doge_test_api',
				'cryptowoo_btc_mpk',
				'cryptowoo_doge_mpk',
				'cryptowoo_doge_mpk_xpub',
				'cryptowoo_ltc_mpk',
				'cryptowoo_ltc_mpk_xpub',
				'cryptowoo_btc_test_mpk',
				'cryptowoo_doge_test_mpk',
				'cryptowoo_blk_mpk',
				'cryptowoo_blk_mpk_xpub',
				'safe_btc_address',
				'safe_ltc_address',
				'safe_doge_address'
			);

			for( $i = 0; $i < count($foo) ; $i++ ) {
				$keys[] = CW_Validate::check_if_unset($foo[$i], $options, '0');
			}

			if(!isset($options['cw_filename'])) {
				$filename = $options['cw_filename'] = urlencode(sha1(NONCE_SALT));
				update_option('cryptowoo_payments', $options);
			} else {
				$filename = $options['cw_filename'];
			}

			if(!empty($keys)) {
				// Write the hash of the current api keys to /uploads/$filename
				file_put_contents(WP_CONTENT_DIR.'/uploads/'.$filename, hash_hmac('sha256', print_r($keys, true), AUTH_SALT));
			}
		}


		public function check_response() {
			if (isset($_POST['customer_reference']) && isset($_POST['responseCode'])):
				@ob_clean();
				$_POST = stripslashes_deep($_POST);
				if (!empty($_POST['customer_reference']) && !empty($_POST['responseCode'])):
					header('HTTP/1.1 200 OK');
					$this->successful_request($_POST);
				else:
					wp_die("Request Failure");
				endif;
			endif;
		}

		/**
		 * Validate Block.io API keys via get_my_addresses
		 *
		 * @param $api_key
		 * @param $currency
		 * @return bool
         */
		public function cw_validate_api_keys($api_key, $currency) {

			$error = null;
			$return = array('status' => false);
			if ($api_key) {
				$blockio = new BlockIo($api_key, '');
				try {
					$result = $blockio->get_my_addresses();
					if(isset($result->status) && $result->status === 'success' && isset($result->data->network) && strcmp($result->data->network, $currency) === 0 ) {
						$return['status'] = true;
					}
				} catch(Exception $e) {
					$return['message'] = $e->getMessage();
					if(WP_DEBUG) {
						file_put_contents(CW_LOG_DIR . 'cryptowoo-error.log', date("Y-m-d H:i:s") . __FILE__ . "\n cw_validate_api_keys: ".$return['message']."\n", FILE_APPEND);
					}
				}
			}
			return $return;
		}

		/**
		 * Check PHP requirements
		 *
		 * @return bool
         */
		public static function check_php_requirements() {

			$options = get_option('cryptowoo_payments');
			$version = phpversion();

			if (version_compare(PHP_VERSION, '5.6.0', '<')) {
				$options['enabled'] = false;
				update_option('woocommerce_cryptowoo_settings', $options);
				update_option('cryptowoo_payments', $options);
				return esc_html__('Your current PHP Version is ' . $version . '.', 'cryptowoo');
			}
			if (!extension_loaded('gmp')) {
				$options['enabled'] = false;
				update_option('woocommerce_cryptowoo_settings', $options);
				update_option('cryptowoo_payments', $options);
				return sprintf(esc_html__('%s extension seems not to be installed.', 'cryptowoo'), 'GMP');
			}

			if (!extension_loaded('curl')) {
				$options['enabled'] = false;
				update_option('woocommerce_cryptowoo_settings', $options);
				update_option('cryptowoo_payments', $options);
				return sprintf(esc_html__('%s extension seems not to be installed.', 'cryptowoo'), 'cURL');
			}

			if (!extension_loaded('mcrypt')) {
				$options['enabled'] = false;
				update_option('woocommerce_cryptowoo_settings', $options);
				update_option('cryptowoo_payments', $options);
				return sprintf(esc_html__('%s extension seems not to be installed.', 'cryptowoo'), 'mCrypt');
			}
			return true;
		}

		/**
		 * Admin Panel Options
		 * - Options for bits like 'title' and availability on a country-by-country basis
		 *
		 * @since 1.0.0
		 */
		public function admin_options() {
			?>
			<h3>CryptoWoo</h3>
			<span style="padding:1em;"><a class="button-primary" href="<?php echo admin_url('admin.php?page=cryptowoo'); ?>" title="<?php echo esc_html__('CryptoWoo Options','cryptowoo'); ?>"><?php echo esc_html__('CryptoWoo Options','cryptowoo'); ?></a></span>
			<span style="padding:1em;"><a class="button" href="<?php echo admin_url('admin.php?page=cryptowoo_database_maintenance'); ?>" title="<?php echo esc_html__('CryptoWoo Database Maintenance','cryptowoo'); ?>"><?php echo esc_html__('CryptoWoo Database Maintenance','cryptowoo'); ?></a></span>
            <?php
             $check_req = WC_CryptoWoo::check_php_requirements();
             if (true !== $check_req) { ?>
				<div class="inline error"><p><strong><?php _e('Gateway Disabled', 'cryptowoo');?></strong>: <?php _e($check_req . '<br>You need to have at least PHP version 5.6 with <a href="http://php.net/manual/en/mcrypt.installation.php" target="_blank">mCrypt</a>, <a href="http://php.net/manual/en/gmp.installation.php" target="_blank">GMP</a>, and <a href="http://php.net/manual/en/curl.installation.php" target="_blank">cURL</a> extensions enabled to use CryptoWoo. <br>', 'cryptowoo');?></p></div>
            <?php }
             wp_die();
		}

		/**
		 * Initialise WooCommerce Gateway Settings
		 *
		 * Actual settings via Redux Framework in options-init.php
		 *
		 * @access public
		 * @return void
		 *
		 */
		public function init_form_fields() {

			$this->form_fields = array(
				/*
				'general_section' => array
				(
					'title' => __('<i class="fa fa-wrench"></i> General Settings', 'cryptowoo'),
					'type' => 'title',
					'class' => 'section-head',
				),
				'enabled' => array
				(
					'title' => __('Enable/Disable', 'cryptowoo'),
					'type' => 'checkbox',
					'label' => __('Enable CryptoWoo payment gateway.', 'cryptowoo'),
					'default' => '0',
				),
				'title' => array
				(
					'title' => __('Title', 'cryptowoo'),
					'type' => 'text',
					'description' => __('This is the title the customer can see when checking out.', 'cryptowoo'),
					'default' => __('Digital Currencies', 'cryptowoo'),
				),
				'description' => array
				(
					'title' => __('Description', 'cryptowoo'),
					'type' => 'textarea',
					'description' => __('This is the description the customer can see when checking out. <strong>HTML</stong> markup allowed', 'cryptowoo'),
					'default' => __("Pay with Bitcoin, Litecoin or Dogecoin?", 'cryptowoo'),
				),
				*/
			);
		}

		/**
		 * Cryptocurrency Payment info and currency select form on checkout page
         */
		function payment_fields() {
			if (isset($this->description)) {
				echo $this->description;
			}
			CW_Formatting::cryptowoo_payment_currency_checkout_field();
		}

        /**
		 * Get WooCommerce Order Data
		 *
		 * @access public
		 * @param mixed $order
		 * @return array
		 */
		public function get_cryptowoo_args($order) {

			$order_id = $order->get_id();
			$data = array();
			$data['customer_reference'] = $order_id . '-' . $order->get_order_key();
			$data['description'] = "Payment for order id " . $order_id;
			$data['email'] = $order->get_billing_email();
			$data['invoice_number'] = $order_id;
			$data['amount'] = $order->get_total();

			if (get_option('woocommerce_prices_include_tax') == 'yes') {

				$data['tax_total'] = 0.00;
				$data['cart_amount'] = number_format($order->get_total() - $order->get_total_shipping() - $order->get_shipping_tax(), 8, '.', '');
				$data['shipping'] = number_format($order->get_total_shipping() + $order->get_shipping_tax(), 8, '.', '');

			} else {

				$data['tax_total'] = $order->get_total_tax();
				$data['cart_amount'] = number_format($order->get_total() - $order->get_total_shipping() - $order->get_total_tax(), 8, '.', '');
				$data['shipping'] = number_format($order->get_total_shipping(), 8, '.', '');

			}

			$data['amount'] = (float)$data['cart_amount']+(float)$data['shipping']+(float)$data['tax_total'];
			$data['enabled_currencies'] = cw_get_enabled_currencies();
			$data['payment_currency'] = get_post_meta($order_id, 'payment_currency', true);
			return $data;
		}

		/**
		 * Process the payment and return the result
		 *
		 * @access public
		 * @param int $order_id
		 * @return array
		 */
		public function process_payment($order_id) {
			$order = wc_get_order($order_id);
			return array (
				'result' => 'success',
				'redirect' => $order->get_checkout_payment_url(true)
			);
		}

		/**
		 * Validate key integrity before displaying payment page
		 *
		 * @param $order_id
		 */
		public function receipt_page($order_id) {
			$admin_main = new CW_AdminMain();
			// Validate API key and forwarding address integrity
			if(!$admin_main->do_cryptowoo_integrity_check()) {
				$redirect = CW_OrderProcessing::decline_order('error', $order_id);
				wp_safe_redirect($redirect);
				exit;
			} else {
				$this->generate_cryptowoo_form($order_id);
			}
		}

		/**
		 * Render order-pay page
		 *
		 * @param $order_id
         */
		public function generate_cryptowoo_form($order_id) {

			global $woocommerce;
			$order = wc_get_order($order_id);

			// Redirect to "order received" page if the order is not pending payment
			if($order->get_status() !== 'pending') {
				$redirect = $order->get_checkout_order_received_url();
				wp_safe_redirect($redirect);
				exit;
            }

			$cryptowoo_args = $this->get_cryptowoo_args($order);

			$options = get_option('cryptowoo_payments');

			$customer_reference = $cryptowoo_args['customer_reference'];
			$invoice_number = $cryptowoo_args['invoice_number'];
			$email = $cryptowoo_args['email'];
			$payment_currency = $cryptowoo_args['payment_currency'];
			$amount = $cryptowoo_args['amount'];

			$message = null;
            $crypto_amount = 0;

			// Get wallet config
			$wallet_config = CW_Address::get_wallet_config($payment_currency, $amount, $options);

			if (false !== strpos($payment_currency, 'TEST')) {
				$message .= sprintf(esc_html__('%s%s currency override enabled. This payment gateway is in testnet mode. No orders shall be fulfilled.%s', 'cryptowoo'),'<p class="cryptowoo-warning">',$payment_currency,'</p>');
			}

			// Get payment details by order_id
			$payment_details = CW_OrderProcessing::get_payment_details($order_id, 'order_id');

			$updated = false;

			if (!empty($payment_details)) {
				$payment_address = $payment_details->address;
				$amount_payments = $payment_details->amount;
				$crypto_amount = $payment_details->crypto_amount;
				$payment_currency = $payment_details->payment_currency;

				// Request a new payment address if the payment currency has changed.
				if ($cryptowoo_args['payment_currency'] != $payment_currency) {
					$message .= sprintf(esc_html__('%sMessage: Currency has changed. New address has been generated.%s', 'cryptowoo'), '<br>','<br>');
					//delete_order_payment_request($order_id);
					$payment_currency = $cryptowoo_args['payment_currency'];
					$payment_address = false;
					$updated = true;
				}

				// Update payment request from table payments if the order amount has changed.
				if ($amount_payments != $cryptowoo_args['amount']) {
					$message .= sprintf(esc_html__('%sMessage: Amount has changed. Order has been updated%s', 'cryptowoo'), '<br>','<br>');
					//delete_order_payment_request($order_id);
					$updated = true;

				}
				//DEBUG var_dump($cryptowoo_args['enabled_currencies']);
			} else {
				$payment_address = false;
			}
			$rate_error = false;
			// If there is no entry for that order_id calculate crypto_amount and generate address
			if (empty($payment_address) || !$payment_address || $updated) {

				$currency = cw_get_woocommerce_currency();

				// Only convert price if the store currency is not BTC, LTC or DOGE
				if ($payment_currency != $currency) {

					// Get the exchange rate from the database
					$price = CW_ExchangeRates::get_exchange_rate($wallet_config['request_coin']);

					if ($price > 0  && is_numeric($price)) {
						$status['amount_error'] = 'success';
					} else {
						$status['amount_error'] = 'fail';
						$rate_error = true;
					}

					// Calculate order_total in cryptocurrency
					$crypto_amount = CW_OrderProcessing::cw_float_to_int(round((($amount * $wallet_config['multiplier']) / $price), $wallet_config['decimals']));

				} else {
					$crypto_amount = CW_OrderProcessing::cw_float_to_int(round(($amount * $wallet_config['multiplier']), $wallet_config['decimals']));// Use unconverted store amount if store currency is BTC, LTC or DOGE
				}

				// Check if we have a valid crypto amount
				$amount_error = !$rate_error && isset($crypto_amount) && is_numeric($crypto_amount) && (int)$crypto_amount > 0 ? false : true;

				if ($amount_error) {
					$status['amount_error'] = 'fail';
					file_put_contents(CW_LOG_DIR . 'cryptowoo-error.log', date("Y-m-d H:i:s") . ' Amount calculation error: ' . var_export($crypto_amount, true) . ' Status: ' . var_export($payment_details, true) . "\r\n", FILE_APPEND);
					// Redirect customer to checkout page so he can resubmit the order
					$redirect = CW_OrderProcessing::decline_order('amount_error', $order_id);
					wp_safe_redirect($redirect);
					exit;
				}

				$nonce = time();
				$label = "{$customer_reference}-{$nonce}";

				// Amount is okay, now let's create an address
				if ($wallet_config['forwarding_enabled']) {
                    // Forwarding is enabled and the order amount is above the threshold
					$api_key = CW_AdminMain::get_blockio_api_key($options, $payment_currency);

					// Create payment forwarding address
					$forwarding_return = CW_Address::create_blockio_forwarding_address($api_key, $wallet_config['safe_address']);
					$payment_address = $forwarding_return['address'];
					$status['address_error'] = $forwarding_return['status'];

					// Save forwarding info to order meta
                    $meta_data = array(
                            'forwarding_private_key' => $forwarding_return['forwarding_private_key'],
                            'forward_to_address' => $forwarding_return['forward_to_address'],
                            'forwarding_address' => $payment_address
                    );
                    CW_OrderProcessing::cwwc_update_order_meta($order_id, $meta_data);

				} elseif ($wallet_config['hdwallet']) {

					// Get new payment address from master public key
					$hdwallet_return = CW_HDwallet::create_address_from_mpk($payment_currency, $options);
					$payment_address = $hdwallet_return['address'];
					$mpk_key_index = $hdwallet_return['mpk_key_index'];
					$mpk_key_position = $hdwallet_return['mpk_key_position'];

				} else {

					$api_key = CW_AdminMain::get_blockio_api_key($options, $payment_currency);

					// Get default MultiSig address via Block.io API
					$multisig_return = CW_Address::create_blockio_multisig_address($api_key, $label);
					$payment_address = $multisig_return['address'];
				}

				$validate = new CW_Validate();
				$address_valid = $validate->offline_validate_address($payment_address, $payment_currency);

				if (!(bool)$address_valid) {
					$status['address_error'] = 'fail';
                    $log_string = sprintf("%s  Address creation error: %s %s Status: %s \n",date('Y-m-d H:i:s'), $payment_currency, $payment_address, var_export($address_valid, true));
					file_put_contents(CW_LOG_DIR . 'cryptowoo-error.log', $log_string, FILE_APPEND);
					// Decline order and redirect customer
					$redirect = CW_OrderProcessing::decline_order('address_error', $order_id);
					wp_safe_redirect($redirect);
					exit;
				} else {
					$status['address_error'] = 'success';
				}

				// Save or update payment details in table payments and WooCommerce order meta
				if (!in_array('fail', $status)) {

					if ($updated == true) {
						CW_OrderProcessing::cryptowoo_update_payment_details($payment_address, $amount, $customer_reference, $invoice_number, $email, $payment_currency, $crypto_amount, $order_id, $options);
					} else {
						CW_OrderProcessing::cryptowoo_save_payment_details($payment_address, $amount, $customer_reference, $invoice_number, $email, $payment_currency, $crypto_amount, $order_id, $options);
					}

					// Maybe add HD Wallet derivation info
					if($wallet_config['hdwallet'] && isset($mpk_key_index)) {
						$meta_data = array(
							'mpk_key_index'    => $mpk_key_index,
							'mpk_key_position' => $mpk_key_position,
						);
						CW_OrderProcessing::cwwc_update_order_meta($order_id, $meta_data);
					}
					do_action('cryptowoo_new_order', $order_id);
				} else {
					file_put_contents(CW_LOG_DIR . 'cryptowoo-error.log', date("Y-m-d H:i:s") . ' Error creating order: ' . var_export($status, true) . ' Status: ' . var_export($payment_details, true) . "\r\n", FILE_APPEND);
					$redirect = CW_OrderProcessing::decline_order(array_search('fail', $status ), $order_id);
					wp_safe_redirect($redirect);
					exit;
				}
			}

		$label = rawurlencode(sprintf('%s %s %s', get_bloginfo('name'), esc_html__('Order', 'cryptowoo'), $order_id));

		$coin_protocols = $wallet_config['coin_protocols'];

        // Prepare QR Code
        $qr_data = sprintf('%s:%s?amount=%s&label=%s', $wallet_config['coin_client'], $payment_address, CW_Formatting::fbits( $crypto_amount, true, 8, true, true),$label);

		// Get payment details from database WHERE `payment_address`= ?
		$payment_details = CW_OrderProcessing::get_payment_details($payment_address);
		$timeout_value = $payment_details->timeout_value;
		$time_left = isset($timeout_value) ? $timeout_value - time() : time() + 1800;

		// Check basic order validity on page load
		$is_valid = CW_OrderProcessing::cw_check_order_validity($payment_details);
		if($is_valid['status'] === false) {
			$redirect = CW_OrderProcessing::decline_order($is_valid['type'], $order_id);
			wp_safe_redirect($redirect);
			exit;
		}

		// WooCommerce "Order Received" URL
		$redirect = $order->get_checkout_order_received_url();

		// if the payment has been received
		if ($payment_details->crypto_amount > 0 && $payment_details->paid == 1) {
			// Empty cart and clear session
			$woocommerce->cart->empty_cart();
			// Redirect to "Order Received" URL
			wp_safe_redirect($redirect);
			exit;
		}

		// Ajax url
		$admin_url = admin_url('admin-ajax.php');

		// Localize the script with new data
		$php_vars_array = array('payment_address' => $payment_address,
								'show_countdown' => (bool)$options['show_countdown'],
								'total_time' => $options['order_timeout_min'] * 60, // Timeout in seconds
								'time_left' => $time_left,
								'admin_url' => $admin_url,
								'redirect' => $redirect,
								'qr_data' => $qr_data,
								'please_wait' => esc_html__('Please wait...', 'cryptowoo'),
		);
		wp_localize_script('cw_polling', 'CryptoWoo', $php_vars_array);

		// Enqueued script with localized data.
		wp_enqueue_script('cw_polling');

		// Then access all the vars like this
		/**
		 * CryptoWoo.payment_address;
		 * CryptoWoo.time_left;
		 * CryptoWoo.admin_url;
		 * [...]
		 */

		// Shapeshift button data
		$shifty = CW_Formatting::cw_shifty_enabled($payment_currency, $payment_address, $crypto_amount, $options);

		// Decimal seperator
		$decimal_sep = wc_get_price_decimal_separator();

		// Block chain link
		$url = CW_Formatting::link_to_address($payment_currency, $payment_address, $options);

        // JavaScipt disabled message
        printf(esc_html__('%sSome elements need JavaScript activated to work but the order will be processed regardless. You will receive an email when your payment is confirmed.%s', 'cryptowoo'), '<noscript><p>', '</p><style>.nojs { display:none; }</style></noscript>');

		// Maybe use custom payment page template
        // Copy the file wp-content/plugins/cryptowoo/includes/payment.php to wp-content/themes/yourtheme/cryptowoo/payment.php
		$custom_template = get_template_directory().'/cryptowoo/payment.php';
		if(file_exists($custom_template)) {
			include_once( $custom_template );
		} else {
			include_once( CWOO_PLUGIN_DIR . 'includes/payment.php' );
		}
	}

	}// Payment gateway class ends here

	/**
	 * Add CryptoWoo to the available payment methods in WooCommerce
	 *
	 * @param $methods
	 * @return array
     */
	function woocommerce_cryptowoo_add_gateway($methods) {
		$methods[] = 'WC_CryptoWoo';

		return $methods;

	}

	add_filter('woocommerce_payment_gateways', 'woocommerce_cryptowoo_add_gateway');

}

/**
 * Get current WooCommerce store currency.
 * File name: woocommerce/includes/wc-core-functions.php
 *
 **/
function cw_get_woocommerce_currency() {
	return apply_filters( 'woocommerce_currency', get_option('woocommerce_currency') );
}

/**
 * Get full list of currency codes, including digital currencies supported by CryptoWoo
 * File name: woocommerce/includes/wc-core-functions.php
 *
 * @return array
 */
function cw_get_woocommerce_currencies() {
	return array_unique(
		apply_filters( 'woocommerce_currencies',
			array(
				'AED' => esc_html__( 'United Arab Emirates Dirham', 'woocommerce' ),
				'ARS' => esc_html__( 'Argentine Peso', 'woocommerce' ),
				'AUD' => esc_html__( 'Australian Dollars', 'woocommerce' ),
				'BDT' => esc_html__( 'Bangladeshi Taka', 'woocommerce' ),
				'BRL' => esc_html__( 'Brazilian Real', 'woocommerce' ),
				'BGN' => esc_html__( 'Bulgarian Lev', 'woocommerce' ),
				'CAD' => esc_html__( 'Canadian Dollars', 'woocommerce' ),
				'CLP' => esc_html__( 'Chilean Peso', 'woocommerce' ),
				'CNY' => esc_html__( 'Chinese Yuan', 'woocommerce' ),
				'COP' => esc_html__( 'Colombian Peso', 'woocommerce' ),
				'CZK' => esc_html__( 'Czech Koruna', 'woocommerce' ),
				'DKK' => esc_html__( 'Danish Krone', 'woocommerce' ),
				'DOP' => esc_html__( 'Dominican Peso', 'woocommerce' ),
				'EUR' => esc_html__( 'Euros', 'woocommerce' ),
				'HKD' => esc_html__( 'Hong Kong Dollar', 'woocommerce' ),
				'HRK' => esc_html__( 'Croatia kuna', 'woocommerce' ),
				'HUF' => esc_html__( 'Hungarian Forint', 'woocommerce' ),
				'ISK' => esc_html__( 'Icelandic krona', 'woocommerce' ),
				'IDR' => esc_html__( 'Indonesia Rupiah', 'woocommerce' ),
				'INR' => esc_html__( 'Indian Rupee', 'woocommerce' ),
				'NPR' => esc_html__( 'Nepali Rupee', 'woocommerce' ),
				'ILS' => esc_html__( 'Israeli Shekel', 'woocommerce' ),
				'JPY' => esc_html__( 'Japanese Yen', 'woocommerce' ),
				'KIP' => esc_html__( 'Lao Kip', 'woocommerce' ),
				'KRW' => esc_html__( 'South Korean Won', 'woocommerce' ),
				'MYR' => esc_html__( 'Malaysian Ringgits', 'woocommerce' ),
				'MXN' => esc_html__( 'Mexican Peso', 'woocommerce' ),
				'NGN' => esc_html__( 'Nigerian Naira', 'woocommerce' ),
				'NOK' => esc_html__( 'Norwegian Krone', 'woocommerce' ),
				'NZD' => esc_html__( 'New Zealand Dollar', 'woocommerce' ),
				'PYG' => esc_html__( 'Paraguayan Guaraní', 'woocommerce' ),
				'PHP' => esc_html__( 'Philippine Pesos', 'woocommerce' ),
				'PLN' => esc_html__( 'Polish Zloty', 'woocommerce' ),
				'GBP' => esc_html__( 'Pounds Sterling', 'woocommerce' ),
				'RON' => esc_html__( 'Romanian Leu', 'woocommerce' ),
				'RUB' => esc_html__( 'Russian Ruble', 'woocommerce' ),
				'SGD' => esc_html__( 'Singapore Dollar', 'woocommerce' ),
				'ZAR' => esc_html__( 'South African rand', 'woocommerce' ),
				'SEK' => esc_html__( 'Swedish Krona', 'woocommerce' ),
				'CHF' => esc_html__( 'Swiss Franc', 'woocommerce' ),
				'TWD' => esc_html__( 'Taiwan New Dollars', 'woocommerce' ),
				'THB' => esc_html__( 'Thai Baht', 'woocommerce' ),
				'TRY' => esc_html__( 'Turkish Lira', 'woocommerce' ),
				'UAH' => esc_html__( 'Ukrainian Hryvnia', 'woocommerce' ),
				'USD' => esc_html__( 'US Dollar', 'woocommerce' ),
				'VND' => esc_html__( 'Vietnamese Dong', 'woocommerce' ),
				'EGP' => esc_html__( 'Egyptian Pound', 'woocommerce' ),
				'BTC' => esc_html__( 'Bitcoin', 'cryptowoo' ),
				'BTCTEST' => esc_html__( 'Testnet Bitcoin', 'cryptowoo' ),
				'LTC' => esc_html__( 'Litecoin', 'cryptowoo' ),
				'DOGE' => esc_html__( 'Dogecoin', 'cryptowoo' ),
				'DOGETEST' => esc_html__( 'Testnet Dogecoin', 'cryptowoo' ),
				'BLK' => esc_html__('BlackCoin', 'cryptowoo')
			)
		)
	);
}

/**
 * Customized get_woocommerce_currency_symbol() function
 * File name: woocommerce/includes/wc-core-functions.php
 * Added BTC, LTC and DOGE symbols
 *
 * @param $currency
 * @return string
 */
function cw_get_currency_symbol($currency = '') {

	if ( ! $currency ) {
		$currency = cw_get_woocommerce_currency();
	}

	switch ( $currency ) {
		case 'AED' :
			$currency_symbol = 'د.إ';
			break;
		case 'BDT':
			$currency_symbol = '৳ ';
			break;
		case 'BRL' :
			$currency_symbol = 'R$';
			break;
		case 'BGN' :
			$currency_symbol = 'лв.';
			break;
		case 'AUD' :
		case 'CAD' :
		case 'CLP' :
		case 'COP' :
		case 'MXN' :
		case 'NZD' :
		case 'HKD' :
		case 'SGD' :
		case 'USD' :
			$currency_symbol = '$';
			break;
		case 'EUR' :
			$currency_symbol = '€';
			break;
		case 'CNY' :
		case 'RMB' :
		case 'JPY' :
			$currency_symbol = '¥';
			break;
		case 'RUB' :
			$currency_symbol = 'руб.';
			break;
		case 'KRW' :
			$currency_symbol = '₩';
			break;
		case 'PYG' :
			$currency_symbol = '₲';
			break;
		case 'TRY' :
			$currency_symbol = '₺';
			break;
		case 'NOK' :
			$currency_symbol = 'kr';
			break;
		case 'ZAR' :
			$currency_symbol = 'R';
			break;
		case 'CZK' :
			$currency_symbol = 'Kč';
			break;
		case 'MYR' :
			$currency_symbol = 'RM';
			break;
		case 'DKK' :
			$currency_symbol = 'kr.';
			break;
		case 'HUF' :
			$currency_symbol = 'Ft';
			break;
		case 'IDR' :
			$currency_symbol = 'Rp';
			break;
		case 'INR' :
			$currency_symbol = 'Rs.';
			break;
		case 'NPR' :
			$currency_symbol = 'Rs.';
			break;
		case 'ISK' :
			$currency_symbol = 'Kr.';
			break;
		case 'ILS' :
			$currency_symbol = '₪';
			break;
		case 'PHP' :
			$currency_symbol = '₱';
			break;
		case 'PLN' :
			$currency_symbol = 'zł';
			break;
		case 'SEK' :
			$currency_symbol = 'kr';
			break;
		case 'CHF' :
			$currency_symbol = 'CHF';
			break;
		case 'TWD' :
			$currency_symbol = 'NT$';
			break;
		case 'THB' :
			$currency_symbol = '฿';
			break;
		case 'GBP' :
			$currency_symbol = '£';
			break;
		case 'RON' :
			$currency_symbol = 'lei';
			break;
		case 'VND' :
			$currency_symbol = '₫';
			break;
		case 'NGN' :
			$currency_symbol = '₦';
			break;
		case 'HRK' :
			$currency_symbol = 'Kn';
			break;
		case 'EGP' :
			$currency_symbol = 'EGP';
			break;
		case 'DOP' :
			$currency_symbol = 'RD$';
			break;
		case 'KIP' :
			$currency_symbol = '₭';
			break;

		// add cryptocurrency symbols
		case 'BTC' :
		case 'BTCTEST' :
			$currency_symbol = '&#3647;';
			break;
		case 'DOGE' :
		case 'DOGETEST' :
			$currency_symbol = '&#208;';
			break;
		case 'LTC' :
			$currency_symbol = '&#321;';
			break;
		case 'BLK' :
			$currency_symbol = '&#1123;';
			break;
		default    :
			$currency_symbol = '';
			break;
	}

	return apply_filters('cw_get_currency_symbol', $currency_symbol, $currency);
}

/**
 * Return array of enabled currencies with readable names
 *
 * @param $with_rates_only
 * @return array
 */
 function cw_get_enabled_currencies($with_rates_only = true) {
	 $enabled_currencies = array();

	 $options = get_option('cryptowoo_payments');

	 // Get norates transient
	 $norates = get_transient('cryptowoo_norates');

	 $currency_nicenames = cw_get_woocommerce_currencies();

	 $mpk_string = 'cryptowoo_%s_mpk'; // beware of BIP44 variant 'cryptowoo_%s_mpk_xpub'
	 $api_string = 'cryptowoo_%s_api';

	 $coin_identifiers = array('BTC' => 'btc',
							   'BTCTEST' => 'btc_test',
							   'DOGE' => 'doge',
							   'DOGETEST' => 'doge_test',
							   'LTC' => 'ltc',
							   'BLK' => 'blk');
     $coin_identifiers = apply_filters('cw_get_enabled_currencies', $coin_identifiers);

	 foreach ($coin_identifiers as $currency => $coin_identifier) {
		 $is_hd  = CW_Validate::check_if_unset(sprintf($mpk_string, $coin_identifier), $options, false) || CW_Validate::check_if_unset(sprintf('cryptowoo_%s_mpk_xpub', $coin_identifier), $options, false);
		 $is_api = CW_Validate::check_if_unset(sprintf($api_string, $coin_identifier), $options, false);
		 // Currency is enabled if we have an API key or an MPK and we did not set the "norates" transient yet
		 if (!isset($norates[$currency]) && ($is_hd || $is_api)) {
			 $enabled_currencies[$currency] = $currency_nicenames[$currency];
		 }
	 }
	 return $with_rates_only ? CW_ExchangeRates::filter_enabled_currencies_without_rates($enabled_currencies) : $enabled_currencies;
 }


/**
 * Add the enabled currencies to the "WooCommerce Currency Switcher" plugin by hooking into "woocs_currency_data_manipulation" filter
 *
 * @param $woocs_currencies
 *
 * @return array
 */
function add_currencies_to_woocs($woocs_currencies) {

	$currencies = array();
	$options = get_option('cryptowoo_payments');

	// Only change defaults on frontend pages
	if (!(bool)$options['add_currencies_to_woocs'] || defined('DOING_CRON') && DOING_CRON || is_admin() && !defined('DOING_AJAX') || (defined('DOING_AJAX') && !DOING_AJAX)) {
		return $woocs_currencies;
	}

	// Get WooCommerce Currency
	$woocommerce_currency = get_option('woocommerce_currency'); // since switcher 1.1.5 cw_get_woocommerce_currency();

	// Reset switcher to store currency on Dokan withdraw pages
	if(false !== get_option('dokan_pages') && false !== strpos($_SERVER['REQUEST_URI'], '/withdraw')) {
		$st = new WOOCS_STORAGE();
		$st->set_val('woocs_current_currency', $woocommerce_currency);
		return $woocs_currencies;
	}

	// Reset to store currency on checkout page
	add_filter('wp_head',function(){
		if(is_checkout()) {
			global $WOOCS;
			$WOOCS->current_currency = get_option('woocommerce_currency');
			$WOOCS->storage->set_val('woocs_current_currency', $WOOCS->current_currency);
		}
	});

	// Maybe get currency settings from 10s transient
	$cached = get_transient('cryptowoo-woocs');
	if(false !== $cached) {
		return $cached;
	}

	// Get currency nicenames
	$wc_currencies = cw_get_woocommerce_currencies();

	if (is_array($options)) {

		$enabled_currencies = cw_get_enabled_currencies();

		foreach ($enabled_currencies as $enabled_currency => $nicename) {
			if (!strpos($enabled_currency, 'TEST')) {

				// Get multiplier option
				$multiplier_key = 'multiplier_' . strtolower($enabled_currency);
				$multiplier     = isset($options[$multiplier_key]) ? $options[$multiplier_key] : 1;

				// Get exchange rate
				$rate = CW_ExchangeRates::get_exchange_rate($enabled_currency, false, true);

				// Create woocs args for currency
				if (is_numeric($rate)) {
					// Prepare exchange rate, set to 1 if rate not found
					$prep_rate = $rate > 0 && $multiplier > 0 ? (1 / ($rate / (float)$multiplier)) : 1;
					$flag_file = sprintf('%sassets/images/flags/%s.png', CWOO_PLUGIN_DIR, strtolower($enabled_currency));
					$flag_exists = $prep_rate !== 1 ? file_exists($flag_file) : false;
					$dec_key = sprintf('decimals_%s',$enabled_currency);
					// Prepare currency data
					$args                                      = array('name' => $prep_rate !== 1 ? $enabled_currency : $woocommerce_currency,
																	   'rate' => $prep_rate,
																	   'symbol' => $prep_rate !== 1 ? cw_get_currency_symbol($enabled_currency) : cw_get_currency_symbol($woocommerce_currency),
																	   'position' => !isset($woocs_currencies[$enabled_currency]['position']) ? 'left_space' : $woocs_currencies[$enabled_currency]['position'],
																	   'is_etalon' => 0,
																	   //'decimals' => isset($woocs_currencies[$enabled_currency]['decimals']) ? (int)$woocs_currencies[$enabled_currency]['decimals'] : 8,
																	   'decimals' => isset($options[$dec_key]) ? $options[$dec_key] : 8,
																	   'description' => $prep_rate !== 1 ? $nicename : $wc_currencies[$woocommerce_currency],
																	   'hide_cents' => 0,
																	   'flag' => $flag_exists ? sprintf('%sassets/images/flags/%s.png', CWOO_PLUGIN_PATH, strtolower($enabled_currency)) : '');

					$currencies[$enabled_currency]             = $args;
					$currencies[$enabled_currency]['raw_rate'] = $rate;
				}
			}
		}
	}
	$merged = array_merge($woocs_currencies, $currencies);
	set_transient('cryptowoo-woocs', $merged, 10);
	return $merged;
}


