<?php
// Do some cleanup during plugin uninstall
//if uninstall not called from WordPress exit
if (!defined('WP_UNINSTALL_PLUGIN'))
    exit();

$options = get_option('cryptowoo_payments');

global $wpdb;

if (function_exists('is_multisite') && is_multisite()) {
	// We are on a multisite - get all blogs
	$blogs = wp_get_sites();
	foreach ( $blogs as $blog ) {

		switch_to_blog($blog['blog_id']);

		// Maybe delete tables
		if ( $options['keep_tables'] != '1' ) {
			$tables = array(
				$wpdb->prefix . 'cryptowoo_exchange_rates',
				$wpdb->prefix . 'cryptowoo_payments_temp',
			);

			// Drop tables
			foreach ( $tables as $table ) {
				$wpdb->query( "DROP TABLE IF EXISTS {$table}" );
			}
		}
		// Maybe clean up options table
		if ( $options['keep_options'] != '1' ) {
			// Get all options containing *cryptowoo*
			$all_options = $wpdb->get_results( "SELECT option_name FROM " . $wpdb->prefix . "options WHERE option_name LIKE '%cryptowoo%'" );

			// Delete options
			foreach ( $all_options as $option ) {
				delete_option( $option->option_name );
			}
		}
	}
} else {
	// Single site

	// Maybe delete tables
	if ( $options['keep_tables'] != '1' ) {
		$tables = array(
			$wpdb->prefix . 'cryptowoo_exchange_rates',
			$wpdb->prefix . 'cryptowoo_payments_temp',
		);

		// Drop tables
		foreach ( $tables as $table ) {
			$wpdb->query( "DROP TABLE IF EXISTS {$table}" );
		}
	}
	// Maybe clean up options table
	if ( $options['keep_options'] != '1' ) {
		// Get all options containing *cryptowoo*
		$all_options = $wpdb->get_results( "SELECT option_name FROM " . $wpdb->prefix . "options WHERE option_name LIKE '%cryptowoo%'" );

		// Delete options
		foreach ( $all_options as $option ) {
			delete_option( $option->option_name );
		}
	}
}

// Remove integrity check file
if(isset($options['cw_filename'])) {
	$path = WP_CONTENT_DIR . '/uploads/' . $options['cw_filename'];
	if ( file_exists( $path ) ) {
		unlink( $path );
	}
}





