<?php
/**
 * WP Cron on WordPress Multisite installs: Get blog urls from database and curl wp-cron.php for each site.
 *
 * Copy this file in your WordPress root directory and create external cronjobs like in the example below:
 *
 *    * * * * * wget -q -O - "http://mydomain.com/wp-cron-multisite.php" > /dev/null 2>&1
 *
 * Modified from https://www.lucasrolff.com/wordpress/why-wp-cron-sucks/
 *
 **/
require('./wp-load.php');
global $wpdb;
echo time();
// Get all blogs in network
$blogs = $wpdb->get_results( @$wpdb->prepare( 
	"
		SELECT domain, path
		FROM $wpdb->blogs 
		WHERE archived = %s AND deleted = %s
	", 
	'0','0'
) );

foreach($blogs as $blog) {
    $command = "http://" . $blog->domain . ($blog->path ? $blog->path : '/') . 'wp-cron.php';
    $ch = curl_init($command);
    $rc = curl_setopt($ch, CURLOPT_RETURNTRANSFER, FALSE);
    $rc = curl_exec($ch);
    curl_close($ch);
}
