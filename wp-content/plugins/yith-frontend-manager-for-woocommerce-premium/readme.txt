== YITH Frontend Manager for WooCommerce Premium ===

Contributors: yithemes
Requires at least: 4.0
Tested up to: 4.9.2
Stable tag: 1.3.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Multi vendor e-commerce is a new idea of e-commerce platform that is becoming more and more popular in the web.

== Changelog ==

= 1.3.2 - Released on 22nd December, 2017 =

* Fix: Style issue with YITH theme with FW 2.0 or greather
* Fix: Tags and Categories list in add/edit product page
* Fix: Unable to set Enable Reviews in add/edit product page

= 1.3.1 - Released on 15th December, 2017 =

* New: Tested up WooCommerce 3.2.6
* Update: Plugin Framework 3.0.1

= 1.3.0 - Released on 13rd December, 2017 =

* New: Translate frontend manager menu with WPML
* New: Support for YITH WooCommerce Colors and Labels Variations Premium (min. version 1.5.1)
* Tweak: Remove dynamics.css file from YITH 2.0 theme options from Skins
* Tweak: Update the YITH Plugin Framework 3.0
* Update: Spanish language files
* Fix: Fatal error if try to change the status of one commission in bulk action
* Fix: Style issue with long Net sales this month value in dashboard
* Fix: Unable to remove all images from product gallery
* Fix: Lost is_active class in navigation menu if custom slug are set
* Fix: Unable to scroll the commissions table on flatsome theme
* Fix: Column post.ID doesn't exists in Dashboard if vendor haven't orders
* Fix: Responsive style on Reports and Product Edit page
* Fix: Vendor with pending account can access to frontend manager dashboard
* Fix: Style issue with Globe theme by YITH in Product Edit Page

= 1.2.1 =

* New: Support to YITH WooCommerce Featured Audio and Video Content Premium (vers. 1.1.16 or greather)
* Tweak: 2.0 accessibility. Text meant only for screen readers

= 1.2.0 =

* New: Support for WooCommerce TM Extra Product Options plugin
* New: Support fo Woocommerce 3.2.1
* Tweak: change the string "Net sales on this month" to "Net commissions on this month" in vendor's dashboard
* Tweak: Change the old wc-tooltip with jquery-tiptip
* Tweak: Add minify js file for coupons script
* Fix: Coupons script loaded two times if the current user is a valid vendor
* Fix: Unable to add variable products with WooCommerce 3.2
* Fix: Unable to add related products with WooCommerce 3.2
* Fix: Net sales commissions on dashboard showing only 0,00 $ with version 1.1.0
* Fix: Vendor can't add order notes
* Fix: WooCommerce admin style.css enqueued in all pages
* Fix: Unable to set percentage coupon for vendors

= 1.1.0 =

* Tweak. Add message if the user enable the plugin without WooCommerce
* Tweak: Prevent Fatal Error if the wc_create_page function doesn't exists
* Tweak: Skin system refactoring
* Fix: Unable to save product shipping class
* Fix: Style in skin-1 sidebar
* Fix: New vendor can see all orders
* Fix: Unable to save shipping for vendors with a long list of shipping classes
* Fix: Wrong net sales in dashboard for vendor users

= 1.0.15 =

* Tweak: Prevent "Nested level too deep" error
* Tweak: Prevent to have different store with the same name
* Fix: Wrong style with Firefox browser
* Fix: Vendor avatar doesn't show in skin-1
* Fix: Vendor name doesn't shown in edit product
* Fix: Wrong net sales value for vendors with no orders in dashboard section
* Fix: Style issue in product variation box
* Fix: Plugin loads admin scripts on frontend on all website pages

= 1.0.14 =

* Tweak: Improved style for shipping zones popup on YITH WooCommerce Multi Vendor panels inside Frontend Manager

= 1.0.13 =

* Fix: Removed notice on templates/skins/skin-1/header.php
* Fix: Datepicker on coupons

= 1.0.12 =

* Tweak: Change vendor admin link with frontend manager page
* Fix: Vendor restrict backend access option fails all admin ajax calls on frontend
* Fix: Administrator can't see WordPress admin bar
* Fix: Products section and Reports section ABSPATH
* Fix ABSPATH in dashboard sections

= 1.0.11 =

* Tweak: Flush permalinks after user login
* Fix: Empty billing and shipping address if admin/vendor click on pencil icon to edit it
* Fix: Vendor can't set shipping zone and shipping method on frontend

= 1.0.10 =

* Fix: Vendor can't see media library if user have no access to admin area
* Fix: Vendor can't upload image in media library if user have no access to admin area
* Fix: Shop Manager can't see media library if user have no access to admin area
* Fix: Shop Manager can't upload image in media library if user have no access to admin area

= 1.0.9 =

* Fix: Unable to save vendor settings if vendor can't access to backend
* Fix: Prevent wrong edit post type url if no default page is set
* Tweak: Enanched style support for Nielsen theme
* Tweak: Enanched style support for Twenty theme
* Tweak: Enanched style support for Flatsome theme
* Tweak: Enanched style support for Sydney theme
* Tweak: Enanched style support for Business Center theme
* Tweak: Enanched style support for Rémy theme
* Tweak: Enanched style support for Mindig theme
* Tweak: Enanched style support for Desire Sexy Shop theme

= 1.0.8 =

* Fix: Unable to set price for simple products with Flatsome and WooCommerce 3.1
* Fix: Undefined index tab in panel page

= 1.0.7 =

* New: Flush permalinks option
* New Support for WooCommerce 3.1-RC2
* Fix: get_current_screen() not defined in frontend class
* Fix: Support for Flatsome theme
* Fix: Support for bb-theme
* Fix: 404 on each sections after plugin activation

= 1.0.6 =

* New: Support to YITH Nielsen theme
* Tweak: Version on skin style.css
* Fix: Some strings have a wrong text-domain
* Fix: Missing icon if change sections slug

= 1.0.5 =

* Fix: Some strings have a wrong text-domain
* Fix: missing CSS Rules on default skin and skin-1
* Fix: All sections return a 404 not found error after theme switching or multi vendor plugin activation
* Tweak: Hide live chat popup in frontend manager section

= 1.0.4 =

* Fix: Super admin can't access to backend in WordPress MultiSite if the vendor restrict admin area access are set to "YES"

= 1.0.3 =

* New: Add language catalog file
* Fix: Translation issue in endpoint sections panel

= 1.0.2 =

* Fix: Failed opening required with multi vendor free

= 1.0.1 =

* Fix: The function YITH_Vendor_Shipping doesn't exists

= 1.0.0 =

* Initial release
