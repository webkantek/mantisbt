= 0.20.2 2017-11-17 =
Feature: Allow custom payment page templates: Copy the file wp-content/plugins/cryptowoo/includes/payment.php to wp-content/themes/YOURTHEME/cryptowoo/payment.php to modify it.
Improved: Redirect to "order received" page if the WC order status is not "pending payment"
Improved: Add order note on doublespend/RBF and keep the order open instead of just cancelling it.
Improved: Remove unneeded file
Improved: Remove redundant <p> element
Fixed: Timer and progress bar on payment page disappearing after the first poll
Fixed: Altcoin exchange rates not working when using BTC as WooCommerce store currency
Fixed: Don't delete addresses of failed orders
Fixed: Exit after wp_safe_redirect()

= 0.20.1 2017-11-11 =
Fixed:  escaped HTML in cryptocurrency payment details

= 0.20.0 = 2017-11-10
Feature: Add option to immediately redirect to the “Thank You” page when we detect a unconfirmed transaction
Feature: Add option for custom Shapeshift Affiliate ID
Feature: Allow connections to blockchain.info’s hidden service exchange rate API
Feature: Add exchange rates from livecoin.net
Improved: Add visual feedback about unconfirmed transaction to the payment page
Improved: Security Image Threshold: Use spinner instead of select field
Improved: Translatable button string
Improved: Add “WC tested up to” header
Improved: Add Dash “flag” for Currency Switcher
Improved: Change text of empty currency batch response
Improved: Update translations
Improved: Remove old license manager menu
Improved: Disable add-on overview
Fixed: Escape translated strings
Fixed: Forced exchange rate update not forcing
Fixed: Wrong parameters in call to get_kraken_price()
Fixed: Make sure the WooCommerce order exists before setting the status to “failed”
Fixed: jQuery dependency
Fixed: Change help desk URL
Fixed: Wrap hash_pbkdf2 compat function in function_exists()
Fixed: Keep address on invalid blockexplorer links
Fixed: Pluralized cron schedule names

= 0.19.4 = 2017-09-20
Fixed: Litecoin multisig validation

= 0.19.3 = 2017-09-20
Feature: Display address in an additional image on the payment page to help the user detect address-replacing malware
Improved: Use "Hide exchange rate provider" as default option
Fixed: Don't override Currency Switcher defaults
Fixed: WP 4.8.2 wpdb->prepare() compatibility

= 0.19.2 = 2017-09-02
Fixed: Revert extended rate error warning emails

= 0.19.1 = 2017-09-02
Fixed: Overzealous exchange rate error warning emails

= 0.19.0 = 2017-09-01
Improved: Add exchange rates from OKCoin.com, OKCoin.cn, Kraken, Luno.com
Improved: Additional filters for easier altcoin integration
Fixed: Duplicate coin icon on single product page pricing table
Fixed: QR Code unreliable on dark background
Fixed: Remove BTC-e from available exchange rate providers

= 0.18.3.1 = 2017-07-31
Fixed: Dash background exchange rate update not being triggered
Improved: Validate address before creating a blockexplorer link

= 0.18.3 = 2017-07-24
Fixed: Skip address if the corresponding WooCommerce order does not exist
Fixed: Update WooCommerce meta upon payment currency change
Fixed: Prevent overzealous overpayment detection
Improved: Move crypto-only store below class CW_Formatting
Improved: Apply filters in update_tx_details if the processing API doesn't exist
Improved: Display link to Database Actions page in exchange rate error email
Improved: Composer update
Improved: Allow up to 3 decimals in multiplier

= 0.18.2.1 = 2017-06-30
Fixed: get_coin_icon() assigning wrong class

= 0.18.2 = 2017-06-30
Feature: Add option to adapt the column width of each currency on the order total estimation
Feature: Add option to hide the exchange rate
Improved: Update aw-cryptocoins
Improved: Use CSS columns instead of table on cryptocurrency price estimation
Improved: Update Redux Framework to 3.6.5
Improved: Remove redundant <br> elements from checkout page order total
Fixed:  WooCommerce 3.0 compatibility issue in cryptocurrency price estimation on product pages
Fixed: Fix cron schedule string
Fixed: Catch undefined index in currency switcher filter

= 0.18.1 = 2017-04-18
Feature: Custom block explorers for links to cryptocurrency addresses
Feature: Insight API class supports InstantSend
Improved: Filter cryptowoo_misconfig_notice()
Improved: Increase number of decimals in altcoin order total estimations on checkout page
Fixed: Missing filter for cw_get_currency_symbol()

= 0.18.0 = 2017-04-13
Improved: WooCommerce 3.0 compatibility update

= 0.17.1 = 2017-04-13
Feature: Add payment method to searchable fields on WooCommerce order overview
Improved: Remove "CryptoWoo Payments" page

= 0.17.0 = 2017-04-12
    Feature: Add CoinDesk Bitcoin Price Index exchange rates
    Feature: Use apply_filters() for coin specific functions
    Feature: Apply filters on CW_Insight::prepare_insight_api() if no Insight API URL is set
    Feature: Add option to fully hide the order total estimation on the checkout page
    Feature: Allow styling of order total estimation via custom CSS:

                 /**
                  * Add custom CSS to WooCommerce product or checkout pages
                  */
                 function enqueue_custom_cryptowoo_styles() {

                     // Use is_product() to determine if we are on the product page
                     // Use is_checkout() to determine if we are on the checkout page

                     if ( is_checkout() ) { // If we are on a WooCommerce checkout page
                 ?>
                     <style type="text/css">
                     .priceinfo { font-weight:bold; }
                     .exchangeinfo { font-size: 70%; }
                     #price-BTC { background-color: red; }
                     #price-LTC { background-color: green; }
                     #price-DOGE { background-color: yellow; }
                     #price-BLK { background-color: turquoise; }
                    </style>
                 <?php }
                 }
                 add_action('wp_head', 'enqueue_custom_cryptowoo_styles', 100);

    Improved: Hide zeroconf options if we are not accepting unconfirmed transactions
    Improved: Update translations
    Improved: Update dependencies
    Fixed: Broken placeholder for minutes in "Underpayment Notice Trigger" select fields
    Fixed: Order status is updated even if the customer switched the payment method
    Fixed: Checkout page CSS issues


= 0.16.9.1 = 2017-02-28

    Fixed: Order processing not always honouring congestion handling settings

= 0.16.9 = 2017-02-27

    Fixed: bitcoincharts.com exchange rate provider not visible for store currencies other than USD

= 0.16.8 = 2017-02-27

    Feature: Add bitcoincharts.com exchange rates
    Improved: Revisit internationalization

= 0.16.7 = 2017-02-10

    Fixed undefined index

= 0.16.6 = 2017-02-10

    Improved: Tweak Insight maybe getting tx confidence from chain.so
    Improved: Refactor get_sum_outputs(), use value_int in smartbit response
    Fixed: Insight not honouring min_conf > 1 setting
    Fixed: Block.io library update: enforce use of TLSv1.2
    Fixed: No gettext for empty string

= 0.16.5 = 2017-01-29

    Feature: Accept “raw” zeroconf transactions
    Feature: Add bitcoin address and txid to WooCommerce order search fields
    Feature: Change the payment page width
    Improved: Rewrite payment page
    Improved: WooCommerce order overview: Link payment address to blockexplorer
    Improved: Update dependencies
    Improved: Update jQuery fallback version
    Fixed: Blockcypher batch update: Check only prioritized batch and make sure we bump last_update
    Fixed: Order sorting by last_update

= 0.16.4 = 2016-11-17

Please visit the settings page and click on “Save Changes” after updating the plugin.

    Fixed: Overzealous underpayment handling

= 0.16.3 = 2016-11-15

    Feature: Payment page amount and address highlight in one click
    Feature: Email admin upon ‘cryptowoo_api_error’ action
    Feature: Log verbosity options
    Feature: Insight check transaction locktime against block height when processing zeroconf transactions
    Feature: enable/disable add_currencies_to_woocs()
    Feature: Define the number of decimals for cryptocurrency amounts added to WooCommerce Currency Switcher
    Improved: Blockcypher zeroconf processing
    Improved: Address reuse detection, log skipped transactions
    Improved: Insight API processing
    Improved: Blockcypher & Insight flag RBF txid in order meta
    Improved: Disable RBF-check for chain.so
    Improved: Custom API & third party confidence metric setting explanation
    Improved: Calculation of amount received for confirmed transactions
    Improved: Bitcoinaverage API v2 compatibility, cache rates for ~10 minutes to stay below request limits
    Improved: Tweak data lag color on database actions page
    Improved: Display orders with status “processing” on CryptoWoo Payments admin page
    Fixed: Order prioritization not using prioritized array
    Fixed: Custom processing API validation not displaying WP_Error message
    Fixed: Pricing table not honoring multiplier
    Fixed: Insight tx confidence request during zeroconf processing not using chain.so
    Fixed: Use actual genesis blocks for Insight API URL validation
    Fixed: add-on status detection
    Fixed: Smartbit to chain.so fallback, add missing action hooks
    Fixed: TX update error transient structure

= 0.16.2 = 2016-10-13

This update contains breaking changes!
After updating the plugin, go to the CryptoWoo settings page (/wp-admin/admin.php?page=cryptowoo-options), refresh your browser cache (ctrl + F5), and make sure all settings are correct.

Please click on “Save Changes”, regardless if you modified any setting or not.

    Feature: Hide exchange rate provider on checkout page
    Feature: Hide countdown and progress bar on payment page
    Feature: Add Bitstamp, Coinbase, and Blockchain.info exchange rates
    Feature: New action hook ‘cryptowoo_new_order’ that fires when a new CryptoWoo order is submitted.
    Improved: Allow maximum order expiration time of 72 hours
    Improved: Ignore slight underpayments
    Improved: Select underpayment notice trigger event
    Improved: Add smartbit.com.au fallback processing
    Improved: Update Redux settings panel to 3.6.2
    Fixed: Skip RBF check when no inputs are present in API response
    Fixed: Bitcoinaverage API URL on Database Actions page
    Fixed: Bitcoinaverage exchange rate fallback fails on large data lag
    Fixed: Log rotation overwriting wrong file
    Fixed: License activation link in admin notice points to wrong page
    Fixed: Reset Currency Switcher to store currency on Dokan withdraw pages
    Modified: Remove “WooCommerce Currency Switcher” from TGMPA

= 0.15.2 = 2016-08-10

    Fixed: Overzealous payment currency validation on checkout page
    Improved: Adapt blockcypher batch updates to new API request limits
    Improved: Update translations
    Fixed: E-mail formatting

= 0.15.1 = 2016-08-05

    Improved: Clarified Blockcypher token requirements
    Fixed: Currency Switcher plugin 1.1.7 compatibility, added instructions for free version – more info
    Fixed: Gateway details on checkout page not visible if gateway description is empty
    Fixed: Undefined index in QR Code and payment URI
    Fixed: License manager undefined index, rename “API Key” to “License” to prevent confusion
    Fixed: Blockcypher account link

= 0.15.0 = 2016-07-25

    Fixed: License manager not displaying updates, update lib to 1.4.6.3

= 0.14.2 = 2016-07-25

    Feature: Custom overpayment order note
    Feature: HTML formatted admin e-Mails
    Improved: Better Overpayment handling – more info

= 0.14.1 = 2016-07-19

    Fixed: Removed redundant HTML tags in settings explanations

= 0.14.0 = 2016-07-19

    Improved: Better handling of overpayments
    Improved: Update Redux Framework to 3.6.0.2
    Fixed: Fixed Customizer path issues

= 0.13.3 = 2016-07-03

    Feature: Add option to select the block explorer used for links to payment addresses
    Feature: Add German translation for frontend
    Improved: Refactor block explorer API helper functions
    Improved: Add currency logos as images to use as flag for “WooCommerce Currency Switcher”
    Improved: Update dependencies
    Fixed: Minor bugs

= 0.13.2 = 2016-03-28

    Improved: Settings explanations
    Improved: New logo
    Fixed: Minor bugs

= 0.13.1 = 2016-03-23

    Fixed: Minor bugs

= 0.13.0 = 2016-03-20

    Feature: Insight API support
    Feature: Add Poloniex & Bittrex altcoin exchange rates
    Feature: Optional exchange rate error visualization
    Improved: Confidence factor seperately for each currency
    Improved: Block.io forwarding threshold amount for each currency
    Improved: Transaction polling
    Improved: Order processing
    Improved: Admin settings validation

= 0.12.2 = 2016-02-02

    Improved: Optional license manager
    Fixed: Remove Cryptsy exchange rates
    Fixed: Dismissable update notice
    Fixed: BlockCypher token limit check

= 0.12.1 = 2016-01-24

    Improved: Batch order processing
    Improved: Network congestion handling
    Improved: Honor BlockCypher API token limits
    Improved: Update Redux Framework to 3.5.8.1
    Improved: TX update error handling
    Fixed: WooCommerce Currency Switcher 1.1.5 compatibility

= 0.12 = 2015-12-24

    Improved: Transaction update uses batch API calls.
    Feature: Select preferred processing API
    Feature: Accept any altcoin via Shapeshift button
    Improved: Progress bar on payment page
    Improved: Switch back to davidshimjs QR code library
    Improved: noscript tags on payment page
    Improved: Payment page styling
    Modified: Implement Blockcypher php-client
    Update BitWasp library to 0.0.19
    Feature: Add action hooks
        cwrc_catch_request # Cron execution result
        cryptowoo_api_error # API error data
        cryptowoo_doublespend # Doublespend alert (Blockcypher)
        cryptowoo_refund_required # Customer sent less funds than required
        cryptowoo_confirmed # An order has been confirmed

= 0.11.3 = 2015-12-07

    Feature: Pre-selected currency on checkout page
    Improved: Set refund address field optional/required/disabled
    Improved: Admin settings explanations
    Removed: Internal price rewriting
    Modified: Shorter polling interval on payment page
    Fix: QR code encoding
    Fix: Minor bugs

= 0.11.2 = 2015-12-01

    Improved: Admin settings explanations
    Improved: HD wallet order processing
    Modified: Use block explorer corresponding to API
    Fix: Order timeout handling

= 0.11.1 = 2015-11-30

    Improved: License notice on plugin pages only
    Fix: Minor bugs

= 0.11.0 = 2015-11-20

    Improved: WP Multisite compatibility
    Improved: HD wallet order processing
    Feature: Custom text on “payment”, “thank you”, and “view order” pages

= 0.10.6 RC = 2015-11-17

    Tweak: Block.io address archival function
    Improved: Don’t enforce TLSv1 in Block.io library, catch SSL handshake errors instead
    Tweak: Wording in admin notices
    Tweak: Thank you page CSS
    Fix: Minor bugs

= 0.10.5 RC = 2015-11-10

    Fix: Payment page CSS

= 0.10.4 RC = 2015-11-06

    Improved: Redirect after order timeout
    Improved: Payment page styling
    Improved: Redux Framework switches instead of checkboxes
    Tweak: Display branding only if the license is not activated
    Fix: Date in exchange rate error notice
    Fix: Wrong order status on some failed orders

= 0.10.3 RC = 2015-09-24

    Improved: Price rewriting
    Improved: Add aw-cryptocoin font
    Improved: Display only icons of enabled currencies on checkout
    Improved: CSS on payment page
    Improved: Redux Framework switches instead of checkboxes
    Tweak: Add branding
    Fix: Catch errors in order amount calculation
    Fix: select2 URL error in Redux Framework

= 0.10.2 RC = 2015-09-11

    Improved: Price rewriting
    Improved: Add switch for “Pay Later” button on payment page
    Improved: Update Redux Framework to 3.5.7
    Fix: timing-based doublespend-check causing order failure

= 0.10.1 RC = 2015-08-31

    Improved: Use Ajax for admin backend operations
    Fix: Litecoin exchange rate updates
    Fix: Order timeout on payment page button
    Improved: Move functions to HD Wallet Addon

= 0.10.0 = 2015-08-19

    Improved: Redux framework admin backend
    Improved: Support for all WooCommerce store currencies except Lao KIP
    Fix: Floating point errors
    Fix: update_payment_details() not resetting amounts received in DB

= 0.9.0 = 2015-06-09

    Release Candidate
    Improved: Exchange rate updates

= 0.8.3 = 2015-05-25

    Improved: Payment page & currency switch styling
    Improved: Exchange rate updates
    Improved: Cache rates for display currency switch in WP object cache
    Improved: Create new logfile if file size > 10MB
    Improved: Verify API key and address integrity before creating new order
    Fix: Minor bugs

= 0.8.2 = 2015-04-27

    Improved: Payment page
    Fix: Display currency switch not honouring selected currency
    Fix: Minor bugs

= 0.8.1 = 2015-04-17

    Feature: Use Block.io forwarding addresses as payment address
    Feature: Optional refund address on checkout page
    Feature: Exchange rates from Bitfinex and BTC-e
    Improved: Use the preferred BTC/USD exchange to calculate DOGE/USD and LTC/USD rates
    Improved: Order timeout handling
    Removed auto-withdrawal feature from plugin core
    Improved: Atomic payment processing
    Improved: Shorter cron interval options (30s & 15s)
    Improved: Set max amount for accepting unconfirmed TXs
    Improved: Payment page timeout countdown
    Improved: Moved to Cryptsy API v2
    Improved: Exchange rate error checking and fallback
    Improved: Checkout settings page explanations

= 0.7.3 = 2015-03-04

    Improved: Archive Block.io addresses of completed orders after executing auto-withdrawal to save API resources
    Improved: More explanations on checkout settings page
    Improved: BTC exchange rate sources added: Bitcoinaverage, BitPay
    Improved: Transaction processing of unconfirmed transactions, checking for double-spends
    Improved: Auto-Withdraw from Block.io
    Improved: Log file information
    Fix: API key and address validation

= 0.7.2 = 2015-01-20

    Improved: Use WooCommerce log file directory for event logging to integrate into WooCommerce “Logs” tab (WooCommerce > System Status > Logs)
    Fix: API key validation

= 0.7.1 = 2015-01-17

    First public release

    Improved: switched payment processing API call from get_address_balance to get_transactions, introduced transaction confidence rating for unconfirmed transactions
    Enhanced: Added a second mode for price rewriting
    Feature: Choose between Cryptsy exchange API or Block.io API for all exchange rates used by the plugin.
    Feature: Use the Display Currency Switch template tag to display the switch anywhere in your theme.
    Fix: Prevent payment processing if the order total is not found.
    Modified: QR Codes are now created locally using https://github.com/davidshimjs/qrcodejs
    Modified: Checkout options structure and explanations

= 0.6 =

    First alpha release


