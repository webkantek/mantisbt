# CryptoWoo #

CryptoWoo is a payment gateway for WooCommerce that enables merchants to accept Bitcoin, Litecoin and Dogecoin as payment currency in their WooCommerce store. It does not require a full node to operate and instead utilizes the free Block.io API & Wallet service, a fast and reliable online wallet for digital currencies.

Features:

*         Bitcoin (BTC), Litecoin (LTC), Dogecoin (DOGE) and Dogecoin Testnet (DOGETEST) support
*         Single Product and Shopping Cart price conversion widget
*         Optional payment completion at zero confirmations (faster payment process).
*         WooCommerce order overview integration
*         No full coin node required


### How do I get set up? ###

* Install the plugin via the Wordpress plugin installation routine
* Configuration: Enter your Block.io API keys and enable the plugin
* Dependencies: Wordpress, WooCommerce
* Database configuration
* How to run tests
* Deployment instructions


### Payment processing flowchart ###


![Atomic-Processing.jpg](https://bitbucket.org/repo/gr4a68/images/3490414642-Atomic-Processing.jpg)  


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* felix@cryptowoo.com