<?php
/**
 * Main class
 *
 * @author Yithemes
 * @package YITH WooCommerce Waiting List
 * @version 1.0.0
 */


if ( ! defined( 'YITH_WCWTL' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YITH_WCWTL' ) ) {
	/**
	 * YITH WooCommerce Waiting List
	 *
	 * @since 1.0.0
	 */
	class YITH_WCWTL {

		/**
		 * Single instance of the class
		 *
		 * @var \YITH_WCWTL
		 * @since 1.0.0
		 */
		protected static $instance;

		/**
		 * Plugin version
		 *
		 * @var string
		 * @since 1.0.0
		 */
		public $version = YITH_WCWTL_VERSION;


		/**
		 * Returns single instance of the class
		 *
		 * @return \YITH_WCWTL
		 * @since 1.0.0
		 */
		public static function get_instance(){
			if( is_null( self::$instance ) ){
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * @since 1.0.0
		 */
		public function __construct() {

			$enable = get_option( 'yith-wcwtl-enable' ) == 'yes';

			// Load Plugin Framework
			add_action( 'after_setup_theme', array( $this, 'plugin_fw_loader' ), 1 );

			// Class admin
			if ( $this->is_admin() ) {

			    // required class
                include_once( 'class.yith-wcwtl-admin.php' );
                include_once( 'class.yith-wcwtl-admin-premium.php' );
                include_once( 'class.yith-wcwtl-meta.php' );

				YITH_WCWTL_Admin_Premium();
				// add meta in product edit page
				if( $enable ) {
					YITH_WCWTL_Meta();
				}

				// compatibility with multi vendor
				$enabled_for_vendor = get_option( 'yith_wpv_vendors_option_waiting_list_management', 'no' ) == 'yes';
				if( defined( 'YITH_WPV_PREMIUM' ) && YITH_WPV_PREMIUM && $enabled_for_vendor ){
                    // required class
                    include_once( 'compatibility/yith-woocommerce-product-vendors.php' );
					YITH_WCWTL_Multivendor();
				}
			}
			elseif( $enable && $this->load_frontend() ) {

			    // required class
                include_once( 'class.yith-wcwtl-frontend.php' );

				// Class frontend
				YITH_WCWTL_Frontend();
			}

			// Email actions
			add_filter( 'woocommerce_email_classes', array( $this, 'add_woocommerce_emails' ) );
			add_action( 'woocommerce_init', array( $this, 'load_wc_mailer' ) );

			if( yith_waitlist_is_wc26() ) {
				add_action( 'init', array( $this, 'add_endpoints' ), 1 );
			}

			// mailout on status change
			if( $enable && get_option( 'yith-wcwtl-auto-mailout' ) == 'yes' ) {
				if ( version_compare( WC()->version, '3.0.0', '<' ) ) {
					// back compatibility
					add_action( 'update_postmeta', array( $this, 'mailout_on_status_change_old' ), 10, 4 );
				} else {
					add_action( 'woocommerce_product_set_stock_status', array( $this, 'mailout_on_status_change' ), 10, 3 );
					add_action( 'woocommerce_variation_set_stock_status', array( $this, 'mailout_on_status_change' ), 10, 3 );
				}
			}
		}

		/**
		 * Load Plugin Framework
		 *
		 * @since  1.0
		 * @access public
		 * @return void
		 * @author Andrea Grillo <andrea.grillo@yithemes.com>
		 */
		public function plugin_fw_loader() {
            if ( ! defined( 'YIT_CORE_PLUGIN' ) ) {
                global $plugin_fw_data;
                if( ! empty( $plugin_fw_data ) ){
                    $plugin_fw_file = array_shift( $plugin_fw_data );
                    require_once( $plugin_fw_file );
                }
            }
		}

        /**
         * Check if is admin
         *
         * @since 1.1.0
         * @access public
         * @author Francesco Licandro
         * @return boolean
         */
        public function is_admin(){
            $context_check = isset( $_REQUEST['context'] ) && $_REQUEST['context'] == 'frontend';
            $actions_to_check = apply_filters( 'yith_wcwtl_actions_to_check_admin', array(
                'jckqv'
            ) );
            $action_check  = isset( $_REQUEST['action'] ) && in_array( $_REQUEST['action'], $actions_to_check );
            $is_admin = is_admin() && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX && ( $context_check || $action_check ) );
	        
	        return apply_filters( 'yith_wcwtl_check_is_admin', $is_admin );
        }

		/**
		 * Check to load frontend class
		 *
		 * @since 1.2.0
		 * @author Francesco Licandro
		 * @return boolean
		 */
		public function load_frontend(){
			return apply_filters( 'yith_wcwtl_check_load_frontend', get_option( 'yith-wcwtl-enable' ) == 'yes' );
		}

		/**
		 * Filters woocommerce available mails, to add waitlist related ones
		 *
		 * @param $emails array
		 *
		 * @return array
		 * @since 1.0
		 */
		public function add_woocommerce_emails( $emails ) {
			$emails['YITH_WCWTL_Mail_Instock'] = include( 'email/class.yith-wcwtl-mail-instock.php' );
			$emails['YITH_WCWTL_Mail_Subscribe'] = include( 'email/class.yith-wcwtl-mail-subscribe.php' );
			return $emails;
		}

		/**
		 * Loads WC Mailer when needed
		 *
		 * @return void
		 * @since 1.0
		 * @author Francesco Licandro <francesco.licandro@yithemes.it>
		 */
		public function load_wc_mailer() {
			add_action( 'send_yith_waitlist_mail_instock', array( 'WC_Emails', 'send_transactional_email' ), 10, 2 );
			add_action( 'send_yith_waitlist_mail_subscribe', array( 'WC_Emails', 'send_transactional_email' ), 10, 2 );
		}

		/**
		 * Add waiting list account endpoints for WC 2.6
		 *
		 * @author Francesco Licandro
		 * @access public
		 * @since 1.1.2
		 */
		public function add_endpoints(){
			WC()->query->query_vars['waiting-list'] = get_option( 'woocommerce_myaccount_waiting_list_endpoint', 'waiting-list' );

			$do_flush = get_option( 'yith-waitlist-flush-rewrite-rules', 1 );

			if( $do_flush ) {
				// change option
				update_option( 'yith-waitlist-flush-rewrite-rules', 0 );
				// the flush rewrite rules
				flush_rewrite_rules();
			}
		}

		/**
		 * Send mail to users in waitlist for product when pass from 'out of stock' status to 'in stock'
		 *
		 * @access public
		 * @since 1.0.0
		 * @param integer $product_id The product ID
		 * @param string $stock_status The new product stock status
		 * @param object $product The product object
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 */
		public function mailout_on_status_change( $product_id, $stock_status, $product ) {

			if( $stock_status != 'instock' ) {
				return;
			}
			
			// get waitlist users for product
			$users = yith_waitlist_get_registered_users( $product );
			// if there are users and product in not in excluded list
			if( ! empty( $users ) && ! yith_waitlist_is_excluded( $product ) ) {
				// send mail
				do_action( 'send_yith_waitlist_mail_instock', $users, $product_id );
			}

			if( class_exists('YITH_WCWTL_Admin_Premium') ) 
				add_filter( 'redirect_post_location', array( YITH_WCWTL_Admin_Premium(), 'add_query_to_redirect_location' ), 20, 2 );

			$response = apply_filters( 'yith_wcwtl_mail_instock_send_response', false );

			if( get_option( 'yith-wcwtl-keep-after-email' ) !== 'yes' && $response ) {
				// empty waitlist
				yith_waitlist_empty( $product );
			}
		}

		/**
		 * Send mail to users in waitlist for product when pass from 'out of stock' status to 'in stock'
		 * This is for WooCommerce version older then 3.0.0
		 *
		 * @access public
		 * @since 1.0.0
		 * @param integer $meta_id
		 * @param integer $object_id
		 * @param string $meta_key
		 * @param mixed $meta_value
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 */
		public function mailout_on_status_change_old( $meta_id, $object_id, $meta_key, $meta_value ) {

			if( $meta_key != '_stock_status' || ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) ) {
				return;
			}
			
			$product = wc_get_product( $object_id );
			$old_value = yit_get_prop( $product, $meta_key, true );

			// return if new value is the same as old or new value is not instock
			if( $old_value === $meta_value ) {
				return;
			}

			$this->mailout_on_status_change( $object_id, $meta_value, $product );
		}
	}
}

/**
 * Unique access to instance of YITH_WCWTL class
 *
 * @return \YITH_WCWTL
 * @since 1.0.0
 */
function YITH_WCWTL(){
	return YITH_WCWTL::get_instance();
}