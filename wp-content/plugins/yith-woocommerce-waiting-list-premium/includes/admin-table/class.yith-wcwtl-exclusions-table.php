<?php
if ( ! defined( 'YITH_WCWTL' ) ) {
	exit; // Exit if accessed directly
}

if( ! class_exists( 'YITH_WCWTL_Exclusions_Table' ) ) {
	/**
	 * Exclusion table
	 *
	 * @class   YITH_WCWTL_Exclusions_Table
	 * @package YITH Woocommerce Waiting List
	 * @since   1.0.0
	 * @author  Yithemes
	 *
	 */
	class YITH_WCWTL_Exclusions_Table {
		/**
		 * Constructor
		 *
		 * @access public
		 * @since 1.0.0
		 */
		public function __construct() {}

		/**
		 * Outputs the exclusions table template with insert form in plugin options panel
		 *
		 * @since   1.0.0
		 * @author  Francesco Licandro <francesco.licandro@yithemes.com>
		 * @return  string
		 */
		public function prepare_table() {

			global $wpdb;

			$table = new YITH_WCWTL_Custom_Table( array(
				'singular' => __( 'product', 'yith-woocommerce-waiting-list' ),
				'plural'   => __( 'products', 'yith-woocommerce-waiting-list' )
			) );

			$table->options = array(
				'select_table'     => "{$wpdb->prefix}posts p INNER JOIN {$wpdb->prefix}postmeta pm",
				'select_columns'   => array(
					'pm.post_id'
				),
				'select_where'     => apply_filters( 'yith-wcwtl-exclusionstable-where', 'p.ID = pm.post_id AND pm.meta_key = "' . YITH_WCWTL_META_EXCLUDE . '" AND pm.meta_value = "1"' ),
				'select_group'     => 'pm.post_id',
				'select_order'     => 'pm.post_id',
				'select_limit'     => 10,
				'count_table'      => '( SELECT COUNT(*) FROM ' . $wpdb->prefix . 'postmeta pm WHERE pm.meta_key = "' . YITH_WCWTL_META_EXCLUDE . '" AND pm.meta_value="1" GROUP BY pm.post_id ) AS count_table',
				'key_column'       => 'post_id',
				'view_columns'     => array(
					'cb'      => '<input type="checkbox" />',
					'product'   => __( 'Product', 'yith-woocommerce-waiting-list' ),
					'variation' => __( 'Variation', 'yith-woocommerce-waiting-list' ),
					'thumb'     => __( 'Thumbnail', 'yith-woocommerce-waiting-list' ),
					'actions'    => __( 'Action', 'yith-woocommerce-waiting-list' )
				),
				'hidden_columns'   => array(),
				'sortable_columns' => array(
					'product' => array( 'p.post_title', true )
				),
				'custom_columns'   => array(
					'column_product' => function ( $item, $me, $product ) {
						/**
						 * @type $product WC_Product
						 */
						$product_query_args = array(
							'post'   => yit_get_base_product_id( $product ),
							'action' => 'edit'
						);
						$product_url        = add_query_arg( $product_query_args, admin_url( 'post.php' ) );

						return sprintf( '<strong><a class="tips" target="_blank" href="%s" data-tip="%s">%s</a></strong>', esc_url( $product_url ), __( 'Edit product', 'yith-woocommerce-waiting-list' ), $product->get_title() );
					},
					'column_variation' => function( $item, $me, $product ) {
						/**
						 * @type $product WC_Product
						 */
						if( $product->is_type( 'variation' ) ) {

							$variations = $product->get_variation_attributes();

							$html = '<ul>';

							foreach( $variations as $key => $value ) {
								$key = ucfirst( str_replace( 'attribute_pa_' , '', $key ) );
								$html .= '<li>' . $key . ': ' . $value . '</li>';
							}

							$html .= '</ul>';

							echo $html;
						}
						else {
							echo '-';
						}

					},
					'column_thumb'   => function ( $item, $me, $product ) {
						/**
						 * @type $product WC_Product
						 */
						return $product->get_image( 'shop_thumbnail' );
					},
					'column_actions' => function( $item, $me, $product ) {

						$delete_query_args = array(
							'page'   => $_GET['page'],
							'tab'    => $_GET['tab'],
							'action' => 'delete',
							'id'     => $item['post_id']
						);
						$delete_url        = add_query_arg( $delete_query_args, admin_url( 'admin.php' ) );

						return '<a href="' . esc_url( $delete_url ) . '" class="button">' . __( 'Remove', 'yith-woocommerce-waiting-list' ) . '</a>';

					}
				),
				'bulk_actions'     => array(
					'actions'   => array(
						'delete' => __( 'Remove from list', 'yith-woocommerce-waiting-list' )
					)
				),
			);

			return $table;
		}
	}
}