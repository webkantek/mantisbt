﻿=== YITH WooCommerce Subscription  ===

Contributors: yithemes
Tags: checkout page, recurring billing, subscription billing, subscription box, Subscription Management, subscriptions, paypal subscriptions
Requires at least: 4.0
Tested up to: 4.8.3
Stable tag: 1.2.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==
It allows you to manage recurring payments for product subscription that grant you constant periodical income


== Installation ==
Important: First of all, you have to download and activate WooCommerce plugin, which is mandatory for YITH WooCommerce Subscription to be working.

1. Unzip the downloaded zip file.
2. Upload the plugin folder into the `wp-content/plugins/` directory of your WordPress site.
3. Activate `YITH WooCommerce Subscription` from Plugins page.


= Configuration =
YITH WooCommerce Subscription will add a new tab called "Subscription" in "YIT Plugins" menu item.
There, you will find all Yithemes plugins with quick access to plugin setting page.



== Changelog ==

= 1.2.9 - Released on Nov 15, 2017 =
Fix: Javascript error in single product page
Fix: WooCommerce Coupon when a subscription is on cart

= 1.2.8 - Released on Nov 7, 2017 =
New: Support to WooCommerce 3.2.3
Fix: Discount calculation when a Custom Coupon is applied
Fix: Shipping Calculation for renew orders


= 1.2.7 - Released on Oct 17, 2017 =
New: Support to WooCommerce 3.2.1
Fix: Label on subscription product price

= 1.2.6 - Released on Oct 12, 2017 =
New: Support to WooCommerce 3.2.0 Rc2
New: German translation
New: Added option ''Disable the reduction of stock in the renew order'
Dev: Filter 'ywsbs_price_check'
Dev: Added a check on content cart item key after order processed
Fix: Order item prices with YITH WooCommerce Product Add-Ons
Fix: Prettyphoto.css removed font rules
Fix: Subscription Status localization
Fix: Removed the vendor taxonomy box in subscription CTP page
Fix: Multiple coupons on cart

= 1.2.5 - Released on Aug 19, 2017 =
Fix: Create renew order manually
Fix: changed plain text to html Subscription Status email

= 1.2.4 - Released on Aug 16, 2017 =
New: Support to WooCommerce 3.1.0
Fix: Renew order shipping rate
Fix: Compatibility with YITH WooCommerce Product Add-ons Premium 1.2.6
Fix: Trial status after order complete
Fix: Variation subscription price
Fix: customer search on subscriptions list table
Fix: wrong text domain in string
Fix: subscription meta containing one subscription only
Fix: Misspelled strings
Dev: Added hook 'ywsbs_get_recurring_totals'
Update: Update Core Framework

= 1.2.3 - Released on May 26, 2017 =
Fix: Check on ipn_track_id due to Paypal issue
Fix: Renew order fix

= 1.2.2 - Released on May 05, 2017 =
Fix: Fix renew shipping costs

= 1.2.1 - Released on Apr 28, 2017 =
New: Support to WooCommerce 3.0.4
New: Set customer notes in the renew from parent order
Fix: Custom billing and shipping address in the renew order
Fix: Compatibility with php 5.4
Dev: Changed endpoint hook
Dev: Changed start time of cron jobs
Update: Plugin Core

= 1.2.0 - Released on Mar 31, 2017 =
New: Support to WooCommerce 3.0 RC 2
Fix: Subtotal price on cart
Dev: Added 'ywsbs_renew_subscription' action
Update: Plugin Core

= 1.1.7 - Released on Dec 19, 2016 =
Added: A new method of class 'YITH_WC_Subscription' that return the list of user's subscription
Added: Compatibility with YITH WooCommerce Product Add-ons Premium 1.2.0.8
Added: Support to WordPress 4.7
Added: Date picker in the metabox of subscription info in the backend
Added: Filter 'ywsbs_order_formatted_line_subtotal'
Tweak: Price with taxes in the subscription related to an order
Fixed: The switch of subscriptions from the free to premium version
Fixed: "Max duration of pauses days" isn't saved properly
Fixed: Issues when the order of a subscription is deleted

= 1.1.6 - Released on Oct 04, 2016 =
Added: Filter 'ywsbs_trigger_email_before' to change the time to send the email before the expiration
Added: Italian translation
Added: Spanish translation
Fixed: Coupons behavior when there's a trial period
Fixed: String localization
Updated: Plugin framework


= 1.1.5 - Released on Juy 20, 2016 =
Added: Action to create a renewal order in subscription administrator details
Tweak: Failed register payments
Fixed: Localization strings for trial period
Fixed: Activity log timestamp


= 1.1.4 - Released on Jun 13, 2016 =
Added: hook for actions on my subscriptions table
Added: Support to WooCommerce 2.6 RC1
Updated: Plugin framework

= 1.1.3 =
Added: Option Delete Subscription is extend also if the main order is deleted
Added: Email to customer when a payment fails
Tweak: Changed method to retrieve billing and shipping info of a subscription
Tweak: Customer info in the email
Fixed: Method is add_params_to_available_variation() and can_be_cancelled()
Fixed: Downgrade/upgrade variations


= 1.1.2 =
Fixed: Paypal IPN Request Fix when the renew order is not present
Fixed: Minor bugs


= 1.1.1 =
Fixed: Few missing and unknown textdomains
Fixed: Minor bugs

= 1.1.0 =
Added: Compatibility with YITH WooCommerce Stripe
Added: Paypal IPN validation amounts
Added: In On-hold orders list failed attempts
Added: Failed Attempts in Subscription list
Added: Enabled possibility to move in Trash or Delete subscriptions
Added: Options Overdue pending payment subscriptions after x hour(s)
Added: Suspend pending payment subscriptions after x hour(s)
Added: Option to Suspend a subscription if a recurring payment fail
Added: In Administrator Subscription Detail added the action "Active Subscription"
Added: In Administrator Subscription Detail added the action "Suspend Subscription"
Added: In Administrator Subscription Detail added the action "Overdue Subscription"
Added: In Administrator Subscription Detail added the action "Cancel Subscription"
Added: In Administrator Subscription Detail added the action "Cancel Subscription Now"
Added: In Administrator Subscriptions List Table added the views of subscription status
Added: Search box in Administrator Subscriptions List Table to search for ID or Product Name
Added: Option Delete subscription if the main order is cancelled
Update: Dates in subscription details
Fixed: Admin can't add subscription if YITH WooCommerce Multi Vendor is enabled
Fixed: Start date if a Paypal Payment Method is 'echeck'
Fixed: Localization issue

= 1.0.1 =
Added: Compatibility with Wordpress 4.4
Added: Compatibility with WooCommerce 2.5 beta 3
Updated: Plugin framework
Fixed: Minor bugs

= 1.0.0 =
Initial release

== Suggestions ==
If you have any suggestions concerning how to improve YITH WooCommerce Subscription, you can [write to us](mailto:plugins@yithemes.com "Your Inspiration Themes"), so that we can improve YITH WooCommerce Subscription.

== Upgrade notice ==
= 1.0.0 =
Initial release