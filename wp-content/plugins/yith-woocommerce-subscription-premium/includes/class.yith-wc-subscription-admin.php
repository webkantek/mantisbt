<?php

if ( ! defined( 'ABSPATH' ) || ! defined( 'YITH_YWSBS_VERSION' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Implements admin features of YITH WooCommerce Subscription
 *
 * @class   YITH_WC_Subscription_Admin
 * @package YITH WooCommerce Subscription
 * @since   1.0.0
 * @author  Yithemes
 */
if ( ! class_exists( 'YITH_WC_Subscription_Admin' ) ) {

	class YITH_WC_Subscription_Admin {

		/**
		 * Single instance of the class
		 *
		 * @var \YITH_WC_Subscription_Admin
		 */

		protected static $instance;

		/**
		 * @var $_panel Panel Object
		 */
		protected $_panel;

		/**
		 * @var $_premium string Premium tab template file name
		 */
		protected $_premium = 'premium.php';

		/**
		 * @var string Premium version landing link
		 */
		protected $_premium_landing = 'http://yithemes.com/themes/plugins/yith-woocommerce-subscription/';

		/**
		 * @var string Panel page
		 */
		protected $_panel_page = 'yith_woocommerce_subscription';

		/**
		 * @var string Doc Url
		 */
		public $doc_url = 'https://yithemes.com/docs-plugins/yith-woocommerce-subscription/';

		/**
		 * @var YITH_YWSBS_Subscriptions_List_Table
		 */
		public $cpt_obj_subscriptions;

		/**
		 * @var YITH_YWSBS_Activities_List_Table
		 */
		public $cpt_obj_activities;

		/**
		 * Returns single instance of the class
		 *
		 * @return \YITH_WC_Subscription_Admin
		 * @since 1.0.0
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		public static function get_instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * Initialize plugin and registers actions and filters to be used
		 *
		 * @since  1.0.0
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		public function __construct() {

			$this->create_menu_items();

			//Add action links.
			add_filter( 'plugin_action_links_' . plugin_basename( YITH_YWSBS_DIR . '/' . basename( YITH_YWSBS_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 4 );

			//Custom styles and javascripts.
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_styles_scripts' ), 11 );

			//Product editor.
			add_filter( 'product_type_options', array( $this, 'add_type_options' ) );
			add_action( 'woocommerce_variation_options', array( $this, 'add_type_variation_options' ), 10, 3 );

			//Custom fields for single product.
			add_action( 'woocommerce_product_options_general_product_data', array( $this, 'add_custom_fields_for_single_products' ) );
			add_action( 'woocommerce_process_product_meta', array( $this, 'save_custom_fields_for_single_products' ), 10, 2 );

			//Custom fields for variation.
			add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'add_custom_fields_for_variation_products' ), 14, 3 );
			add_action( 'woocommerce_save_product_variation', array( $this, 'save_custom_fields_for_variation_products' ), 10 );

			add_action( 'admin_init', array( $this, 'change_url_to_sendback' ), 10 );

			add_filter( 'manage_shop_order_posts_columns', array( $this, 'add_order_post_columns' ), 20 );
			add_action( 'manage_shop_order_posts_custom_column', array( $this, 'add_order_posts_custom_column' ) );
		}

		/**
		 * Called by hook admin_init to change the send back link
		 *
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		public function change_url_to_sendback() {
			global $pagenow;

			if ( $pagenow == 'edit.php' && isset( $_GET['post_type'] ) && $_GET['post_type'] == 'ywsbs_subscription' ) {
				wp_safe_redirect( admin_url( 'admin.php?page=yith_woocommerce_subscription' ) );
				exit;
			}
		}

		/**
		 * Add a product type option in single product editor
		 *
		 * @access public
		 *
		 * @param $types
		 *
		 * @return array
		 * @since 1.0.0
		 *
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		public function add_type_options( $types ) {
			$types['ywsbs_subscription'] = array(
				'id'            => '_ywsbs_subscription',
				'wrapper_class' => 'show_if_simple',
				'label'         => __( 'Subscription', 'yith-woocommerce-subscription' ),
				'description'   => __( 'Create a subscription for this product', 'yith-woocommerce-subscription' ),
				'default'       => 'no'
			);

			return $types;
		}

		/**
		 * Add a product type option in variable product editor
		 *
		 * @access public
		 *
		 * @param $loop
		 * @param $variation_data
		 * @param $variation
		 *
		 * @return array
		 * @since 1.0.0
		 *
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		public function add_type_variation_options( $loop, $variation_data, $variation ) {

			$is_subscription = yit_get_prop( $variation, '_ywsbs_subscription' );
			$checked         = checked( $is_subscription, 'yes', false );
			echo '<label><input type="checkbox" class="checkbox variable_ywsbs_subscription" name="variable_ywsbs_subscription[' . $loop . ']" ' . $checked . ' /> ' . __( 'Subscription', 'yith-woocommerce-subscription' ) . ' <a class="tips" data-tip="' . __( 'Sell this variable product as a subscription product.', 'yith-woocommerce-subscription' ) . '" href="#">[?]</a></label>';

		}

		/**
		 * Add custom fields for single product
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		public function add_custom_fields_for_single_products() {

			global $thepostid;

			$product = wc_get_product( $thepostid );

			$_ywsbs_price_is_per      = yit_get_prop( $product, '_ywsbs_price_is_per' );
			$_ywsbs_price_time_option = yit_get_prop( $product, '_ywsbs_price_time_option' );

			$_ywsbs_trial_per         = yit_get_prop( $product, '_ywsbs_trial_per' );
			$_ywsbs_trial_time_option = yit_get_prop( $product, '_ywsbs_trial_time_option' );

			$_ywsbs_fee        = yit_get_prop( $product, '_ywsbs_fee' );
			$_ywsbs_max_length = yit_get_prop( $product, '_ywsbs_max_length' );

			$_ywsbs_max_pause          = yit_get_prop( $product, '_ywsbs_max_pause' );
			$_ywsbs_max_pause_duration = yit_get_prop( $product, '_ywsbs_max_pause_duration' );

			$max_lengths = ywsbs_get_max_length_period();

			?>
            <h3 class="ywsbs_price_is_per"><?php _e( 'Subscription Settings', 'yith-woocommerce-subscription' ) ?></h3>
            <div class="options_group show_if_simple">
                <p class="form-field ywsbs_price_is_per">
                    <label for="_ywsbs_price_is_per"><?php _e( 'Price is per', 'yith-woocommerce-subscription' ); ?></label>
                    <input type="text" class="short" name="_ywsbs_price_is_per" id="_ywsbs_price_is_per"
                           value="<?php echo esc_attr( $_ywsbs_price_is_per ); ?>" style="float: left; width:15%;"/>
                    <select id="_ywsbs_price_time_option" name="_ywsbs_price_time_option" class="select"
                            style="margin-left: 3px;">
						<?php foreach ( ywsbs_get_time_options() as $key => $value ):
							$select = selected( $_ywsbs_price_time_option, $key, false );
							echo '<option value="' . $key . '" ' . $select . ' data-max="' . $max_lengths[ $key ] . '">' . $value . '</option>';
						endforeach;
						?>
                    </select>
                </p>

                <p class="form-field ywsbs_max_length">
                    <label for="_ywsbs_max_length"><?php _e( 'Max Lenght:', 'yith-woocommerce-subscription' ); ?>
                        <a href="#" class="tips"
                           data-tip="<?php _e( 'Leave it empty for unlimited subscription', 'yith-woocommerce-subscription' ) ?>">
                            [?]</a></label>
                    <input type="text" class="short" name="_ywsbs_max_length" id="_ywsbs_max_length"
                           value="<?php echo esc_attr( $_ywsbs_max_length ); ?>" style="float: left; width:15%; "/>
                    <span class="description"><span><?php echo $time_opt = ( $_ywsbs_price_time_option ) ? $_ywsbs_price_time_option : __( 'days', 'yith-woocommerce-subscription' ); ?></span> <?php printf( __( '(Max: <span class="max-l">%d</span>)', 'yith-woocommerce-subscription' ), $max_lengths[ $time_opt ] ) ?></span>
                </p>

                <p class="form-field ywsbs_trial_per">
                    <label for="_ywsbs_trial_per"><?php _e( 'Trial period', 'yith-woocommerce-subscription' ); ?></label>
                    <input type="text" class="short" name="_ywsbs_trial_per" id="_ywsbs_trial_per"
                           value="<?php echo esc_attr( $_ywsbs_trial_per ); ?>" style="float: left; width:15%; "/>
                    <select id="_ywsbs_trial_time_option" name="_ywsbs_trial_time_option" class="select"
                            style="margin-left: 3px;">
						<?php foreach ( ywsbs_get_time_options() as $key => $value ):
							$select = selected( $_ywsbs_trial_time_option, $key );
							echo '<option value="' . $key . '" ' . $select . '>' . $value . '</option>';
						endforeach;
						?>
                    </select>
                </p>

                <p class="form-field ywsbs_fee">
                    <label for="_ywsbs_fee"><?php printf( __( 'Sign-up fee (%s)', 'yith-woocommerce-subscription' ), get_woocommerce_currency_symbol() ); ?></label>
                    <input type="text" class="short" size="5" name="_ywsbs_fee"
                           value="<?php echo esc_attr( $_ywsbs_fee ); ?>" style="float: left; width:15%; "/>
                </p>

                <p class="form-field ywsbs_pause">
                    <label><?php _e( 'Max Number of pauses', 'yith-woocommerce-subscription' ) ?><a href="#"
                                                                                                    class="tips"
                                                                                                    data-tip="<?php _e( 'Leave empty if you do not want to allow pauses', 'yith-woocommerce-subscription' ) ?>">
                            [?]</a></label>
                    <input type="text" class="short" name="_ywsbs_max_pause" id="_ywsbs_max_pause"
                           value="<?php echo esc_attr( $_ywsbs_max_pause ); ?>" style="float: left; width:15%;"/>
                </p>

                <p class="form-field ywsbs_pause">
                    <label for="_ywsbs_max_pause"><?php _e( 'Max duration of pauses', 'yith-woocommerce-subscription' ) ?></label>
                    <input type="text" class="short" size="5" name="_ywsbs_max_pause_duration"
                           value="<?php echo esc_attr( $_ywsbs_max_pause_duration ); ?>"
                           style="float: left; width:15%; "/>
                    <span class="description"><?php _e( 'days', 'yith-woocommerce-subscription' ) ?></span>
                </p>

            </div>

			<?php

		}

		/**
		 * Save custom fields for single product
		 *
		 * @param $post_id
		 * @param $post
		 *
		 * @return void
		 * @since   1.0.0
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		public function save_custom_fields_for_single_products( $post_id, $post ) {

			if ( isset( $_POST['product-type'] ) && $_POST['product-type'] == 'variable' ) {
				$this->reset_custom_field_for_product( $post_id );

				return;
			}

			$product = wc_get_product( $post_id );
			$args    = array();
			//reset custom field for the parent product
			if ( isset( $_POST['_ywsbs_subscription'] ) ) {
				$args['_ywsbs_subscription'] = 'yes';
			} else {
				$args['_ywsbs_subscription'] = 'no';
			}

			if ( isset( $_POST['_ywsbs_price_is_per'] ) ) {
				$args['_ywsbs_price_is_per'] = $_POST['_ywsbs_price_is_per'];
			}

			if ( isset( $_POST['_ywsbs_price_time_option'] ) ) {
				$args['_ywsbs_price_time_option'] = $_POST['_ywsbs_price_time_option'];
			}

			if ( isset( $_POST['_ywsbs_fee'] ) ) {
				$args['_ywsbs_fee'] = $_POST['_ywsbs_fee'];
			}

			if ( isset( $_POST['_ywsbs_trial_per'] ) ) {
				$args['_ywsbs_trial_per'] = $_POST['_ywsbs_trial_per'];
			}

			if ( isset( $_POST['_ywsbs_trial_time_option'] ) ) {
				$args['_ywsbs_trial_time_option'] = $_POST['_ywsbs_trial_time_option'];
			}

			if ( isset( $_POST['_ywsbs_price_time_option'] ) && isset( $_POST['_ywsbs_max_length'] ) ) {
				$max_length                = ywsbs_validate_max_length( $_POST['_ywsbs_max_length'], $_POST['_ywsbs_price_time_option'] );
				$args['_ywsbs_max_length'] = $max_length;
			}

			if ( isset( $_POST['_ywsbs_max_pause'] ) ) {
				$args['_ywsbs_max_pause'] = $_POST['_ywsbs_max_pause'];
			}

			if ( isset( $_POST['_ywsbs_max_pause_duration'] ) ) {
				$args['_ywsbs_max_pause_duration'] = $_POST['_ywsbs_max_pause_duration'];
			}

			if ( $args ) {
				yit_save_prop( $product, $args, false, true );
			}

		}

		/**
		 * Add custom fields for variation products
		 *
		 * @param $loop
		 * @param $variation_data
		 * @param $variation
		 *
		 * @since   1.0.0
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		public function add_custom_fields_for_variation_products( $loop, $variation_data, $variation ) {

			$_ywsbs_price_is_per      = yit_get_prop( $variation, '_ywsbs_price_is_per' );
			$_ywsbs_price_time_option = yit_get_prop( $variation, '_ywsbs_price_time_option' );

			$_ywsbs_trial_per         = yit_get_prop( $variation, '_ywsbs_trial_per' );
			$_ywsbs_trial_time_option = yit_get_prop( $variation, '_ywsbs_trial_time_option' );

			$_ywsbs_fee = yit_get_prop( $variation, '_ywsbs_fee' );

			$_ywsbs_max_length = yit_get_prop( $variation, '_ywsbs_max_length' );

			$_ywsbs_max_pause          = yit_get_prop( $variation, '_ywsbs_max_pause' );
			$_ywsbs_max_pause_duration = yit_get_prop( $variation, '_ywsbs_max_pause_duration' );
			$_ywsbs_switchable         = yit_get_prop( $variation, '_ywsbs_switchable' );
			$checked_switchable        = checked( $_ywsbs_switchable, 'yes', false );
			$_ywsbs_prorate_length     = yit_get_prop( $variation, '_ywsbs_prorate_length' );
			$checked_prorate_length    = checked( $_ywsbs_prorate_length, 'yes', false );

			$_ywsbs_gap_payment  = yit_get_prop( $variation, '_ywsbs_gap_payment' );
			$checked_gap_payment = checked( $_ywsbs_gap_payment, 'yes', false );

			$_ywsbs_switchable_priority = yit_get_prop( $variation, '_ywsbs_switchable_priority' );

			$_ywsbs_switchable_priority = ( empty( $_ywsbs_switchable_priority ) && $_ywsbs_switchable == 'yes' ) ? $loop : $_ywsbs_switchable_priority;

			$max_lengths = ywsbs_get_max_length_period();
			?>
            <div class="ywsbs_subscription_variation_products">
                <h3><?php _e( 'Subscription Settings', 'yith-woocommerce-subscription' ) ?></h3>

                <p class="form-row form-row-first variable_ywsbs_price_is_per">
                    <label for="_ywsbs_price_is_per"><?php _e( 'Price is per', 'yith-woocommerce-subscription' ); ?></label>
                    <input type="text" size="5" class="variable_ywsbs_price_is_per"
                           name="variable_ywsbs_price_is_per[<?php echo $loop; ?>]"
                           value="<?php echo esc_attr( $_ywsbs_price_is_per ); ?>"/>
                    <select id="_ywsbs_price_time_option" name="variable_ywsbs_price_time_option[<?php echo $loop; ?>]"
                            class="select" style="margin-left: 3px;">
						<?php foreach ( ywsbs_get_time_options() as $key => $value ):
							$select = selected( $_ywsbs_price_time_option, $key );
							echo '<option value="' . $key . '" ' . $select . ' data-max="' . $max_lengths[ $key ] . '">' . $value . '</option>';
						endforeach;
						?>
                    </select>
                </p>

                <p class="form-row form-row-last variable_ywsbs_max_length">
                    <label for="_ywsbs_max_length"><?php _e( 'Max Lenght:', 'yith-woocommerce-subscription' ); ?><a
                                href="#" class="tips"
                                data-tip="<?php _e( 'Leave empty for unlimited subscription', 'yith-woocommerce-subscription' ) ?>">
                            [?]</a></label>
                    <input type="text" size="5" name="variable_ywsbs_max_length[<?php echo $loop; ?>]"
                           value="<?php echo esc_attr( $_ywsbs_max_length ); ?>"/>
                    <span class="description"><span><?php echo $time_opt = ( $_ywsbs_price_time_option ) ? $_ywsbs_price_time_option : 'days'; ?></span> <?php printf( __( '(Max: <span class="max-l">%d</span>)', 'yith-woocommerce-subscription' ), $max_lengths[ $time_opt ] ) ?></span>
                </p>

                <p class="form-row form-row-first variable_ywsbs_trial_per">
                    <label for="_ywsbs_trial_per"><?php _e( 'Trial period', 'yith-woocommerce-subscription' ); ?></label>
                    <input type="text" class="variable_ywsbs_trial_per"
                           name="variable_ywsbs_trial_per[<?php echo $loop; ?>]"
                           value="<?php echo esc_attr( $_ywsbs_trial_per ); ?>"/>
                    <select id="_ywsbs_trial_time_option" name="variable_ywsbs_trial_time_option[<?php echo $loop; ?>]"
                            class="select" style="margin-left: 3px;">
						<?php foreach ( ywsbs_get_time_options() as $key => $value ):
							$select = selected( $_ywsbs_trial_time_option, $key );
							echo '<option value="' . $key . '" ' . $select . '>' . $value . '</option>';
						endforeach;
						?>
                    </select>
                </p>

                <p class="form-row form-row-last variable_ywsbs_fee">
                    <label for="_ywsbs_fee"><?php printf( __( 'Sign-up fee (%s)', 'yith-woocommerce-subscription' ), get_woocommerce_currency_symbol() ); ?></label>
                    <input type="text" class="short" size="5" name="variable_ywsbs_fee[<?php echo $loop; ?>]"
                           value="<?php echo esc_attr( $_ywsbs_fee ); ?>"/>
                </p>

                <p class="form-row form-row-first variable_ywsbs_pauses">
                    <label><?php _e( 'Max Number of pauses', 'yith-woocommerce-subscription' ) ?><a href="#"
                                                                                                    class="tips"
                                                                                                    data-tip="<?php _e( 'Leave empty if you do not want to allow pauses', 'yith-woocommerce-subscription' ) ?>">
                            [?]</a></label>
                    <input type="text" class="short" size="5" name="variable_ywsbs_max_pause[<?php echo $loop; ?>]"
                           value="<?php echo esc_attr( $_ywsbs_max_pause ); ?>"/>
                </p>

                <p class="form-row form-row-last variable_ywsbs_pauses">
                    <label><?php _e( 'Max duration of pauses', 'yith-woocommerce-subscription' ) ?></label>
                    <input type="text" class="short" size="5"
                           name="variable_ywsbs_max_pause_duration[<?php echo $loop; ?>]"
                           value="<?php echo esc_attr( $_ywsbs_max_pause_duration ); ?>"/>
                    <span class="description"><?php _e( 'days', 'yith-woocommerce-subscription' ) ?></span>
                </p>

                <p class="form-row form-row-first variable_ywsbs_switchable">
                    <label><?php _e( 'Allow switch to this variation', 'yith-woocommerce-subscription' ); ?></label>
                    <input type="checkbox"
                           name="variable_ywsbs_switchable[<?php echo $loop; ?>]" <?php echo $checked_switchable ?> />
                </p>

                <p class="form-row form-row-last variable_ywsbs_switchable">
                    <label><?php _e( 'Priority', 'yith-woocommerce-subscription' ); ?><a href="#" class="tips"
                                                                                         data-tip="<?php _e( 'This field allows you to set a hierarchy in the subscription variations. For example, if you switch from a variation with a lower priority to another with higher priority, the switching process is regarded as an upgrade.', 'yith-woocommerce-subscription' ) ?>">
                            [?]</a></label>
                    <input type="text" class="short" size="5"
                           name="variable_ywsbs_switchable_priority[<?php echo $loop; ?>]"
                           value="<?php echo $_ywsbs_switchable_priority ?>"/>
                </p>

                <p class="form-row form-row-first variable_ywsbs_switchable">
                    <label><?php _e( 'Change duration', 'yith-woocommerce-subscription' ); ?><a href="#" class="tips"
                                                                                                data-tip="<?php _e( 'This field allows you to change the duration of the
                     subscription considering also the time the customer has benefitted of the subscription', 'yith-woocommerce-subscription' ) ?>">
                            [?]</a></label>
                    <input type="checkbox"
                           name="variable_ywsbs_prorate_length[<?php echo $loop; ?>]" <?php echo $checked_prorate_length ?> />
                </p>

                <p class="form-row form-row-last variable_ywsbs_switchable">
                    <label><?php _e( 'Permit catch up payment when upgrading', 'yith-woocommerce-subscription' ); ?><a
                                href="#" class="tips" data-tip="<?php _e( 'This field allows users
                    to catch up with payments of the previous subscription', 'yith-woocommerce-subscription' ) ?>">
                            [?]</a></label>
                    <input type="checkbox"
                           name="variable_ywsbs_gap_payment[<?php echo $loop; ?>]" <?php echo $checked_gap_payment ?> />
                </p>

            </div>
			<?php
		}

		/**
		 * Save custom fields for variation products
		 *
		 * @param $variation_id
		 *
		 * @return void
		 * @since   1.0.0
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		public function save_custom_fields_for_variation_products( $variation_id ) {

			//reset custom field for the parent product
			if ( isset( $_POST['product_id'] ) ) {
				$this->reset_custom_field_for_product( $_POST['product_id'] );
			}

			$variation = wc_get_product( $variation_id );
			$args      = array();
			if ( isset( $_POST['variable_post_id'] ) && ! empty( $_POST['variable_post_id'] ) ) {
				$current_variation_index = array_search( $variation_id, $_POST['variable_post_id'] );
			}

			if ( $current_variation_index === false ) {
				return false;
			}

			if ( isset( $_POST['variable_ywsbs_subscription'][ $current_variation_index ] ) ) {
				$args['_ywsbs_subscription'] = 'yes';
			} else {
				$args['_ywsbs_subscription'] = 'no';
			}

			if ( isset( $_POST['variable_ywsbs_price_is_per'][ $current_variation_index ] ) ) {
				$args['_ywsbs_price_is_per'] = $_POST['variable_ywsbs_price_is_per'][ $current_variation_index ];
			}

			if ( isset( $_POST['variable_ywsbs_price_time_option'][ $current_variation_index ] ) ) {
				$args['_ywsbs_price_time_option'] = $_POST['variable_ywsbs_price_time_option'][ $current_variation_index ];
			}

			if ( isset( $_POST['variable_ywsbs_max_length'][ $current_variation_index ] ) && isset( $_POST['variable_ywsbs_price_time_option'][ $current_variation_index ] ) ) {
				$max_length                = ywsbs_validate_max_length( $_POST['variable_ywsbs_max_length'][ $current_variation_index ], $_POST['variable_ywsbs_price_time_option'][ $current_variation_index ] );
				$args['_ywsbs_max_length'] = $max_length;
			}

			if ( isset( $_POST['variable_ywsbs_fee'][ $current_variation_index ] ) ) {
				$args['_ywsbs_fee'] = $_POST['variable_ywsbs_fee'][ $current_variation_index ];
			}

			if ( isset( $_POST['variable_ywsbs_trial_per'][ $current_variation_index ] ) ) {
				$args['_ywsbs_trial_per'] = $_POST['variable_ywsbs_trial_per'][ $current_variation_index ];
			}

			if ( isset( $_POST['variable_ywsbs_trial_time_option'][ $current_variation_index ] ) ) {
				$args['_ywsbs_trial_time_option'] = $_POST['variable_ywsbs_trial_time_option'][ $current_variation_index ];
			}

			if ( isset( $_POST['variable_ywsbs_switchable'][ $current_variation_index ] ) ) {
				$args['_ywsbs_switchable'] = 'yes';
			} else {
				$args['_ywsbs_switchable'] = 'no';
			}

			if ( isset( $_POST['variable_ywsbs_prorate_length'][ $current_variation_index ] ) ) {
				$args['_ywsbs_prorate_length'] = 'yes';
			} else {
				$args['_ywsbs_prorate_length'] = 'no';
			}

			if ( isset( $_POST['variable_ywsbs_gap_payment'][ $current_variation_index ] ) ) {
				$args['_ywsbs_gap_payment'] = 'yes';
			} else {
				$args['_ywsbs_gap_payment'] = 'no';
			}

			if ( isset( $_POST['variable_ywsbs_switchable_priority'][ $current_variation_index ] ) ) {
				$args['_ywsbs_switchable_priority'] = $_POST['variable_ywsbs_switchable_priority'][ $current_variation_index ];
			}

			if ( isset( $_POST['variable_ywsbs_max_pause'][ $current_variation_index ] ) ) {
				$args['_ywsbs_max_pause'] = $_POST['variable_ywsbs_max_pause'][ $current_variation_index ];
			}

			if ( isset( $_POST['variable_ywsbs_max_pause_duration'][ $current_variation_index ] ) ) {
				$args['_ywsbs_max_pause_duration'] = $_POST['variable_ywsbs_max_pause_duration'][ $current_variation_index ];
			}

			if ( $args ) {
				yit_save_prop( $variation, $args, false, true );
			}
		}

		/**
		 * Reset custom field
		 *
		 * @access public
		 *
		 * @param $product_id
		 *
		 * @return void
		 * @since  1.0.0
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		private function reset_custom_field_for_product( $product_id ) {

			$product       = wc_get_product( $product_id );
			$custom_fields = array(
				'_ywsbs_subscription',
				'_ywsbs_price_is_per',
				'_ywsbs_price_time_option',
				'_ywsbs_max_length',
				'_ywsbs_fee',
				'_ywsbs_trial_per',
				'_ywsbs_trial_time_option',
				'_ywsbs_switchable',
				'_ywsbs_prorate_length',
				'_ywsbs_gap_payment',
				'_ywsbs_switchable_priority',
				'_ywsbs_max_pause',
				'_ywsbs_max_pause_duration'
			);

			foreach ( $custom_fields as $cf ) {
				yit_delete_prop( $product, $cf );
			}
		}


		/**
		 * Enqueue styles and scripts
		 *
		 * @access public
		 * @return void
		 * @since 1.0.0
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		public function enqueue_styles_scripts() {
			wp_enqueue_style( 'yith_ywsbs_backend', YITH_YWSBS_ASSETS_URL . '/css/backend.css', YITH_YWSBS_VERSION );
			wp_enqueue_script( 'yith_ywsbs_admin', YITH_YWSBS_ASSETS_URL . '/js/ywsbs-admin' . YITH_YWSBS_SUFFIX . '.js', array( 'jquery' ), YITH_YWSBS_VERSION, true );
			wp_enqueue_script( 'jquery-blockui', YITH_YWSBS_ASSETS_URL . '/js/jquery.blockUI.min.js', array( 'jquery' ), false, true );
			wp_localize_script( 'yith_ywsbs_admin', 'yith_ywsbs_admin', array(
					'ajaxurl'      => admin_url( 'admin-ajax.php' ),
					'block_loader' => apply_filters( 'yith_ywsbs_block_loader_admin', YITH_YWSBS_ASSETS_URL . '/images/block-loader.gif' ),
				) );
		}

		/**
		 * Create Menu Items
		 *
		 * Print admin menu items
		 *
		 * @since  1.0.0
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		private function create_menu_items() {

			// Add a panel under YITH Plugins tab
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );
			add_action( 'yith_ywsbs_subscriptions_tab', array( $this, 'subscriptions_tab' ) );
			add_action( 'yith_ywsbs_activities_tab', array( $this, 'activities_tab' ) );
			add_action( 'yith_ywsbs_premium_tab', array( $this, 'premium_tab' ) );
		}

		/**
		 * Add a panel under YITH Plugins tab
		 *
		 * @return   void
		 * @since    1.0
		 * @author   Andrea Grillo <andrea.grillo@yithemes.com>
		 * @use      /YIT_Plugin_Panel_WooCommerce class
		 * @see      plugin-fw/lib/yit-plugin-panel.php
		 */
		public function register_panel() {

			if ( ! empty( $this->_panel ) ) {
				return;
			}

			$admin_tabs = array(
				'subscriptions' => __( 'Subscriptions', 'yith-woocommerce-subscription' ),
				'activities'    => __( 'Activities', 'yith-woocommerce-subscription' ),
				'general'       => __( 'Settings', 'yith-woocommerce-subscription' ),
			);

			$admin_tabs = apply_filters( 'ywsbs_register_panel_tabs', $admin_tabs );

			$args = array(
				'create_menu_page' => apply_filters( 'ywsbs_register_panel_create_menu_page', true ),
				'parent_slug'      => '',
				'page_title'       => __( 'Subscription', 'yith-woocommerce-subscription' ),
				'menu_title'       => __( 'Subscription', 'yith-woocommerce-subscription' ),
				'capability'       => apply_filters( 'ywsbs_register_panel_capabilities', 'manage_options' ),
				'parent'           => '',
				'parent_page'      => apply_filters( 'ywsbs_register_panel_parent_page', 'yit_plugin_panel' ),
				'page'             => $this->_panel_page,
				'admin-tabs'       => $admin_tabs,
				'options-path'     => YITH_YWSBS_DIR . '/plugin-options'
			);

			/* === Fixed: not updated theme  === */
			if ( ! class_exists( 'YIT_Plugin_Panel' ) ) {
				require_once( YITH_YWSBS_DIR . '/plugin-fw/lib/yit-plugin-panel.php' );
			}

			if ( ! class_exists( 'YIT_Plugin_Panel_WooCommerce' ) ) {
				require_once( YITH_YWSBS_DIR . '/plugin-fw/lib/yit-plugin-panel-wc.php' );
			}


			$this->_panel = new YIT_Plugin_Panel_WooCommerce( $args );

		}

		/**
		 * Premium Tab Template
		 *
		 * Load the premium tab template on admin page
		 *
		 * @return   void
		 * @since    1.0
		 * @author   Andrea Grillo <andrea.grillo@yithemes.com>
		 */
		public function premium_tab() {
			$premium_tab_template = YITH_YWSBS_TEMPLATE_PATH . '/admin/' . $this->_premium;
			if ( file_exists( $premium_tab_template ) ) {
				include_once( $premium_tab_template );
			}
		}

		/**
		 * Action Links
		 *
		 * add the action links to plugin admin page
		 *
		 * @param $links | links plugin array
		 *
		 * @return   mixed Array
		 * @since    1.0
		 * @author   Andrea Grillo <andrea.grillo@yithemes.com>
		 * @return mixed
		 * @use      plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {

			$links[] = '<a href="' . admin_url( "admin.php?page={$this->_panel_page}" ) . '">' . __( 'Settings', 'yith-woocommerce-subscription' ) . '</a>';
			if ( defined( 'YITH_YWSBS_FREE_INIT' ) ) {
				$links[] = '<a href="' . $this->get_premium_landing_uri() . '" target="_blank">' . __( 'Premium Version', 'yith-woocommerce-subscription' ) . '</a>';
			}

			return $links;
		}


		/**
		 * plugin_row_meta
		 *
		 * add the action links to plugin admin page
		 *
		 * @param $plugin_meta
		 * @param $plugin_file
		 * @param $plugin_data
		 * @param $status
		 *
		 * @return   Array
		 * @since    1.0
		 * @author   Andrea Grillo <andrea.grillo@yithemes.com>
		 * @use      plugin_row_meta
		 */

		public function plugin_row_meta( $plugin_meta, $plugin_file, $plugin_data, $status ) {

			if ( defined( 'YITH_YWSBS_INIT' ) && YITH_YWSBS_INIT == $plugin_file ) {
				$plugin_meta[] = '<a href="' . $this->doc_url . '" target="_blank">' . __( 'Plugin Documentation', 'yith-woocommerce-subscription' ) . '</a>';
			}

			return $plugin_meta;
		}


		/**
		 * Get the premium landing uri
		 *
		 * @since   1.0.0
		 * @author  Andrea Grillo <andrea.grillo@yithemes.com>
		 * @return  string The premium landing link
		 */
		public function get_premium_landing_uri() {
			return defined( 'YITH_REFER_ID' ) ? $this->_premium_landing . '?refer_id=' . YITH_REFER_ID : $this->_premium_landing . '?refer_id=1030585';
		}

		/**
		 * Subscriptions List Table
		 *
		 * Load the subscriptions on admin page
		 *
		 * @return   void
		 * @since    1.0
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		public function subscriptions_tab() {
			$this->cpt_obj_subscriptions = new YITH_YWSBS_Subscriptions_List_Table();

			$subscriptions_tab = YITH_YWSBS_TEMPLATE_PATH . '/admin/subscriptions-tab.php';

			if ( file_exists( $subscriptions_tab ) ) {
				include_once( $subscriptions_tab );
			}
		}

		/**
		 * Activities List Table
		 *
		 * Load the activites on admin page
		 *
		 * @return   void
		 * @since    1.0.0
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		public function activities_tab() {
			$this->cpt_obj_activities = new YITH_YWSBS_Activities_List_Table();

			$activities_tab = YITH_YWSBS_TEMPLATE_PATH . '/admin/activities-tab.php';

			if ( file_exists( $activities_tab ) ) {
				include_once( $activities_tab );
			}
		}


		/**
		 * @param $columns
		 *
		 * @return mixed
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		function add_order_post_columns( $columns ) {
			if ( ! isset( $_GET['post_status'] ) || $_GET['post_status'] != 'wc-' . YWSBS_Subscription_Order()->get_renew_order_status() ) {
				return $columns;
			}
			foreach ( $columns as $key => $column ) {
				$new_columns[ $key ] = $column;
				if ( $key == 'order_date' ) {
					$new_columns['membership']     = __( 'Membership Status', 'yith-woocommerce-subscription' );
					$new_columns['failed_payment'] = __( 'Failed Attempts', 'yith-woocommerce-subscription' );
				}
			}

			return $new_columns;
		}


		/**
		 * @param $column
		 *
		 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
		 */
		function add_order_posts_custom_column( $column ) {

			if ( ! isset( $_GET['post_status'] ) || $_GET['post_status'] != 'wc-' . YWSBS_Subscription_Order()->get_renew_order_status() ) {
				return;
			}

			// wc-pending-renewal
			global $post, $the_order;

			if ( empty( $the_order ) || $the_order->id != $post->ID ) {
				$the_order = wc_get_order( $post->ID );
			}

			$subscriptions = yit_get_prop( $the_order, 'subscriptions' );
			if ( empty( $subscriptions ) ) {
				return;
			}

			$subscription = ywsbs_get_subscription( $subscriptions[0] );

			switch ( $column ) {
				case 'membership' :
					if ( function_exists( 'YWSBS_Membership' ) ) {
						echo YWSBS_Membership()->subscription_column_default( '', $subscription->post, 'membership' );
					}
					break;
				case 'failed_payment' :

					$failed_attemps        = $subscription->has_failed_attemps();
					$date_of_attemp_string = '';

					if ( isset( $failed_attemps['max_failed_attemps'] ) ) {
						if ( $failed_attemps['num_of_failed_attemps'] > 0 && $failed_attemps['num_of_failed_attemps'] < $failed_attemps['max_failed_attemps'] ) {
							$date_of_attemp = yit_get_prop( $the_order, 'next_payment_attempt' );
							if ( empty( $date_of_attemp ) || $date_of_attemp <= current_time( 'timestamp' ) ) {
								$date_of_attemp = intval( $subscription->payment_due_date ) + ( ( $failed_attemps['day_between_attemps'] * DAY_IN_SECONDS ) * ( $failed_attemps['num_of_failed_attemps'] ) );
							}
							$date_of_attemp_string = empty( $date_of_attemp ) ? '' : date_i18n( get_option( 'date_format' ), $date_of_attemp );
						}
						echo $failed_attemps['num_of_failed_attemps'] . '/' . $failed_attemps['max_failed_attemps'] . '<br>' . $date_of_attemp_string;
					}
					break;
			}
		}

	}
}

/**
 * Unique access to instance of YITH_WC_Subscription_Admin class
 *
 * @return \YITH_WC_Subscription_Admin
 */
function YITH_WC_Subscription_Admin() {
	return YITH_WC_Subscription_Admin::get_instance();
}
