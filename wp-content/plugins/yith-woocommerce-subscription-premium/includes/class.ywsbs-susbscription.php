<?php

if ( !defined( 'ABSPATH' ) || !defined( 'YITH_YWSBS_VERSION' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Implements YWSBS_Subscription Class
 *
 * @class   YWSBS_Subscription
 * @package YITH WooCommerce Subscription
 * @since   1.0.0
 * @author  Yithemes
 *
 * @property    string $status Subscription status
 */
if ( !class_exists( 'YWSBS_Subscription' ) ) {

    class YWSBS_Subscription {

        /**
         * The subscription (post) ID.
         *
         * @var int
         */
        public $id = 0;


	    /**
         * @var string
         */
        public $price_time_option;

	    /**
         * @var int
         */
        public $variation_id;

        /**
         * $post Stores post data
         *
         * @var $post WP_Post
         */
        public $post = null;

        /**
         * $post Stores post data
         *
         * @var string
         */
        public $status;



        /**
         * Constructor
         *
         * Initialize plugin and registers actions and filters to be used
         *
         * @param int $subscription_id
         * @param array $args
         *
         * @since  1.0.0
         * @author Emanuela Castorina
         */
        public function __construct( $subscription_id = 0, $args = array() ) {

            //populate the subscription if $subscription_id is defined
            if ( $subscription_id ) {
                $this->id = $subscription_id;
                $this->populate();
            }

            //add a new subscription if $args is passed
            if ( $subscription_id == '' && ! empty( $args ) ) {
                $this->add_subscription( $args );
            }

        }

        /**
         * __get function.
         *
         * @param string $key
         *
         * @return mixed
         */
        public function __get( $key ) {
            $value = get_post_meta( $this->id, $key, true );

            if ( ! empty( $value ) ) {
                $this->$key = $value;
            }

            return $value;
        }

        public function __isset( $key ) {
            if ( ! $this->id ) {
                return false;
            }

            return metadata_exists( 'post', $this->id, $key );
        }

        /**
         * set function.
         *
         * @param string $property
         * @param mixed  $value
         *
         * @return bool|int
         */
        public function set( $property, $value ) {

            $this->$property = $value;

            return update_post_meta( $this->id, $property, $value );
        }

        /**
         * Populate the subscription
         *
         * @return void
         * @since  1.0.0
         */
        public function populate() {

            $this->post = get_post( $this->id );

            foreach ( $this->get_subscription_meta() as $key => $value ) {
                $this->$key = $value;
            }

            do_action( 'ywsbs_subscription_loaded', $this );
        }

        /**
         * Add new subscription
         *
         * @param array $args
         *
         * @return void
         * @since  1.0.0
         */
        public function add_subscription( $args ) {

            $subscription_id = wp_insert_post( array(
                'post_status' => 'publish',
                'post_type'   => 'ywsbs_subscription',
            ) );

            if ( $subscription_id ) {
                $this->id = $subscription_id;
                $meta     = apply_filters( 'ywsbs_add_subcription_args', wp_parse_args( $args, $this->get_default_meta_data() ), $this );
                $this->update_subscription_meta( $meta );
                $this->populate();

                YITH_WC_Activity()->add_activity( $subscription_id, 'new', 'success', $this->order_id, __( 'Subscription successfully created.', 'yith-woocommerce-subscription' ) );
            }
        }

        /**
         * Update post meta in subscription
         *
         * @param array $meta
         *
         * @return void
         * @since  1.0.0
         */
        function update_subscription_meta( $meta ) {
            foreach ( $meta as $key => $value ) {
                update_post_meta( $this->id, $key, $value );
            }
        }

        /**
         * Fill the default metadata with the post meta stored in db
         *
         * @return array
         * @since  1.0.0
         */
        function get_subscription_meta() {
            $subscription_meta = array();
            foreach ( $this->get_default_meta_data() as $key => $value ) {
                $subscription_meta[ $key ] = get_post_meta( $this->id,  $key, true );
            }

            return $subscription_meta;
        }

        /**
         * Start the subscription if a first payment is done
         * order_id is the id of the first order created
         *
         * @param int $order_id
         *
         * @return void
         * @since  1.0.0
         */
        public function start( $order_id ) {

            $payed = $this->payed_order_list;

            //do not nothing if this subscription has payed with this order
            if ( ! empty( $payed ) && is_array( $payed ) && in_array( $order_id, $payed ) ) {
                return;
            }

            $order       = wc_get_order( $order_id );
            $new_status  = 'active';
            $rates_payed = 1;
            if ( $this->start_date == '' ) {
                $this->set( 'start_date', current_time('timestamp') );
            }

            if ( $this->payment_due_date == '' ) {
                $trial_period = 0;

                //if there's a trial period shift the date of payment due
                if ( $this->trial_per != '' && $this->trial_per > 0 ) {
                    $trial_period = ywsbs_get_timestamp_from_option( 0, $this->trial_per, $this->trial_time_option );
                    $rates_payed  = 0; //if there's a trial period the first payment is for signup
                    $new_status   = 'trial';
                }

                //Change the next payment_due_date
                $this->set( 'payment_due_date', $this->get_next_payment_due_date( $trial_period, $this->start_date ) );

            }

            if ( $this->expired_date == '' && $this->max_length != '' ) {

                $trial_period = 0;

                if ( $this->trial_per != '' && $this->trial_per > 0 ) {
                    $trial_period = ywsbs_get_timestamp_from_option( 0, $this->trial_per, $this->trial_time_option );

                }

                $timestamp = ywsbs_get_timestamp_from_option( current_time('timestamp'), $this->max_length, $this->price_time_option ) + $trial_period;
                $this->set( 'expired_date', $timestamp );

            }

            //Change the status to active
            $update = $this->update_status( $new_status );
            if( $update ){
                YITH_WC_Activity()->add_activity( $this->id, 'activated', 'success', $this->order_id, __( 'Payment received for #' . $order_id, 'yith-woocommerce-subscription' ) );
            }else{
                YITH_WC_Activity()->add_activity( $this->id, 'activated', 'info', $this->order_id, __( 'Payment received for #' . $order_id .' no status changed', 'yith-woocommerce-subscription' ) );
            }

            //correct the payment methods
            $this->update_payment_method();

            do_action( 'ywsbs_customer_subscription_payment_done_mail', $this );

            $payed[] = $order_id;

            $this->set( 'rates_payed', $rates_payed );
            $this->set( 'payed_order_list', $payed );

            //if there's an upgrade/downgrade
            $subscription_to_cancel_info = get_post_meta($order_id, '_ywsbs_subscritpion_to_cancel', true);

            if ( ! empty( $subscription_to_cancel_info ) ) {
              
                YITH_WC_Subscription()->cancel_subscription_after_upgrade( $subscription_to_cancel_info['subscription_to_cancel'] );
                update_post_meta( $subscription_to_cancel_info['subscription_to_cancel'], 'ywsbs_switched', 'yes' );
                
                if( $subscription_to_cancel_info['process_type'] == 'upgrade' ){
                    delete_user_meta( $subscription_to_cancel_info['user_id'], 'ywsbs_upgrade_' . $subscription_to_cancel_info['product_id'] );
                }elseif( $subscription_to_cancel_info['process_type'] == 'downgrade' ){
                    delete_user_meta( $subscription_to_cancel_info['user_id'], 'ywsbs_downgrade_' . $subscription_to_cancel_info['product_id'] );
                }

            }
        }

        /**
         * Update the subscription if a payment is done manually from user
         * order_id is the id of the last order created
         *
         * @param int $order_id
         *
         * @return void
         * @since  1.0.0
         */
        public function update( $order_id ) {

            $payed = $this->payed_order_list;

            //do not nothing if this subscription has payed with this order
            if ( ! empty( $payed ) && is_array( $payed ) && in_array( $order_id, $payed ) ) {
                return;
            }

            //Change the status to active
            $this->update_status( 'active' );

            //Change the next payment_due_date
            $this->set( 'payment_due_date', $this->get_next_payment_due_date() );


            YITH_WC_Activity()->add_activity( $this->id, 'activated', 'success', $this->order_id, __( 'Payment received for #' . $order_id . '. Next payment due date set.', 'yith-woocommerce-subscription' ) );

            //send email to customer for payment done
            do_action( 'ywsbs_customer_subscription_payment_done_mail', $this );

            //update _payed_order_list
            $payed[] = $order_id;
            $this->set( 'payed_order_list', $payed );
            $this->set( 'rates_payed', $this->rates_payed + 1 );

            //reset _renew_order
            $this->set( 'renew_order', 0 );

        }

        /**
         * Updates status of subscription
         *
         * @param  string $new_status
         * @param string  $from
         *
         * @return  bool
         * @since   1.0.0
         */
        public function update_status( $new_status, $from = '' ) {
            if ( ! $this->id ) {
                return false;
            }

            $old_status = $this->status;
            $from_list = ywsbs_get_from_list();

            if ( $new_status !== $old_status || ! in_array( $new_status, array_keys( ywsbs_get_status() ) ) ) {

                $from_text = ( $from != '' && isset( $from_list[$from] )) ? 'By ' . $from_list[$from] : '';

                switch ( $new_status ) {
                    case 'active' :
                        //reset some custom data
                        $this->set( 'expired_pause_date', '' );
                        // Check if subscription is cancelled. Es. for echeck payments
                        if( $old_status == 'cancelled' ){
                            if( $from == 'administrator'){
                                $this->set( 'status', $new_status );
                                do_action( 'ywsbs_customer_subscription_actived_mail', $this );
                                YITH_WC_Activity()->add_activity( $this->id, 'activated', 'success', $this->order_id, sprintf( __( 'Subscription is now active. %s ', 'yith-woocommerce-subscription' ), $from_text ) );

                                $this->set( 'payment_due_date', $this->end_date );
                                $this->set( 'end_date', '' );
                                $this->set( 'cancelled_date', '' );
                            }else{
                                $this->set( 'end_date', $this->payment_due_date );
                                $this->set( 'payment_due_date', '' );
                                do_action( 'ywsbs_no_activated_just_cancelled', $this );
                                return false;
                            }

                        }else{
                            $this->set( 'status', $new_status );
                            do_action( 'ywsbs_customer_subscription_actived_mail', $this );
                            YITH_WC_Activity()->add_activity( $this->id, 'activated', 'success', $this->order_id, sprintf( __( 'Subscription is now active. %s', 'yith-woocommerce-subscription' ), $from_text ) );
                        }


                        break;

                    case 'paused' :
                        $pause_options = $this->get_subscription_product_pause_options();

                        //add the date of pause
                        $this->date_of_pauses[] = current_time('timestamp');
                        $this->set( 'date_of_pauses', $this->date_of_pauses );

                        //increase the num of pauses done
                        $this->set( 'num_of_pauses', $this->num_of_pauses + 1 );

                        //expired_pause_date
                        if ( $pause_options['max_pause_duration'] != '' ) {
                            $this->set( 'expired_pause_date', current_time('timestamp') + $pause_options['max_pause_duration'] * 86400 );
                        }

                        // Update the subscription status
                        $this->set( 'status', $new_status );
                        do_action( 'ywsbs_customer_subscription_paused_mail', $this );
                        YITH_WC_Activity()->add_activity( $this->id, 'paused', 'success', $this->order_id, sprintf( __( 'Subscription paused. %s', 'yith-woocommerce-subscription' ), $from_text ) );

                        break;
                    case 'resume' :
                        $this->set( 'expired_pause_date', '' );

                        //change payment_due_date
                        $offset           = $this->get_payment_due_date_paused_offset();
                        $payment_due_date = $this->payment_due_date + $offset;

                        $this->set( 'sum_of_pauses', $this->sum_of_pauses + $offset );
                        $this->set( 'payment_due_date', $payment_due_date );

                        if( $this->expired_date ){
                            //shift expiry date
                            $this->set( 'expired_date', $this->expired_date + $offset );
                        }

                        // Update the subscription status
                        $this->set( 'status', 'active' );
                        do_action( 'ywsbs_customer_subscription_resumed_mail', $this );
                        YITH_WC_Activity()->add_activity( $this->id, 'resumed', 'success', $this->order_id, sprintf( __( 'Subscription resumed. Payment due on %s. %s', 'yith-woocommerce-subscription' ), date_i18n( wc_date_format(), $payment_due_date ), $from_text ) );

                        break;

                    case 'overdue' :
                        // Update the subscription status
                        $this->set( 'status', $new_status );
                        do_action( 'ywsbs_customer_subscription_request_payment_mail', $this );
                        YITH_WC_Activity()->add_activity( $this->id, 'overdue', 'success', $this->order_id, __( 'Overdue subscription.', 'yith-woocommerce-subscription' ) );
                        break;

                    case 'trial':
                        if( $old_status == 'cancelled' ){
                            $this->set( 'end_date', $this->payment_due_date );
                            $this->set( 'payment_due_date', '' );
                            do_action( 'ywsbs_no_activated_just_cancelled', $this );
                            return false;
                        }else{
                            $this->set( 'status', $new_status );
                            YITH_WC_Activity()->add_activity( $this->id, 'trial', 'success', $this->order_id, sprintf( __( 'Started a trial period of %s %s', 'yith-woocommerce-subscription' ), $this->trial_per, $this->trial_time_option ) );
                        }
                        break;

                    case 'cancelled' :
                        //if the subscription is cancelled the payment_due_date become the expired_date
                        //the subscription will be actin until the date of the next payment

                        $this->set( 'end_date', $this->payment_due_date );
                        $this->set( 'payment_due_date', '' );
                        $this->set( 'cancelled_date', current_time('timestamp') );
                        $this->set( 'status', $new_status );

                        do_action( 'ywsbs_customer_subscription_cancelled_mail', $this );
                        YITH_WC_Activity()->add_activity( $this->id, 'cancelled', 'success', $this->order_id, sprintf( __( 'The subscription has been cancelled. %s', 'yith-woocommerce-subscription' ), $from_text ) );
                        break;
                    case 'cancel-now' :
                        //if the subscription is cancelled now the end_date is the current timestamp
                        $new_status = 'cancelled';
                        $tstamp = current_time('timestamp');
                        $this->set( 'end_date', $tstamp );
                        $this->set( 'payment_due_date', '' );
                        $this->set( 'cancelled_date', $tstamp );
                        $this->set( 'status', $new_status );

                        do_action( 'ywsbs_customer_subscription_cancelled_mail', $this );

                        YITH_WC_Activity()->add_activity( $this->id, 'cancelled', 'success', $this->order_id, sprintf( __( 'The subscription has been NOW cancelled. %s', 'yith-woocommerce-subscription' ), $from_text ) );
                        break;
                    case 'expired' :
                        $this->set( 'status', $new_status );
                        do_action( 'ywsbs_customer_subscription_expired_mail', $this );
                        YITH_WC_Activity()->add_activity( $this->id, 'expired', 'success', $this->order_id, __( 'Subscription expired.', 'yith-woocommerce-subscription' ) );
                        break;
                    case 'suspended' :
                        $this->set( 'status', $new_status );
                        do_action( 'ywsbs_customer_subscription_suspended_mail', $this );
                        YITH_WC_Activity()->add_activity( $this->id, 'suspended', 'success', $this->order_id, __( 'Subscription suspended.', 'yith-woocommerce-subscription' ) );
                        break;
                    default:
                }

                // Status was changed
                do_action( 'ywsbs_subscription_status_' . $new_status, $this->id );
                do_action( 'ywsbs_subscription_status_' . $old_status . '_to_' . $new_status, $this->id );
                do_action( 'ywsbs_subscription_status_changed', $this->id, $old_status, $new_status );
                do_action( 'ywsbs_subscription_admin_mail', $this );

                return true;
            }

            return false;
        }

        /**
         * Get the next payment due date.
         *
         * If paused, calculate the next date for payment, checking
         */
        public function get_payment_due_date_paused_offset() {
            if ( 'paused' != $this->status ) {
                return 0;
            }

            $date_pause = $this->date_of_pauses;
            $last       = ( $date_pause[ count( $date_pause ) - 1 ] );
            $offset     = current_time('timestamp') - $last;

            return $offset;
        }

        /**
         * Return the subscription recurring price formatted
         *
         * @return  string
         * @since   1.0.0
         */
        public function get_formatted_recurring($tax_display = '') {

	        $price_time_option_string = ywsbs_get_price_per_string( $this->price_is_per,  $this->price_time_option );

	        $tax_inc = get_option( 'woocommerce_prices_include_tax' ) === 'yes';

	        if ( wc_tax_enabled() && $tax_inc ) {
		        $sbs_price = $this->line_total + $this->line_tax;

	        } else {
		        $sbs_price = $this->line_total;
	        }

            $recurring = wc_price( $sbs_price, array( 'currency' => $this->order_currency ) ) . ' / ' . $price_time_option_string;

            return apply_filters( 'ywsbs-recurring-price', $recurring, $this );
        }


        /**
         * Return the subscription detail page url
         *
         * @param bool $admin
         *
         * @return  string
         * @since   1.0.0
         */
        public function get_view_subscription_url( $admin = false ) {

            if ( $admin ) {
                $view_subscription_url = admin_url( 'post.php?post=' . $this->id . '&action=edit' );
            } else {
                $view_subscription_url = wc_get_endpoint_url( 'view-subscription', $this->id, wc_get_page_permalink( 'myaccount' ) );
            }

            return apply_filters( 'ywsbs_get_subscription_url', $view_subscription_url, $this->id, $admin );
        }

        /**
         * Return if the subscription can br stopped by user
         *
         * @return  bool
         * @since   1.0.0
         */
        public function can_be_paused() {

            $pause_info = $this->get_subscription_product_pause_options();

            //an administrator can change the status in paused
            $post_type_object = get_post_type_object( YITH_WC_Subscription()->post_name );

	        if ( $pause_info['allow_pause'] == 'yes' && current_user_can( $post_type_object->cap->delete_post, $this->ID ) &&  $pause_info['max_pause'] && $this->num_of_pauses < $pause_info['max_pause'] ) {
		        return true;
	        }

	        if ( $pause_info['allow_pause'] != 'yes' || $this->status != 'active' ) {
                return false;
            }


            return false;
        }


        /**
         * Return if the subscription can be set active
         *
         * @return  bool
         * @since   1.0.0
         */
        public function can_be_active() {
            $status = array( 'pending', 'overdue', 'suspended', 'cancelled' );

            //the administrator and shop manager can switch the status to cancelled
            $post_type_object = get_post_type_object( YITH_WC_Subscription()->post_name );
            if( current_user_can( $post_type_object->cap->delete_post, $this->ID ) && in_array( $this->status, $status ) ){
                return true;
            }

            return false;
        }

        /**
         * Return if the subscription can be set as suspended
         *
         * @return  bool
         * @since   1.0.0
         */
        public function can_be_suspended() {

            if ( ! YITH_WC_Subscription()->suspension_time() ){
                return false;
            }

            $status = array( 'active', 'overdue', 'cancelled' );

            //the administrator and shop manager can switch the status to cancelled
            $post_type_object = get_post_type_object( YITH_WC_Subscription()->post_name );
            if( current_user_can( $post_type_object->cap->delete_post, $this->ID ) && in_array( $this->status, $status ) ){
                return true;
            }

            return false;
        }

        /**
         * Return if the subscription can be set as suspended
         *
         * @return  bool
         * @since   1.0.0
         */
        public function can_be_overdue() {

            if ( ! YITH_WC_Subscription()->overdue_time() ){
                return false;
            }

            $status = array( 'active', 'suspended', 'cancelled' );

            //the administrator and shop manager can switch the status to cancelled
            $post_type_object = get_post_type_object( YITH_WC_Subscription()->post_name );
            if( current_user_can( $post_type_object->cap->delete_post, $this->ID ) && in_array( $this->status, $status ) ){
                return true;
            }

            return false;
        }
        /**
         * Return if the subscription can be resumed by user
         *
         * @return  bool
         * @since   1.0.0
         */
        public function can_be_resumed() {
            if ( $this->status != 'paused' ) {
                return false;
            } else {
                return true;
            }
        }

        /**
         * Return if the subscription can be cancelled by user
         *
         * @return  bool
         * @since   1.0.0
         */
        public function can_be_cancelled() {
            $status = array( 'pending', 'overdue', 'suspended', 'cancelled' );

            //the administrator and shop manager can switch the status to cancelled
            $post_type_object = get_post_type_object( YITH_WC_Subscription()->post_name );
            if ( current_user_can( $post_type_object->cap->delete_post, $this->ID ) ) {
                $return = true;
            } else if ( ! in_array( $this->status, $status ) && get_option( 'ywsbs_allow_customer_cancel_subscription' ) == 'yes' ) {
                $return = true;
            } else {
                $return = false;
            }

            return apply_filters( 'ywsbs_can_be_cancelled', $return, $this );
        }

        /**
         * Return if the subscription can be reactivate by user
         *
         * @return  bool
         * @since   1.0.0
         */
        public function can_be_renewed() {
            $status = array( 'cancelled', 'expired' );

            if ( in_array( $this->status, $status ) && get_option( 'ywsbs_allow_customer_renew_subscription' ) == 'yes' ) {
                return true;
            } else {
                return false;
            }

        }

        /**
         * Return if the subscription can be reactivate by user
         *
         * @return  bool
         * @since   1.0.0
         */
        public function can_be_create_a_renew_order() {
            $status = array( 'pending', 'expired' );

            // exit if no valid subscription status
            if( in_array( $this->status, $status ) ){
                return false;
            }

            //check if the subscription have a renew order
            $renew_order = $this->has_a_renew_order();

            // if order doesn't exist, or is cancelled, we create order
            if ( ! $renew_order || ( $renew_order && ( $renew_order->get_status() == 'cancelled' ) ) ) {
               return true;
            }
            // otherwise we return order id
            else {
            	return yit_get_order_id( $renew_order );
            }
        }

        /**
         * Return the renew order if exists
         *
         * @return  bool|WC_Order
         * @since   1.1.5
         */
        public function has_a_renew_order(){

            $return = false;
            $renew_order_id = $this->renew_order;

            if( $renew_order_id ){
                $order = wc_get_order( $renew_order_id );
                $order && $return = $order;
            }

            return $return;
        }

        /**
         * Return if the subscription can be switchable
         *
         * @since   1.0.0
         * @return  bool
         */
        public function can_be_switchable() {

            $status = array(  'paused', 'cancelled', 'expired' );

            if ( ! $this->variation_id || in_array( $this->status, $status ) ) {
                return false;
            }

            if( get_post_meta( $this->id, 'ywsbs_switched', true) == 'yes'){
                return false;
            }

            $variation = wc_get_product( $this->variation_id );

            if ( isset( $variation->parent ) ) {
            	$parent_id  = yit_get_base_product_id( $variation );
                $product    = wc_get_product( $parent_id );
                $variations = $product->get_available_variations();

                $available_switch = array();

                if ( ! empty( $variations ) ) {

                    $current_subscription_priority = yit_get_prop( $variation, '_ywsbs_switchable_priority' );
                    foreach ( $variations as $item ) {
                        $current_subscription_days_of_activity = ywsbs_get_days( $this->get_activity_period() );

                        if ( $item['variation_id'] != $this->variation_id && $item['is_subscription'] && $item['is_switchable'] && $item['variation_is_visible'] && $item['variation_is_active'] && $item['is_purchasable'] ) {
	                        $item_product     = wc_get_product( $item['variation_id'] );
	                        $max_length       = yit_get_prop( $item_product, '_ywsbs_max_length' );
	                        $time_option      = yit_get_prop( $item_product, '_ywsbs_price_time_option' );
	                        $prorate_length   = yit_get_prop( $item_product, '_ywsbs_prorate_length' );
	                        $duration_in_days = '';

                            if ( $max_length != '' ) {
                                $duration         = ywsbs_get_timestamp_from_option( 0, $max_length, $time_option );
                                $duration_in_days = ywsbs_get_days( $duration );
                            }

                            $gap_payment = yit_get_prop( $item_product, '_ywsbs_gap_payment' );
                            $priority    = yit_get_prop( $item_product, '_ywsbs_switchable_priority' );
                            //if is an upgrade and if the admin has checked gap payment the choice must be showed to the user
	                        if ( $priority >= $current_subscription_priority && $gap_payment == 'yes' ) {
		                        $item['has_gap_payment'] = 'yes';
		                        $item['gap']             = $this->calculate_gap_payment( $item['variation_id'] );
	                        }

	                        if ( $prorate_length == 'yes' && ( $max_length == '' || $duration_in_days >= $current_subscription_days_of_activity ) ) {
		                        $available_switch[] = $item;
	                        } elseif ( $prorate_length == 'no' ) {
		                        $available_switch[] = $item;
	                        }
                        }
                    }

                    return $available_switch;
                }
            }

            return false;

        }

        /**
         * Calculate the gap payment in the upgrade processing
         *
         * @param   int $variation_id
         *
         * @return  float
         * @since   1.0.0
         */
        function calculate_gap_payment( $variation_id ) {

            $activity_period = $this->get_activity_period();
	        $variation       = wc_get_product( $variation_id );
	        $time_option     = yit_get_prop( $variation, '_ywsbs_price_time_option' );
            $num_old_rates   = ceil( $activity_period / ywsbs_get_timestamp_from_option( 0, 1, $time_option ) );
            $var_price       = ( $variation->get_price() - $this->line_subtotal) * $num_old_rates;

            return ( $var_price > 0 ) ? $var_price : 0;
        }

        /**
         * Cancel the subscription
         *
         * @return  void
         * @since   1.0.0
         */
        function cancel( $now = true ) {

            if( $now ){
                $this->update_status( 'cancel-now' );
            }else{
                $this->update_status( 'cancelled' );
            }

            //Change the status to active

            do_action( 'ywsbs_subscription_cancelled', $this->id );

            //if there's a pending order for this subscription change the status of the order to cancelled
            if ( $this->renew_order ) {
                $order = wc_get_order( $this->renew_order );
                if ( $order ) {
                    $order->update_status( 'cancelled' );
                    $order->add_order_note( sprintf( __( 'This order has been cancelled because subscription #%d has been cancelled', 'yith-woocommerce-subscription' ), $this->id ) );
                }
            }
        }

        /**
         * Delete the subscription
         *
         * @since  1.0.0
         */
        public function delete(){
            wp_delete_post( $this->id, true );
            do_action( 'ywsbs_subscription_deleted', $this->id );
        }

        /**
         * Return an array of all custom fields subscription
         *
         * @return array
         * @since  1.0.0
         */
        private function get_default_meta_data(){
            $subscription_meta_data = array(
                'status'                  => 'pending',
                'start_date'              => '',
                'payment_due_date'        => '',
                'expired_date'            => '',
                'cancelled_date'          => '',
                'end_date'                => '',
                //pauses
                'num_of_pauses'           => 0,
                'date_of_pauses'          => '',
                'expired_pause_date'      => '',
                'sum_of_pauses'           => '',
                //paypal
                'paypal_subscriber_id'    => '',
                'paypal_transaction_id'   => '',
                'payed_order_list'        => array(),
                'product_id'              => '',
                'variation_id'            => '',
                'variation'               => '',
                'product_name'            => '',
                'quantity'                => '',
                'line_subtotal'           => '',
                'line_total'              => '',
                'line_subtotal_tax'       => '',
                'line_tax'                => '',
                'line_tax_data'           => '',
                'cart_discount'           => '',
                'cart_discount_tax'       => '',
                'coupons'                 => '',
                'order_total'             => '',
                'order_subtotal'          => '',
                'order_tax'               => '',
                'order_discount'          => '',
                'order_shipping'          => '',
                'order_shipping_tax'      => '',
                'order_currency'          => '',
                'renew_order'             => 0,
                'prices_include_tax'      => '',
                'payment_method'          => '',
                'payment_method_title'    => '',
                'transaction_id'          => '',
                'subscriptions_shippings' => '',
                'subscription_total'      => '',
                'price_is_per'            => '',
                'price_time_option'       => '',
                'max_length'              => '',
                'trial_per'               => '',
                'trial_time_option'       => '',
                'fee'                     => '',
                'num_of_rates'            => '',
                'rates_payed'             => '',
                'order_ids'               => array(),
                'order_id'                => '',
                'order_item_id'           => '',
                'user_id'                 => 0,
                'customer_ip_address'     => '',
                'customer_user_agent'     => '',
            );

            return $subscription_meta_data;
        }

        /**
         * Return an array of pause options
         *
         * @return array
         * @since  1.0.0
         */
	    private function get_subscription_product_pause_options() {
		    $id                                          = ( $this->variation_id ) ? $this->variation_id : $this->product_id;
		    $product                                     = wc_get_product( $id );
		    $product_pause_options['max_pause']          = yit_get_prop( $product, '_ywsbs_max_pause' );
		    $product_pause_options['max_pause_duration'] = yit_get_prop( $product, '_ywsbs_max_pause_duration' );

		    if ( $product_pause_options['max_pause'] ) {
			    $product_pause_options['allow_pause'] = true;
		    } else {
			    $product_pause_options['allow_pause'] = false;
		    }

		    return $product_pause_options;
	    }

        /**
         * Return the a link for change the status of subscription
         *
         * @param string $status
         *
         * @return string
         * @since  1.0.0
         */
        public function get_change_status_link( $status ) {

            $action_link = add_query_arg( array( 'subscription' => $this->id, 'change_status' => $status ) );
            $action_link = wp_nonce_url( $action_link, $this->id );

            return apply_filters( 'ywsbs_change_status_link', $action_link, $this, $status );
        }

        /**
         * Return the next payment due date if there are rates not payed
         *
         * @param int $trial_period
         *
         * @since  1.0.0
         * @author Emanuela Castorina
         * @return array
         */
        public function get_next_payment_due_date( $trial_period = 0, $start_date = 0) {

            $start_date = ( $start_date ) ? $start_date : current_time('timestamp');
            if ( $this->num_of_rates == '' || ( $this->num_of_rates - $this->rates_payed ) > 0 ) {
                $payment_due_date = ( $this->payment_due_date == '' ) ?  $start_date : $this->payment_due_date;
                if( $trial_period != 0){
                    $timestamp = $start_date + $trial_period;
                }else{
                    $timestamp = ywsbs_get_timestamp_from_option( $payment_due_date, $this->price_is_per, $this->price_time_option );
                }

                return $timestamp;
            }

            return false;

        }

        /**
         * Return the next payment due date if there are rates not payed
         *
         *
         * @return int
         * @since  1.0.0
         */
        public function get_left_time_to_next_payment() {

            $left_time = 0;

            if ( $this->payment_due_date ) {
                $left_time = $this->payment_due_date - current_time('timestamp');
            } elseif ( $this->expired_date ) {
                $left_time = $this->expired_date - current_time('timestamp');
            }

            return $left_time;
        }

        /**
         * Return the timestamp from activation of subscription escluding pauses
         *
         * @param bool $exclude_pauses
         *
         * @return array
         * @since  1.0.0
         */
        public function get_activity_period( $exclude_pauses = true ) {
            $timestamp = current_time('timestamp') - $this->start_date;
            if ( $exclude_pauses && $this->sum_of_pauses ) {
                $timestamp -= $this->sum_of_pauses;
            }

            return abs( $timestamp );
        }


	    /**
	     * @return array|bool
	     */
	    public function has_failed_attemps(){
            $return         = false;
            $order_id       = $this->order_id;
            $order = wc_get_order( $order_id);

	        $failed_attemps       = yit_get_prop( $order, 'failed_attemps' ) ? yit_get_prop( $order, 'failed_attemps' ) : 0;
	        $payment_method       = yit_get_prop( $order, '_payment_method' );
	        $max_failed_attemp    = ywsbs_get_max_failed_attemps_list();
	        $days_between_attemps = ywsbs_get_num_of_days_between_attemps();

	        if ( isset( $max_failed_attemp[ $payment_method ] ) ) {
		        $return = array(
			        'num_of_failed_attemps' => $failed_attemps,
			        'max_failed_attemps'    => ( isset( $max_failed_attemp[ $payment_method ] ) ) ? $max_failed_attemp[ $payment_method ] : '',
			        'day_between_attemps'   => ( isset( $days_between_attemps[ $payment_method ] ) ) ? $days_between_attemps[ $payment_method ] : '',
		        );
	        }

            return $return;
        }

        /**
         * Get subscription customer billing fields
         *
         * @param string $type
         * @param boolean $no_type
         * @return array
         */
	    public function get_address_fields( $type = 'billing', $no_type = false ) {

		    $fields = array();

		    if ( $type == 'shipping' ) {
			    $order = wc_get_order( $this->order_id );
			    if ( $order ) {
				    $meta_fields = $order->get_address( 'shipping' );

				    if ( is_array( $meta_fields ) ) {
					    foreach ( $meta_fields as $key => $value ) {
						    $field_key            = $no_type ? $key : 'shipping_' . $key;
						    $fields[ $field_key ] = $value;
					    }
				    }
			    }
		    } else {
			    // get all address fields
			    $meta_fields = WC()->countries->get_address_fields( 'give_me_all', 'billing_' );

			    if ( is_array( $meta_fields ) ) {
				    foreach ( $meta_fields as $key => $value ) {
					    $field_value          = get_user_meta( $this->user_id, $key, true );
					    $field_key            = $no_type ? str_replace( 'billing_', '', $key ) : $key;
					    $fields[ $field_key ] = $field_value;
				    }
			    }
		    }

		    return $fields;
	    }

        /**
         * Add failed attemp
         *
         * @param bool $attempts
         * @param bool $latest_attemp if is the last attemp doesn't send email
         *
         * @since  1.1.3
         * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
         */
        public function register_failed_attemp( $attempts = false, $latest_attemp = false ) {

        	$order = wc_get_order( $this->order_id );

            if ( false === $attempts ) {
                $failed_attemp = yit_get_prop( $order, 'failed_attemps' );
                $attempts = $failed_attemp + 1;
            }

	        if( ! $latest_attemp ){
		        YITH_WC_Activity()->add_activity( $this->id, 'failed-payment', 'success', $this->order_id, sprintf( __( 'Failed payment for order %d', 'yith-woocommerce-subscription' ), $this->order_id ) );
		        yit_save_prop( $order, 'failed_attemps', $attempts, false, true);
		        do_action( 'ywsbs_customer_subscription_payment_failed_mail', $this );
			}
        }
        
        /**
         * Get billing customer email
         * 
         * @return string
         */
        public function get_billing_email(){
            $billing_email = get_user_meta( $this->user_id, 'billing_email', true );
            return apply_filters( 'ywsbs_customer_billing_email', $billing_email, $this );
        }        
        
        /**
         * Get billing customer email
         *
         * @return string
         */
        public function get_billing_phone(){
            $billing_email = get_user_meta( $this->user_id, 'billing_phone', true );
            return apply_filters( 'ywsbs_customer_billing_phone', $billing_email, $this );
        }

        /**
         * Update the payment method after that the order is completed
         *
         * @return string
         */
	    public function update_payment_method() {
		    $order = wc_get_order( $this->order_id );

		        if ( ! $order ) {
			    return;
		    }

		    $this->payment_method       = yit_get_prop( $order, '_payment_method' );
		    $this->payment_method_title = yit_get_prop( $order, '_payment_method_title' );
	    }
        
    }

}