<?php

if ( !defined( 'ABSPATH' ) || !defined( 'YITH_YWSBS_VERSION' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Implements YWSBS_Subscription_Cron Class
 *
 * @class   YWSBS_Subscription_Cron
 * @package YITH WooCommerce Subscription
 * @since   1.0.0
 * @author  Yithemes
 */
if ( !class_exists( 'YWSBS_Subscription_Cron' ) ) {

    class YWSBS_Subscription_Cron {

        /**
         * Single instance of the class
         *
         * @var \YWSBS_Subscription_Cron
         */
        protected static $instance;

        /**
         * Returns single instance of the class
         *
         * @return \YWSBS_Subscription_Cron
         * @since 1.0.0
         */
        public static function get_instance() {
            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }
            return self::$instance;
        }

        /**
         * Constructor
         *
         * Initialize plugin and registers actions and filters to be used
         *
         * @since  1.0.0
         * @author Emanuela Castorina
         */
        public function __construct() {

            add_action( 'wp_loaded', array( $this, 'set_cron'), 30 );

        }


        public function set_cron(){

	        $ve = get_option( 'gmt_offset' ) > 0 ? '+' : '-';
			$time_start = strtotime( '00:00 ' . $ve . get_option( 'gmt_offset' ) . ' HOURS' );

        	if ( !wp_next_scheduled( 'ywsbs_renew_orders' ) ) {
                wp_schedule_event( $time_start, 'hourly', 'ywsbs_renew_orders' );
            }

            if ( !wp_next_scheduled( 'ywsbs_check_subscription_payment' ) ) {
                wp_schedule_event( $time_start, 'daily', 'ywsbs_check_subscription_payment' );
            }

            if ( !wp_next_scheduled( 'ywsbs_cancel_subscription_expired' ) ) {
                wp_schedule_event( $time_start, 'daily', 'ywsbs_cancel_subscription_expired' );
            }

            if ( !wp_next_scheduled( 'ywsbs_trigger_email_renew_reminder' ) ) {
                wp_schedule_event( $time_start, 'daily', 'ywsbs_trigger_email_renew_reminder' );
            }


            if ( !wp_next_scheduled( 'ywsbs_trigger_email_before_subscription_expired' ) ) {
                wp_schedule_event( $time_start, 'twicedaily', 'ywsbs_trigger_email_before_subscription_expired' );
            }

            //Reactive the subscription when the period of pause is expired
            if ( !wp_next_scheduled( 'ywsbs_resume_orders' ) ) {
                wp_schedule_event( $time_start, 'daily', 'ywsbs_resume_orders' );
            }

            //Check subscriptions that are in overdue status
            if (  YITH_WC_Subscription()->overdue_time()  &&  !wp_next_scheduled( 'ywsbs_check_overdue_subscriptions' ) ) {
                wp_schedule_event( $time_start, 'daily', 'ywsbs_check_overdue_subscriptions' );
            }

            //Check subscriptions that are in suspended status
            if (  YITH_WC_Subscription()->suspension_time()  &&  !wp_next_scheduled( 'ywsbs_check_suspended_subscriptions' ) ) {
                wp_schedule_event( $time_start, 'daily', 'ywsbs_check_suspended_subscriptions' );
            }


        }


        /**
         * Renew Order
         *
         * Create new order for active or in trial period subscription
         *
         * @since  1.0.0
         * @author Emanuela Castorina
         */
        public function renew_orders() {

            global $wpdb;

            $from = current_time('timestamp');
            $to = current_time('timestamp') + 86400;
            $query = $wpdb->prepare( "SELECT ywsbs_p.ID FROM {$wpdb->prefix}posts as ywsbs_p
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm ON ( ywsbs_p.ID = ywsbs_pm.post_id )
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm2 ON ( ywsbs_p.ID = ywsbs_pm2.post_id )
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm3 ON ( ywsbs_p.ID = ywsbs_pm3.post_id )
                 WHERE ( ywsbs_pm.meta_key='status' AND  ( ywsbs_pm.meta_value = 'active' OR  ywsbs_pm.meta_value = 'trial') )
                 AND ( ywsbs_pm3.meta_key='renew_order' AND  ywsbs_pm3.meta_value = 0 )
                 AND ywsbs_p.post_type = %s
                 AND ywsbs_p.post_status = 'publish'
                 AND ( ywsbs_pm2.meta_key='payment_due_date' AND  ( ywsbs_pm2.meta_value  < $to
                 AND  ywsbs_pm2.meta_value  >= $from ) )
                 GROUP BY ywsbs_p.ID ORDER BY ywsbs_p.ID DESC
                ", 'ywsbs_subscription' );

            update_option('ywsbs_renew_orders', $query);
            $subscriptions = $wpdb->get_results( $query ) ;

            if ( !empty( $subscriptions ) ) {
                foreach ( $subscriptions as $subscription_post ) {
                    $subscription  = ywsbs_get_subscription( $subscription_post->ID );
                    $order_pending = $subscription->renew_order;
                    if ( $order_pending == 0 ) {
                        $order_id = YWSBS_Subscription_Order()->renew_order( $subscription->id );
                    }
                }
            }
        }

        /**
         * Resume Order
         *
         * Resume the subscription if the pause period is expired
         *
         * @since  1.0.0
         * @author Emanuela Castorina
         */
        public function resume_orders() {

            global $wpdb;

            $from = current_time('timestamp');

            $query = $wpdb->prepare( "SELECT ywsbs_p.ID FROM {$wpdb->prefix}posts as ywsbs_p
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm ON ( ywsbs_p.ID = ywsbs_pm.post_id )
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm2 ON ( ywsbs_p.ID = ywsbs_pm2.post_id )
                 WHERE ( ywsbs_pm.meta_key='status' AND  ywsbs_pm.meta_value = 'paused' )
                 AND ywsbs_p.post_type = %s
                 AND ywsbs_p.post_status = 'publish'
                 AND ( ywsbs_pm2.meta_key='expired_pause_date' AND  ywsbs_pm2.meta_value  < $from )
                 GROUP BY ywsbs_p.ID ORDER BY ywsbs_p.ID DESC
                ", 'ywsbs_subscription' );


            $subscriptions = $wpdb->get_results( $query ) ;

            if ( !empty( $subscriptions ) ) {
                foreach ( $subscriptions as $subscription_post ) {
                    $subscription  = ywsbs_get_subscription( $subscription_post->ID );
                    $subscription->update_status( 'resume' );
                }
            }
        }

        /**
         * Check if there are subscription expired and change the status to expired
         *
         * @since  1.0.0
         * @author Emanuela Castorina
         */
        public function ywsbs_cancel_subscription_expired() {

            global $wpdb;

            //get all subscriptions that have status active and _expired_data < NOW to cancel the subscription
            $timestamp = current_time('timestamp');
            $subscriptions = $wpdb->get_results( $wpdb->prepare( "SELECT ywsbs_p.ID FROM {$wpdb->prefix}posts as ywsbs_p
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm ON ( ywsbs_p.ID = ywsbs_pm.post_id )
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm2 ON ( ywsbs_p.ID = ywsbs_pm2.post_id )
                 WHERE ( ywsbs_pm.meta_key='status' AND  ywsbs_pm.meta_value = 'active' )
                 AND ywsbs_p.post_type = %s
                 AND ywsbs_p.post_status = 'publish'
                 AND ( ywsbs_pm2.meta_key='expired_date' AND  ( ywsbs_pm2.meta_value <> '' AND ywsbs_pm2.meta_value  < $timestamp ) )
                 GROUP BY ywsbs_p.ID ORDER BY ywsbs_p.ID DESC
                ", 'ywsbs_subscription' )) ;


           if( !empty( $subscriptions ) ){
                foreach ( $subscriptions as $subscription_post ) {
                    $subscription = new YWSBS_Subscription( $subscription_post->ID );
                    $subscription->update_status( 'expired' );
                    YITH_WC_Activity()->add_activity( $subscription->id, 'expired', 'success', $subscription->order_id, __('The subscription has been cancelled because it has expired','yith-woocommerce-subscription') );
                }
            }
        }

        /**
         * Check if there are email to send as renew reminder
         *
         * @since  1.0.0
         * @author Emanuela Castorina
         */
        public function ywsbs_trigger_email_renew_reminder() {

            global $wpdb;

            $enabled = apply_filters('ywsbs_enable_email_renew_reminder', false );
			$product_ids =  implode( ',', apply_filters('ywsbs_enable_email_renew_reminder_products', array() ) ) ;

	        if ( ! $enabled || empty( $product_ids ) ) {
	            return false;
	        }

            //get all subscriptions that have status active and _expired_data < NOW to cancel the subscription
	        $to = current_time( 'timestamp' ) + apply_filters( 'ywsbs_enable_email_renew_reminder_time', 30 * 86400 );
	        $from = $to - 86400;

	        $subscriptions = $wpdb->get_results( $wpdb->prepare( "SELECT ywsbs_p.ID FROM {$wpdb->prefix}posts as ywsbs_p
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm ON ( ywsbs_p.ID = ywsbs_pm.post_id )
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm2 ON ( ywsbs_p.ID = ywsbs_pm2.post_id )
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm3 ON ( ywsbs_p.ID = ywsbs_pm3.post_id )
                 WHERE ( ywsbs_pm.meta_key='status' AND  ywsbs_pm.meta_value = 'active' )
                 AND ywsbs_p.post_type = %s
                 AND ywsbs_p.post_status = 'publish'
                 AND ( ywsbs_pm2.meta_key='payment_due_date' AND  ( ywsbs_pm2.meta_value  < $to AND  ywsbs_pm2.meta_value  >= $from ) )
                 AND ( ywsbs_pm3.meta_key='product_id' AND  ywsbs_pm3.meta_value IN ( {$product_ids}) )
                 GROUP BY ywsbs_p.ID ORDER BY ywsbs_p.ID DESC
                ", 'ywsbs_subscription' )) ;


           if( !empty( $subscriptions ) ){
                foreach ( $subscriptions as $subscription_post ) {
                    $subscription = new YWSBS_Subscription( $subscription_post->ID );
                    do_action('ywsbs_customer_subscription_renew_reminder_mail', $subscription);
                }
            }
        }

        /**
         * Check if there are subscription expired and change the status to expired
         *
         * @since  1.0.0
         * @author Emanuela Castorina
         */
        public function ywsbs_trigger_email_before_subscription_expired() {

            global $wpdb;

            //get all subscriptions that have status active and _expired_data < NOW to cancel the subscription
	        $timestamp = current_time( 'timestamp' ) + apply_filters( 'ywsbs_trigger_email_before', 86400 );
            $subscriptions = $wpdb->get_results( $wpdb->prepare( "SELECT ywsbs_p.ID FROM {$wpdb->prefix}posts as ywsbs_p
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm ON ( ywsbs_p.ID = ywsbs_pm.post_id )
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm2 ON ( ywsbs_p.ID = ywsbs_pm2.post_id )
                 WHERE ( ywsbs_pm.meta_key='status' AND  ywsbs_pm.meta_value = 'active' )
                 AND ywsbs_p.post_type = %s
                 AND ywsbs_p.post_status = 'publish'
                 AND ( ywsbs_pm2.meta_key='expired_date' AND  ( ywsbs_pm2.meta_value <> '' AND ywsbs_pm2.meta_value  < $timestamp ) )
                 GROUP BY ywsbs_p.ID ORDER BY ywsbs_p.ID DESC
                ", 'ywsbs_subscription' )) ;


           if( !empty( $subscriptions ) ){
                foreach ( $subscriptions as $subscription_post ) {
                    $subscription = new YWSBS_Subscription( $subscription_post->ID );
                    do_action('ywsbs_customer_subscription_before_expired_mail', $subscription);
                }
            }
        }

        /**
         * Check if there are subscription with payment in pending and change the status to overdue or suspended or remove the subscription
         *
         * @since  1.0.0
         * @author Emanuela Castorina
         */
        public function ywsbs_check_subscription_payment() {

            global $wpdb;

            //get all subscriptions that have status active and _payment_due_date < NOW to cancel the subscription
            $start_period = get_option('ywsbs_cancel_start_period');

            $offset = ! empty( $start_period ) ? $start_period : 0;

            if( YITH_WC_Subscription()->overdue_time() ){
                $offset = get_option('ywsbs_overdue_start_period');
            }

            if( YITH_WC_Subscription()->suspension_time() ){
                $offset = get_option('ywsbs_enable_suspension_period');
            }

            $timestamp = current_time('timestamp') - $offset * 3600;


            $subscriptions = $wpdb->get_results( $wpdb->prepare( "SELECT ywsbs_p.ID FROM {$wpdb->prefix}posts as ywsbs_p
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm ON ( ywsbs_p.ID = ywsbs_pm.post_id )
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm2 ON ( ywsbs_p.ID = ywsbs_pm2.post_id )
                 WHERE ( ywsbs_pm.meta_key='status' AND  ywsbs_pm.meta_value = 'active' )
                 AND ywsbs_p.post_type = %s
                 AND ywsbs_p.post_status = 'publish'
                 AND ( ywsbs_pm2.meta_key='payment_due_date' AND  ywsbs_pm2.meta_value  < $timestamp )
                 GROUP BY ywsbs_p.ID ORDER BY ywsbs_p.ID DESC
                ", 'ywsbs_subscription' )) ;


            if( !empty( $subscriptions ) ){
                foreach ( $subscriptions as $subscription_post ) {
                    $subscription = new YWSBS_Subscription( $subscription_post->ID );


                    //if the subscription have a payment with a gateway and the option suspend for failed recurring payment is checked
                    // the plugin doesn't change the status of the subscription. If the gateway will send the failed payment the subscription will
                    // change the status to suspended
                    if ( get_option( 'ywsbs_suspend_for_failed_recurring_payment' ) == 'yes' && in_array( $subscription->payment_method, ywsbs_get_gateways_list() ) ) {
                        continue;
                    }

                    if( YITH_WC_Subscription()->overdue_time() ){
                        $subscription->update_status( 'overdue' );
                        YITH_WC_Activity()->add_activity( $subscription->id, 'overdue', 'success', $subscription->order_id, __('The subscription is overdue because the payment has not been received','yith-woocommerce-subscription') );
                        continue;
                    }

                    if( YITH_WC_Subscription()->suspension_time() ){
                        $subscription->update_status( 'suspended' );
                        YITH_WC_Activity()->add_activity( $subscription->id, 'suspended', 'success', $subscription->order_id, __('The subscription is suspended because the payment has not been received','yith-woocommerce-subscription') );
                        continue;
                    }

                    YITH_WC_Activity()->add_activity( $subscription->id, 'auto-cancelled', 'success', $subscription->order_id, __('The subscription is cancelled because the payment has not been received','yith-woocommerce-subscription') );
                    $subscription->cancel();
                }
            }
        }

        /**
         * Check if there are subscription with payment in pending and change the status to overdue or suspended or remove the subscription
         *
         * @since  1.0.0
         * @author Emanuela Castorina
         */
        public function ywsbs_check_overdue_subscriptions() {

            global $wpdb;

            //get all subscriptions that have status overdue and _payment_due_date < NOW+ overdue period
            $overdue_period = YITH_WC_Subscription()->overdue_time();
            $timestamp = current_time('timestamp') - $overdue_period;

            $subscriptions = $wpdb->get_results( $wpdb->prepare( "SELECT ywsbs_p.ID FROM {$wpdb->prefix}posts as ywsbs_p
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm ON ( ywsbs_p.ID = ywsbs_pm.post_id )
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm2 ON ( ywsbs_p.ID = ywsbs_pm2.post_id )
                 WHERE ( ywsbs_pm.meta_key='status' AND  ywsbs_pm.meta_value = 'overdue' )
                 AND ywsbs_p.post_type = %s
                 AND ywsbs_p.post_status = 'publish'
                 AND ( ywsbs_pm2.meta_key='payment_due_date' AND  ywsbs_pm2.meta_value  < $timestamp )
                 GROUP BY ywsbs_p.ID ORDER BY ywsbs_p.ID DESC
                ", 'ywsbs_subscription' )) ;


	        if ( ! empty( $subscriptions ) ) {
		        foreach ( $subscriptions as $subscription_post ) {
			        $subscription = new YWSBS_Subscription( $subscription_post->ID );

			        if ( YITH_WC_Subscription()->suspension_time() ) {
				        $subscription->update_status( 'suspended' );
				        YITH_WC_Activity()->add_activity( $subscription->id, 'suspended', 'success', $subscription->order_id, __( 'The subscription is suspended because the overdue period has finished.', 'yith-woocommerce-subscription' ) );
				        continue;
			        }

			        YITH_WC_Activity()->add_activity( $subscription->id, 'auto-cancelled', 'success', $subscription->order_id, __( 'The subscription has been cancelled because the overdue period has finished.', 'yith-woocommerce-subscription' ) );
			        $subscription->cancel();
		        }
	        }
        }

        /**
         * Check if there are subscription with status suspended and if the period of suspension is expired
         *
         * @since  1.0.0
         * @author Emanuela Castorina
         */
        public function ywsbs_check_suspended_subscriptions() {

            global $wpdb;

            //get all subscriptions that have status active and _expired_data < NOW to cancel the subscription

            $suspension_time = YITH_WC_Subscription()->suspension_time();
            $timestamp = current_time('timestamp') - $suspension_time;
            $subscriptions = $wpdb->get_results( $wpdb->prepare( "SELECT ywsbs_p.ID FROM {$wpdb->prefix}posts as ywsbs_p
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm ON ( ywsbs_p.ID = ywsbs_pm.post_id )
                 INNER JOIN  {$wpdb->prefix}postmeta as ywsbs_pm2 ON ( ywsbs_p.ID = ywsbs_pm2.post_id )
                 WHERE ( ywsbs_pm.meta_key='status' AND  ywsbs_pm.meta_value = 'suspended' )
                 AND ywsbs_p.post_type = %s
                 AND ywsbs_p.post_status = 'publish'
                 AND ( ywsbs_pm2.meta_key='payment_due_date' AND  ywsbs_pm2.meta_value  < $timestamp )
                 GROUP BY ywsbs_p.ID ORDER BY ywsbs_p.ID DESC", 'ywsbs_subscription' )) ;


            if( !empty( $subscriptions ) ){
                foreach ( $subscriptions as $subscription_post ) {
                    $subscription = new YWSBS_Subscription( $subscription_post->ID );
                    $subscription->cancel();
                    YITH_WC_Activity()->add_activity( $subscription->id, 'cancelled', 'success', $subscription->order_id, __('The subscription has been cancelled because the suspension period has finished.','yith-woocommerce-subscription') );
                }
            }

        }



    }
}

/**
 * Unique access to instance of YWSBS_Subscription_Cron class
 *
 * @return \YWSBS_Subscription_Cron
 */
function YWSBS_Subscription_Cron() {
    return YWSBS_Subscription_Cron::get_instance();
}

