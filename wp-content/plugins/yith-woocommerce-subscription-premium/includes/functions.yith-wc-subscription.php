<?php
if ( ! defined( 'ABSPATH' ) || ! defined( 'YITH_YWSBS_VERSION' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Implements helper functions for YITH WooCommerce Subscription
 *
 * @package YITH WooCommerce Subscription
 * @since   1.0.0
 * @author  Yithemes
 */

global $yith_ywsbs_db_version;

$yith_ywsbs_db_version = '1.0.0';

if ( ! function_exists( 'yith_ywsbs_db_install' ) ) {

    /**
     * Install the table yith_ywsbs_activities_log
     *
     * @return void
     * @since 1.0.0
     */

    function yith_ywsbs_db_install() {
        global $wpdb;
        global $yith_ywsbs_db_version;

        $installed_ver = get_option( "yith_ywsbs_db_version" );

        if ( $installed_ver != $yith_ywsbs_db_version ) {

            $table_name = $wpdb->prefix . 'yith_ywsbs_activities_log';

            $charset_collate = $wpdb->get_charset_collate();

            $sql = "CREATE TABLE $table_name (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`activity` varchar(255) NOT NULL,
		`status` varchar(255) NOT NULL,
		`subscription` int(11) NOT NULL,
		`order` int(11) NOT NULL,
		`description` varchar(255) NOT NULL,
		`timestamp_date` datetime NOT NULL,
		PRIMARY KEY (id)
		) $charset_collate;";

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );

            add_option( 'yith_ywsbs_db_version', $yith_ywsbs_db_version );
        }
    }
}


if ( ! function_exists( 'yith_ywsbs_update_db_check' ) ) {


    /**
     * Check if the function yith_ywsbs_db_install must be installed or updated
     *
     * @return void
     * @since 1.0.0
     */

    function yith_ywsbs_update_db_check() {
        global $yith_ywsbs_db_version;

        if ( get_site_option( 'yith_ywsbs_db_version' ) != $yith_ywsbs_db_version ) {
            yith_ywsbs_db_install();
        }
    }
}


if( !function_exists('ywsbs_get_time_options')){

    /**
     * Return the list of time options to add in product editor panel
     *
     *
     * @return array
     * @since 1.0.0
     */

    function ywsbs_get_time_options(){
        $options = array(
            'days'   => __( 'days', 'yith-woocommerce-subscription' ),
            'weeks'  => __( 'weeks', 'yith-woocommerce-subscription' ),
            'months' => __( 'months', 'yith-woocommerce-subscription' ),
            'years'  => __( 'years', 'yith-woocommerce-subscription' ),
        );

        return apply_filters('ywsbs_time_options', $options);
    }
}

if ( ! function_exists( 'ywsbs_get_paypal_limit_options' ) ) {

    /**
     * Return the list of time options with the max value that paypal accept
     *
     *
     * @return array
     * @since 1.0.0
     */

    function ywsbs_get_paypal_limit_options() {
        $options = array(
            'days'   => 90,
            'weeks'  => 52,
            'months' => 24,
            'years'  => 5,
        );

        return apply_filters( 'ywsbs_paypal_limit_options', $options );
    }
}

if ( ! function_exists( 'ywsbs_get_price_time_option_paypal' ) ) {

    /**
     * Return the list of time options to add in product editor panel
     *
     * @param $time_option string
     *
     * @return array
     * @since 1.0.0
     */

    function ywsbs_get_price_time_option_paypal( $time_option ) {
        $options = array(
            'days'   => 'D',
            'weeks'  => 'W',
            'months' => 'M',
            'years'  => 'Y'
        );

        return isset( $options[ $time_option ] ) ? $options[ $time_option ] : '';
    }
}

if ( !function_exists( 'yith_ywsbs_locate_template' ) ) {

    /**
     * Locate the templates and return the path of the file found
     *
     * @param string $path
     * @param array  $var
     *
     * @return string
     * @since 1.0.0
     */

    function yith_ywsbs_locate_template( $path, $var = NULL ) {

        global $woocommerce;

        if ( function_exists( 'WC' ) ) {
            $woocommerce_base = WC()->template_path();
        }
        elseif ( defined( 'WC_TEMPLATE_PATH' ) ) {
            $woocommerce_base = WC_TEMPLATE_PATH;
        }
        else {
            $woocommerce_base = $woocommerce->plugin_path() . '/templates/';
        }

        $template_woocommerce_path = $woocommerce_base . $path;
        $template_path             = '/' . $path;
        $plugin_path               = YITH_YWSBS_DIR . 'templates/' . $path;

        $located = locate_template( array(
            $template_woocommerce_path, // Search in <theme>/woocommerce/
            $template_path,             // Search in <theme>/
            $plugin_path                // Search in <plugin>/templates/
        ) );

        if ( !$located && file_exists( $plugin_path ) ) {
            return apply_filters( 'yith_ywsbs_locate_template', $plugin_path, $path );
        }

        return apply_filters( 'yith_ywsbs_locate_template', $located, $path );
    }
}

if ( ! function_exists( 'ywsbs_add_date' ) ) {

    /**
     * Add day, months or year to a date
     *
     * @param int $given_date
     * @param int $day
     * @param int $mth
     * @param int $yr
     *
     * @return string
     * @since 1.0.0
     */

    function ywsbs_add_date( $given_date, $day = 0, $mth = 0, $yr = 0 ) {
        $new_date = $given_date;
        $new_date = strtotime( "+".$day." days", $new_date );
        $new_date = strtotime( "+".$mth." month", $new_date );
        $new_date = strtotime( "+".$yr." year", $new_date );
        return $new_date;
    }
}

if( !function_exists('ywsbs_get_timestamp_from_option')){

    /**
     * Add a date to a timestamp
     *
     * @param int    $time_from
     * @param int    $qty
     * @param string $time_opt
     *
     * @return string
     * @since 1.0.0
     */

    function ywsbs_get_timestamp_from_option( $time_from, $qty, $time_opt){

        $timestamp = 0;
        switch( $time_opt ){
            case 'days':
               $timestamp = ywsbs_add_date( $time_from, intval($qty));
                break;
            case 'weeks':
                $timestamp = ywsbs_add_date( $time_from, intval($qty) * 7);
                break;
            case 'months':
                $timestamp = ywsbs_add_date( $time_from, 0, intval($qty));
                break;
            case 'years':
                $timestamp = ywsbs_add_date( $time_from, 0, 0, intval($qty));
                break;
            default:
        }

        return $timestamp;
    }
}


if ( ! function_exists( 'ywsbs_get_max_length_period' ) ) {

    /**
     * Return the max length of period that can be accepted from paypal
     *
     *
     * @return string
     * @internal param int $time_from
     * @internal param int $qty
     * @since    1.0.0
     */

    function ywsbs_get_max_length_period() {

        $max_length = array(
            'days'   => 90,
            'weeks'  => 52,
            'months' => 24,
            'years'  => 5
        );

        return apply_filters( 'ywsbs_get_max_length_period', $max_length );

    }
}

if ( ! function_exists( 'ywsbs_validate_max_length' ) ) {

    /**
     * Return the max length of period that can be accepted from paypal
     *
     *
     * @param int    $max_length
     * @param string $time_opt
     *
     * @return int
     * @since    1.0.0
     */

    function ywsbs_validate_max_length( $max_length, $time_opt ) {

        $max_lengths = ywsbs_get_max_length_period();
        $max_length  = ( $max_length > $max_lengths[$time_opt] ) ? $max_lengths[$time_opt] : $max_length;

        return $max_length;
    }
}

if ( ! function_exists( 'ywsbs_get_status' ) ) {

    /**
     * Return the list of status available
     *
     * @return array
     * @since 1.0.0
     */

    function ywsbs_get_status() {
        $options = array(
            'active'     => __( 'active', 'yith-woocommerce-subscription' ),
            'paused'     => __( 'paused', 'yith-woocommerce-subscription' ),
            'pending'    => __( 'pending', 'yith-woocommerce-subscription' ),
            'overdue'    => __( 'overdue', 'yith-woocommerce-subscription' ),
            'trial'      => __( 'trial', 'yith-woocommerce-subscription' ),
            'cancelled'  => __( 'cancelled', 'yith-woocommerce-subscription' ),
            'expired'    => __( 'expired', 'yith-woocommerce-subscription' ),
            'suspended'  => __( 'suspended', 'yith-woocommerce-subscription' ),
        );

        return apply_filters( 'ywsbs_status', $options );
    }
}


if ( ! function_exists( 'ywsbs_get_from_list' ) ) {

    /**
     * Return the list of whi make action on subscriptioon
     *
     * @return array
     * @since 1.0.0
     */

    function ywsbs_get_from_list() {
        $options = array(
            'customer'      => __( 'customer', 'yith-woocommerce-subscription' ),
            'administrator' => __( 'administrator', 'yith-woocommerce-subscription' ),
            'gateway'       => __( 'gateway', 'yith-woocommerce-subscription' ),
        );

        return apply_filters( 'ywsbs_from_list', $options );
    }
}

if( !function_exists('ywsbs_get_subscription')){

    /**
     * Return the subscription object
     *
     * @param int  $subscription_id
     *
     * @return YWSBS_Subscription
     * @since 1.0.0
     */

    function ywsbs_get_subscription( $subscription_id ) {
        return new YWSBS_Subscription( $subscription_id );
    }
}

if ( ! function_exists( 'ywsbs_get_attribute_name' ) ) {

    /**
     * Return the name of a variation product with the price
     *
     * @param array $variation
     *
     * @return string
     * @since 1.0.0
     */

    function ywsbs_get_attribute_name( $variation ) {
        $var          = wc_get_product( $variation['variation_id'] );
        $label_string = '';
        if ( ! empty( $variation['attributes'] ) ) {
            foreach ( $variation['attributes'] as $key => $value ) {
                $label_string .= $value . ' ';
            }

            $label_string .= $var->get_price_html();
        }

        return $label_string;
    }
}

if ( !function_exists( 'yith_ywsbs_get_product_meta' ) ) {

    /**
     * Return the product meta in a variation product
     *
     * @param   array $subscription
     * @param   array $attributes
     * @param   bool  $echo
     *
     * @return string
     * @since 1.0.0
     */

    function yith_ywsbs_get_product_meta( $subscription, $attributes = array(), $echo = true ) {

        $item_data = array();

        if ( ! empty( $subscription->variation_id ) ) {
            $variation = wc_get_product( $subscription->variation_id );

            if ( empty( $attributes ) ) {
                $attributes = $variation->get_variation_attributes();
            }

            foreach ( $attributes as $name => $value ) {
                if ( '' === $value ) {
                    continue;
                }

                $taxonomy = wc_attribute_taxonomy_name( str_replace( 'attribute_pa_', '', urldecode( $name ) ) );

                // If this is a term slug, get the term's nice name
                if ( taxonomy_exists( $taxonomy ) ) {
                    $term = get_term_by( 'slug', $value, $taxonomy );
                    if ( ! is_wp_error( $term ) && $term && $term->name ) {
                        $value = $term->name;
                    }
                    $label = wc_attribute_label( $taxonomy );

                } else {
                    if ( strpos( $name, 'attribute_' ) !== false ) {
                        $custom_att = str_replace( 'attribute_', '', $name );
                        if ( $custom_att != '' ) {
                            $label = wc_attribute_label( $custom_att );
                        } else {
                            $label = apply_filters( 'woocommerce_attribute_label', wc_attribute_label( $name ), $name );
                        }
                    }
                }

                $item_data[] = array(
                    'key'   => $label,
                    'value' => $value
                );
            }

        }

        $item_data = apply_filters( 'ywsbs_item_data', $item_data, $subscription );
        $out       = "";
        // Output flat or in list format
        if ( sizeof( $item_data ) > 0 ) {
            foreach ( $item_data as $data ) {
                if ( $echo ) {
                    echo esc_html( $data['key'] ) . ': ' . wp_kses_post( $data['value'] ) . "\n";
                } else {
                    $out .= ' - ' . esc_html( $data['key'] ) . ': ' . wp_kses_post( $data['value'] ) . ' ';
                }
            }
        }

        return $out;

    }
}

if( !function_exists('ywsbs_time')){

    /**
     * Return the timestamp of the day + 1 second
     *
     *
     * @return int
     * @since 1.0.0
     */

    function ywsbs_time() {
        $timestamp = mktime( 0, 0, 1 );
        return $timestamp;
    }

}

if( !function_exists('ywsbs_get_days')){

    /**
     * Return the days from timestamp
     *
     * @param $timestamp int
     *
     * @return int
     * @since 1.0.0
     */

    function ywsbs_get_days( $timestamp ) {
        $days = ceil( $timestamp / 86400 );
        return $days;
    }

}

if( !function_exists('ywsbs_get_price_per_string')){

    /**
     * Return the days from timestamp
     *
     * @param $timestamp int
     *
     * @return int
     * @since 1.0.0
     */

    function ywsbs_get_price_per_string( $price_per, $time_option ) {
        $price_html = ( ( $price_per == 1 ) ? '' : $price_per ) . ' ';

        switch( $time_option ){
            case 'days':
                $price_html .= _n( 'day', 'days', $price_per, 'yith-woocommerce-subscription' );
                break;
            case 'weeks':
                $price_html .= _n( 'week', 'weeks', $price_per, 'yith-woocommerce-subscription' );
                break;
            case 'months':
                $price_html .= _n( 'month', 'months', $price_per, 'yith-woocommerce-subscription' );
                break;
            case 'years':
                $price_html .= _n( 'year', 'years', $price_per, 'yith-woocommerce-subscription' );
                break;
            default:
        }

        return $price_html;
    }

}


if ( ! function_exists( 'ywsbs_get_gateways_list' ) ) {

	/**
     * Return the list of gateways compatible with plugin
     *
     * @return array
     */

    function ywsbs_get_gateways_list() {
        return apply_filters( 'ywsbs_get_gateways_list', array( 'paypal', 'yith-stripe' ) );
    }

}


if ( ! function_exists( 'ywsbs_get_max_failed_attemps_list' ) ) {

    /**
     * Return the list of max failed attemps for each compatible gateways
     *
     * @return array
     */

    function ywsbs_get_max_failed_attemps_list() {
        $arg = array(
            'paypal'      => 3,
            'yith-stripe' => 4
        );

        return apply_filters( 'ywsbs_max_failed_attemps_list', $arg );
    }

}

if ( ! function_exists( 'ywsbs_get_num_of_days_between_attemps' ) ) {

    /**
     * Return the list of max failed attemps for each compatible gateways
     *
     * @return array
     */

    function ywsbs_get_num_of_days_between_attemps() {
        $arg = array(
            'paypal'      => 5,
            'yith-stripe' => 5
        );

        return apply_filters( 'ywsbs_get_num_of_days_between_attemps', $arg );
    }

}


if ( ! function_exists( 'ywsbs_subscription_status_product_check' ) ) {

    /**
     * Return the list of max failed attemps for each compatible gateways
     *
     * @return string | bool
     */

    function ywsbs_subscription_status_product_check( $product, $user_id, $order_id) {

        $status = false;

        if ( ! is_object( $product ) ) {
            $product = wc_get_product( $product );
        }

        $subscriptions_users = YWSBS_Subscription_Helper()->get_subscriptions_by_user( $user_id );

        if( !empty( $subscriptions_users ) ){
            
            foreach ( $subscriptions_users as $subscription_post ) {
                $subscription = new YWSBS_Subscription( $subscription_post->ID );

                if( $subscription->order_id == $order_id && $subscription->product_id == $product->get_id() ){
                        $status = $subscription->status;
                }
            }
        }

        return apply_filters( 'ywsbs_subscription_status_product_check', $status, $product, $user_id, $order_id );
    }

}

if ( ! function_exists ( 'ywsbs_coupon_is_valid' ) ) {

	/**
	 * Check if a coupon is valid
	 * @param $coupon
	 * @param array $object
	 *
	 * @return bool|WP_Error
	 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
	 */
	function ywsbs_coupon_is_valid ( $coupon, $object = array() ) {
		if ( version_compare( WC()->version, '3.2.0', '>=' ) ) {
			$wc_discounts = new WC_Discounts( $object );
			$valid        = $wc_discounts->is_coupon_valid( $coupon );
			$valid        = is_wp_error( $valid ) ? false : $valid;
		}else{
			$valid = $coupon->is_valid();
		}

		return $valid;
	}

}

if ( ! function_exists ( 'ywsbs_get_applied_coupons' ) ) {


	/**
	 * @param $cart WC_Cart
	 *
	 * @return array
	 * @author Emanuela Castorina <emanuela.castorina@yithemes.com>
	 */
	function ywsbs_get_applied_coupons ( $cart ) {
		if ( version_compare( WC()->version, '3.2.0', '>=' ) ) {
			$coupons = array();
			$coupons_id = $cart->get_applied_coupons();
			if( $coupons_id ){
				foreach ( $coupons_id as $coupon_code ) {
					$coupons[] = new WC_Coupon( $coupon_code );
				}
			}

		}else{
			$coupons = $cart->coupon;
		}

		return $coupons;
	}

}