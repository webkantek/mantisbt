<?php

if ( ! defined( 'ABSPATH' ) || ! defined( 'YITH_YWSBS_VERSION' ) ) {
	exit; // Exit if accessed directly
}


/**
 * YWSBS_Multivendor class to add compatibility with YITH WooCommerce Multivendor
 *
 * @class   YWSBS_Multivendor
 * @package YITH WooCommerce Subscription
 * @since   1.0.0
 * @author  Yithemes
 */
if ( ! class_exists( 'YWSBS_Multivendor' ) && function_exists( 'YITH_Vendors' ) ) {

	class YWSBS_Multivendor {

		/**
		 * Single instance of the class
		 *
		 * @var \YWSBS_Multivendor
		 */
		protected static $instance;



		/**
		 * Returns single instance of the class
		 *
		 * @return \YWSBS_Multivendor
		 * @since 1.0.0
		 */
		public static function get_instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * Initialize class and registers actions and filters to be used
		 *
		 * @since  1.0.0
		 * @author Emanuela Castorina
		 */
		public function __construct() {
			add_filter( 'yith_wcmv_register_taxonomy_object_type', array( $this, 'remove_vendor_taxonomy_from_subscription_object_type' ), 20 );
			add_action( 'init', array( $this, 'init' ), 20 );
		}

		public function init(){
			$vendor = yith_get_vendor( 'current', 'user' );

			if( $vendor->is_valid() && $vendor->has_limited_access() ){
				if( 'yes' == get_option( 'yith_wpv_vendors_option_subscription_management', 'no' ) ){
					add_action( 'admin_menu', array($this,'vendor_admin_init'), 4);
					add_filter( 'yith_wpv_vendors_allowed_post_types', array( $this, 'add_allowed_post_types_for_vendors' ) );
					add_filter( 'yith_wcmv_subscription_caps', array( $this, 'add_subscription_cap' ) );
				} else{
					remove_action( 'woocommerce_variation_options', array( YITH_WC_Subscription_Admin(), 'add_type_variation_options' ), 10, 3 );
					remove_filter( 'product_type_options', array( YITH_WC_Subscription_Admin(), 'add_type_options' ) );
				}
			}
		}

		public function add_subscription_cap( $caps ){
			return array( 'edit_subscription' );
		}

		public function vendor_admin_init( ) {
			$vendor = yith_get_vendor( 'current', 'user' );

			if ( $vendor->is_valid() && $vendor->has_limited_access() ) {
				add_filter( 'ywsbs_register_panel_create_menu_page', array( $this, 'admin_vendor_create_menu_page' ) );
				add_filter( 'ywsbs_register_panel_parent_page', array( $this, 'admin_vendor_parent_page' ) );
				add_filter( 'ywsbs_register_panel_capabilities', array( $this, 'admin_vendor_register_panel_capabilities' ) );
				add_filter( 'ywsbs_register_panel_tabs', array( $this, 'admin_vendor_register_panel_tabs' ) );


				add_filter( 'ywsbs_subscriptions_list_table_join', array( $this, 'admin_vendor_subscriptions_list_table_join' ) );
				add_filter( 'ywsbs_subscriptions_list_table_where', array( $this, 'admin_vendor_subscriptions_list_table_where' ) );
				add_filter( 'ywsbs_activities_list_table_join', array( $this, 'admin_vendor_activities_list_table_join' ), 10, 2 );
				add_filter( 'ywsbs_activities_list_table_where', array( $this, 'admin_vendor_activities_list_table_where' ), 10, 2 );

				/* Add/Remove Subscription capabilities to vendors */



			}
		}

		/**
		 * Permit vendor to see the subscription menu in administration panel
		 *
		 * @access   public
		 *
		 * @param bool $create_menu_page
		 *
		 * @return bool
		 * @since    1.0.0
		 */
		public function admin_vendor_create_menu_page( $create_menu_page ) {

			return true;
		}

		/**
		 * Permit vendor to see the subscription menu in administration panel
		 *
		 * @access public
		 *
		 * @param string $parent_page
		 *
		 * @return string
		 * @since  1.0.0
		 */
		public function admin_vendor_parent_page( $parent_page ) {
			return '';
		}

		/**
		 * Permit vendor to see the subscription and activities panel
		 *
		 * @access public
		 *
		 * @param array $capabilities
		 *
		 * @return array
		 * @since  1.0.0
		 */
		public function admin_vendor_register_panel_capabilities( $capabilities ) {

			$capabilities = 'manage_vendor_store';
			return $capabilities;
		}

		/**
		 * Permit vendor to see the subscription and activities panel
		 *
		 * @access public
		 *
		 * @param array $tabs
		 *
		 * @return array
		 * @since  1.0.0
		 */
		public function admin_vendor_register_panel_tabs( $tabs ) {

			$tabs = array(
				'subscriptions' => __( 'Subscriptions', 'yith-woocommerce-subscription' ),
				'activities'    => __( 'Activities', 'yith-woocommerce-subscription' ),

			);

			return $tabs;
		}

		/**
		 * Permit vendor to see the subscription and activities panel
		 *
		 * @access public
		 *
		 * @param array $join
		 *
		 * @return string
		 * @since  1.0.0
		 */
		public function admin_vendor_subscriptions_list_table_join( $join ) {
			global $wpdb;

			$join .= " LEFT JOIN " . $wpdb->prefix . "postmeta as sub_pm ON ( ywsbs_p.ID = sub_pm.post_id ) ";
		//	$join .= " LEFT JOIN " . $wpdb->prefix . "postmeta as sub_pm2 ON ( ywsbs_p.ID = sub_pm2.post_id AND ( sub_pm2.meta_key='variation_id') ) ";

			return $join;
		}

		/**
		 * Permit vendor to see the subscription and activities panel
		 *
		 * @access public
		 *
		 * @param array $where
		 *
		 * @return string
		 * @since  1.0.0
		 */
		public function admin_vendor_subscriptions_list_table_where( $where ) {
			$vendor = yith_get_vendor( 'current', 'user' );
			$products = $vendor->get_products();
			$where .= " AND ( sub_pm.meta_key='product_id' AND sub_pm.meta_value IN  (". implode(',',$products).")  ) " ;

			return $where;
		}

		/**
		 * Permit vendor to see the subscription and activities panel
		 *
		 * @access public
		 *
		 * @param array $join
		 *
		 * @return string
		 * @since  1.0.0
		 */
		public function admin_vendor_activities_list_table_join( $join, $tablename ) {
			global $wpdb;

			$join .= " LEFT JOIN " . $wpdb->prefix . "postmeta as sub_pm ON ( act.subscription = sub_pm.post_id ) ";
			$join .= " LEFT JOIN " . $wpdb->prefix . "posts as sub_p ON ( sub_p.ID = act.subscription ) ";
			//	$join .= " LEFT JOIN " . $wpdb->prefix . "postmeta as sub_pm2 ON ( ywsbs_p.ID = sub_pm2.post_id AND ( sub_pm2.meta_key='variation_id') ) ";

			return $join;
		}

		/**
		 * Permit vendor to see the subscription and activities panel
		 *
		 * @access public
		 *
		 * @param array $where
		 *
		 * @return string
		 * @since  1.0.0
		 */
		public function admin_vendor_activities_list_table_where( $where, $tablename ) {
			$vendor = yith_get_vendor( 'current', 'user' );
			$products = $vendor->get_products();
			$where .= " AND ( sub_pm.meta_key='product_id' AND sub_pm.meta_value IN  (". implode(',',$products).")  ) " ;

			return $where;
		}



		/**
		 * Add Allowed Post Types for Vendors
		 *
		 * @param array $allowed_post_types the allowed post types for Vendors; default are 'product' and 'shop_coupon'
		 *
		 * @since  1.0.0
		 * @return array
		 */
		public function add_allowed_post_types_for_vendors( $allowed_post_types ) {

			$allowed_post_types[ ] = 'ywsbs_subscription';

			return $allowed_post_types;
		}

		public function remove_vendor_taxonomy_from_subscription_object_type( $object_type ){
			$key = array_search( 'ywsbs_subscription', $object_type );

			if( $key !== false ){
				unset( $object_type[ $key ] );
			}

			return $object_type;
		}

	}

}

/**
 * Unique access to instance of YWSBS_Multivendor class
 *
 * @return \YWSBS_Multivendor
 */
function YWSBS_Multivendor() {
	return YWSBS_Multivendor::get_instance();
}
