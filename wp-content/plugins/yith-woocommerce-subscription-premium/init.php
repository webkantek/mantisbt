<?php
/*
Plugin Name: YITH WooCommerce Subscription Premium
Description: It allows you to manage recurring payments for product subscription that grant you constant periodical income
Version: 1.2.9
Author: YITHEMES
Author URI: http://yithemes.com/
Text Domain: yith-woocommerce-subscription
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

/*
 * @package YITH WooCommerce Subscription
 * @since   1.0.0
 * @author  YITHEMES
 */


if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

if ( ! function_exists( 'is_plugin_active' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}

// Free version deactivation if installed __________________
if( ! function_exists( 'yit_deactive_free_version' ) ) {
    require_once 'plugin-fw/yit-deactive-plugin.php';
}
yit_deactive_free_version( 'YITH_YWSBS_FREE_INIT', plugin_basename( __FILE__ ) );



if ( ! defined( 'YITH_YWSBS_DIR' ) ) {
    define( 'YITH_YWSBS_DIR', plugin_dir_path( __FILE__ ) );
}


/* Plugin Framework Version Check */
if( ! function_exists( 'yit_maybe_plugin_fw_loader' ) && file_exists( YITH_YWSBS_DIR . 'plugin-fw/init.php' ) ) {
    require_once( YITH_YWSBS_DIR . 'plugin-fw/init.php' );
}
yit_maybe_plugin_fw_loader( YITH_YWSBS_DIR  );


// Define constants ________________________________________
if ( defined( 'YITH_YWSBS_VERSION' ) ) {
    return;
}else{
    define( 'YITH_YWSBS_VERSION', '1.2.9' );
}

if ( ! defined( 'YITH_YWSBS_PREMIUM' ) ) {
    define( 'YITH_YWSBS_PREMIUM', plugin_basename( __FILE__ ) );
}

if ( ! defined( 'YITH_YWSBS_INIT' ) ) {
    define( 'YITH_YWSBS_INIT', plugin_basename( __FILE__ ) );
}

if ( ! defined( 'YITH_YWSBS_FILE' ) ) {
    define( 'YITH_YWSBS_FILE', __FILE__ );
}

if ( ! defined( 'YITH_YWSBS_URL' ) ) {
    define( 'YITH_YWSBS_URL', plugins_url( '/', __FILE__ ) );
}

if ( ! defined( 'YITH_YWSBS_ASSETS_URL' ) ) {
    define( 'YITH_YWSBS_ASSETS_URL', YITH_YWSBS_URL . 'assets' );
}

if ( ! defined( 'YITH_YWSBS_TEMPLATE_PATH' ) ) {
    define( 'YITH_YWSBS_TEMPLATE_PATH', YITH_YWSBS_DIR . 'templates' );
}

if ( ! defined( 'YITH_YWSBS_INC' ) ) {
    define( 'YITH_YWSBS_INC', YITH_YWSBS_DIR . '/includes/' );
}

if ( ! defined( 'YITH_YWSBS_SUFFIX' ) ) {
    $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
    define( 'YITH_YWSBS_SUFFIX', $suffix );
}

if ( ! defined( 'YITH_YWSBS_SLUG' ) ) {
    define( 'YITH_YWSBS_SLUG', 'yith-woocommerce-subscription' );
}

if ( ! defined( 'YITH_YWSBS_SECRET_KEY' ) ) {
    define( 'YITH_YWSBS_SECRET_KEY', 'TfE4SfRN6mtm8qpsumyL' );
}

if ( ! defined( 'YITH_YWSBS_TEST_ON' ) ) {
    define( 'YITH_YWSBS_TEST_ON', ( defined('WP_DEBUG') && WP_DEBUG )  );
}

/* Plugin Framework Version Check */
if( ! function_exists( 'yit_maybe_plugin_fw_loader' ) && file_exists( YITH_YWSBS_DIR . 'plugin-fw/init.php' ) ) {
    require_once( YITH_YWSBS_DIR . 'plugin-fw/init.php' );
}
yit_maybe_plugin_fw_loader( YITH_YWSBS_DIR  );


// Woocommerce installation check _________________________
function yith_ywsbs_install_woocommerce_admin_notice_premium() {
	?>
	<div class="error">
		<p><?php _e( 'YITH WooCommerce Subscription is enabled but not effective. It requires WooCommerce in order to work.', 'yith-woocommerce-subscription' ); ?></p>
	</div>
	<?php
}

function yith_ywsbs_premium_install() {
	if ( !function_exists( 'WC' ) ) {
		add_action( 'admin_notices', 'yith_ywsbs_install_woocommerce_admin_notice_premium' );
	} else {
		do_action( 'yith_ywsbs_init' );

		// check for update table
		if( function_exists( 'yith_ywsbs_update_db_check' ) ) {
			yith_ywsbs_update_db_check();
		}
	}
}
add_action( 'plugins_loaded', 'yith_ywsbs_premium_install', 11 );


function yith_ywsbs_premium_constructor() {
    // Load YWSBS text domain ___________________________________
    load_plugin_textdomain( 'yith-woocommerce-subscription', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

    if( ! class_exists( 'WP_List_Table' ) ) {
        require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
    }

    require_once( YITH_YWSBS_INC . 'functions.yith-wc-subscription.php' );
	require_once( YITH_YWSBS_INC . 'class.yith-wc-subscription.php' );
	require_once( YITH_YWSBS_INC . 'class.yith-wc-activity.php' );
	require_once( YITH_YWSBS_INC . 'class.ywsbs-susbscription-helper.php' );
	require_once( YITH_YWSBS_INC . 'class.ywsbs-susbscription.php' );
	require_once( YITH_YWSBS_INC . 'class.yith-wc-subscription-coupons.php' );
	require_once( YITH_YWSBS_INC . 'class.yith-wc-subscription-cart.php' );
	require_once( YITH_YWSBS_INC . 'class.yith-wc-subscription-admin.php' );
	require_once( YITH_YWSBS_INC . 'class.yith-wc-subscription-cron.php' );
	require_once( YITH_YWSBS_INC . 'class.yith-wc-subscription-order.php' );
	require_once( YITH_YWSBS_INC . 'gateways/paypal/class.yith-wc-subscription-paypal.php' );
	require_once( YITH_YWSBS_INC . 'admin/class.ywsbs-subscriptions-list-table.php' );
	require_once( YITH_YWSBS_INC . 'admin/class.ywsbs-activities-list-table.php' );



	if ( is_admin() ) {
        YITH_WC_Subscription_Admin();
	}

    YITH_WC_Subscription();


    add_action( 'ywsbs_renew_orders', array( YWSBS_Subscription_Cron(), 'renew_orders' ) );
    add_action( 'ywsbs_check_subscription_payment', array( YWSBS_Subscription_Cron(), 'ywsbs_check_subscription_payment' ) );
    add_action( 'ywsbs_cancel_subscription_expired', array( YWSBS_Subscription_Cron(), 'ywsbs_cancel_subscription_expired' ) );
    add_action( 'ywsbs_trigger_email_renew_reminder', array( YWSBS_Subscription_Cron(), 'ywsbs_trigger_email_renew_reminder' ) );
    add_action( 'ywsbs_trigger_email_before_subscription_expired', array( YWSBS_Subscription_Cron(), 'ywsbs_trigger_email_before_subscription_expired' ) );
    add_action( 'ywsbs_resume_orders', array( YWSBS_Subscription_Cron(), 'resume_orders' ) );
    add_action( 'ywsbs_check_overdue_subscriptions', array( YWSBS_Subscription_Cron(), 'ywsbs_check_overdue_subscriptions' ) );
    add_action( 'ywsbs_check_suspended_subscriptions', array( YWSBS_Subscription_Cron(), 'ywsbs_check_suspended_subscriptions' ) );
}

add_action( 'yith_ywsbs_init', 'yith_ywsbs_premium_constructor' );