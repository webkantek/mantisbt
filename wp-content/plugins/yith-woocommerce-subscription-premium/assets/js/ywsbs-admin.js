/**
 * admin.js
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Subscription
 * @version 1.0.0
 */

jQuery(document).ready( function($) {
    "use strict";

    function toggle_product_editor_single_product(){
        if( $('#_ywsbs_subscription').prop('checked')){
            $('.ywsbs_price_is_per, .ywsbs_max_length, .ywsbs_trial_per, .ywsbs_fee, .ywsbs_pause').show();
        }else{
            $('.ywsbs_price_is_per, .ywsbs_max_length, .ywsbs_trial_per, .ywsbs_fee, .ywsbs_pause').hide();
        }
    }
    $('#_ywsbs_subscription').on('change', function(){
        toggle_product_editor_single_product();
    });

    $('#_ywsbs_price_time_option').on('change', function(){
        $('.ywsbs_max_length .description span').text( $(this).val() );
        var selected = $(this).find(':selected'),
            max_value = selected.data('max');
        $('.ywsbs_max_length .description .max-l').text( max_value );
    });
    toggle_product_editor_single_product();



    function toggle_product_editor_variable_product(){
        var $subscription_variable = $(document).find('.variable_ywsbs_subscription');

        $subscription_variable.each(function(){
            var $t =$(this),
                $price_is_per =  $t.closest('.woocommerce_variable_attributes').find('.ywsbs_subscription_variation_products'),
                $time_option =  $t.closest('.woocommerce_variable_attributes').find('#_ywsbs_price_time_option');


            $time_option.on('change', function(){
                $(this).closest('.ywsbs_subscription_variation_products').find('.variable_ywsbs_max_length .description span').text( $(this).val() );
                var selected = $(this).find(':selected'),
                    max_value = selected.data('max');

                $(this).closest('.ywsbs_subscription_variation_products').find('.variable_ywsbs_max_length .description .max-l').text( max_value );
            });


            if( $t.prop('checked')){
                $price_is_per.show();

            }else{
                $price_is_per.hide();
            }

            /*
            * variable_ywsbs_max_length
            * */

            $t.on('change', function(){
                if( $t.prop('checked')){
                    $price_is_per.show();
                }else{
                    $price_is_per.hide();
                }
            });
        });
    }

    $('#variable_product_options').on('woocommerce_variations_added', function() {
        toggle_product_editor_variable_product();
    });
    $('#woocommerce-product-data').on('woocommerce_variations_loaded', function() {
        toggle_product_editor_variable_product();
    });

    if( $.fn.datepicker !== undefined ) {
        $('#yit-subscription-updates input').each(function () {

            $(this).prop('pattern', '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])');
            $(this).prop('maxlength', '10');
            $(this).prop('placeholder', 'YYYY-MM-DD')

        }).datepicker({
            defaultDate: '',
            dateFormat: 'yy-mm-dd',
            numberOfMonths: 1,
            showButtonPanel: true
        });
    }

});
