<?php

/**
 * Metabox for Subscription Items details
 */

// Exit if accessed directly
if (!defined('ABSPATH')) {
	exit;
}

?>

<table class="ywsbs_subscription_items_list">
	<thead>
	<tr>
		<th class="ywsbs_subscription_items_list_item"><?php _e('Item', 'yith-woocommerce-subscription'); ?></th>
		<th class="ywsbs_subscription_items_list_quantity"><?php _e('Qty', 'yith-woocommerce-subscription'); ?></th>
		<th class="ywsbs_subscription_items_list_total"><?php _e('Total', 'yith-woocommerce-subscription'); ?></th>
		<th class="ywsbs_subscription_items_list_tax"><?php _e('Tax', 'yith-woocommerce-subscription'); ?></th>
	</tr>
	</thead>

	<tbody>

		<tr>
			<td class="ywsbs_subscription_items_list_item">
				<a href="<?php echo admin_url( 'post.php?post=' . $subscription->product_id . '&action=edit' ) ?>"><?php echo $subscription->product_name ?></a>
				<?php

				if ( version_compare( WC()->version, '2.7.0', '>=' ) ) {
					$item = new WC_Order_Item_Product( $subscription->order_item_id );
					wc_display_item_meta( $item );
				}elseif ( $subscription->variation_id ) {
					yith_ywsbs_get_product_meta( $subscription, $subscription->variation );
				}
				?>
			</td>
			<td class="ywsbs_subscription_items_list_quantity"><?php echo $subscription->quantity; ?></td>
			<td class="ywsbs_subscription_items_list_total"><?php echo wc_price($subscription->line_total); ?></td>
			<td class="ywsbs_subscription_items_list_tax"><?php echo wc_price($subscription->line_tax); ?></td>
		</tr>


	<?php if (!empty($subscription->subscriptions_shippings)): ?>
		<tr>
			<td class="ywsbs_subscription_items_list_item"><?php echo $subscription->subscriptions_shippings['name'] ?></td>
			<td class="ywsbs_subscription_items_list_quantity"><?php echo '1'; ?></td>
			<td class="ywsbs_subscription_items_list_total"><?php echo wc_price($subscription->order_shipping); ?></td>
			<td class="ywsbs_subscription_items_list_tax"><?php echo wc_price($subscription->order_shipping_tax); ?></td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>
