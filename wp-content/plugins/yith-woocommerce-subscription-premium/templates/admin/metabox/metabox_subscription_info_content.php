<?php

/**
 * Metabox for Subscription Info Content
 */

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

$billing_address = $subscription->get_address_fields( 'billing', true );
$shipping_address = $subscription->get_address_fields( 'shipping', true );

?>

<div id="subscription-data" class="panel">

    <h2><?php printf(__('Subscription #%d details', 'yith-woocommerce-subscription'), $subscription->id) ?> <span class="status <?php echo $subscription->status ?>"><?php echo $subscription->status ?></span></h2>
    <p class="subscription_number"> <?php echo $subscription->product_name ?> </p>
    <p class="subscription_number"> <?php echo $subscription->get_formatted_recurring() ?> </p>

    <div class="subscription_data_column_container">
        <div class="subscription_data_column">
            <h4>General Details</h4>

            <p class="form-field form-field-wide"><label><?php  _e( '<strong>Started date</strong>:', 'yith-woocommerce-subscription') ?></label>
                <?php echo ( $subscription->start_date ) ? date_i18n( wc_date_format(), $subscription->start_date ).' '.date_i18n(__( wc_time_format()), $subscription->start_date ) : ''; ?>
            </p>

            <p class="form-field form-field-wide"><label><?php  _e( '<strong>Expired date</strong>:', 'yith-woocommerce-subscription') ?></label>
                <?php echo ( $subscription->expired_date ) ? date_i18n( wc_date_format(), $subscription->expired_date ).' '.date_i18n(__( wc_time_format()), $subscription->expired_date ) : ''; ?>
            </p>

            <p class="form-field form-field-wide"><label><?php  _e( '<strong>Payment due date</strong>:', 'yith-woocommerce-subscription') ?></label>
                <?php echo ( $subscription->payment_due_date ) ? date_i18n( wc_date_format(), $subscription->payment_due_date ).' '.date_i18n(__( wc_time_format()), $subscription->payment_due_date ) : ''; ?>
            </p>

            <?php if( $subscription->cancelled_date != ''): ?>
            <p class="form-field form-field-wide"><label><?php  _e( '<strong>Cancelled date</strong>:', 'yith-woocommerce-subscription') ?></label>
                <?php echo ( $subscription->cancelled_date ) ? date_i18n( wc_date_format(), $subscription->cancelled_date ).' '.date_i18n(__( wc_time_format()), $subscription->cancelled_date ) : ''; ?>
            </p>
            <?php endif ?>

            <?php if( $subscription->end_date != ''): ?>
                <p class="form-field form-field-wide"><label><?php  _e( '<strong>End date</strong>:', 'yith-woocommerce-subscription') ?></label>
                    <?php echo ( $subscription->end_date ) ? date_i18n( wc_date_format(), $subscription->end_date ).' '.date_i18n(__( wc_time_format()), $subscription->end_date ) : ''; ?>
                </p>
            <?php endif ?>

            <?php if( $subscription->payed_order_list != ''): ?>
                <p class="form-field form-field-wide"><label><?php  _e( '<strong>List of paid orders</strong>:', 'yith-woocommerce-subscription') ?></label>
                   <?php foreach ( $subscription->payed_order_list as $order ) : ?>
                       <a href="<?php echo admin_url( 'post.php?post=' . $order . '&action=edit' ) ?>">#<?php echo $order ?></a>
                   <?php endforeach;
                   ?>
                </p>
            <?php endif ?>

            <?php if( $renew_order = $subscription->has_a_renew_order() ):
	            $renew_order_id = yit_get_order_id( $renew_order );
				?>
                <p class="form-field form-field-wide"><label><?php  _e( '<strong>Renew Order</strong>:', 'yith-woocommerce-subscription') ?></label>
                        <a href="<?php echo admin_url( 'post.php?post=' . $renew_order_id . '&action=edit' ) ?>">#<?php echo $renew_order_id ?></a>
                </p>
            <?php endif ?>

            <p class="form-field form-field-wide"><label><?php  _e( '<strong>Payment Method</strong>:', 'yith-woocommerce-subscription') ?></label>
                <?php echo $subscription->payment_method_title  ?>
            </p>
            <?php if( $subscription->transaction_id != ''): ?>
            <p class="form-field form-field-wide"><label><?php  _e( '<strong>Transaction ID</strong>:', 'yith-woocommerce-subscription') ?></label>
                <?php echo $subscription->transaction_id  ?>
            </p>
            <?php endif ?>
        </div>
        <div class="subscription_data_column">
            <h4><?php _e('Billing Details', 'yith-woocommerce-subscription') ?></h4>
            <div class="address">
                <p><strong><?php _e('Address:', 'yith-woocommerce-subscription') ?></strong>
                    <?php
                    echo WC()->countries->get_formatted_address( $billing_address );    
                    ?>
                </p>

                 <p><strong><?php _e('Email:', 'yith-woocommerce-subscription') ?></strong> <a href="mailto:<?php echo $billing_address['email'] ?>"><?php echo $billing_address['email'] ?></a></p>
                 <p><strong><?php _e('Phone:', 'yith-woocommerce-subscription') ?></strong> <?php echo $billing_address['phone'] ?></p>
            </div>
        </div>

        <div class="subscription_data_column">
            <h4><?php _e('Shipping Details', 'yith-woocommerce-subscription') ?></h4>

            <div class="address"><p><strong><?php _e( 'Address:', 'yith-woocommerce-subscription' ) ?></strong><?php
                    echo WC()->countries->get_formatted_address( $shipping_address )
                    ?></p>

                <?php $shipping = $subscription->subscriptions_shippings;

                if( !empty($shipping) ): ?>
                    <p><strong><?php _e( 'Shipping method:', 'yith-woocommerce-subscription' ) ?></strong>
                    <?php echo $shipping['name'];
                    endif ?>
                </p>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>